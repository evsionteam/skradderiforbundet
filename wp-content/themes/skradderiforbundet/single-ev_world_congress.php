<?php
get_header();
while (have_posts()) : the_post();
?>
    <div class="sk-c-double-section">
        <div class="container">
            <div class="col-md-12">
                <div id="primary" class="content-area">
                    <main id="main" class="site-main" role="main">
                        <?php
                        $intranet_main_post = get_post_meta(get_the_ID(),'_intranet_main_post',true);
                        if(!empty($intranet_main_post)){
                            switch_to_blog(2);
                            $intranet_wc = new WP_Query(
                                array(
                                    'post_type' => 'ev_world_congress',
                                    'p' => $intranet_main_post,
                                )
                            );
                            if($intranet_wc->have_posts()):
                                while($intranet_wc->have_posts()):$intranet_wc->the_post();
                                    ?>
                                    <article id="post-<?php the_ID(); ?>" class="sk-news">
                                        <?php
                                        $news_thumbnail_id = get_post_thumbnail_id(get_the_ID());
                                        if(!empty($news_thumbnail_id)){
                                            $news_image_src = wp_get_attachment_image_src($news_thumbnail_id, 'full');
                                            echo '<div class="sk-img" style="background-image:url('.$news_image_src[0].');"></div>';
                                        }else{
                                            $news_image_src = get_post_meta(get_the_ID(),'_thumbnail_url',true);
                                            if(!empty($news_image_src)){
                                                echo '<div class="sk-img" style="background-image:url('.$news_image_src.');"></div>';
                                            }
                                        }
                                        ?>
                                        <div class="sk-title-n-des">
                                            <h2 class="sk-title"><?php the_title(); ?></h2>
                                            <span class="sk-date"><i class="fa fa-calendar"></i><?php echo get_the_date(' j M'); ?></span>
                                            <div class="sk-description">
                                                <?php the_content(); ?>
                                            </div>
                                        </div>
                                        <!-- .sk-title-n-des -->
                                    </article><!-- #post-## -->
                                    <?php
                                endwhile;wp_reset_postdata();
                            endif;
                            restore_current_blog();
                        }
                        the_post_navigation();
                        ?>
                    </main>
                </div>
            </div>
        </div>
    </div>
<?php
endwhile; // End of the loop.
get_footer();
