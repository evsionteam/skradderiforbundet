jQuery(document).ready(function(){

    var page = 1;
    var ppp = 6;

    jQuery("#home-page-load-more").on('click',function(e){
        e.preventDefault();
        var _that = jQuery(this),
            className         = 'loading',
            noMorePost        = 'No More Posts',
            $loadMoreSpinner  = jQuery('.load-more-spinner'),
            $skLoadMoreNews   = jQuery('.sk-news-load-more'),
            $loadMoreText     = jQuery('.sk-arrow-n-text'),
            $downArrow        = jQuery('.sk-arrow-n-text a i'),
            $newWrapper       = jQuery('.sk-news-archive-wrapper .news-listing');

            jQuery.ajax({
                type : 'post',
                url : skAjax.ajaxurl,
                data : {
                    action : 'load_mixed_results',
                    offset: (page * ppp),
                    ppp: ppp,
                },
                dataType:'json',
                beforeSend: function() {
                    $loadMoreSpinner.fadeIn();
                    $skLoadMoreNews.addClass(className);
                },
                success : function( response ) {
                    if(true == response.more_post){
                        $loadMoreSpinner.fadeOut();
                        $skLoadMoreNews.removeClass(className);
                        page++;
                        $newWrapper.append( response.data );
                    }
                    else{
                        $loadMoreSpinner.fadeOut();
                        $skLoadMoreNews.removeClass(className);
                        $loadMoreText.html(noMorePost);
                        $downArrow.remove();
                    }
                }
            });

    });
});
