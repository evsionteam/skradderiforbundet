/*some global variables for map*/
var map, infoWindow ;
var markersData = [];
var filteredMarkers = [];
var visibleMarkers = [];
var visibleCompanies = [];
/**/

/*called on window load*/
function initialize() {

    /*fetch companies*/
    jQuery.ajax({
        url: skAjax.ajaxurl,
        type: 'POST',
        data : {
            action : 'sk_get_companies'
        },
        dataType:'json',
        beforeSend: function() {
        },
        success: function(response) {
            if (response.have_companies) {
                /*load map with companies*/
                markersData = response.company_data;
                initialize_map();
            }
        }
    });
    /**/

    /*fetch sub cats*/
    jQuery("#company-cat").on('change',function(e){
        var cat = jQuery(this).val();
        if(cat){
            jQuery.ajax({
                url: skAjax.ajaxurl,
                type: 'POST',
                data : {
                    action : 'sk_get_sub_cats',
                    cat: cat
                },
                dataType: 'json',
                beforeSend: function() {

                },
                success: function(response) {
                    jQuery('#company-sub-cat').html('').selectpicker('refresh');
                    if( true == response.success ){
                        if(response.data){
                            jQuery.each( response.data, function( index, value ){
                                jQuery('#company-sub-cat').append('<option value='+index+'>'+value+'</option>').selectpicker('refresh');
                            });
                        }
                    }
                }
            });
        }else{
            jQuery('#company-sub-cat').html('').append('<option value="">Välj Sub Kategori</option>').selectpicker('refresh');
        }
    });
    /**/
}

function initialize_map(){

    var mapOptions = {
        center: new google.maps.LatLng(0,0),
        zoom: 15,
        mapTypeId: 'roadmap',
        disableDefaultUI: true,
        styles: [
            {
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#212121"
                    }
                ]
            },
            {
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#757575"
                    }
                ]
            },
            {
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "color": "#212121"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#757575"
                    }
                ]
            },
            {
                "featureType": "administrative.country",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#9e9e9e"
                    }
                ]
            },
            {
                "featureType": "administrative.locality",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#bdbdbd"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#757575"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#181818"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#616161"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "color": "#1b1b1b"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#2c2c2c"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#8a8a8a"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#373737"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#3c3c3c"
                    }
                ]
            },
            {
                "featureType": "road.highway.controlled_access",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#4e4e4e"
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#616161"
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#757575"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#000000"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#3d3d3d"
                    }
                ]
            }
        ]
    };

    map = new google.maps.Map(document.getElementById('company-listing-map'), mapOptions);

    /*a new Info Window is created*/
    infoWindow = new google.maps.InfoWindow();

    /*Event that closes the Info Window with a click on the map*/
    google.maps.event.addListener(map, 'click', function() {
        infoWindow.close();
    });

    /*Finally displayMarkers() function is called to begin the markers creation*/
    displayMarkers();
}

/*call companies loading function which will call map*/

if( jQuery('#company-listing-map').length ){
    google.maps.event.addDomListener(window, 'load', initialize);
}


function displayMarkers(){

    /*sets the map bounds according to markers position*/
    var bounds = new google.maps.LatLngBounds();

    for (var i = 0; i < markersData.length; i++){
        var latlng = new google.maps.LatLng(markersData[i].lat, markersData[i].lng);
        var name = markersData[i].name;
        var address = markersData[i].address;
        var phone = markersData[i].phone;
        var cat = markersData[i].cat;
        var city = markersData[i].city;
        var email = markersData[i].email;
        var website = markersData[i].website;
        var link = markersData[i].link;

        createMarker(latlng, name, address, phone, email, website, cat, city, link);

        /*marker position is added to bounds variable*/
        bounds.extend(latlng);
    }

    /*use to maintain the zoom with fitbounds*/
    google.maps.event.addListenerOnce(map, 'bounds_changed', function(event) {
        //this.setZoom(map.getZoom()-1);
        if (this.getZoom() > 15) {
            this.setZoom(15);
        }
    });
    /*Set the map bounds*/
    map.fitBounds(bounds);
}

/*Creates each marker and it sets their Info Window content*/
function createMarker(latlng, name, address, phone, email, website, cat, city, link){
    var marker = new google.maps.Marker({
        map: map,
        position: latlng,
        icon: 'http://maps.google.com/mapfiles/ms/icons/yellow-dot.png',
        title: name,
        cat: cat,
        city: city
    });

    filteredMarkers.push(marker);

    if(email){
        email = email + '<br/>';
    }
    if(website){
        website = website + '<br/>';
    }

    /*Create info window content and opron the window.*/
    google.maps.event.addListener(marker, 'click', function() {

        /*Creating the content to be inserted in the infowindow*/
        var iwContent = '<div id="iw_container">' +
            '<div class="iw_title">' + name + '</div>' +
            '<div class="iw_content">' + address + '<br />' +
            city + '<br />' +
            phone + '<br />' +
            email +
            website +
            link + '<br />' +
            '</div></div>';

        /*including content to the Info Window.*/
        infoWindow.setContent(iwContent);

        /*opening the Info Window in the current map and at the current marker location.*/
        infoWindow.open(map, marker);
    });
}

/*Filter Map markers*/
jQuery('#company-map-filter').on('submit',function(e){
    e.preventDefault();

    var cat = jQuery('#company-cat').val();
    var sub_cat = jQuery('#company-sub-cat').val();
    var city_search = jQuery('#place-search').val();
    var title_search = jQuery('#company-search').val();

    filterMarkers(cat,sub_cat,city_search,title_search);

    /* Always Call this after filterMarkers() */
    filterComapnyListings();
    /**/
});

/*Filter marker when clicked on company listing*/
jQuery('.single-company-listing .map-content-link').on('click',function(e){
    e.preventDefault();
    company_name = jQuery(this).closest('.single-company-listing').attr('data-name');
    if(company_name){
        filterMarkers('','','',company_name);
    }
});
/**/

function filterMarkers(cat,sub_cat,city,title){
    for (i = 0; i < markersData.length; i++) {
        var marker = filteredMarkers[i];

        var cat_exists = marker.cat.indexOf(cat);
        if(sub_cat){
            cat_exists = marker.cat.indexOf(sub_cat);
        }
        /*cat_exists != -1*/
       /* marker.cat == cat*/

        var company_name = marker.title.toLowerCase();
        var title_exist = company_name.indexOf(title.toLowerCase());

        var city_name = marker.city.toLowerCase();
        var city_exist = city_name.indexOf(city.toLowerCase());

        if( cat && city && title){
            if ( (cat_exists != -1 || cat.length === 0) && (city_exist != -1) && (title_exist != -1) ) {
                marker.setVisible(true);
            }
            else {
                marker.setVisible(false);
            }
        }else if(cat && city && !title){
            if ( (cat_exists != -1 || cat.length === 0) && (city_exist != -1) ) {
                marker.setVisible(true);
            }
            else {
                marker.setVisible(false);
            }
        }else if(cat && !city && title){
            if ( (cat_exists != -1 || cat.length === 0) && (title_exist != -1) ) {
                marker.setVisible(true);
            }
            else {
                marker.setVisible(false);
            }
        }else if(cat && !city && !title){
            if ( (cat_exists != -1 || cat.length === 0) ) {
                marker.setVisible(true);
            }
            else {
                marker.setVisible(false);
            }
        }else if(!cat && city && title){
            if ( (city_exist != -1) && (title_exist != -1) ) {
                marker.setVisible(true);
            }
            else {
                marker.setVisible(false);
            }
        }else if(!cat && city && !title){
            if ( (city_exist != -1) ) {
                marker.setVisible(true);
            }
            else {
                marker.setVisible(false);
            }
        }else if(!cat && !city && title){
            if ( (title_exist != -1) ) {
                marker.setVisible(true);
            }
            else {
                marker.setVisible(false);
            }
        }else{
            marker.setVisible(true);
        }

    }
    /*Fit the markers after search filter*/
    fitVisibleMarkers();
}

function fitVisibleMarkers() {
    visibleMarkers = [];
    var bounds = new google.maps.LatLngBounds();
    for (var i = 0; i < markersData.length; i++) {
        var marker = filteredMarkers[i];
        if(marker.getVisible()) {
            bounds.extend( marker.getPosition() );
            visibleMarkers.push(marker);
        }
    }
    /*use to maintain the zoom with fitbounds*/
    google.maps.event.addListenerOnce(map, 'bounds_changed', function(event) {
        //this.setZoom(map.getZoom()-1);
        if (this.getZoom() > 15) {
            this.setZoom(15);
        }
    });
    map.fitBounds(bounds);
}

function filterComapnyListings(){
    if(visibleMarkers){
        visibleCompanies = [];
        visibleMarkers.forEach( function(elem, i){
            visibleCompanies.push(elem.title );
        });
        jQuery('.single-company-listing').each(function(){
            var company_name = jQuery(this).attr('data-name');
            if(company_name){
                if(jQuery.inArray(company_name, visibleCompanies) !== -1){
                    jQuery(this).css('display','block');
                }else{
                    jQuery(this).css('display','none')
                }
            }
        });
    }
}