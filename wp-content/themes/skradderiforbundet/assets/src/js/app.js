jQuery(document).ready(function () {
    var found_posts = jQuery('.sk-product-section').data('found');
    if(found_posts){
        var controller = new ScrollMagic.Controller();
        for (var i = 1; i <= found_posts; i++) {
            var text = new ScrollMagic.Scene({
                triggerElement: '.scroll-text-' + i + ''
            }).setClassToggle('.scroll-text-' + i + '', 'fade-in').addTo(controller);

            var image = new ScrollMagic.Scene({
                triggerElement: '.scroll-img-' + i + ''
            }).setClassToggle('.scroll-img-' + i + '', 'fade-in').addTo(controller);
        }
    }
});
