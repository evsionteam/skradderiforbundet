+(function($) {
    $.fn.mrSlider = function(config) {

        var defaultConfig = {
            next: '#mr-next',
            prev: '#mr-prev',
            duration: 25000,
            autoPlay: false,
            navigation : true
        }
        $.extend(defaultConfig, config);
        var _this = this;

        var slider = function() {

                this.item = '.mr-slider-inner .item',
                this.activeItem = '.mr-slider-inner .item.active',
                this.leaveItem = '.mr-slider-inner .item.leave',
                this.navigation = '.sk-slider-navigation',
                this.currentPosition = 1,
                this.activeClass = 'active',
                this.leaveClass = 'leave',
                this.shiftClass = null;

            var $firstItem = $(_this.selector + ' ' + this.item),
                $activeItem = $(_this.selector + ' ' + this.activeItem),
                $leaveItem = $(_this.selector + ' ' + this.leaveItem),
                that = this;

            this.init = function() {
                $(document).ready(function() {
                    that.getTotalNoOfSlider();
                    that.addClassActive();
                    /* next previous button */
                    if( defaultConfig.navigation ){
                      that.prevButton();
                      that.nextButton();
                    }
                    /* auto play slider*/
                    if (defaultConfig.autoPlay) {
                        that.autoChangeSlider();
                    }
                    that.changeIndicatorText();
                    that.animateNavigation();
                });
            };

            this.getTotalNoOfSlider = function() {
                var noOfSlider = $(this.item, _this).length;
                return noOfSlider;
            };

            this.animateNavigation = function(){
              $(this.navigation).hide().delay(1000).fadeIn(500);
            };

            this.changeIndicatorText = function(){
              var title = $('.active .sk-title', _this).html();
              $('.sk-slider-navigation div', _this).html(title);
            };

            this.addClassActive = function() {
                $(this.item, _this).first().addClass(this.activeClass);
                $($firstItem.selector)
                  .last()
                  .addClass(that.leaveClass);
            };

            this.changePrevSlider = function() {
                // console.log('changePrevSlider');
                window.clearInterval( that.shiftClass );
                that.prevSlider();
                if (defaultConfig.autoPlay) {
                    that.autoChangeSlider();
                }
            }

            this.prevButton = function() {
              $(document).on( 'click', defaultConfig.prev, that.changePrevSlider );
            };

            this.changeNextSlider = function() {
              // console.log('changeNextSlider');
              window.clearInterval(that.shiftClass);
              that.nextSlider();
              if (defaultConfig.autoPlay) {
                that.autoChangeSlider();
              }
              that.changeIndicatorText();
            };

            this.nextButton = function() {
                $(document).on('click', defaultConfig.next, that.changeNextSlider);
            };

            this.isLast = function() {

                if (that.currentPosition == that.getTotalNoOfSlider()) {
                    that.currentPosition = 0;
                    $($leaveItem.selector)
                      .removeClass(that.leaveClass);
                    $($firstItem.selector)
                      .removeClass(that.activeClass)
                      .last()
                      .addClass(that.leaveClass);
                    $($activeItem.selector).removeClass(that.activeClass);
                    $firstItem.first().addClass(that.activeClass);
                    return;
                }
                $($leaveItem.selector)
                  .removeClass(that.leaveClass);
                $($activeItem.selector)
                    .removeClass(that.activeClass)
                    .addClass(that.leaveClass)
                    .next()
                    .addClass(that.activeClass);
            };

            this.prevSlider  = function() {
                // console.log('currentPos :' + that.currentPosition, 'lastPost' + that.getTotalNoOfSlider());
                if (1 == that.currentPosition) {
                    that.currentPosition = that.getTotalNoOfSlider();
                    $($firstItem.selector)
                      .removeClass(that.leaveClass)
                      .removeClass(that.activeClass)
                      .first()
                      .addClass(that.leaveClass);

                    $($firstItem.selector)
                      .last()
                      .addClass(that.activeClass);
                      that.changeIndicatorText();

                    return;
                }
                $($leaveItem.selector)
                  .removeClass(that.leaveClass);
                $($activeItem.selector)
                    .addClass(that.leaveClass)
                    .removeClass(that.activeClass)
                    .prev()
                    .addClass(that.activeClass);
                that.currentPosition--;
                that.changeIndicatorText();

            };

            this.nextSlider = function() {
                // console.log('currentPos :' + that.currentPosition, 'lastPost' + that.getTotalNoOfSlider());
                that.isLast();
                that.currentPosition++;
            };

            this.autoChangeSlider = function() {
                this.shiftClass = setInterval(that.nextSlider, defaultConfig.duration);
            };


        };

        new slider().init();

    };
})(jQuery)
