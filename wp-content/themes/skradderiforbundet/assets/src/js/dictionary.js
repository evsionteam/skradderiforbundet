jQuery(document).ready(function(){

    jQuery('.sk-search-form input[name="search"]').on('input',function(){
        if( jQuery(this).val().length > 3){
            load_dictionary_words();
        }
    });


    jQuery('.sk-alphabet-filter a').on('click',function(e){

        e.preventDefault();

        jQuery('.sk-alphabet-filter a').removeClass('active');
        jQuery(this).addClass('active');

        load_dictionary_words();
    });

    function load_dictionary_words(){
        var alphabet = jQuery('.sk-alphabet-filter').find('.active').attr('data-alphabet');
        var search_text = jQuery('.sk-search-form input[name="search"]').val();
        jQuery.ajax({
            type : 'post',
            url : skAjax.ajaxurl,
            data : {
                action : 'sk_load_words',
                alphabet: alphabet,
                search_text: search_text
            },
            dataType:'json',
            beforeSend: function() {
                jQuery('.fetch-words-loader').css('display','inline-block');
            },
            success : function( response ) {
                jQuery('.fetch-words-loader').css('display','none');
                jQuery('.dictionary-words-wrapper').html('');
                if( true == response.success ){
                    if(response.data){
                        jQuery.each(response.data, function( index, value ) {
                            jQuery('.dictionary-words-wrapper').append(value);
                        });
                    }
                }else{
                }
            }
        });
    }
});