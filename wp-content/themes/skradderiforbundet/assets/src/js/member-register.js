jQuery(document).ready(function($){

    jQuery('.datepicker').datepicker({
        dateFormat : 'yy-mm-dd',
        changeMonth: true,
        changeYear:true,
        yearRange:'-100:+0'
    });

    jQuery('#register_member_form').on('submit', function(e){
        e.preventDefault();
        var $this = jQuery(this),
            nonce = $this.find('#register_member_nonce').val(),
            form_values = jQuery(this).serialize();

        jQuery.ajax({
            url: skAjax.ajaxurl,
            type: 'POST',
            data: {
                action : 'register_member_profile',
                nonce : nonce,
                form_data : form_values
            },
            dataType: 'json',
            beforeSend: function() {
                jQuery('.add-member-loader').css('display','inline-block');
            },
            success: function(data) {
                jQuery('.add-member-loader').hide();
                if (data.status) {
                    jQuery('.sk-member-register-message').fadeIn().html('<div class="alert alert-success">' + data.message + '</div>');
                } else {
                    jQuery('.sk-member-register-message').fadeIn().html('<div class="alert alert-danger">' + data.message +'</div>');
                }
                jQuery('html, body').animate({
                    scrollTop: $(".sk-member-register-form").offset().top
                }, 1000);
            }
        });
    });
});