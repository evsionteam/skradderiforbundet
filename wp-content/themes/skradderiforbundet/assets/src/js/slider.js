+(function ($) {
 $(window).load( function(){  

    var slider = $('.slider');
    var sliderItem = $('.slider-item');
    var tlSlider;
    var tlSliderItem;
    var sliderDelay = 2;

    console.log(slider);
    
    tlSlider = new TimelineMax();  
    tlSliderItem = new TimelineMax({
        repeat: -1
    });

    sliderItem.each(function (index, element) {

        var sliderItemImage = $(this).find('.slider-img');
        var sliderItemCaption = $(this).find('.slider-caption');

        tlSliderItem
            .from($(this), 1, {
                x: '100%',
                ease: Power4.easeOut
            })
            .from(sliderItemImage, 1, {
                x: '-75%',
                ease: Power4.easeOut
            }, '-=1')
            .from(sliderItemCaption, 1, {
                opacity: 0
            })
            .to($(this), 1, {
                x: '-100%',
                ease: Power4.easeIn
            }, '+=' + sliderDelay)
            .to(sliderItemImage, 1, {
                x: '75%',
                ease: Power4.easeIn
            }, '-=1');

        tlSlider.add(tlSliderItem);

    });

 });

}(jQuery));
