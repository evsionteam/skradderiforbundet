//scroll animation jquery Plugin
+(function ($) {
    $.fn.videoPlayOnView = function (setting) {
        //defaultsetting
        var defaultSetting = {
            videoId: '#sk-video-ad',
            className: 'stop'
        }
        $.extend(defaultSetting, setting);

        try{
          //variables declearation
          var _this           = $(this),
              lastScrollTop   = 0,
              $window         = $(window),
              windowHeight    = $window.height(),
              videoHeight     = $(this.selector).outerHeight(),
              videoOffset     = $(_this.selector).offset().top,
              videoTopGap     = windowHeight - videoHeight,
              videoPlayOffset = videoOffset - videoTopGap,
              videoPauseOffset= videoOffset + videoHeight,
              suVideoPauseOffset = videoOffset - windowHeight;
              $window.scroll(addClass);
              //addclass function
              function addClass(){

                var scrollTopPosition = $window.scrollTop(),
                    st = $(window).scrollTop();
                    if (st > lastScrollTop){
                        //console.log('downscroll');
                        if(scrollTopPosition >= videoPlayOffset && scrollTopPosition <= videoPauseOffset && $(defaultSetting.videoId).get(0).paused){
                          console.log('video play');
                          $(defaultSetting.videoId).trigger('play').parent().removeClass(defaultSetting.className);
                        }
                        if(scrollTopPosition >= videoPauseOffset && !$(defaultSetting.videoId).get(0).paused){
                          console.log('video pause');
                          $(defaultSetting.videoId).trigger('pause').parent().addClass(defaultSetting.className);
                        }
                    } else {
                      if( scrollTopPosition <= videoOffset && scrollTopPosition >= suVideoPauseOffset && $(defaultSetting.videoId).get(0).paused){
                        //console.log('upscroll');
                        console.log('video play');
                        $(defaultSetting.videoId).trigger('play').parent().removeClass(defaultSetting.className);
                      }
                      if( scrollTopPosition <= suVideoPauseOffset && !$(defaultSetting.videoId).get(0).paused){
                        //console.log('upscroll');
                        console.log('video pause');
                        $(defaultSetting.videoId).trigger('pause').parent().addClass(defaultSetting.className);
                      }
                    }
                    lastScrollTop = st;

                  //console.clear();
                  //console.log( "videoTopGap"+videoTopGap+ "\nvideoOffset"+videoOffset+" \nvidoeHeight "+videoHeight+"\nscrollTop " + scrollTopPosition + "\nvideoplayOffset "+ videoPlayOffset + "\nvideoPauseOffset "+ videoPauseOffset);

              }
        }catch(e){
          console.warn( "No element found for " + this.selector );
        }
    };

    $(window).load(function(){
      $('#video').videoPlayOnView();
    });
})(jQuery);
