+(function($) {

    function frontEndUI() {

        var isMobile = false;
        var _this = this;
        this.init = function() {
            $(document).ready(function() {
                // _this.niceScoll();
                _this.scrollDown();
                //_this.copyToClipboard();
                _this.mrMobileMenu();
                _this.productSlider();
                _this.videoAd();
                /*_this.formsubmit();*/
                _this.footable();
                _this.loadMoreNews();
                _this.removeOverflowHiddenClass();
                _this.sliderToggleFormCheckbox();
                _this.leftImageFixed();
                _this.filterSearch();
                _this.shareLink();
                _this.logoSlider();
                _this.dynamicContentHeight();
                // _this.initprettyPhoto();
                _this.dynamicEmail();
                _this.lightBox();
                _this.filterMap();
                /*Remove medlem from the nextgen gallery pagination so that it works on public site too*/
                _this.removeTextForNextGen();
            });
            $(window).resize(_this.leftImageFixed);
        };

        // this.skWow= function(){
        //   new WOW().init();
        // };


        this.shareLink = function(){
            $(document).on( 'click', '.share-icon', function(e){
                e.preventDefault();
                    $(this).parents('.sk-date-n-copy').next().toggle(200);
                });
        };

        this.filterSearch = function(){
            var $filterMap =  $('.sk-mobile-filter');

            if ( $filterMap.length > 0 ){
              $filterMap.on('click', function(e){
                e.preventDefault();
                $filterMap.next().slideToggle();
              });
            }
        };

        this.filterMap = function(){
           $('.map-filtericon').on('click', function(e){
              e.preventDefault();
              $('.companies-list-mode').slideToggle(200);
           });
            $('.companies-list-mode.list-toggle a').on('click', function(e){
                e.preventDefault();
                $('.companies-list-mode').slideToggle(200);
            });
        };

        this.leftImageFixed = function(){
          var windowWidth = $(window).width(),
          headerHeight    = $('#site-navigation').outerHeight(),
          $leftImage      = $('#sk-img-wrapper'),
          $footer         = $('#colophon'),
          lexiExist       = $('.lexi').length > 0,
          footerExist     = $footer.length > 0 ,
          offsetClass     = 'col-sm-offset-6';
          if( windowWidth > 767 && footerExist && lexiExist ){
                  isMobile = false;
              var footerHeight  = $footer.offset().top,
                  leftImageHeight = $leftImage.outerHeight(),
                  $window       = $(window),
                  totalHeight   = footerHeight - leftImageHeight;
                  $(window).bind("scroll.myScroll", fixedImage);
            }else{
              if( !isMobile ){
                isMobile = true;
                $(window).unbind('.myScroll');
                $leftImage.removeAttr('style');
                $leftImage.next().removeClass(offsetClass);
              }
            }

            function fixedImage(){
              var scrollTop = $window.scrollTop();
              console.clear();
              console.log("TotalHeight "+totalHeight+" scrollHeight "+scrollTop);
                if( scrollTop >= headerHeight && scrollTop <= totalHeight){
                  $leftImage.css({'position':'fixed', 'top':'0', 'left':'0', 'margin-top': '0', 'z-index':'9'});
                  $leftImage.next().addClass(offsetClass);
                }else if( scrollTop <= headerHeight ){
                  $leftImage.removeAttr('style');
                  $leftImage.next().removeClass(offsetClass);
                }else{
                  $leftImage.next().removeClass(offsetClass);
                  $leftImage.css({'position': 'relative', 'margin-top': totalHeight-headerHeight+'px' })
                }
            };
        };

        this.sliderToggleFormCheckbox = function(){
            var checkBox = '.slide-toggle-form-checkbox';
            $(document).on( 'click', checkBox, toggleForm );
            function toggleForm(){
                var slideDiv = $(this).attr('data-toggle');
                if( typeof slideDiv == 'string'){
                  if( $(this).attr('checked') ){
                      $(slideDiv).slideDown(300).find('input, select').each(function(){
                          $(this).prop("disabled", false);
                          var required_fields = $(this).attr('data-type');
                          if(required_fields != undefined){
                              $(this).prop("required", true);
                          }
                      });
                      $(slideDiv).slideDown(300).find('select').each(function(){
                          $('select').selectpicker('refresh');
                          $(this).prop("disabled", false);
                          var required_fields = $(this).attr('data-type');
                          if(required_fields != undefined){
                              $(this).prop("required", true);
                          }
                      });
                      return;
                  }
                  $(slideDiv).find('input').each(function(){
                    $(this).prop("disabled", true);
                    $(this).prop("required", false);
                  });
                  $(slideDiv).find('select').each(function(){
                    $('select').selectpicker('refresh');
                    $(this).prop("disabled", true);
                    $(this).prop("required", false);
                  });
                  $(slideDiv).slideUp(300);
                }
            };
        };

        this.footable = function(){
          $('#sk-master-table').footable();
        };

        this.formsubmit = function() {
            $(".term-select, .date-select").on('change',function(e){
                e.preventDefault();
                this.form.submit();
            });
        };

        this.loadMoreNews = function() {
            var page = 1;
            var ppp = 6;

            $(".sk-load-ajax-posts").on('click',function(e){
                e.preventDefault();
                var _that = $(this),
                    className         = 'loading',
                    noMorePost        = 'No More Posts',
                    $loadMoreSpinner  = $('.load-more-spinner'),
                    $skLoadMoreNews   = $('.sk-news-load-more'),
                    $loadMoreText     = $('.sk-arrow-n-text'),
                    $downArrow        = $('.sk-arrow-n-text a i'),
                    $newWrapper       = $('.sk-news-archive-wrapper .news-listing'),
                    news_cat          = _this.getParameterByName('news_cat'),
                    exhibition_cat    = _this.getParameterByName('exhibition_cat'),
                    date              = _this.getParameterByName('date');
                    search            = _this.getParameterByName('search');
                    loading_from      = '';

                if(jQuery('.loading-from-home-page').length){
                    loading_from = 'home';
                }

                if(jQuery('.loading-from-news-page').length){
                    loading_from = 'news';
                }

                if(jQuery('.loading-from-exhibition-page').length){
                    loading_from = 'exhibition';
                }

                if(jQuery('.loading-from-modemuseum-page').length){
                    loading_from = 'modemuseum';
                }

                jQuery.ajax({
                    type : 'post',
                    url : skAjax.ajaxurl,
                    data : {
                        action : 'load_more_results',
                        loading_from : loading_from,
                        offset: (page * ppp),
                        ppp: ppp,
                        news_cat: news_cat,
                        exhibition_cat: exhibition_cat,
                        date_arc: date,
                        search: search
                    },
                    dataType:'json',
                    beforeSend: function() {
                        $loadMoreSpinner.fadeIn();
                        $skLoadMoreNews.addClass(className);
                    },
                    success : function( response ) {
                        if(true == response.more_post){
                            $loadMoreSpinner.fadeOut();
                            $skLoadMoreNews.removeClass(className);
                            page++;
                            $newWrapper.append( response.data );
                        }
                        else{
                            $loadMoreSpinner.fadeOut();
                            $skLoadMoreNews.removeClass(className);
                            $loadMoreText.html(noMorePost);
                            $downArrow.remove();
                        }
                    }
                });

            });
        };

        this.getParameterByName = function(name, url) {
            if (!url) {
                url = window.location.href;
            }
            var name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        };

        this.mrMobileMenu = function() {
            $('.sk-nav-wrapper').mrMobileMenu();
        };

        // this.niceScoll = function() {
        //     jQuery("html").niceScroll({
        //         cursorcolor: "#000",
        //         cursorwidth: "8px",
        //         cursorborder: "none",
        //         smoothscroll: !0,
        //         mousescrollstep: 80,
        //         cursorborderradius: "0",
        //         zindex: 99
        //     })
        // };

        this.scrollDown = function() {
            $('.sk-scroll-down').scrollDown();
        };

        this.copyToClipboard = function() {
            var $copyTextareaBtn = $('.sk-copy');
            $copyTextareaBtn.on('click', function(e) {
                e.preventDefault();
                $(this).prev().select();
                var _this = this,
                    copyHTML = '<span>Copied</span>';
                try {
                    var successful = document.execCommand('copy');
                    successful && $(this).find('span').length == 0 ? $(this).children().append(copyHTML).hide().fadeIn() : console.log('sorry');
                    setTimeout(function(){
                        $(_this).find('span').fadeOut(500, function(){
                            $(this).remove();
                        });
                    }, 2000);
                } catch (err) {
                    console.log('Oops, unable to copy');
                }
            });
        };

        this.videoAd = function() {
            var $video = $('#sk-video-ad'),
                $playIcon = $('#play-icon');

            $playIcon.on('click', function() {
                $video.trigger('play');
                $video.parent().removeClass('stop');
            });

            $video.on('ended', function() {
                $(this).parent().addClass('stop');
            });
        };

        this.removeOverflowHiddenClass = function() {
            $('body').removeClass('overflow-hidden');
        };

        this.productSlider = function() {
            /*$('#main-sk-slider').mrSlider();*/
        };

        this.logoSlider = function(){
            $('.owl-carousel').owlCarousel({
                items:6,
                lazyLoad:true,
                loop:false,
                margin:10,
                autoplay:true,
                responsiveClass:true,
                 responsive:{
                     0:{
                         items:3,
                         nav:false
                     },
                     600:{
                         items:3,
                         nav:false
                     },
                     1000:{
                         items:6,
                         nav:false,
                         loop:false
                     }
                 }
            });
        }

        /* DymanicContent Height */
        this.dynamicContentHeight = function(){
            var $content = $('#content'),
                windowHeight = $(window).height(),
                headerHeight = $('#masthead').outerHeight(),
                footerHeight = $('#colophon').outerHeight(),
                remainHeight = windowHeight - footerHeight;
                $content.css("min-height", remainHeight + "px");
        }

       this.lightBox = function(){
           /*jQuery('body .sk-gallery-wrapper').masonry({
               columnWidth: 1,
               itemSelector: '.single-gallery-item'
           });*/
           // var $grid = jQuery('body .sk-gallery-wrapper').imagesLoaded( function() {
           //     $grid.masonry({
           //         columnWidth: 1,
           //         itemSelector: '.single-gallery-item'
           //     });
           // });
           jQuery('.sk-gallery-item figure a').each(function(e){
            var yoimg = $(this).find('img').attr("src");
            $(this).find('img').css({'opacity' : '0', 'visibility': 'hidden'});
            $(this).css({'background-image': 'url(' + yoimg + ')'});
           });
            jQuery(document).on('click', '[data-toggle="lightbox"]', function(event){
                event.preventDefault();
                $(this).ekkoLightbox();
                
            });
       }
      

        this.dynamicEmail = function(){
            jQuery('.contact-page-form').on('click',function(){
                email_address = jQuery(this).data('email');
                jQuery('#sk-contact-form-email').val('');
                if(email_address){
                    jQuery('#sk-contact-form-email').val(email_address);
                }
            })
        }

        this.removeTextForNextGen = function(){
            jQuery('.ngg-navigation a').each(function(){
                var elem = jQuery(this).attr('href');
                var txt = elem.replace('/medlem','');
                jQuery(this).attr('href',txt);
            });
        }

    }
    new frontEndUI().init();

})(jQuery);
