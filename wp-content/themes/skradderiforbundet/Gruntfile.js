module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        makepot: {
            target: {
                options: {
                    mainFile: "./style.css",
                    type: "wp-theme",
                    domainPath: "./languages/"
                }
            }
        },

        // COMPASS
        compass: {
            style: {
                options: { // Target options
                    sassDir: './assets/src/sass/below-fold',
                    cssDir: './assets/build/css/',
                    environment: 'development',
                    outputStyle: 'compact',
                    sourcemap: true
                }
            },
            aboveFold: {
                options: { // Target options
                    sassDir: './assets/src/sass/above-fold',
                    cssDir: './assets/build/css/',
                    environment: 'development',
                    outputStyle: 'compact',
                    sourcemap: true
                }
            },
            bootstrap:{
              options: { // Target options
                  sassDir: './assets/src/sass/vender',
                  cssDir: './assets/src/css/',
                  environment: 'development',
                  outputStyle: 'compact',
                  sourcemap: false
              }
            }
        },

        // CSS AND JS CONCAT
        concat: {
            // options: {
            //     separator: "\n \n /*** New File ***/ "
            // },
            vender:{
              src: [
                  './assets/src/css/bootstrap.css',
                  './assets/src/css/ekko-lightbox.css',
                  './assets/src/css/footable.css',
                  './node_modules/bootstrap-select/dist/css/bootstrap-select.css',
                  './node_modules/font-awesome/css/font-awesome.css',
                  './node_modules/owl.carousel/dist/assets/owl.carousel.css',
                  './node_modules/owl.carousel/dist/assets/owl.theme.default.css',
                  './node_modules/wowjs/css/libs/animate.css'

              ],
              dest: './assets/build/css/vender.css'
            },
            css:{
              src: [
                  './assets/build/css/vender.css',
                  './assets/build/css/main.css'

              ],
              dest: './assets/build/css/style.css'
            },
            js: {
                src: [
                    './node_modules/jquery.nicescroll/jquery.nicescroll.js',
                    './node_modules/bootstrap-sass/assets/javascripts/bootstrap.js',
                    './node_modules/bootstrap-select/dist/js/bootstrap-select.js',
                    './node_modules/owl.carousel/dist/owl.carousel.js',
                    './node_modules/clipboard/dist/clipboard.js',
                    './assets/src/js/member-register.js',
                    './assets/src/js/scroll-down.js',
                    './assets/src/js/skip-link-focus-fix.js',
                    './assets/src/js/mrMobileMenu.js',
                    './assets/src/js/ekko-lightbox.min.js',
                    "./assets/src/js/imagesloaded.pkgd.min.js",
                    './assets/src/js/main.js',
                    './assets/src/js/home-page-load-more.js',
                    './assets/src/js/TweenMax.min.js',
                    './assets/src/js/slider.js', 
                    './assets/src/js/footable.js',
                    './assets/src/js/videoPlayOnView.js',
                    './assets/src/js/ScrollMagic.js',
                    './assets/src/js/app.js',
                    './assets/src/js/dictionary.js',
                    './assets/src/js/map.js',
                ],
                dest: './assets/build/js/main.js'
            }

        },

        // MINIFIE CSS
        cssmin: {
            options: {
                shorthandCompacting: false,
                roundingPrecision: -1,
                keepSpecialComments: 0,
                sourceMap: false
            },
            target: {
                files: [{
                    expand: true,
                    cwd: './assets/build/css/',
                    src: ['above-fold.css', 'style.css'],
                    dest: './assets/build/css/',
                    ext: '.min.css'
                }]
            }
        },

        // MIINIFY JS
        uglify: {
            options: {
                report: 'gzip'
            },
            main: {
                src: ['./assets/build/js/main.js'],
                dest: './assets/build/js/main.min.js'
            }
        },

        // COPY CONTENT
        copy: {
            img: {
                files: [
                {
                    expand: true,
                    cwd: './assets/src/img',
                    src: '**',
                    dest: './assets/build/img/',
                    filter: 'isFile'
                },
                {
                    expand: true,
                    cwd: "./assets/src/images",
                    src: "**",
                    dest: "./assets/build/images/",
                    filter: "isFile"
                }
                ]
            },
            fonts:{
              files: [{
                  expand: true,
                  cwd: './node_modules/font-awesome/fonts',
                  src: '**',
                  dest: './assets/build/fonts/',
                  filter: 'isFile'
              }]
            },
            video:{
              files: [{
                  expand: true,
                  cwd: './assets/src/video/',
                  src: '**',
                  dest: './assets/build/video/',
                  filter: 'isFile'
              }]
            },
            svg: {
                files: [{
                    expand: true,
                    cwd: './assets/src/svg',
                    src: '**',
                    dest: './assets/build/svg/',
                    filter: 'isFile'
                }]
            }
        },

        // GRUNT WATCH
        watch: {

            sass: {
                files: [
                    './assets/src/sass/*.scss',
                    './assets/src/sass/**/*.scss',
                    '../assets/src/sass/**/**/*.scss'
                ],
                tasks: ['css']
            },

            img: {
                files: [
                    './assets/src/img/*'
                ],
                tasks: ['img']
            },

            js: {
                files: [
                    './assets/src/js/*.js'
                ],
                tasks: ['js']
            }

        }

    });

    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks("grunt-wp-i18n");

    //register grunt default task
    grunt.registerTask('css', ['compass:style', 'compass:aboveFold']);
    grunt.registerTask('js', ['concat']);
    grunt.registerTask('img', ['copy']);

    grunt.registerTask('default', ['compass', 'concat', 'cssmin', 'uglify', 'copy']);
}
