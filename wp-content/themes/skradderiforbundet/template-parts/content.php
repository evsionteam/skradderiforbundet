<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package skradderiforbundet
 */
?>
<article id="post-<?php the_ID(); ?>" class="sk-news">
    <?php
    $news_thumbnail_id = get_post_thumbnail_id(get_the_ID());
    if(!empty($news_thumbnail_id)){
        $news_image_src = wp_get_attachment_image_src($news_thumbnail_id, 'full');
        echo '<div class="sk-img" style="background-image:url('.$news_image_src[0].');"></div>';
    }else{
        $news_image_src = get_post_meta(get_the_ID(),'_thumbnail_url',true);
        if(!empty($news_image_src)){
            echo '<div class="sk-img" style="background-image:url('.$news_image_src.');"></div>';
        }
    }
    ?>
    <div class="sk-title-n-des">
        <h2 class="sk-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
        <span class="sk-date"><i class="fa fa-calendar"></i><?php echo get_the_date(' j M'); ?></span>
        <div class="sk-description">
            <?php the_content(); ?>
        </div>
    </div>
    <!-- .sk-title-n-des -->
</article><!-- #post-## -->
