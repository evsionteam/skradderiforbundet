<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package skradderiforbundet
 */
?>
<article id="post-<?php the_ID(); ?>" class="sk-news">
    <?php
    $news_thumbnail_id = get_post_thumbnail_id(get_the_ID());
    if(!empty($news_thumbnail_id)){
        $news_image_src = wp_get_attachment_image_src($news_thumbnail_id, 'full');
        echo '<div class="sk-img" style="background-image:url('.$news_image_src[0].');"></div>';
    }
    ?>
    <div class="sk-title-n-des">
        <h2 class="sk-title"><?php the_title(); ?></h2>
        <div class="sk-description">
            <?php the_content(); ?>
        </div>
    </div>
    <!-- .sk-title-n-des -->
</article>
