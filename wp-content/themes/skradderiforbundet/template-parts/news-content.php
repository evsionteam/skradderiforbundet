<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package skradderiforbundet
 */
?>
<div class="col-xs-6 col-sm-6 col-md-4">
    <div class="row">
        <article id="post-<?php the_ID(); ?>" class="sk-news">
            <?php
            $news_image_src = '';
            if(has_post_thumbnail()){
                $news_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'sk-archive-img-size');
                $news_image_src = $news_image[0];
            }else{
                $news_image_src = get_post_meta(get_the_ID(),'_thumbnail_url',true);
                /*Get the appropriate image size from member site*/
                if(!empty($news_image_src)){
                    if(has_image_size('sk-archive-img-size')) {
                        $img_size = get_image_size('sk-archive-img-size');
                        if(!empty($img_size)){
                            $news_image_arr = array();
                            if( isset($img_size['width']) && isset($img_size['height']) ){
                               $news_image_arr = pathinfo($news_image_src);
                                if(!empty($news_image_arr)){
                                    $news_image_src = $news_image_arr['dirname'].'/'.$news_image_arr['filename'].'-'.$img_size['width'].'x'.$img_size['height'].'.'.$news_image_arr['extension'];
                                }
                            }
                        }
                    }
                }
            }

            $url = get_permalink();

            if (!empty($news_image_src)) {
                ?>
                <div class="sk-img-n-date">
                    <div class="category-name">
                        <?php
                        $post_type =get_post_type(get_the_ID());
                        $obj = get_post_type_object( $post_type );
                        echo $obj->labels->singular_name;
                        ?>
                    </div>
                    <a href="<?php the_permalink()?>">
                        <img src="<?php echo $news_image_src; ?>" alt="post-image"/>
                    </a>
                    <ul class="sk-date-n-copy">
                        <li><span class="sk-date"><?php echo get_the_date(' j M'); ?></span></li>
                        <li>
                            <a href="javascript:void(0)" class="sk-copy share-icon">
                                <i class="fa fa-share-alt"></i>
                            </a>
                        </li>
                    </ul>
                    <div class="social-share-links">
                        <ul>
                            <li>
                                <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $url?>" target="_blank">
                                    <i class="fa fa-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $url?>&title=<?php echo get_the_title()?>" target="_blank">
                                    <i class="fa fa-linkedin"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://plus.google.com/share?url=<?php echo $url?>" target="_blank">
                                    <i class="fa fa-google-plus"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://pinterest.com/pin/create/button/?url=<?php echo $url?>&media=<?php echo $news_image_src?>&description=<?php echo strip_tags(get_the_excerpt());?>" target="_blank">
                                    <i class="fa fa-pinterest"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="sk-title-n-des">
                    <h2 class="sk-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                    <div class="sk-description">
                        <?php
                        global $post;
                        echo $post->post_excerpt;
                        ?>
                    </div>
                </div><!-- .sk-title-n-des -->
            <?php } else { ?>
                <div class="sk-img-n-date">
                    <div class="category-name">
                        <?php
                        $post_type =get_post_type(get_the_ID());
                        $obj = get_post_type_object( $post_type );
                        echo $obj->labels->singular_name;
                        ?>
                    </div>
                    <div class="no-image">
                        <ul class="sk-date-n-copy ">
                            <li><span class="sk-date"><?php echo get_the_date(' j M'); ?></span></li>
                            <li>
                                <a href="javascript:void(0)" class="sk-copy share-icon">
                                    <i class="fa fa-share-alt"></i>
                                </a>
                            </li>
                        </ul>
                        <div class="social-share-links">
                            <ul>
                                <li>
                                    <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $url?>" target="_blank">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $url?>&title=<?php echo get_the_title()?>" target="_blank">
                                        <i class="fa fa-linkedin"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://plus.google.com/share?url=<?php echo $url?>" target="_blank">
                                        <i class="fa fa-google-plus"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://pinterest.com/pin/create/button/?url=<?php echo $url?>&description=<?php echo strip_tags(get_the_excerpt());?>" target="_blank">
                                        <i class="fa fa-pinterest"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="sk-title-n-des">
                            <h2 class="sk-title">
                                <a href="<?php the_permalink()?>">
                                    <?php the_title(); ?>
                                </a>
                            </h2>
                            <div class="sk-description">
                                <?php
                                global $post;
                                echo $post->post_excerpt;
                                ?>
                            </div>
                        </div>
                        <!-- End .sk-title-n-des -->
                    </div>
                    <!-- End .sk-title-n-des -->
                </div><!-- End .sk-img-n-date -->
            <?php } ?>
        </article>
        <!-- #post-## -->
    </div>
    <!-- End .row -->
</div><!-- End .col-sm-4 -->
