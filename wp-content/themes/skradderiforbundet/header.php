<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package skradderiforbundet
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> >
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'skradderiforbundet' ); ?></a>

	<header id="masthead" class="site-header" role="banner">

			<div class="col-md-12">
				<div class="sk-container sk-c-gap">
			        <div class="site-branding">
			        	<?php global $sk_option;
			        	$logo_img = $sk_option['logo_image'];
			        	$logo_image_src = wp_get_attachment_image_src($logo_img,'full');
			        	?>
			        	<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
			        		<?php if(!empty($logo_image_src)){?>
			        		<img src="<?php echo $logo_image_src[0]; ?>" alt="site-logo"/>
			        		<?php } ?>
			        	</a>
			        </div><!-- .site-branding -->
			   

			
			    	<nav id="site-navigation" class="main-navigation" role="navigation">
			            <div class="menu-icon-wrapper">
        					<a href="#" id="menu-icon" class="">
        						<span></span>
        						<span></span>
        						<span></span>
        					</a>
        				</div>
        				
				    	<div class="sk-nav-wrapper">
				    		
				    		<?php wp_nav_menu( array( 'theme_location' => 'right_nav', 'menu_id' => 'left-navigation', 'container' => false ) ); ?>
				    	</div>
				    </nav><!-- #site-navigation -->
			</div>
		
			 
			 
		
		</div><!-- .container -->

	</header><!-- #masthead -->

	<div id="content" class="site-content">
