<?php
    get_header();
    $exhibition_category = $date_selected = $free_search = '';
    if (isset($_GET['exhibition_cat'])) {
        $exhibition_category = $_GET['exhibition_cat'];
    }
    if (isset($_GET['date'])) {
        $date_selected = $_GET['date'];
    }
    if (isset($_GET['search'])) {
        $free_search = $_GET['search'];
    }
    /*Fetch archive dates*/
    $archive_dates = sk_get_post_type_archive_dates('ev_exhibitions');
?>
    <div class="sk-inner-page">
        <div id="primary" class="content-area">
            <main id="main" class="site-main" role="main">
                <div class="sk-news-wrapper clearfix">
                    <?php
                    $exhibition_category_args = array(
                        'hide_empty' => 0,
                        'taxonomy' => 'exhibition_cat',
                    );
                    $exhibition_taxonomy = get_terms($exhibition_category_args);
                    ?>
                    <div class="sk-title-n-filter sk-filter clearfix">
                        <div class="col-md-12">
                            <h1 class="visible-xs"><?php _e('Exhibitions','skradderiforbundet')?></h1>
                            <div class="category-filter clearfix sk-c-gap ">
                                <div class="sk-search-form">
                                    <form action="" method="get" autocomplete="off">
                                        <div class="sk-form-group">
                                            <div class="sk-c-select hidden-xs">
                                                <select id="exhibition-cat" name="exhibition_cat" class="term-select selectpicker">
                                                    <option value=""><?php _e('Kategori','skradderiforbundet')?></option>
                                                    <?php
                                                    if(!empty($exhibition_taxonomy)){
                                                        foreach ($exhibition_taxonomy as $category) {
                                                            ?>
                                                            <option value="<?php echo $category->slug; ?>" <?php selected($category->slug, $exhibition_category); ?>><?php echo $category->name ?></option>
                                                        <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="sk-c-select">
                                                <select id="date" name="date" class="date-select selectpicker">
                                                    <option value=""><?php _e('Datum','skradderiforbundet')?></option>
                                                    <?php
                                                    if(!empty($archive_dates)){
                                                        foreach ($archive_dates as $key => $value) {
                                                            ?>
                                                            <option value="<?php echo $key; ?>" <?php selected($key, $date_selected); ?>><?php echo $value; ?></option>
                                                        <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="sk-search">
                                                <div class="sk-form-group">
                                                    <input type="text" name="search" placeholder="<?php _e('Free Search','skradderiforbundet') ?>" value="<?php echo $free_search;?>">
                                                </div>
                                                <button type="submit"><?php _e('Search','skradderiforbundet'); ?></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div><!-- End .category-filter -->
                        </div><!-- End .col-md-12 -->
                    </div><!-- End .sk-title-n-filter -->

                    <div class="sk-news-wrapper clearfix sk-c-gap">
                      <div class="sk-news-archive-wrapper">
                        <div class="news-listing">
                            <?php
                            if(have_posts()):?>
                                <?php
                                $counter = 1;
                                while (have_posts()) : the_post();
                                    get_template_part('template-parts/news-content', get_post_format());
                                    if( $counter % 3 == 0 )
                                      echo '<div class="clearfix visible-md visible-lg"></div>';
                                    if( $counter % 2 == 0 )
                                      echo '<div class="clearfix visible-xs visible-sm"></div>';
                                      $counter ++;
                                endwhile;
                            else :
                                ?>
                                <section class="no-results not-found">
                                    <header class="page-header">
                                        <h1 class="page-title"><?php esc_html_e( 'Exhibitions Not Found', 'skradderiforbundet' ); ?></h1>
                                    </header><!-- .page-header -->
                                </section>
                            <?php endif; ?>
                        </div><!-- End .col-md-12 -->
                    </div><!-- End .sk-news-archive-wrapper -->
                  </div><!-- End .sk-news-wrapper -->
                </div>
                <div class="clearfix"></div>

                <?php
                global $wp_query;
                $ppp = $wp_query->query_vars['posts_per_page'];
                $found_posts = $wp_query->found_posts;
                if($found_posts > $ppp){
                    ?>
                    <div id="sk-load-more-news" class="sk-news-load-more loading-from-exhibition-page">
                        <div class="sk-arrow-n-text">
                            <a href="#" class="sk-load-ajax-posts">
                                <span><?php _e('Äldre Artiklar', 'skradderiforbundet') ?></span>
                                <i class="fa fa-angle-down fa-3x"></i>
                            </a>
                            <span class="load-more-spinner">
                                <i class="fa fa-spinner fa-spin"></i>
                            </span>
                        </div>
                    </div>
                    <?php
                }
                ?>

            </main>
        </div>
    </div>
<?php
get_footer();
