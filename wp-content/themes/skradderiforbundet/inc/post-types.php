<?php
add_action( 'init', 'sk_posts_types_init' );
function sk_posts_types_init() {
    
    $slider_labels = array(
        'name'               => _x( 'Sliders', 'post type general name', 'skradderiforbundet' ),
        'singular_name'      => _x( 'Slider', 'post type singular name', 'skradderiforbundet' ),
        'menu_name'          => _x( 'Sliders', 'admin menu', 'skradderiforbundet' ),
        'name_admin_bar'     => _x( 'Slider', 'add new on admin bar', 'skradderiforbundet' ),
        'add_new'            => _x( 'Add New', 'Slider', 'skradderiforbundet' ),
        'add_new_item'       => __( 'Add New Slider', 'skradderiforbundet' ),
        'new_item'           => __( 'New Slider', 'skradderiforbundet' ),
        'edit_item'          => __( 'Edit Slider', 'skradderiforbundet' ),
        'view_item'          => __( 'View Slider', 'skradderiforbundet' ),
        'all_items'          => __( 'All Sliders', 'skradderiforbundet' ),
        'search_items'       => __( 'Search Sliders', 'skradderiforbundet' ),
        'parent_item_colon'  => __( 'Parent Sliders:', 'skradderiforbundet' ),
        'not_found'          => __( 'No Sliders found.', 'skradderiforbundet' ),
        'not_found_in_trash' => __( 'No Sliders found in Trash.', 'skradderiforbundet' )
    );

    $slider_args = array(
        'labels'             => $slider_labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'slider' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'menu_icon'         => 'dashicons-slides',
        'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt' )
    );

    register_post_type( 'sk_slider', $slider_args );

    $logo_slider_labels = array(
        'name'               => _x( 'Advertisers Logo', 'post type general name', 'skradderiforbundet' ),
        'singular_name'      => _x( 'Advertisers Logo', 'post type singular name', 'skradderiforbundet' ),
        'menu_name'          => _x( 'Advertisers Logo', 'admin menu', 'skradderiforbundet' ),
        'name_admin_bar'     => _x( 'Advertisers Logo', 'add new on admin bar', 'skradderiforbundet' ),
        'add_new'            => _x( 'Add New', 'logo', 'skradderiforbundet' ),
        'add_new_item'       => __( 'Add New Logo', 'skradderiforbundet' ),
        'new_item'           => __( 'New Logo', 'skradderiforbundet' ),
        'edit_item'          => __( 'Edit Logo', 'skradderiforbundet' ),
        'view_item'          => __( 'View Logo', 'skradderiforbundet' ),
        'all_items'          => __( 'All Logos', 'skradderiforbundet' ),
        'search_items'       => __( 'Search Logos', 'skradderiforbundet' ),
        'parent_item_colon'  => __( 'Parent Logos:', 'skradderiforbundet' ),
        'not_found'          => __( 'No Logos found.', 'skradderiforbundet' ),
        'not_found_in_trash' => __( 'No Logos found in Trash.', 'skradderiforbundet' )
    );

    $logo_slider_args = array(
        'labels'             => $logo_slider_labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'logo' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'menu_icon'         => 'dashicons-format-image',
        'supports'           => array( 'title', 'thumbnail' )
    );

    register_post_type( 'sk_logo_slider', $logo_slider_args );
}


add_action( 'init', 'sk_news_post_init' );
function sk_news_post_init() {
    $bl_labels = array(
        'name'               => _x( 'News', 'post type general name', 'skradderiforbundet' ),
        'singular_name'      => _x( 'News', 'post type singular name', 'skradderiforbundet' ),
        'menu_name'          => _x( 'News', 'admin menu', 'skradderiforbundet' ),
        'name_admin_bar'     => _x( 'News', 'add new on admin bar', 'skradderiforbundet' ),
        'add_new'            => _x( 'Add New', 'News', 'skradderiforbundet' ),
        'add_new_item'       => __( 'Add New News', 'skradderiforbundet' ),
        'new_item'           => __( 'New News', 'skradderiforbundet' ),
        'edit_item'          => __( 'Edit News', 'skradderiforbundet' ),
        'view_item'          => __( 'View News', 'skradderiforbundet' ),
        'all_items'          => __( 'All', 'skradderiforbundet' ),
        'search_items'       => __( 'Search News', 'skradderiforbundet' ),
        'parent_item_colon'  => __( 'Parent News:', 'skradderiforbundet' ),
        'not_found'          => __( 'No News found.', 'skradderiforbundet' ),
        'not_found_in_trash' => __( 'No News found in Trash.', 'skradderiforbundet' )
    );
    $bl_args = array(
        'labels'             => $bl_labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'news' ),
        'taxonomies'         => array( 'news_cat'),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt' )
    );
    register_post_type( 'sk_news', $bl_args );
}
add_action( 'init', 'create_news_taxonomies', 0 );
function create_news_taxonomies() {
    $news_cat_labels = array(
        'name'              => _x( 'Category', 'taxonomy general name', 'skradderiforbundet' ),
        'singular_name'     => _x( 'Category', 'taxonomy singular name', 'skradderiforbundet' ),
        'search_items'      => __( 'Search Category', 'skradderiforbundet' ),
        'all_items'         => __( 'All Category', 'skradderiforbundet' ),
        'parent_item'       => __( 'Parent Category', 'skradderiforbundet' ),
        'parent_item_colon' => __( 'Parent Category:', 'skradderiforbundet' ),
        'edit_item'         => __( 'Edit Category', 'skradderiforbundet' ),
        'update_item'       => __( 'Update Category', 'skradderiforbundet' ),
        'add_new_item'      => __( 'Add New Category', 'skradderiforbundet' ),
        'new_item_name'     => __( 'New Category Name', 'skradderiforbundet' ),
        'menu_name'         => __( 'Category', 'skradderiforbundet' ),
    );
    $news_cat_args = array(
        'hierarchical'      => true,
        'labels'            => $news_cat_labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'news-cat' ),
    );
    register_taxonomy( 'news_cat', array( 'sk_news' ), $news_cat_args );
    
}
add_action('init','add_categories_to_news');
function add_categories_to_news(){
    register_taxonomy_for_object_type('news_cat', 'sk_news');
}