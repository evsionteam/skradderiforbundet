<?php
function skradderiforbundet_customizer($wp_customize)
{

    $wp_customize->add_section(
        'homepage_section',
        array(
            'title' => __('Startsida', 'skradderiforbundet'),
            'description' => __('Startsida Settings !!!', 'skradderiforbundet'),
        )
    );

    $wp_customize->add_setting(
        'sk_option[logo_image]',
        array()
    );
    $wp_customize->add_control(
        new WP_Customize_Image_Control(
            $wp_customize,
            'sk_option[logo_image]',
            array(
                'label' => __('Sid logo', 'skradderiforbundet'),
                'section' => 'homepage_section',
                'type' => 'media'
            )
        )
    );

    $wp_customize->add_setting(
        'sk_option[homepage_img]',
        array()
    );
    $wp_customize->add_control(
        new WP_Customize_Upload_Control(
            $wp_customize,
            'sk_option[homepage_img]',
            array(
                'label' => __('Startsida Bild', 'skradderiforbundet'),
                'section' => 'homepage_section',
                'type' => 'media',
            )
        )
    );
    $wp_customize->add_setting(
        'sk_option[homepage_vid]',
        array()
    );
    $wp_customize->add_control(
        new WP_Customize_Upload_Control(
            $wp_customize,
            'sk_option[homepage_vid]',
            array(
                'label' => __('Startsida Video', 'skradderiforbundet'),
                'section' => 'homepage_section',
                'type' => 'media',
            )
        )
    );

    $wp_customize->add_setting(
        'sk_option[site_color]',
        array(
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'sk_option[site_color]',
            array(
                'label' => __( 'Site Color', 'skradderiforbundet' ) ,
                'section' =>  'homepage_section',
            )
        )
    );


    $wp_customize->add_panel(
        'email_panel',
        array(
            'title'          =>__( 'Email inställningar', 'skradderiforbundet' ),
        )
    );

    /**/
    $wp_customize->add_section(
        'member_register_email_section',
        array(
            'title' => __( 'Medlems verifikations mail', 'skradderiforbundet' ),
            'panel'  => 'email_panel'
        )
    );

    $wp_customize->add_setting(
        'sk_option[mr_email_header]',
        array(
            'sanitize_callback' => 'sk_sanitize_text'
        )
    );
    $wp_customize->add_control(
        'sk_option[mr_email_header]',
        array(
            'label' => __( 'Titel text', 'skradderiforbundet' ),
            'section' => 'member_register_email_section',
            'type' => 'textarea'
        )
    );

    $wp_customize->add_setting(
        'sk_option[mr_email_content]',
        array(
            'sanitize_callback' => 'sk_sanitize_text'
        )
    );
    $wp_customize->add_control(
        'sk_option[mr_email_content]',
        array(
            'label' => __( 'Email innehåll', 'skradderiforbundet' ),
            'section' => 'member_register_email_section',
            'type' => 'textarea'
        )
    );

    $wp_customize->add_setting(
        'sk_option[mr_email_footer]',
        array(
            'sanitize_callback' => 'sk_sanitize_text'
        )
    );
    $wp_customize->add_control(
        'sk_option[mr_email_footer]',
        array(
            'label' => __( 'Sidfot text', 'skradderiforbundet' ),
            'section' => 'member_register_email_section',
            'type' => 'textarea'
        )
    );
    /**/

    /**/
    $wp_customize->add_section(
        'member_approved_email_section',
        array(
            'title' => __( 'Medlems godkänt mail', 'skradderiforbundet' ),
            'panel'  => 'email_panel'
        )
    );

    $wp_customize->add_setting(
        'sk_option[ma_email_header]',
        array(
            'sanitize_callback' => 'sk_sanitize_text'
        )
    );
    $wp_customize->add_control(
        'sk_option[ma_email_header]',
        array(
            'label' => __( 'Titel text', 'skradderiforbundet' ),
            'section' => 'member_approved_email_section',
            'type' => 'textarea'
        )
    );

    $wp_customize->add_setting(
        'sk_option[ma_email_content]',
        array(
            'sanitize_callback' => 'sk_sanitize_text'
        )
    );
    $wp_customize->add_control(
        'sk_option[ma_email_content]',
        array(
            'label' => __( 'Email innehåll', 'skradderiforbundet' ),
            'section' => 'member_approved_email_section',
            'type' => 'textarea'
        )
    );

    $wp_customize->add_setting(
        'sk_option[ma_email_footer]',
        array(
            'sanitize_callback' => 'sk_sanitize_text'
        )
    );
    $wp_customize->add_control(
        'sk_option[ma_email_footer]',
        array(
            'label' => __( 'Sidfot text', 'skradderiforbundet' ),
            'section' => 'member_approved_email_section',
            'type' => 'textarea'
        )
    );

    /**/
}

add_action('customize_register', 'skradderiforbundet_customizer');

function sk_sanitize_text( $input ) {
    return wp_kses_post( force_balance_tags( $input ) );
}