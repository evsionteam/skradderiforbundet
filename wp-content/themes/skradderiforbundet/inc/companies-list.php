<?php

/*get all terms of a taxonomy provided by using sql query*/
function sk_get_tax_terms_by_sql($taxonomy, $parents_only = false, $hide_empty = false){
    global $wpdb;
    $empty_arg = $parents_only_arg = '';

    if($hide_empty == true){
        $empty_arg = " AND tt.count > 0 ";
    }
    if($parents_only == true){
        $parents_only_arg = " AND tt.parent = 0 ";
    }

    $query = $wpdb->prepare(
        "
        SELECT t.term_id, t.name, t.slug
        FROM ".$wpdb->prefix."terms AS t
        INNER JOIN ".$wpdb->prefix."term_taxonomy AS tt ON (t.term_id = tt.term_id)
        WHERE tt.taxonomy IN ('%s') ".$empty_arg."".$parents_only_arg."
        ORDER BY t.name ASC
        ",
        $taxonomy
    );
    $terms = $wpdb->get_results($query,ARRAY_A );
    return $terms;
}

/*get all terms associated with a post by using sql query*/
function sk_get_post_terms_by_sql($post_id ,$taxonomy){
    global $wpdb;
    $query = $wpdb->prepare(
        "
        SELECT t.*, tt.*
        FROM ".$wpdb->prefix."terms AS t
        INNER JOIN ".$wpdb->prefix."term_taxonomy AS tt ON (t.term_id = tt.term_id)
        INNER JOIN ".$wpdb->prefix."term_relationships AS tr ON (tr.term_taxonomy_id = tt.term_taxonomy_id)
        WHERE tt.taxonomy IN ('%s') AND tr.object_id IN (%s)
        ORDER BY t.name ASC
        "
        ,
        $taxonomy,
        $post_id
    );
    $terms = $wpdb->get_results($query,ARRAY_A );
    return $terms;
}

/*get all sub terms of a parent term by using sql query*/
function sk_get_term_sub_cats_by_sql($term_id,$taxonomy,$hide_empty = false){
    global $wpdb;
    $empty_arg = '';
    if($hide_empty == true){
        $empty_arg = " AND tt.count > 0 ";
    }
    $query = $wpdb->prepare(
        "
        SELECT t.term_id, t.name, t.slug
        FROM ".$wpdb->prefix."terms AS t
        INNER JOIN ".$wpdb->prefix."term_taxonomy AS tt ON (t.term_id = tt.term_id)
        WHERE tt.taxonomy = '%s' AND tt.parent = '%s' ".$empty_arg."
        ORDER BY t.name ASC
        ",
        $taxonomy,
        $term_id
    );
    $terms = $wpdb->get_results($query,ARRAY_A );
    return $terms;
}
/**/

/*Get sub categories*/
add_action( 'wp_ajax_sk_get_sub_cats', 'sk_get_sub_cats_callback' );
add_action( 'wp_ajax_nopriv_sk_get_sub_cats', 'sk_get_sub_cats_callback' );
function sk_get_sub_cats_callback(){
    $data = array();
    $parent_cat = $_POST['cat'];
    if(!empty($parent_cat)){
        switch_to_blog(2);
            $sub_cats = sk_get_term_sub_cats_by_sql( $parent_cat,'company_category' );
            if(!empty($sub_cats) && is_array($sub_cats)){
                foreach($sub_cats as $sub_cat){
                    $data[$sub_cat['term_id']] = $sub_cat['name'];
                }
            }
            if(!empty($data)){
                wp_send_json_success( $data );
            }else{
                wp_send_json_error();
            }
        restore_current_blog();
    }else{
        wp_send_json_error();
    }
    die();
}

/*ajax fetch companies*/
add_action('wp_ajax_nopriv_sk_get_companies', 'sk_get_companies');
add_action('wp_ajax_sk_get_companies', 'sk_get_companies');
function sk_get_companies($from = ''){

    $output['have_companies'] = false;
    $output['company_data'] = $company_data = array();

    $args = array(
        'post_type' => 'ev_companies',
        'posts_per_page' => -1,
        'post_status' => 'publish'
    );

    /*switch to medlem site to fetch companies*/
    switch_to_blog(2);

    $query = new WP_Query($args);
    if ($query->have_posts()):
        while ($query->have_posts()):$query->the_post();
            global $post;
            $link = '<a href="'.network_site_url('/hitta-skradderi/').'?q='.$post->post_name.'" target="_blank">'.__("Läs mer","skradderiforbundet").'</a>';

            $email = get_post_meta(get_the_ID(),'_company_email',true);
            if(!empty($email)){
                $email = '<a href="mailto:'.$email.'">'.$email.'</a>';
            }

            $website = get_post_meta(get_the_ID(),'_company_website',true);
            if(!empty($website)){
                $website = '<a href="'.esc_url($website).'" target="_blank">'.$website.'</a>';
            }

            $temp = array(
                'name' => get_the_title(),
                'lat' => get_post_meta(get_the_ID(),'_company_latitude',true),
                'lng' => get_post_meta(get_the_ID(),'_company_longitude',true),
                'address' => get_post_meta(get_the_ID(),'_company_address',true),
                'phone' => get_post_meta(get_the_ID(),'_company_phone',true),
                'city' => get_post_meta(get_the_ID(),'_company_city',true),
                'email' => $email,
                'website' => $website,
                'link' => $link,
            );

            /*Get post terms*/
            $categories = '';
            $cat_arr = array();
            $company_cats = sk_get_post_terms_by_sql(get_the_ID(),'company_category');
            if(!empty($company_cats) && is_array($company_cats)){
               foreach($company_cats as $company_cat){
                   $cat_arr[] = $company_cat['term_id'];
               }
            }
            if(!empty($cat_arr)){
                $categories = implode(',',$cat_arr);
            }
            $temp['cat'] = $categories;
            /**/

            /*Get Image*/
            $temp['image'] = get_the_post_thumbnail_url(get_the_ID(),'post-thumbnail');
            /**/

            $company_data[] = $temp;
        endwhile;wp_reset_query();
        $output['have_companies'] = true;
    endif;

    /*restore to current blog */
    restore_current_blog();

    $output['company_data'] = $company_data;

    /*Return the data directly for list mode*/
    if('list-mode' == $from){
        return $output;
    }
    /**/

    echo json_encode($output);
    exit;
}
