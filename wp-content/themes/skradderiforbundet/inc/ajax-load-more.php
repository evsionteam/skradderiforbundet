<?php
function load_more_results(){

    $output['more_post'] = false;
    $output['data'] = '';

    $offset = $_POST['offset'];
    $ppp = $_POST['ppp'];
    $news_category = $_POST['news_cat'];
    $exhibition_category = $_POST['exhibition_cat'];
    $date = $_POST['date_arc'];

    if(!empty($date)){
      $date_values = explode('-',$date);
    }

    if( !empty($date_values) && is_array($date_values) ){
        $year = $date_values[0];
        $month = $date_values[1];
    } else {
        $year ='';
        $month ='';
    }


    $args = array(
        'posts_per_page' => $ppp,
        'offset' => $offset,
    );

    $loading_from = $_POST['loading_from'];
    if(!empty($loading_from)){
        if('home' == $loading_from){
            $args['post_type'] = array('sk_news','ev_courses','ev_exhibitions','ev_world_congress');
        }
        if('news' == $loading_from){
            $args['post_type'] = array( 'sk_news' );
        }
        if('exhibition' == $loading_from){
            $args['post_type'] = array( 'ev_exhibitions' );
        }
        if('modemuseum' == $loading_from){
            $args['post_type'] = array( 'ev_modemuseum' );
        }
    }

    /*News Category*/
    if(isset($_POST['news_cat']) && !empty($_POST['news_cat'])){
      $args['tax_query'] = array(
          array(
            'taxonomy' => 'news_cat',
            'field'    => 'slug',
            'terms'    => $news_category,
          ),
      );
    }
    /**/

    /*Exhibition Category*/
    if(isset($_POST['exhibition_cat']) && !empty($_POST['exhibition_cat'])){
        $args['tax_query'] = array(
            array(
                'taxonomy' => 'exhibition_cat',
                'field'    => 'slug',
                'terms'    => $exhibition_category,
            ),
        );
    }
    /**/

    /*Date Query*/
    if((isset($_POST['date_arc'])) && (!empty($_POST['date_arc']))){
      $args['date_query'] = array(
          array(
            'year' => $year,
            'month' => $month,
          ),
      );
    }
    /**/

    if(!empty($loading_from)){
        if('home' == $loading_from){
            $args['meta_key'] = 'sk_post_order';
            $args['orderby'] = 'meta_value_num';
            $args['order'] = 'ASC';
        }
    }
    /*Order By featured Post*/
    //$args['meta_key'] = '_is_featured_post';
    //$args['orderby'] = 'meta_value_num';
    //$args['order'] = 'DESC';
    /**/


    /*meta query*/
    $args['meta_query'] = array(
        'relation' => 'OR',
        array(
            'key'     => 'expires_in',
            'value'   => '',
            'compare' => '=',
        ),
        array(
            'key'     => 'expires_in',
            'compare' => 'NOT EXISTS',
        ),
        array(
            'key'     => 'expires_in',
            'value'   => date("Y-m-d"),
            'type'    => 'DATE',
            'compare' => '>',
        ),
    );
    /**/

    /*search filter*/
    if(isset($_POST['search']) && !empty($_POST['search'])){
        $args['s'] = $_POST['search'];
    }
    /**/

    $query = new WP_Query($args);
    if (($query->found_posts)):
        $counter = 1;
        $output['more_post'] = true;
        while ($query->have_posts()): $query->the_post();
            ob_start();
            get_template_part('template-parts/news-content', get_post_format());
            if( $counter % 3 == 0 )
                echo '<div class="clearfix visible-md visible-lg"></div>';
            if( $counter % 2 == 0 )
                echo '<div class="clearfix visible-xs visible-sm"></div>';
            $counter ++;
            $output['data'][] = ob_get_clean();
        endwhile;
    else:
        $output['more_post'] = false;
        $output['data'] = __('No More Posts','skradderiforbundet');
    endif;
    echo json_encode($output);
    exit;
}
add_action('wp_ajax_nopriv_load_more_results', 'load_more_results');
add_action('wp_ajax_load_more_results', 'load_more_results');