<?php 
add_action( 'add_meta_boxes', 'sk_meta_box_lexikonet' );
function sk_meta_box_lexikonet()
{   
    $page_template = get_post_meta(get_the_ID(), '_wp_page_template', true);
    if($page_template == 'page-templates/template-lexikonet.php' ){
        add_meta_box( 'sk-meta-lexikonet', 'Lexikonet Info', 'sk_meta_box_lexikonet_callback', 'page', 'normal', 'high' );
    }
}

function sk_meta_box_lexikonet_callback( $post )
{
    $values1 = get_post_custom( $post->ID );
    $sk_om_lexikonet_title = isset( $values1['sk_om_lexikonet_title'] ) ? $values1['sk_om_lexikonet_title'][0] : '';
    $sk_om_lexikonet_content = isset( $values1['sk_om_lexikonet_content'] ) ? $values1['sk_om_lexikonet_content'][0] : '';

    $sk_om_lexikonet_buy_title = isset( $values1['sk_om_lexikonet_buy_title'] ) ? $values1['sk_om_lexikonet_buy_title'][0] : '';
    $sk_om_lexikonet_buy_content = isset( $values1['sk_om_lexikonet_buy_content'] ) ? $values1['sk_om_lexikonet_buy_content'][0] : '';
    $sk_om_lexikonet_member_title = isset( $values1['sk_om_lexikonet_member_title'] ) ? $values1['sk_om_lexikonet_member_title'][0] : '';
    $sk_om_lexikonet_member_content = isset( $values1['sk_om_lexikonet_member_content'] ) ? $values1['sk_om_lexikonet_member_content'][0] : '';

    $sk_om_lexikonet_order_button = isset( $values1['sk_om_lexikonet_order_button'] ) ? $values1['sk_om_lexikonet_order_button'][0] : '';

    $modal_content = isset( $values1['modal_content'] ) ? $values1['modal_content'][0] : '';
    

    wp_nonce_field( 'sk_om_lexikonet_title', 'my_sk_om_lexikonet_title' );
    wp_nonce_field( 'sk_om_lexikonet_content', 'my_sk_om_lexikonet_content' );
    wp_nonce_field( 'sk_om_lexikonet_buy_title', 'my_sk_om_lexikonet_buy_title' );
    wp_nonce_field( 'sk_om_lexikonet_buy_content', 'my_sk_om_lexikonet_buy_content' );
    wp_nonce_field( 'sk_om_lexikonet_member_title', 'my_sk_om_lexikonet_member_title' );
    wp_nonce_field( 'sk_om_lexikonet_member_content', 'my_sk_om_lexikonet_member_content' );
    wp_nonce_field( 'sk_om_lexikonet_order_button', 'my_sk_om_lexikonet_order_button' );
    ?>
   
    <p><b><?php _e('Om lexikonet Title', 'skradderiforbundet'); ?></b></p>
    <input type="text" name="sk_om_lexikonet_title" id="sk_om_lexikonet_title" size="80" value="<?php echo $sk_om_lexikonet_title; ?>"><br/>
    <p><b><?php _e('Om lexikonet Content', 'skradderiforbundet'); ?></b></p>
     <textarea rows="13" cols="130" name="sk_om_lexikonet_content" id="sk_om_lexikonet_content"><?php echo $sk_om_lexikonet_content; ?></textarea><br/>
    <hr>


    <p><b><?php _e('Köp ditt lexikon av oss Title', 'skradderiforbundet'); ?></b></p>
    <input type="text" name="sk_om_lexikonet_buy_title" id="sk_om_lexikonet_buy_title" size="80" value="<?php echo $sk_om_lexikonet_buy_title; ?>"><br/>
    <p><b><?php _e('Köp ditt lexikon av oss Content', 'skradderiforbundet'); ?></b></p>
     <textarea rows="13" cols="130" name="sk_om_lexikonet_buy_content" id="sk_om_lexikonet_buy_content"><?php echo $sk_om_lexikonet_buy_content; ?></textarea><br/>
    <hr>


    <p><b><?php _e('Medlemar Title', 'skradderiforbundet'); ?></b></p>
    <input type="text" name="sk_om_lexikonet_member_title" id="sk_om_lexikonet_member_title" size="80" value="<?php echo $sk_om_lexikonet_member_title; ?>"><br/>
    <p><b><?php _e('Medlemar Content', 'skradderiforbundet'); ?></b></p>
     <textarea rows="13" cols="130" name="sk_om_lexikonet_member_content" id="sk_om_lexikonet_member_content"><?php echo $sk_om_lexikonet_member_content; ?></textarea><br/>
    <hr>

    <p><b><?php _e('Modal Button Text', 'skradderiforbundet'); ?></b></p>
    <input type="text" name="sk_om_lexikonet_order_button" id="sk_om_lexikonet_order_button" size="30" value="<?php echo $sk_om_lexikonet_order_button; ?>"><br/>
    <hr>

    <p><b><?php _e('Modal Content', 'skradderiforbundet'); ?></b></p>
    <textarea rows="13" cols="130" name="modal_content"><?php echo $modal_content; ?></textarea><br/>
    <hr>


    <?php   
}

add_action( 'save_post', 'sk_meta_lexikonet_save' );
function sk_meta_lexikonet_save( $post_id )
{
    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

    // if our nonce isn't there, or we can't verify it, bail
    if( !isset( $_POST['my_sk_om_lexikonet_title'] ) || !wp_verify_nonce( $_POST['my_sk_om_lexikonet_title'], 'sk_om_lexikonet_title' ) ) return;
    if( !isset( $_POST['my_sk_om_lexikonet_content'] ) || !wp_verify_nonce( $_POST['my_sk_om_lexikonet_content'], 'sk_om_lexikonet_content' ) ) return;

    if( !isset( $_POST['my_sk_om_lexikonet_buy_title'] ) || !wp_verify_nonce( $_POST['my_sk_om_lexikonet_buy_title'], 'sk_om_lexikonet_buy_title' ) ) return;
    if( !isset( $_POST['my_sk_om_lexikonet_buy_content'] ) || !wp_verify_nonce( $_POST['my_sk_om_lexikonet_buy_content'], 'sk_om_lexikonet_buy_content' ) ) return;

    if( !isset( $_POST['my_sk_om_lexikonet_member_title'] ) || !wp_verify_nonce( $_POST['my_sk_om_lexikonet_member_title'], 'sk_om_lexikonet_member_title' ) ) return;
    if( !isset( $_POST['my_sk_om_lexikonet_member_content'] ) || !wp_verify_nonce( $_POST['my_sk_om_lexikonet_member_content'], 'sk_om_lexikonet_member_content' ) ) return;

    if( !isset( $_POST['my_sk_om_lexikonet_order_button'] ) || !wp_verify_nonce( $_POST['my_sk_om_lexikonet_order_button'], 'sk_om_lexikonet_order_button' ) ) return;



    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;

    // Probably a good idea to make sure your data is set
     if( isset( $_POST['sk_om_lexikonet_title'] ) )
        update_post_meta( $post_id, 'sk_om_lexikonet_title', $_POST['sk_om_lexikonet_title'] );

    if( isset( $_POST['sk_om_lexikonet_content'] ) )
        update_post_meta( $post_id, 'sk_om_lexikonet_content', $_POST['sk_om_lexikonet_content'] );

    if( isset( $_POST['sk_om_lexikonet_buy_title'] ) )
        update_post_meta( $post_id, 'sk_om_lexikonet_buy_title', $_POST['sk_om_lexikonet_buy_title'] );

    if( isset( $_POST['sk_om_lexikonet_buy_content'] ) )
        update_post_meta( $post_id, 'sk_om_lexikonet_buy_content', $_POST['sk_om_lexikonet_buy_content'] );

    if( isset( $_POST['sk_om_lexikonet_member_title'] ) )
        update_post_meta( $post_id, 'sk_om_lexikonet_member_title', $_POST['sk_om_lexikonet_member_title'] );

    if( isset( $_POST['sk_om_lexikonet_member_content'] ) )
        update_post_meta( $post_id, 'sk_om_lexikonet_member_content', $_POST['sk_om_lexikonet_member_content'] );

    if( isset( $_POST['sk_om_lexikonet_order_button'] ) )
        update_post_meta( $post_id, 'sk_om_lexikonet_order_button', $_POST['sk_om_lexikonet_order_button'] );

    if( isset( $_POST['modal_content'] ) )
        update_post_meta( $post_id, 'modal_content', $_POST['modal_content'] );
    

}