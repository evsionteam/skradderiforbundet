<?php
function load_mixed_results(){

    $output['more_post'] = false;
    $output['data'] = '';

    $offset = $_POST['offset'];
    $ppp = $_POST['ppp'];

    $args = array(
        'post_type' => array('sk_news','ev_exhibitions','ev_world_congress','ev_courses'),
        'posts_per_page' => $ppp,
        'offset' => $offset,
        'ignore_custom_sort' => true,
        'meta_key' => 'sk_post_order',
        'orderby' => array( 'meta_value_num' => 'ASC', 'date' => 'DESC' ),
        'meta_query' => array(
            'relation' => 'AND',
            array(
                'relation' => 'OR',
                array(
                    'key'     => 'expires_in',
                    'value'   => '',
                    'compare' => '=',
                ),
                array(
                    'key'     => 'expires_in',
                    'compare' => 'NOT EXISTS',
                ),
                array(
                    'key'     => 'expires_in',
                    'value'   => date("Y-m-d"),
                    'type'    => 'DATE',
                    'compare' => '>',
                ),
            )
        ),
    );

    $query = new WP_Query($args);
    if (($query->found_posts)):
        $counter = 1;
        $output['more_post'] = true;
        while ($query->have_posts()): $query->the_post();
            ob_start();
            get_template_part('template-parts/news-content', get_post_format());
            if( $counter % 3 == 0 )
                echo '<div class="clearfix visible-md visible-lg"></div>';
            if( $counter % 2 == 0 )
                echo '<div class="clearfix visible-xs visible-sm"></div>';
            $counter ++;
            $output['data'][] = ob_get_clean();
        endwhile;
    else:
        $output['more_post'] = false;
        $output['data'] = __('No More Posts','skradderiforbundet');
    endif;
    echo json_encode($output);
    exit;
}
add_action('wp_ajax_nopriv_load_mixed_results', 'load_mixed_results');
add_action('wp_ajax_load_mixed_results', 'load_mixed_results');