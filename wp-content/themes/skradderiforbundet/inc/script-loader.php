<?php
add_action( 'wp_enqueue_scripts', 'sk_intranet_scripts' );
function sk_intranet_scripts() {

    wp_enqueue_style( 'skradderiforbundet-style', get_stylesheet_uri() );

    $suffix = '.min';
    if( defined('SCRIPT_DEBUG') && SCRIPT_DEBUG ){
        $suffix = '';
    }

    $assets_url = get_stylesheet_directory_uri().'/assets/build/';

    if(empty($suffix)){
        wp_register_style( 'skradderiforbundet-vender', $assets_url .'/css/vender'. $suffix . '.css', array(), '20151215');
        wp_register_style( 'skradderiforbundet-above-fold', $assets_url .'/css/above-fold'. $suffix . '.css', array(), '20151215' );
        wp_enqueue_style( 'skradderiforbundet-main-style', $assets_url .'/css/main'. $suffix . '.css', array('skradderiforbundet-vender', 'skradderiforbundet-above-fold'), '20151215' );
    }else{
        wp_enqueue_style( 'skradderiforbundet-above-fold', $assets_url .'/css/above-fold'. $suffix . '.css', array(), '20151215' );
        wp_enqueue_style( 'skradderiforbundet-main-style', $assets_url .'/css/style'. $suffix . '.css', array(), '20151215' );
    }

    global $sk_option;
    if( isset($sk_option['site_color']) && !empty($sk_option['site_color']) ){
        $color = $sk_option['site_color'];
        $custom_css = "
        .site-header .sk-nav-wrapper .menu > li > a:after {
            background-color: $color;
        }
        .site-header .sk-nav-wrapper .menu > li ul.sub-menu li:hover {
            background-color: $color;
        }
        .sk-main-banner.slider .slider-item .slider-caption h1:after {
            border-bottom: 5px $color solid;
        }
        .sk-main-banner.slider .slider-item .slider-caption a.hightlight-text {
            color: $color;
        }
        .sk-product-section .sk-product-detail .sk-product-description .padded-multi-line {
            -webkit-box-shadow: 10px 10px 0 $color;
            box-shadow: 10px 10px 0 $color;
        }
        .sk-news-wrapper .sk-news .sk-img-n-date .category-name {
            background-color: $color
        }
        .sk-search-form button[type='submit']{
            background: $color;
        }
        .sk-table tbody tr td.footable-last-visible a {
            color: $color;
        }
        .sk-table tbody tr td.footable-last-visible a:hover:after {
            background-color: $color;
        }
        .bootstrap-select button.dropdown-toggle .caret:after {
            color: $color;
        }
        .map button[type='submit'] {
            background-color: $color;
        }
        #sk-modal-box .modal-dialog .sk-wcontact-form-popup.modal-content .modal-body .wpcf7 .modal-send-btn input[type='submit'] {
            background-color: $color;
        }
        #sk-modal-box .modal-dialog .sk-wcontact-form-popup.modal-content .close {
            background-color: $color;
        }
        .sk-archive-link .see-all {
            background-color: $color;
        }
        ";
        wp_add_inline_style( 'skradderiforbundet-main-style', $custom_css );
    }

    wp_enqueue_script('jquery-ui-core');
    wp_enqueue_script('jquery-ui-datepicker');
    wp_enqueue_style('jquery-ui-css', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
    wp_enqueue_style( 'skradderiforbundet-google-fonts', 'https://fonts.googleapis.com/css?family=Merriweather:300,400,700|Montserrat:400,500,700' );

    //scripts
    wp_enqueue_script('google-maps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDIuunzpLhUeoYDivVb08zklnJgOVsLRvM');

    wp_enqueue_script('masonry');
    wp_enqueue_script( 'skradderiforbundet-main', $assets_url . '/js/main'.$suffix.'.js', array('jquery'), '20151215', true );
    wp_localize_script( 'skradderiforbundet-main', 'skAjax' ,array(
            'ajaxurl' => admin_url( 'admin-ajax.php' )
        )
    );

    $webfont = "WebFontConfig = {
					google: { families: [
						'Merriweather:300,300i,400,400i,700,700i,900,900i',
						'Montserrat:300,300i,400i,500,500i,600,600i,700,700i',
						'Playfair+Display:400,700,700i,900'
					] }
				};
				(function() {
					var wf = document.createElement('script');
					wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
					wf.type = 'text/javascript';
					wf.async = 'true';
					var s = document.getElementsByTagName('script')[0];
					s.parentNode.insertBefore(wf, s);
				})();

				(function(d) {
				    var config = {
				      kitId: 'bct1apf',
				      scriptTimeout: 3000,
				      async: true
				    },
				    h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,'')+' wf-inactive';},config.scriptTimeout),tk=d.createElement('script'),f=false,s=d.getElementsByTagName('script')[0],a;h.className+=' wf-loading';tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!='complete'&&a!='loaded')return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
				  })(document);";

    wp_add_inline_script('skradderiforbundet-main', $webfont );

    wp_add_inline_style( 'skradderiforbundet-main-style','body{
		opacity: 0
	}');

    $preloader = 'function preloader( param ){

		this.time = param.time;

		this.init = function(){

			var time = this.time;

			jQuery(window).load( function(){
				jQuery( "body" ).animate({
					opacity : 1
				},time);
			})
		}
	} jQuery(document).ready(function(){ new preloader({ time: 400 }).init(); });';

    wp_add_inline_script( 'skradderiforbundet-main',$preloader );
}

add_action( 'admin_enqueue_scripts', 'sk_admin_scripts' );
function sk_admin_scripts() {

    $screen = get_current_screen();
    if ( $screen->id == 'sk_education' || $screen->id == 'sk_news' || $screen->id == 'ev_exhibitions' ) {
        wp_enqueue_script('jquery-ui-core');
        wp_enqueue_script('jquery-ui-datepicker');
        wp_enqueue_style('jquery-ui-css', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
    }
}

function sk_print_admin_scripts() {
    $screen = get_current_screen();
    if ( $screen->id == 'sk_education' || $screen->id == 'sk_news' || $screen->id == 'ev_exhibitions' ) {
        ?>
        <script type="text/javascript">
            jQuery('.datepicker').datepicker({
                dateFormat : 'yy-mm-dd',
                changeMonth: true
            });
        </script>
    <?php
    }
}
add_action( 'admin_print_footer_scripts', 'sk_print_admin_scripts' );


