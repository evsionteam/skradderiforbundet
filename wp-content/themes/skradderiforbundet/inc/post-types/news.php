<?php
add_action( 'init', 'sk_news_post_init' );
function sk_news_post_init() {
    $bl_labels = array(
        'name'               => _x( 'Nyheter', 'post type general name', 'skradderiforbundet' ),
        'singular_name'      => _x( 'Nyheter', 'post type singular name', 'skradderiforbundet' ),
        'menu_name'          => _x( 'Nyheter', 'admin menu', 'skradderiforbundet' ),
        'name_admin_bar'     => _x( 'Nyheter', 'add new on admin bar', 'skradderiforbundet' ),
        'add_new'            => _x( 'Lägg till ny', 'Nyheter', 'skradderiforbundet' ),
        'add_new_item'       => __( 'Lägg till ny Nyheter', 'skradderiforbundet' ),
        'new_item'           => __( 'New Nyheter', 'skradderiforbundet' ),
        'edit_item'          => __( 'Edit Nyheter', 'skradderiforbundet' ),
        'view_item'          => __( 'View Nyheter', 'skradderiforbundet' ),
        'all_items'          => __( 'Alla nyheter', 'skradderiforbundet' ),
        'search_items'       => __( 'Search Nyheter', 'skradderiforbundet' ),
        'parent_item_colon'  => __( 'Parent Nyheter:', 'skradderiforbundet' ),
        'not_found'          => __( 'No Nyheter found.', 'skradderiforbundet' ),
        'not_found_in_trash' => __( 'No Nyheter found in Trash.', 'skradderiforbundet' )
    );
    $bl_args = array(
        'labels'             => $bl_labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'nyheter' ),
        'taxonomies'         => array( 'news_cat'),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt' )
    );
    register_post_type( 'sk_news', $bl_args );
}
add_action( 'init', 'create_news_taxonomies', 0 );
function create_news_taxonomies() {
    $news_cat_labels = array(
        'name'              => _x( 'Kategori', 'taxonomy general name', 'skradderiforbundet' ),
        'singular_name'     => _x( 'Kategori', 'taxonomy singular name', 'skradderiforbundet' ),
        'search_items'      => __( 'Search Kategori', 'skradderiforbundet' ),
        'all_items'         => __( 'All Kategori', 'skradderiforbundet' ),
        'parent_item'       => __( 'Parent Kategori', 'skradderiforbundet' ),
        'parent_item_colon' => __( 'Parent Kategori:', 'skradderiforbundet' ),
        'edit_item'         => __( 'Edit Kategori', 'skradderiforbundet' ),
        'update_item'       => __( 'Update Kategori', 'skradderiforbundet' ),
        'add_new_item'      => __( 'Lägg till ny Kategori', 'skradderiforbundet' ),
        'new_item_name'     => __( 'New Kategori Name', 'skradderiforbundet' ),
        'menu_name'         => __( 'Kategori', 'skradderiforbundet' ),
    );
    $news_cat_args = array(
        'hierarchical'      => true,
        'labels'            => $news_cat_labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'news-cat' ),
    );
    register_taxonomy( 'news_cat', array( 'sk_news' ), $news_cat_args );
    
}
add_action('init','add_categories_to_news');
function add_categories_to_news(){
    register_taxonomy_for_object_type('news_cat', 'sk_news');
}