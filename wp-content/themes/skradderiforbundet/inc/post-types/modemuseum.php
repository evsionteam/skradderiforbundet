<?php
add_action( 'init', 'ev_modemuseum_init' );
function ev_modemuseum_init() {

    $modemuseum_labels = array(
        'name'               => _x( 'Modemuseum', 'post type general name', 'ev-intranet' ),
        'singular_name'      => _x( 'Modemuseum', 'post type singular name', 'ev-intranet' ),
        'menu_name'          => _x( 'Modemuseum', 'admin menu', 'ev-intranet' ),
        'name_admin_bar'     => _x( 'Modemuseum', 'add new on admin bar', 'ev-intranet' ),
        'add_new'            => _x( 'Lägg till nytt', 'Modemuseum', 'ev-intranet' ),
        'add_new_item'       => __( 'Lägg till nytt Modemuseum', 'ev-intranet' ),
        'new_item'           => __( 'New Modemuseum', 'ev-intranet' ),
        'edit_item'          => __( 'Edit Modemuseum', 'ev-intranet' ),
        'view_item'          => __( 'View Modemuseum', 'ev-intranet' ),
        'all_items'          => __( 'Alla inlägg', 'ev-intranet' ),
        'search_items'       => __( 'Search Modemuseum', 'ev-intranet' ),
        'parent_item_colon'  => __( 'Parent Modemuseum:', 'ev-intranet' ),
        'not_found'          => __( 'No Modemuseum found.', 'ev-intranet' ),
        'not_found_in_trash' => __( 'No Modemuseum found in Trash.', 'ev-intranet' )
    );

    $modemuseum_args = array(
        'labels'             => $modemuseum_labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'modemuseum' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'menu_icon'          => 'dashicons-tickets-alt',
        'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt','author' )
    );

    register_post_type( 'ev_modemuseum', $modemuseum_args );
}