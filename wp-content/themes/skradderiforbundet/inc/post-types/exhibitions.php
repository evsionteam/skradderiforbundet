<?php
add_action( 'init', 'sk_exhibitions_init' );
function sk_exhibitions_init() {

    $exhibitions_labels = array(
        'name'               => _x( 'Utställningar', 'post type general name', 'skradderiforbundet' ),
        'singular_name'      => _x( 'Utställningar', 'post type singular name', 'skradderiforbundet' ),
        'menu_name'          => _x( 'Utställningar', 'admin menu', 'skradderiforbundet' ),
        'name_admin_bar'     => _x( 'Utställningar', 'add new on admin bar', 'skradderiforbundet' ),
        'add_new'            => _x( 'Lägg till ny', 'Utställningar', 'skradderiforbundet' ),
        'add_new_item'       => __( 'Lägg till ny Utställningar', 'skradderiforbundet' ),
        'new_item'           => __( 'New Utställningar', 'skradderiforbundet' ),
        'edit_item'          => __( 'Edit Utställningar', 'skradderiforbundet' ),
        'view_item'          => __( 'View Utställningar', 'skradderiforbundet' ),
        'all_items'          => __( 'Alla utställningar', 'skradderiforbundet' ),
        'search_items'       => __( 'Search Utställningar', 'skradderiforbundet' ),
        'parent_item_colon'  => __( 'Parent Utställningar:', 'skradderiforbundet' ),
        'not_found'          => __( 'No Utställningar found.', 'skradderiforbundet' ),
        'not_found_in_trash' => __( 'No Utställningar found in Trash.', 'skradderiforbundet' )
    );

    $exhibitions_args = array(
        'labels'             => $exhibitions_labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'utstallningar' ),
        'taxonomies'         => array( 'exhibition_cat'),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'menu_icon'          => 'dashicons-groups',
        'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt','author' )
    );

    register_post_type( 'ev_exhibitions', $exhibitions_args );
}

add_action( 'init', 'create_exhibition_taxonomies', 0 );
function create_exhibition_taxonomies() {
    $exhibition_cat_labels = array(
        'name'              => _x( 'Kategorier', 'taxonomy general name', 'skradderiforbundet' ),
        'singular_name'     => _x( 'Kategorier', 'taxonomy singular name', 'skradderiforbundet' ),
        'search_items'      => __( 'Search Kategorier', 'skradderiforbundet' ),
        'all_items'         => __( 'All Kategorier', 'skradderiforbundet' ),
        'parent_item'       => __( 'Parent Kategorier', 'skradderiforbundet' ),
        'parent_item_colon' => __( 'Parent Kategorier:', 'skradderiforbundet' ),
        'edit_item'         => __( 'Edit Kategorier', 'skradderiforbundet' ),
        'update_item'       => __( 'Update Kategorier', 'skradderiforbundet' ),
        'add_new_item'      => __( 'Lägg till ny Kategorier', 'skradderiforbundet' ),
        'new_item_name'     => __( 'New Kategorier Name', 'skradderiforbundet' ),
        'menu_name'         => __( 'Kategorier', 'skradderiforbundet' ),
    );
    $exhibition_cat_args = array(
        'hierarchical'      => true,
        'labels'            => $exhibition_cat_labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'exhibition-cat' ),
    );
    register_taxonomy( 'exhibition_cat', array( 'ev_exhibitions' ), $exhibition_cat_args );

}
add_action('init','add_categories_to_exhibition');
function add_categories_to_exhibition(){
    register_taxonomy_for_object_type('exhibition_cat', 'ev_exhibitions');
}
