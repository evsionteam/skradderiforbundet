<?php
add_action( 'init', 'sk_world_congress_init' );
function sk_world_congress_init() {

    $world_congress_labels = array(
        'name'               => _x( 'Världskongressen', 'post type general name', 'skradderiforbundet' ),
        'singular_name'      => _x( 'Världskongressen', 'post type singular name', 'skradderiforbundet' ),
        'menu_name'          => _x( 'Världskongressen', 'admin menu', 'skradderiforbundet' ),
        'name_admin_bar'     => _x( 'Världskongressen', 'add new on admin bar', 'skradderiforbundet' ),
        'add_new'            => _x( 'Add New', 'Världskongressen', 'skradderiforbundet' ),
        'add_new_item'       => __( 'Add New Världskongressen', 'skradderiforbundet' ),
        'new_item'           => __( 'New Världskongressen', 'skradderiforbundet' ),
        'edit_item'          => __( 'Edit Världskongressen', 'skradderiforbundet' ),
        'view_item'          => __( 'View Världskongressen', 'skradderiforbundet' ),
        'all_items'          => __( 'All Världskongressen', 'skradderiforbundet' ),
        'search_items'       => __( 'Search Världskongressen', 'skradderiforbundet' ),
        'parent_item_colon'  => __( 'Parent Världskongressen:', 'skradderiforbundet' ),
        'not_found'          => __( 'No Världskongressen found.', 'skradderiforbundet' ),
        'not_found_in_trash' => __( 'No Världskongressen found in Trash.', 'skradderiforbundet' )
    );

    $world_congress_args = array(
        'labels'             => $world_congress_labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'world-congress' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'menu_icon'          => 'dashicons-tickets-alt',
        'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt','author' )
    );

    register_post_type( 'ev_world_congress', $world_congress_args );
}
