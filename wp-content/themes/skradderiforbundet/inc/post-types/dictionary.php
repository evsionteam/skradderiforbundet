<?php
add_action( 'init', 'sk_dictionary_init' );
function sk_dictionary_init() {
    
    $dictionary_labels = array(
        'name'               => _x( 'Ordlista', 'post type general name', 'skradderiforbundet' ),
        'singular_name'      => _x( 'Ord', 'post type singular name', 'skradderiforbundet' ),
        'menu_name'          => _x( 'Ordlista', 'admin menu', 'skradderiforbundet' ),
        'name_admin_bar'     => _x( 'Ord', 'add new on admin bar', 'skradderiforbundet' ),
        'add_new'            => _x( 'Lägg till nytt', 'Ord', 'skradderiforbundet' ),
        'add_new_item'       => __( 'Lägg till nytt Ord', 'skradderiforbundet' ),
        'new_item'           => __( 'New Ord', 'skradderiforbundet' ),
        'edit_item'          => __( 'Edit Ord', 'skradderiforbundet' ),
        'view_item'          => __( 'View Ord', 'skradderiforbundet' ),
        'all_items'          => __( 'Alla ord', 'skradderiforbundet' ),
        'search_items'       => __( 'Search Ordlista', 'skradderiforbundet' ),
        'parent_item_colon'  => __( 'Parent Ordlista:', 'skradderiforbundet' ),
        'not_found'          => __( 'No Ordlista found.', 'skradderiforbundet' ),
        'not_found_in_trash' => __( 'No Ordlista found in Trash.', 'skradderiforbundet' )
    );

    $dictionary_args = array(
        'labels'             => $dictionary_labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'dictionary' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'editor')
    );

    register_post_type( 'sk_dictionary', $dictionary_args );
}


add_action('save_post','sk_dictionary_post_save');
function sk_dictionary_post_save($post_id){
    if( isset($_POST['post_title']) && !empty($_POST['post_title'])){
        $firstCharacter = mb_substr($_POST['post_title'], 0, 1,'utf-8');
        if(!empty($firstCharacter)){

            switch ($firstCharacter) {
                case "Å":
                case "å":
                    update_post_meta($post_id,'word_index','#1');
                    break;
                case "Ä":
                case "ä":
                    update_post_meta($post_id,'word_index','#2');
                    break;
                case "Ö":
                case "ö":
                    update_post_meta($post_id,'word_index','#3');
                    break;
                default:
                    update_post_meta($post_id,'word_index',$firstCharacter);
            }

        }
    }
}