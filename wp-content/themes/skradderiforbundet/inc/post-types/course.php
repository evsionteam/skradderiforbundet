<?php
add_action( 'init', 'sk_courses_init' );
function sk_courses_init() {

    $courses_labels = array(
        'name'               => _x( 'Kurser', 'post type general name', 'skradderiforbundet' ),
        'singular_name'      => _x( 'Kurser', 'post type singular name', 'skradderiforbundet' ),
        'menu_name'          => _x( 'Kurser', 'admin menu', 'skradderiforbundet' ),
        'name_admin_bar'     => _x( 'Kurser', 'add new on admin bar', 'skradderiforbundet' ),
        'add_new'            => _x( 'Add New', 'Kurser', 'skradderiforbundet' ),
        'add_new_item'       => __( 'Add New Kurser', 'skradderiforbundet' ),
        'new_item'           => __( 'New Kurser', 'skradderiforbundet' ),
        'edit_item'          => __( 'Edit Kurser', 'skradderiforbundet' ),
        'view_item'          => __( 'View Kurser', 'skradderiforbundet' ),
        'all_items'          => __( 'All Kurser', 'skradderiforbundet' ),
        'search_items'       => __( 'Search Kurser', 'skradderiforbundet' ),
        'parent_item_colon'  => __( 'Parent Kurser:', 'skradderiforbundet' ),
        'not_found'          => __( 'No Kurser found.', 'skradderiforbundet' ),
        'not_found_in_trash' => __( 'No Kurser found in Trash.', 'skradderiforbundet' )
    );

    $courses_args = array(
        'labels'             => $courses_labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => false,
        'show_in_menu'       => false,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'kurser' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'menu_icon'          => 'dashicons-book-alt',
        'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt','author' )
    );

    register_post_type( 'ev_courses', $courses_args );
}