<?php
add_action( 'init', 'sk_home_slider_init' );
function sk_home_slider_init() {
    
    $slider_labels = array(
        'name'               => _x( 'Hero sliders', 'post type general name', 'skradderiforbundet' ),
        'singular_name'      => _x( 'Hero slider', 'post type singular name', 'skradderiforbundet' ),
        'menu_name'          => _x( 'Hero sliders', 'admin menu', 'skradderiforbundet' ),
        'name_admin_bar'     => _x( 'Hero slider', 'add new on admin bar', 'skradderiforbundet' ),
        'add_new'            => _x( 'Lägg till ny', 'Slider', 'skradderiforbundet' ),
        'add_new_item'       => __( 'Lägg till ny Slider', 'skradderiforbundet' ),
        'new_item'           => __( 'New Slider', 'skradderiforbundet' ),
        'edit_item'          => __( 'Edit Slider', 'skradderiforbundet' ),
        'view_item'          => __( 'View Slider', 'skradderiforbundet' ),
        'all_items'          => __( 'Alla sliders', 'skradderiforbundet' ),
        'search_items'       => __( 'Search Sliders', 'skradderiforbundet' ),
        'parent_item_colon'  => __( 'Parent Sliders:', 'skradderiforbundet' ),
        'not_found'          => __( 'No Sliders found.', 'skradderiforbundet' ),
        'not_found_in_trash' => __( 'No Sliders found in Trash.', 'skradderiforbundet' )
    );

    $slider_args = array(
        'labels'             => $slider_labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'home-slider' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'menu_icon'         => 'dashicons-slides',
        'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt' )
    );

    register_post_type( 'sk_home_slider', $slider_args );
}
