<?php
add_action( 'init', 'sk_posts_types_init_logo' );
function sk_posts_types_init_logo() {
    
 $logo_slider_labels = array(
        'name'               => _x( 'Annonsörer logo', 'post type general name', 'skradderiforbundet' ),
        'singular_name'      => _x( 'Annonsörer logo', 'post type singular name', 'skradderiforbundet' ),
        'menu_name'          => _x( 'Annonsörer logo', 'admin menu', 'skradderiforbundet' ),
        'name_admin_bar'     => _x( 'Annonsörer logo', 'add new on admin bar', 'skradderiforbundet' ),
        'add_new'            => _x( 'Lägg till ny', 'logo', 'skradderiforbundet' ),
        'add_new_item'       => __( 'Lägg till ny Logo', 'skradderiforbundet' ),
        'new_item'           => __( 'New Logo', 'skradderiforbundet' ),
        'edit_item'          => __( 'Edit Logo', 'skradderiforbundet' ),
        'view_item'          => __( 'View Logo', 'skradderiforbundet' ),
        'all_items'          => __( 'Alla logos', 'skradderiforbundet' ),
        'search_items'       => __( 'Search Logos', 'skradderiforbundet' ),
        'parent_item_colon'  => __( 'Parent Logos:', 'skradderiforbundet' ),
        'not_found'          => __( 'No Logos found.', 'skradderiforbundet' ),
        'not_found_in_trash' => __( 'No Logos found in Trash.', 'skradderiforbundet' )
    );

    $logo_slider_args = array(
        'labels'             => $logo_slider_labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'logo' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'menu_icon'         => 'dashicons-format-image',
        'supports'           => array( 'title', 'thumbnail' )
    );

    register_post_type( 'sk_logo_slider', $logo_slider_args );
}
