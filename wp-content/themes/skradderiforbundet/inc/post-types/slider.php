<?php
add_action( 'init', 'sk_posts_types_init' );
function sk_posts_types_init() {
    
    $slider_labels = array(
        'name'               => _x( 'Scroll sliders', 'post type general name', 'skradderiforbundet' ),
        'singular_name'      => _x( 'Scroll Slider', 'post type singular name', 'skradderiforbundet' ),
        'menu_name'          => _x( 'Scroll sliders', 'admin menu', 'skradderiforbundet' ),
        'name_admin_bar'     => _x( 'Scroll Slider', 'add new on admin bar', 'skradderiforbundet' ),
        'add_new'            => _x( 'Lägg till ny', 'Scroll Slider', 'skradderiforbundet' ),
        'add_new_item'       => __( 'Lägg till ny Scroll Slider', 'skradderiforbundet' ),
        'new_item'           => __( 'New Scroll Slider', 'skradderiforbundet' ),
        'edit_item'          => __( 'Edit Scroll Slider', 'skradderiforbundet' ),
        'view_item'          => __( 'View Scroll Slider', 'skradderiforbundet' ),
        'all_items'          => __( 'Alla Scroll Sliders', 'skradderiforbundet' ),
        'search_items'       => __( 'Search Scroll sliders', 'skradderiforbundet' ),
        'parent_item_colon'  => __( 'Parent Scroll sliders:', 'skradderiforbundet' ),
        'not_found'          => __( 'No Scroll sliders found.', 'skradderiforbundet' ),
        'not_found_in_trash' => __( 'No Scroll sliders found in Trash.', 'skradderiforbundet' )
    );

    $slider_args = array(
        'labels'             => $slider_labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'slider' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'menu_icon'         => 'dashicons-slides',
        'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt' )
    );

    register_post_type( 'sk_slider', $slider_args );
}
