<?php

/*email verification for new memeber*/
function sk_new_user_notification($user_id){
    $user = get_userdata( $user_id );
    /*Check for user role. Only modified for member role*/
    if(in_array('member',$user->roles)){

        $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);

        $ma_key = wp_generate_password( 20, false );
        $aa_key = wp_hash($user->data->user_email);


        /*add the activation key in meta value*/
        add_user_meta($user_id,'member_activation_key',$ma_key);
        add_user_meta($user_id,'admin_activation_key',$aa_key);
        /**/

        global $sk_option;

        $mail_template = file_get_contents( __DIR__.'/email-template/member-verification-email.html' );

        /*Logo*/
        $logo = $site_logo = $org_name = '';
        $org_id =  get_user_meta($user_id,'_organization_id',true);
        if(!empty($org_id)){
            $logo = get_post_meta($org_id,'organization_logo',true);
            $org_name = get_the_title($org_id);
        }

        /*site logo*/
        if( isset($sk_option['logo_image']) && !empty($sk_option['logo_image']) ){
            switch_to_blog(1);
            $temp = wp_get_attachment_image_src($sk_option['logo_image']);
            if(!empty($temp)){
                $site_logo = $temp[0];
                if(empty($logo)){
                    $logo = $temp[0];
                }
            }
            restore_current_blog();
        }
        /**/

        $logo = '<img editable="true" src="'.$logo.'" width="120" alt="" border="0" mc:edit="40">';
        $site_logo = '<img src="'.$site_logo.'" alt="site-logo" style="border-radius:50%;display:block;margin:0;padding:0;" width="50" class="image_target">';
        /**/

        $mr_header_text = isset($sk_option['mr_email_header']) ? wpautop($sk_option['mr_email_header']) : '';
        $mr_header_text = str_replace('@username@', $user->data->display_name, $mr_header_text);

        $mr_body_text = isset($sk_option['mr_email_content']) ? wpautop($sk_option['mr_email_content']) : '';
        $mr_body_text = str_replace('@verification_link@', "<a style='color:#000000;font-weight:bold' href='".network_site_url("/verify-account/?action=mr&key=$ma_key")."'>".network_site_url("/verify-account/?action=mr&key=$ma_key")."</a>", $mr_body_text);

        $mr_footer_text = isset($sk_option['mr_email_footer']) ? wpautop($sk_option['mr_email_footer']) : '';

        $mr_contact_info = get_field('contact_info',$org_id);
        $mr_fb_link = get_field('facebook_link',$org_id);
        $mr_twitter_link = get_field('twitter_link',$org_id);
        $mr_instagram_link = get_field('instagram_link',$org_id);

        $mr_body_text = str_replace('@fb_link@', "<a style='color:#000000;font-weight:bold' href='".$mr_fb_link."'>".__('Facebook','skradderiforbundet')."</a>", $mr_body_text);
        $mr_body_text = str_replace('@insta_link@', "<a style='color:#000000;font-weight:bold' href='".$mr_instagram_link."'>".__('Instagram','skradderiforbundet')."</a>", $mr_body_text);

        $mail_template = str_replace('@logo@', $logo, $mail_template);
        $mail_template = str_replace('@organization_name@', $org_name, $mail_template);
        $mail_template = str_replace('@header_text@', $mr_header_text, $mail_template);
        $mail_template = str_replace('@body_text@', $mr_body_text, $mail_template);
        $mail_template = str_replace('@site_logo@', $site_logo, $mail_template);
        $mail_template = str_replace('@footer_text@', $mr_footer_text, $mail_template);

        $fb_icon = get_template_directory_uri().'/assets/src/img/facebook.png';
        $twitter_icon = get_template_directory_uri().'/assets/src/img/twitter.png';
        $insta_icon = get_template_directory_uri().'/assets/src/img/instagram.png';

        $mail_template = str_replace('@contact_info@', $mr_contact_info, $mail_template);
        $mail_template = str_replace('@fb_link@', $mr_fb_link, $mail_template);
        $mail_template = str_replace('@fb_icon@', $fb_icon, $mail_template);
        $mail_template = str_replace('@twitter_link@', $mr_twitter_link, $mail_template);
        $mail_template = str_replace('@twitter_icon@', $twitter_icon, $mail_template);
        $mail_template = str_replace('@insta_link@', $mr_instagram_link, $mail_template);
        $mail_template = str_replace('@insta_icon@', $insta_icon, $mail_template);

        $upload_dir = wp_upload_dir();
        $pdf = md5($user->data->user_email);
        $attachment_path = $upload_dir['basedir'].'/users/'.$pdf.'.pdf';

        $attachments = array($attachment_path);

        $headers = array('Content-Type: text/html; charset=UTF-8','From: Skrädderiförbundet <skradderiforbundet@wordpress.org>');

        wp_mail($user->user_email, sprintf(__('[%s] Verification Email'), $blogname), $mail_template,$headers,$attachments);

    }
}
/**/

add_action( 'wp_ajax_register_member_profile', 'register_member_profile_callback' );
add_action( 'wp_ajax_nopriv_register_member_profile', 'register_member_profile_callback' );
function register_member_profile_callback() {

    $data = $params = array();
    $data['status'] = false;
    $empty_optional_values = false;

    check_ajax_referer( 'register_member', 'nonce' );

    parse_str($_POST['form_data'], $params);

    //sk_save_user_info_pdf($params);
    //die();

    if(isset($params['has_certificate'])){
        if( empty($params['date_of_certificate']) ){
            $data['message'] = __('Please fill all the required fields.','skradderiforbundet');
            $empty_optional_values = true;

        }
    }
    if(isset($params['has_master_craft'])){
        if( empty($params['master_year']) ){
            $data['message'] = __('Please fill all the required fields.','skradderiforbundet');
            $empty_optional_values = true;
        }
    }
    if(isset($params['has_business'])){
        if( empty($params['company_name']) || empty($params['company_phone']) || empty($params['company_start_date']) || empty($params['company_address']) || empty($params['company_post_number']) || empty($params['company_city']) || empty($params['company_nature']) || empty($params['company_email'])){
            $data['message'] = __('Please fill all the required fields.','skradderiforbundet');
            $empty_optional_values = true;
        }
    }
    if(isset($params['is_employee'])){
        if( empty($params['employee_company_name']) || empty($params['employee_company_phone']) || empty($params['employee_company_address']) || empty($params['employee_company_post_number'])|| empty($params['employee_company_city']) ){
            $data['message'] = __('Please fill all the required fields.','skradderiforbundet');
            $empty_optional_values = true;
        }
    }
    if(isset($params['has_school'])){
        if( empty($params['school_name']) || empty($params['school_phone']) || empty($params['school_email']) || empty($params['school_address'])|| empty($params['school_post_number']) || empty($params['school_place']) ){
            $data['message'] = __('Please fill all the required fields.','skradderiforbundet');
            $empty_optional_values = true;
        }
    }

    if( (true == $empty_optional_values) || ( empty($params['first_name']) || empty($params['last_name']) || empty($params['member_birthdate']) || empty($params['email']) || empty($params['username']) || empty($params['member_profession']) || empty($params['member_home_address']) || empty($params['member_post_number']) || empty($params['member_place']) || empty($params['member_country']) || empty($params['member_home_phone']) || empty($params['member_organization']) ) ){
        $data['message'] = __('Please fill all the required fields.','skradderiforbundet');
    }else{
        $username = strtolower($params['username']);
        if(username_exists($username) || !validate_username($username)){
            $data['message'] = __('Username already exist or not valid.','skradderiforbundet');
        }else{
            if( !is_email( $params['email'] ) ) {
                $data['message'] = __('Email address is not valid.','skradderiforbundet');
            }elseif ( email_exists( $params['email'] ) || username_exists( $params['email'] ) ) {
                $data['message'] = __('Email address already exist.','skradderiforbundet');
            }else{

                $user_data = array(
                    'user_login'	    => 	$username,
                    'user_email' 	    => 	$params['email'],
                    'first_name' 	    => 	$params['first_name'],
                    'last_name' 	    => 	$params['last_name'],
                    'user_pass' 	    => 	wp_generate_password(),
                    'user_registered'	=>  date('Y-m-d H:i:s'),
                    'role'				=> 'member'
                );

                /*switch to medlem site and insert new user*/
                switch_to_blog(2);

                $user_id = wp_insert_user( $user_data );
                if(!empty($user_id)){

                    /*only insert the allowed values in meta*/
                    $allowed = array(
                        'member_birthdate',
                        'member_profession',
                        'member_home_address',
                        'member_post_number',
                        'member_place',
                        'member_country',
                        'member_home_phone',
                        'company_name',
                        'company_phone',
                        'company_start_date',
                        'company_address',
                        'company_post_number',
                        'company_city',
                        'company_nature',
                        'company_email',
                        'company_website',
                        'employee_company_name',
                        'employee_company_phone',
                        'employee_company_address',
                        'employee_company_post_number',
                        'employee_company_city',
                        'school_name',
                        'school_phone',
                        'school_email',
                        'school_address',
                        'school_post_number',
                        'school_place',
                        'other_information',
                    );


                    /*insert user metadata*/
                    $filtered_data = array_intersect_key($params, array_flip($allowed));
                    foreach( $filtered_data as $key => $value ){
                        add_user_meta( $user_id, $key, $value);
                    }
                    /**/

                    /*set organization details*/
                    if(isset($params['member_organization']) && !empty($params['member_organization'])){
                        $local_admin = get_post_meta($params['member_organization'],'_organization_admin',true);
                        if(!empty($local_admin)){
                            add_user_meta($user_id,'_organization_id',$params['member_organization']);
                            add_user_meta($user_id,'_local_admin',$local_admin);
                        }
                    }
                    /**/

                    /*set tax and vat status*/
                    if(isset($params['company_f_tax'])){
                        add_user_meta($user_id,'company_f_tax','yes');
                    }else{
                        add_user_meta($user_id,'company_f_tax','no');
                    }
                    if(isset($params['company_vat'])){
                        add_user_meta($user_id,'company_vat','yes');
                    }else{
                        add_user_meta($user_id,'company_vat','no');
                    }
                    /**/


                    /*set certificate details*/
                    if( isset($params['has_certificate']) ){
                        $certificate_value = array();
                        if( isset($params['certificate_in_man']) ){
                            $certificate_value[] = 'him';
                        }
                        if( isset($params['certificate_in_woman']) ){
                            $certificate_value[] = 'her';
                        }
                        if( isset($params['certificate_in_dress']) ){
                            $certificate_value[] = 'dress';
                        }
                        if(!empty($certificate_value)){
                            add_user_meta($user_id,'certificate_in',$certificate_value);
                            add_user_meta($user_id,'date_of_certificate',$params['date_of_certificate']);
                        }
                    }
                    /**/

                    /*set master details*/
                    if( isset($params['has_master_craft']) ){
                        $master_value = array();
                        if( isset($params['master_in_man']) ){
                            $master_value[] = 'him';
                        }
                        if( isset($params['master_in_woman']) ){
                            $master_value[] = 'her';
                        }
                        if( isset($params['master_in_dress']) ){
                            $master_value[] = 'dress';
                        }
                        if(!empty($master_value)){
                            add_user_meta($user_id,'master_in',$master_value);
                            add_user_meta($user_id,'master_year',$params['master_year']);
                        }
                    }
                    /**/

                    /*set user inactive status to true and approved status to false*/
                    add_user_meta($user_id,'inactive_member',true);
                    add_user_meta($user_id,'approved_member',false);
                    /**/

                    /*generate member pdf using user information and save it in users folder*/
                    sk_save_user_info_pdf($params);
                    /**/

                    /*send notification email*/
                    sk_new_user_notification($user_id);
                    /**/

                    $data['status'] = true;
                    $data['message'] = __('Tack för din ansökan.<br/> Vi har skickat ett verifikations-mail till dig. <br/>Fullfölj ansökan genom att klicka på länken i mailet. <br/>Vi behandlar din ansökan först när det är verifierat.','skradderiforbundet');
                }else{
                    $data['message'] = __('Cannot register your account. Please try again.','skradderiforbundet');
                }

                /*restore to current blog */
                restore_current_blog();
            }
        }
    }
    echo json_encode($data);
    die();
}
