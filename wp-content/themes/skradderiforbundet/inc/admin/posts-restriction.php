<?php

/*show only current author post for local admin and member also remove post count*/
function ev_posts_for_current_author($query) {
    global $pagenow,$current_user;
    if( 'edit.php' != $pagenow || !$query->is_admin || current_user_can( 'administrator' ) )
        return $query;

    if( current_user_can( 'local_admin' ) ) {
        $query->set('author', $current_user->ID );
        add_filter('views_edit-ev_exhibitions', 'sk_hide_post_counts');
        add_filter('views_edit-sk_news', 'sk_hide_post_counts');
    }
    return $query;
}
add_filter('pre_get_posts', 'ev_posts_for_current_author');

/*hide post counts for local admin*/
function sk_hide_post_counts($views){
    $views = array();
    return $views;
}

/*only show current user attachments*/
add_filter( 'ajax_query_attachments_args', 'show_current_user_attachments', 10, 1 );
function show_current_user_attachments( $query = array() ) {
    $user_id = get_current_user_id();
    if( $user_id ) {
        $user_data = get_userdata($user_id);
        if(!empty($user_data)){
            if(in_array('administrator',$user_data->roles)){
                return $query;
            }
            $query['author'] = $user_id;
        }
        $query['author'] = $user_id;
    }
    return $query;
}

