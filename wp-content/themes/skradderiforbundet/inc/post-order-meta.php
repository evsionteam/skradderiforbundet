<?php

if(is_user_logged_in()){
    global $current_user;
    if(!in_array('administrator',$current_user->roles)){
        return;
    }
}

/*Display post order in admin column*/
$post_types = array( 'sk_news','ev_exhibitions' );
foreach( $post_types as $post_type ){

    /*Adding custom column*/
    add_filter( 'manage_'.$post_type.'_posts_columns' , 'sk_add_post_order_column_head' );
    add_action( 'manage_'.$post_type.'_posts_custom_column' , 'sk_add_post_order_column_content', 10, 2 );
    /**/

    /*Making the custom column sortable*/
    add_filter( 'manage_edit-'.$post_type.'_sortable_columns', 'ev_sortable_post_order_post_column' );
    add_action( 'pre_get_posts', 'ev_sort_ft_post_orderby' );
    /**/
}
function sk_add_post_order_column_head( $columns ){
    $columns['sk_post_order_column'] = __( 'Order', 'skradderiforbundet' );
    return $columns;
}
function sk_add_post_order_column_content( $column, $post_id ){

    if ( 'sk_post_order_column' != $column ){
        return;
    }
    $order = get_post_meta( $post_id, 'sk_post_order', true );
    if( 999999 == $order ){
        $order = 'N/A';
    }
    echo $order;
}

function ev_sortable_post_order_post_column($sortable_columns ){
    $sortable_columns[ 'sk_post_order_column' ] = 'sk_post_order';
    return $sortable_columns;
}
function ev_sort_ft_post_orderby( $query ) {

    if( ! is_admin() ){
        return;
    }

    if ( $query->is_main_query() && ( $orderby = $query->get( 'orderby' ) ) ) {
        if( 'sk_post_order' == $orderby ) {
            $query->set('meta_key','sk_post_order');
            $query->set( 'orderby', 'meta_value_num' );
        }
    }

}
/**/

