<?php
function sk_add_expiry_date_metabox() {
    $post_types = array('sk_news','ev_exhibitions');
    foreach($post_types as $post_type){
        add_meta_box(
            'sk_expiry_date_metabox',
            __( 'Post Settings', 'skradderiforbundet'),
            'sk_expiry_date_metabox_callback',
            $post_type,
            'side',
            'high'
        );
    }
}
add_action( 'add_meta_boxes', 'sk_add_expiry_date_metabox' );

function sk_expiry_date_metabox_callback($post){
    wp_nonce_field( basename( __FILE__ ), 'sk_exp_nonce' );
    $expire_date = get_post_meta($post->ID,'expires_in',true);
    $sk_post_order = get_post_meta($post->ID,'sk_post_order',true);
    if( 999999 == $sk_post_order){
        $sk_post_order = '';
    }
    ?>
    <div>
        <p><label for="sk_post_expiry_date"><?php _e('Avpublicering datum', 'skradderiforbundet' ); ?></label></p>
        <input type="text" id="sk_post_expiry_date" class="datepicker" name="sk_post_expiry_date" value="<?php echo $expire_date;?>" / >
    </div>
    <div>
        <p><label for="sk-post-order"><?php _e('Order', 'skradderiforbundet' ); ?></label></p>
        <input type="text" id="sk-post-order" name="sk_post_order" value="<?php echo $sk_post_order;?>"/>
    </div>

<?php
}

function sk_save_expiry_date_meta( $post_id ) {

    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ 'sk_exp_nonce' ] ) && wp_verify_nonce( $_POST[ 'sk_exp_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
    if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
        return;
    }

    if ( isset( $_POST['sk_post_expiry_date'] ) ) {
        update_post_meta( $post_id, 'expires_in', $_POST['sk_post_expiry_date'] );
    }

    if ( isset( $_POST['sk_post_order'] ) ) {
        if(!empty($_POST['sk_post_order'])){
            update_post_meta( $post_id, 'sk_post_order', (int)$_POST['sk_post_order'] );
        }else{
            /*Storing a huge number so that the key exists on each post with higher value*/
            update_post_meta( $post_id, 'sk_post_order', 999999 );
        }
    }

}
add_action( 'save_post', 'sk_save_expiry_date_meta' );