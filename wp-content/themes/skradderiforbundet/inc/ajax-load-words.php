<?php
function sk_load_words(){

    $output = array();

    $args = array(
        'post_type' => 'sk_dictionary',
        'posts_per_page' => '-1',
        'posts_status' => 'publish',
        /*'orderby' => 'name',
        'order' => 'ASC',*/
    );

    $alphabet = isset($_POST['alphabet']) ? $_POST['alphabet'] : '';
    if(!empty($alphabet)){

        switch ($alphabet) {
            case "Å":
            case "å":
                $alphabet = '#1';
                break;
            case "Ä":
            case "ä":
                $alphabet = '#2';
                break;
            case "Ö":
            case "ö":
                $alphabet = '#3';
                break;
        }

        $args['meta_query'] = array(
            array(
                'key' => 'word_index',
                'value' => $alphabet,
            )
        );
    }

    $search_text = isset($_POST['search_text']) ? $_POST['search_text'] : '';
    if(!empty($search_text)){
        $args['s'] = $search_text;
    }

    $query = new WP_Query($args);
    if (($query->found_posts)):
        while ($query->have_posts()): $query->the_post();
            ob_start();
            ?>
            <div class="sk-dictionary-word">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="sk-word-name dic-cm-pad">
                                <?php the_title();?>
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <div class="sk-word-meaning dic-cm-pad">
                                <?php echo get_the_content();?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            $output[] = ob_get_clean();
        endwhile;
    wp_send_json_success($output);
    else:
        wp_send_json_error();
    endif;
    exit;
}
add_action('wp_ajax_nopriv_sk_load_words', 'sk_load_words');
add_action('wp_ajax_sk_load_words', 'sk_load_words');