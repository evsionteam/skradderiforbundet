<?php 
add_action( 'add_meta_boxes', 'sk_meta_box_about' );
function sk_meta_box_about()
{   
    $page_template = get_post_meta(get_the_ID(), '_wp_page_template', true);
    if($page_template == 'page-templates/template-about-us.php' ){
        add_meta_box( 'sk-meta-education', 'Education Info', 'sk_meta_box_about_callback', 'page', 'normal', 'high' );
    }
}

function sk_meta_box_about_callback( $post )
{
    $values1 = get_post_custom( $post->ID );
    $sk_about_wed_title = isset( $values1['sk_about_wed_title'] ) ? $values1['sk_about_wed_title'][0] : '';
    $sk_about_wed_content = isset( $values1['sk_about_wed_content'] ) ? $values1['sk_about_wed_content'][0] : '';
    $sk_about_purpose_title = isset( $values1['sk_about_purpose_title'] ) ? $values1['sk_about_purpose_title'][0] : '';
    $sk_about_purpose_content = isset( $values1['sk_about_purpose_content'] ) ? $values1['sk_about_purpose_content'][0] : '';
    $sk_about_history_title = isset( $values1['sk_about_history_title'] ) ? $values1['sk_about_history_title'][0] : '';
    $sk_about_history_content = isset( $values1['sk_about_history_content'] ) ? $values1['sk_about_history_content'][0] : '';
    

    wp_nonce_field( 'sk_about_wed_title', 'my_sk_about_wed_title' );
    wp_nonce_field( 'sk_about_wed_content', 'my_sk_about_wed_content' );
    wp_nonce_field( 'sk_about_purpose_title', 'my_sk_about_purpose_title' );
    wp_nonce_field( 'sk_about_purpose_content', 'my_sk_about_purpose_content' );
    wp_nonce_field( 'sk_about_history_title', 'my_sk_about_history_title' );
    wp_nonce_field( 'sk_about_history_content', 'my_sk_about_history_content' );
    ?>
   
    <p><b><?php _e('Vad gör vi Title', 'skradderiforbundet'); ?></b></p>
    <input type="text" name="sk_about_wed_title" id="sk_about_wed_title" size="80" value="<?php echo $sk_about_wed_title; ?>"><br/>
    <p><b><?php _e('Vad gör vi Content', 'skradderiforbundet'); ?></b></p>
     <textarea rows="13" cols="130" name="sk_about_wed_content" id="sk_about_wed_content"><?php echo $sk_about_wed_content; ?></textarea><br/>
    <hr>


    <p><b><?php _e('Syfte Title', 'skradderiforbundet'); ?></b></p>
    <input type="text" name="sk_about_purpose_title" id="sk_about_purpose_title" size="80" value="<?php echo $sk_about_purpose_title; ?>"><br/>
    <p><b><?php _e('Syfte Content', 'skradderiforbundet'); ?></b></p>
     <textarea rows="13" cols="130" name="sk_about_purpose_content" id="sk_about_purpose_content"><?php echo $sk_about_purpose_content; ?></textarea><br/>
    <hr>


    <p><b><?php _e('Historik Title', 'skradderiforbundet'); ?></b></p>
    <input type="text" name="sk_about_history_title" id="sk_about_history_title" size="80" value="<?php echo $sk_about_history_title; ?>"><br/>
    <p><b><?php _e('Historik Content', 'skradderiforbundet'); ?></b></p>
     <textarea rows="13" cols="130" name="sk_about_history_content" id="sk_about_history_content"><?php echo $sk_about_history_content; ?></textarea><br/>
    <hr>


    <?php   
}

add_action( 'save_post', 'sk_meta_about_save' );
function sk_meta_about_save( $post_id )
{
    // Bail if we're doing an auto save
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

    // if our nonce isn't there, or we can't verify it, bail
    if( !isset( $_POST['my_sk_about_wed_title'] ) || !wp_verify_nonce( $_POST['my_sk_about_wed_title'], 'sk_about_wed_title' ) ) return;
    if( !isset( $_POST['my_sk_about_wed_content'] ) || !wp_verify_nonce( $_POST['my_sk_about_wed_content'], 'sk_about_wed_content' ) ) return;
    if( !isset( $_POST['my_sk_about_purpose_title'] ) || !wp_verify_nonce( $_POST['my_sk_about_purpose_title'], 'sk_about_purpose_title' ) ) return;
    if( !isset( $_POST['my_sk_about_purpose_content'] ) || !wp_verify_nonce( $_POST['my_sk_about_purpose_content'], 'sk_about_purpose_content' ) ) return;
    if( !isset( $_POST['my_sk_about_history_title'] ) || !wp_verify_nonce( $_POST['my_sk_about_history_title'], 'sk_about_history_title' ) ) return;
    if( !isset( $_POST['my_sk_about_history_content'] ) || !wp_verify_nonce( $_POST['my_sk_about_history_content'], 'sk_about_history_content' ) ) return;

    // if our current user can't edit this post, bail
    if( !current_user_can( 'edit_post' ) ) return;

    // Probably a good idea to make sure your data is set
     if( isset( $_POST['sk_about_wed_title'] ) )
        update_post_meta( $post_id, 'sk_about_wed_title', $_POST['sk_about_wed_title'] );

    if( isset( $_POST['sk_about_wed_content'] ) )
        update_post_meta( $post_id, 'sk_about_wed_content', $_POST['sk_about_wed_content'] );

    if( isset( $_POST['sk_about_purpose_title'] ) )
        update_post_meta( $post_id, 'sk_about_purpose_title', $_POST['sk_about_purpose_title'] );

    if( isset( $_POST['sk_about_purpose_content'] ) )
        update_post_meta( $post_id, 'sk_about_purpose_content', $_POST['sk_about_purpose_content'] );

    if( isset( $_POST['sk_about_history_title'] ) )
        update_post_meta( $post_id, 'sk_about_history_title', $_POST['sk_about_history_title'] );

    if( isset( $_POST['sk_about_history_content'] ) )
        update_post_meta( $post_id, 'sk_about_history_content', $_POST['sk_about_history_content'] );
    

}