<?php
get_header(); ?>
    <div class="sk-c-double-section">
        <div class="container">
                <div id="primary" class="content-area">
                    <main id="main" class="site-main" role="main">
                        <?php
                        while (have_posts()) : the_post();
                            ?>
                            <article id="post-<?php the_ID(); ?>" class="sk-news">
                                <?php
                                $thumbnail = get_post_meta( get_the_ID(), '_thumbnail_url', true );
                                if (!empty($thumbnail)) {
                                    echo '<div class="sk-img" style="background-image:url(' . $thumbnail . ');"></div>';
                                }
                                ?>
                            <div class="col-md-12">
                                <div class="sk-title-n-des">
                                    <h2 class="sk-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                    </h2>
                                    <span class="sk-date"><i class="fa fa-calendar"></i><?php echo get_the_date(' j M'); ?></span>
                                        <div class="row">
                                            <div class="col-md-8 col-offset-md-2">
                                                <div class="sk-description">
                                                    <?php the_content(); ?>
                                                    <div class="sk-couse-content">
                                                        <table>
                                                            <tbody>
                                                                <tr>
                                                                    <th><?php _e('Startdatum','skradderiforbundet')?></th>
                                                                    <td><?php echo get_post_meta(get_the_ID(),'_course_start_date',true);?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th><?php _e('Slutdatum','skradderiforbundet')?></th>
                                                                    <td><?php echo get_post_meta(get_the_ID(),'_course_end_date',true);?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th><?php _e('Time:','skradderiforbundet')?></th>
                                                                    <td><?php echo get_post_meta(get_the_ID(),'course_time',true);?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th><?php _e('Kurshållare','skradderiforbundet')?></th>
                                                                    <td><?php echo get_post_meta(get_the_ID(),'_course_holder',true);?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th><?php _e('Plats','skradderiforbundet')?></th>
                                                                    <td><?php echo get_post_meta(get_the_ID(),'_training_place',true);?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th><?php _e('Website','skradderiforbundet')?></th>
                                                                    <td><a href="<?php echo esc_url(get_post_meta(get_the_ID(),'_course_link',true));?>" target="_blank"><?php echo get_post_meta(get_the_ID(),'_course_link',true);?></a></td>
                                                                </tr>
                                                                <tr>
                                                                    <th><?php _e('Kontaktinformation','skradderiforbundet')?></th>
                                                                    <td><?php echo get_post_meta(get_the_ID(),'_course_contact_info',true);?></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                <!-- .sk-title-n-des -->
                            </div>
                            </article><!-- #post-## -->
                            <?php
                            the_post_navigation();
                        endwhile; // End of the loop.
                        ?>
                    </main>
                    <!-- #main -->
                <!-- #primary -->
            </div>
        </div>
    </div>
<?php
get_footer();
