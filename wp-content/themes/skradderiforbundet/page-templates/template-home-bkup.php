<?php
/*Template Name: Home Page*/
get_header();
global $sk_option;
?>


<div class="slider">
    <div class="slider__item">
        <div class="slider__img" style="background-image: url(<?php the_post_thumbnail_url();?>);"></div>
        <div class="text__container">
            <h1>Moooooon!</h1>
            <p>
                Subtext Nullam quis risus eget urna mollis ornare vel eu leo.
            </p>
            <p style="color:#F6D258;"><b>Made in Sweden</b></p>
        </div>
    </div>
    <div class="slider__item">
        <div class="slider__img" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
        <div class="text__container">
            <h1>Molly does the party</h1>
            <p>
                Subtext Nullam quis risus eget urna mollis ornare vel eu leo.
            </p>
            <p style="color:#F6D258;"><b>I do highligt things</b></p>
        </div>
    </div>
    <div class="slider__item">
        <div class="slider__img" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
        <div class="text__container">
            <h1>Lord Monty</h1>
            <p>
                Subtext Nullam quis risus eget urna mollis ornare vel eu leo.
            </p>
            <p style="color:#F6D258;"><b>I'm a special message</b></p>
        </div>
    </div>
</div>



<!---->
<div class="clearfix"></div>

<!--Slider-->
<?php
$args = array(
    'post_type' => 'sk_slider',
    'post_status' => 'publish',
    'posts_per_page' => -1,
);
$query = new WP_Query($args);
if($query->have_posts()):
?>
    <div class="sk-product-slider" id="sk-product-slider">
        <div class="container">
            <div class="row">
                <div id="main-sk-slider">
                    <div class="sk-slider-navigation">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/build/img/sk-slider-up-arrow.png" id="mr-prev" alt="sk-slider-up-arrow"/>
                        <div>Merriweather3</div>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/build/img/sk-slider-down-arrow.png" id="mr-next" alt="sk-slider-up-arrow"/>
                    </div>
                    <div class="mr-slider-inner">
                        <?php while ($query->have_posts()): $query->the_post(); ?>
                            <div class="item">
                                <div class="col-md-7 c-article col-sm-8">
                                    <div class="sk-title-des">
                                        <h1 class="sk-title"><?php the_title(); ?></h1>
                                        <div class="sk-description">
                                            <p><?php the_excerpt(); ?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-md-offset-1 col-sm-4 img">
                                    <?php if(has_post_thumbnail()):?>
                                        <img src="<?php the_post_thumbnail_url(); ?>" alt="product-image"/>
                                    <?php endif;?>
                                </div>
                            </div>
                        <?php endwhile; wp_reset_postdata(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif;?>
<!---->


<!--Video or Image banner-->
<?php
$homepage_img_or_vid = isset($sk_option['homepage_img_or_vid']) ? $sk_option['homepage_img_or_vid'] : '';
if(!empty($homepage_img_or_vid)){

    $file_src = wp_get_attachment_url($homepage_img_or_vid);
    $type = get_post_mime_type($homepage_img_or_vid);

    switch ($type) {
        case 'image/jpeg':
        case 'image/png':
        case 'image/gif':
            $file_type = 'image'; break;
        case 'video/mpeg':
        case 'video/mp4':
        case 'video/quicktime':
            $file_type = 'video'; break;
        default:
            $file_type = '';
    }

    if(!empty($file_type)){
        if($file_type == 'image'){
            ?>
            <div class="sk-home-img-banner" style="background-image: url(<?php echo $file_src; ?>)"></div>
            <?php
        }elseif($file_type == 'video'){
            ?>
            <div class="sk-video-ad hidden-xs" id="video">
                <div class="sk-control">
                    <img id="play-icon" src="<?php echo get_template_directory_uri(); ?>/assets/build/svg/play.svg" alt="sk-play-button"/>
                </div>
                <video width="100%" id="sk-video-ad">
                    <source src="<?php echo $file_src; ?>" type="video/mp4">
                </video>
            </div>
            <?php
        }
    }
}
?>
<!---->

<?php $args = array(
    'post_type' => array('sk_news','ev_courses','ev_exhibitions','ev_world_congress'),
    'posts_per_page' => 6,
    'posts_status' => 'publish',
);
$query = new WP_Query($args); ?>

    <div class="clearfix"></div>
    <div class="col-md-12">
        <div class="sk-news-wrapper sk-c-gap clearfix">
            <h1><?php _e('Nyheter', 'skradderiforbundet') ?></h1>

            <div class="sk-news-archive-wrapper">
                <div class="news-listing">
                    <?php
                    if ($query->have_posts()): ?>
                        <?php
                        $counter = 1;
                        while ($query->have_posts()) : $query->the_post();
                            get_template_part('template-parts/news-content', get_post_format());
                            if ($counter % 3 == 0)
                                echo '<div class="clearfix visible-md visible-lg"></div>';
                            if ($counter % 2 == 0)
                                echo '<div class="clearfix visible-xs visible-sm"></div>';
                            $counter++;
                        endwhile;
                        wp_reset_postdata();
                    else :
                        get_template_part('template-parts/news-content', 'none');
                    endif; ?>
                </div>
                <!-- End .col-md-12 -->
            </div>
        </div>
        <!-- sk-news-wrapper -->
    </div><!-- /.col-md-12 -->

    <div class="clearfix"></div>
    <div id="sk-load-more-news" class="sk-news-load-more loading-from-home-page">
        <div class="sk-arrow-n-text">
            <a href="#">
                <span><?php _e('Äldre Artiklar', 'skradderiforbundet') ?></span>
                <i class="fa fa-angle-down fa-3x"></i>
            </a>
      <span class="load-more-spinner">
        <i class="fa fa-spinner fa-spin"></i>
      </span>
        </div>
    </div><!-- End #sk-load-more-news -->

<!--Advertisers Logo-->
<?php
$args = array(
    'post_type' => 'sk_logo_slider',
    'post_status' => 'publish',
    'posts_per_page' => -1,
);
$advertisers_logo = new WP_Query($args);
if($advertisers_logo->have_posts()):
?>
    <div class="sk-advertiser-slder">
        <?php global $sk_option;
        $logo_img = $sk_option['logo_image'];
        $logo_image_src = wp_get_attachment_image_src($logo_img);
        ?>
        <div class="sk-advertiser-logo sk-c-gap">
            <div class="owl-carousel owl-theme">
                <?php
                while($advertisers_logo->have_posts()):$advertisers_logo->the_post();
                    if(has_post_thumbnail()):
                        $link = get_field('link');
                        if(empty($link)){
                            $link = "javascript:void(0)";
                        }
                        ?>
                        <a href="<?php echo $link;?>" target="_blank">
                            <img class="owl-lazy" src="<?php the_post_thumbnail_url();?>" data-src="<?php the_post_thumbnail_url();?>" alt="site-logo"/>
                        </a>
                    <?php
                    endif;
                endwhile;wp_reset_postdata();
                ?>
            </div>
        </div>
    </div>
<?php endif;?>
<!---->
<?php get_footer(); ?>