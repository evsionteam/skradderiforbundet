<?php
/* Template Name: Dictionary Page */
get_header();
while (have_posts()):the_post();
?>
<div class="sk-inner-page dictionary">
    <div class="sk-search-form sk-filter">
        <div class="col-md-12">
            <div class="sk-c-gap">
                <form autocomplete="off">
                    <div class="sk-search">
                        <div class="sk-form-group">
                            <input type="text" name="search" placeholder="<?php _e('Free Search','skradderiforbundet')?>" value="<?php ?>"/>
                        </div>
                    </div>
                </form>
                <div class="sk-alphabet-filter">
                    <div class="sk-alphabet-letter">
                        <?php
                        $alphabets = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','Å','Ä','Ö');
                        foreach($alphabets as $alphabet){
                            ?>
                            <a href="javascript:void(0)" data-alphabet="<?php echo $alphabet;?>">
                                <span><?php echo $alphabet;?></span>
                            </a>
                            <?php
                        }
                        ?>
                    </div>
                    <a href="javascript:void(0)" class="dic-show-all"><?php _e('Visa alla','skradderiforbundet')?></a>
                    <div class="fetch-words-loader" style="display: none">
                        <i class="fa fa-spinner fa-spin fa-2x"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="dictionary-words-wrapper">
        <?php
        $args = array(
            'post_type' => 'sk_dictionary',
            'posts_per_page' => '-1',
            'posts_status' => 'publish',
            /*'orderby' => 'name',
            'order' => 'ASC',*/
            /*'meta_query' => array(
                array(
                    'key' => 'word_index',
                    'value' => 'A',
                )
            )*/
        );
        $words = new WP_Query($args);
        if($words->have_posts()):
        while($words->have_posts()):$words->the_post();
            ?>
            <div class="sk-dictionary-word">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="sk-word-name dic-cm-pad">
                                <?php the_title();?>
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <div class="sk-word-meaning dic-cm-pad">
                                <?php echo get_the_content();?>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- sk-dictionary-word -->
        <?php endwhile; wp_reset_postdata(); endif?>
    </div>
</div>
<?php
endwhile;
get_footer();
