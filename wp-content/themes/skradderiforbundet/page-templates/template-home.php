<?php
/*Template Name: Home Page*/
get_header();
global $sk_option;
?>

    <!--Banner-->
<?php
$home_slider_args = array(
    'post_type' => 'sk_home_slider',
    'posts_per_page' => -1,
    'post_status' => 'publish'
);
$home_slider = new WP_Query($home_slider_args);
if($home_slider->have_posts()):
    ?>
    <div class="sk-main-banner slider">
        <?php while($home_slider->have_posts()):$home_slider->the_post();?>
            <div class="slider-item">
                <div class="slider-img" style="background-image: url('<?php the_post_thumbnail_url(); ?>');"></div>
                <div class="big-letter"><span><?php the_field('letter')?></span></div>
                <div class="slider-caption">
                    <h1><?php the_title(); ?></h1>
                    <span class="slider-sub-content"><?php echo get_the_content();?></span>
                    <?php
                    $link = get_field('custom_link');
                    $link_text = get_field('link_text');
                    if(!empty($link) && !empty($link_text)){
                        ?>
                        <a class="hightlight-text" href="<?php echo $link;?>" target="_blank"><?php echo $link_text;?></a>
                    <?php
                    }
                    ?>
                </div>
            </div>
        <?php endwhile;wp_reset_postdata();?>
        <a href="" class="sk-scroll-down" data-target=".sk-product-section" data-animation-duration="500">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/build/img/sk-down-arrow.png" alt="down-arrow"/>
        </a>
    </div>
<?php endif; ?>
<!---->
<div class="clearfix"></div>


<!--Slider-->
<?php
$slider_args = array(
    'post_type' => 'sk_slider',
    'posts_per_page' => -1,
    'post_status' => 'publish'
);
$sliders = new WP_Query($slider_args);
if($sliders->have_posts()):
    $counter = 1;
    ?>
    <div class="sk-product-section" data-found="<?php echo $sliders->found_posts;?>">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-8 col-md-offset-2">
                    <?php
                    while($sliders->have_posts()):$sliders->the_post();
                        if($counter % 2 != 0){
                            ?>
                            <div class="row">
                                <div class="col-sm-7">
                                    <div class="sk-product-detail scroll-text-<?php echo $counter;?>">
                                        <h2><?php the_title();?></h2>
                                        <div class="sk-product-description">
                                            <p class="padded-multi-line">
                                                <?php echo get_the_content();?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="sk-products-img img-right scroll-img-<?php echo $counter;?>">
                                        <img src="<?php the_post_thumbnail_url();?>" class="img-responsive" alt="product-image">
                                    </div>
                                </div>
                            </div>
                        <?php
                        }else{
                            ?>
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="sk-products-img img-left scroll-img-<?php echo $counter;?>">
                                        <img src="<?php the_post_thumbnail_url()?>" class="img-responsive" alt="product-image">
                                    </div>
                                </div>
                                <div class="col-sm-7">
                                    <div class="sk-product-detail scroll-text-<?php echo $counter;?>">
                                        <h2><?php the_title();?></h2>
                                        <div class="sk-product-description">
                                            <p class="padded-multi-line">
                                                <?php echo get_the_excerpt();?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php
                        }
                        ?>
                        <?php $counter++; endwhile; wp_reset_postdata();?>
                </div>
            </div>
        </div>
    </div>
<?php endif;?>
<!---->


<!--Video or Image banner-->
<?php
$homepage_img = isset($sk_option['homepage_img']) ? $sk_option['homepage_img'] : '';
$homepage_vid = isset($sk_option['homepage_vid']) ? $sk_option['homepage_vid'] : '';
if(!empty($homepage_vid)){
    $file_src = wp_get_attachment_url($homepage_vid);
    /*Display image  for ios*/
    $detect = new Mobile_Detect;
    if( $detect->isiOS() ){
        if(!empty($homepage_img)){
            $file_src = wp_get_attachment_url($homepage_img);
            ?>
            <div class="sk-home-img-banner" style="background-image: url(<?php echo $file_src; ?>)"></div>
            <?php
        }
    }else{
        ?>
        <div class="sk-video-ad" id="video">
            <div class="sk-control">
                <img id="play-icon" src="<?php echo get_template_directory_uri(); ?>/assets/build/svg/play.svg" alt="sk-play-button"/>
            </div>
            <video width="100%" id="sk-video-ad">
                <source src="<?php echo $file_src; ?>" type="video/mp4">
            </video>
        </div>
        <?php
    }
    /**/
}elseif( empty($homepage_vid) && !empty($homepage_img) ){
    $file_src = wp_get_attachment_url($homepage_img);
    ?>
    <div class="sk-home-img-banner" style="background-image: url(<?php echo $file_src; ?>)"></div>
    <?php
}
?>
<!---->

<!---->
<?php
$ppp = 6;
$mixed_posts_args = array(
    'post_type' => array('sk_news','ev_exhibitions','ev_world_congress','ev_courses'),
    'posts_per_page' => $ppp,
    'ignore_custom_sort' => true,
    'meta_key' => 'sk_post_order',
    'orderby' => array( 'meta_value_num' => 'ASC', 'date' => 'DESC' ),
    'meta_query' => array(
        'relation' => 'AND',
        array(
            'relation' => 'OR',
            array(
                'key'     => 'expires_in',
                'value'   => '',
                'compare' => '=',
            ),
            array(
                'key'     => 'expires_in',
                'compare' => 'NOT EXISTS',
            ),
            array(
                'key'     => 'expires_in',
                'value'   => date("Y-m-d"),
                'type'    => 'DATE',
                'compare' => '>',
            ),
        )
    ),
);
$mixed_posts_query = new WP_Query($mixed_posts_args);
?>
<div class="clearfix"></div>
<div class="col-md-12">
    <div class="sk-news-wrapper sk-c-gap clearfix">
        <div class="sk-news-archive-wrapper">
            <div class="news-listing">
                <?php
                if ($mixed_posts_query->have_posts()): ?>
                    <?php
                    $counter = 1;
                    while ($mixed_posts_query->have_posts()) : $mixed_posts_query->the_post();
                        get_template_part('template-parts/news-content', get_post_format());
                        if ($counter % 3 == 0)
                            echo '<div class="clearfix visible-md visible-lg"></div>';
                        if ($counter % 2 == 0)
                            echo '<div class="clearfix visible-xs visible-sm"></div>';
                        $counter++;
                    endwhile;
                    wp_reset_postdata();
                endif; ?>
            </div>
            <!-- End .col-md-12 -->
        </div>
    </div>
    <!-- sk-news-wrapper -->
</div><!-- /.col-md-12 -->

<div class="clearfix"></div>
<?php
$found_posts = $mixed_posts_query->found_posts;
if($found_posts > $ppp){
?>
    <div id="sk-load-more-news" class="sk-news-load-more loading-from-home-page">
        <div class="sk-arrow-n-text">
            <a href="#" id="home-page-load-more">
                <span><?php _e('Äldre Artiklar', 'skradderiforbundet') ?></span>
                <i class="fa fa-angle-down fa-3x"></i>
            </a>
            <span class="load-more-spinner">
                <i class="fa fa-spinner fa-spin"></i>
            </span>
        </div>
    </div><!-- End #sk-load-more-news -->
<?php }?>


<!--Advertisers Logo-->
<?php
$args = array(
    'post_type' => 'sk_logo_slider',
    'post_status' => 'publish',
    'posts_per_page' => -1,
);
$advertisers_logo = new WP_Query($args);
if($advertisers_logo->have_posts()):
    ?>
    <div class="sk-advertiser-slder">
        <?php global $sk_option;
        $logo_img = $sk_option['logo_image'];
        $logo_image_src = wp_get_attachment_image_src($logo_img);
        ?>
        <div class="sk-advertiser-logo sk-c-gap">
            <div class="owl-carousel owl-theme">
                <?php
                while($advertisers_logo->have_posts()):$advertisers_logo->the_post();
                    if(has_post_thumbnail()):
                        $link = get_field('link');
                        if(empty($link)){
                            $link = "javascript:void(0)";
                        }
                        ?>
                        <a href="<?php echo $link;?>" target="_blank">
                            <img class="owl-lazy" src="<?php the_post_thumbnail_url();?>" data-src="<?php the_post_thumbnail_url();?>" alt="site-logo"/>
                        </a>
                    <?php
                    endif;
                endwhile;wp_reset_postdata();
                ?>
            </div>
        </div>
    </div>
<?php endif;?>
<!---->

<?php get_footer(); ?>