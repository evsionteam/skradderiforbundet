<?php
/*Template Name: Lesikonet*/
?>
<?php get_header();
$slider_thumbnail_id = get_post_thumbnail_id( get_the_ID() );
$slider_image_src = wp_get_attachment_image_src($slider_thumbnail_id, 'full');
?>

<div class="sk-inner-page lexi">

		<div class="col-sm-6 sk-img-wrapper sk-c-double-section" id="sk-img-wrapper">
			<div class = "sk-title">
				<?php the_title(); ?>
			</div>
			<div class = "image" style="background-image:url('<?php echo $slider_image_src[0]; ?>');">
			</div>
		</div>

		<div class="col-sm-6 sk-c-double-section">
			<div class="sk-section">
			<h2 class="sk-title"><?php if(!empty(get_post_meta( get_the_ID() ,'sk_om_lexikonet_title', true ))) { echo get_post_meta( get_the_ID() ,'sk_om_lexikonet_title', true ); } ?></h2>
			<div class="sk-description">
				<?php if(!empty(get_post_meta( get_the_ID() ,'sk_om_lexikonet_content', true ))) { echo wpautop(get_post_meta( get_the_ID() ,'sk_om_lexikonet_content', true )); } ?>
			</div>
		</div>
			<div class="sk-section">
			<h2 class="sk-title"><?php if(!empty(get_post_meta( get_the_ID() ,'sk_om_lexikonet_buy_title', true ))) { echo get_post_meta( get_the_ID() ,'sk_om_lexikonet_buy_title', true ); } ?></h2>
			<div class="sk-description">
				<?php if(!empty(get_post_meta( get_the_ID() ,'sk_om_lexikonet_buy_content', true ))) { echo wpautop(get_post_meta( get_the_ID() ,'sk_om_lexikonet_buy_content', true )); } ?>
			</div>
		</div>
			<div class="sk-section">
			<h2 class="sk-title"><?php if(!empty(get_post_meta( get_the_ID() ,'sk_om_lexikonet_member_title', true ))) { echo get_post_meta( get_the_ID() ,'sk_om_lexikonet_member_title', true ); } ?></h2>
			<div class="sk-description">
				<?php if(!empty(get_post_meta( get_the_ID() ,'sk_om_lexikonet_member_content', true ))) { echo wpautop(get_post_meta( get_the_ID() ,'sk_om_lexikonet_member_content', true )); } ?>
			</div>
		</div>
            <?php
            $modal_button = get_post_meta( get_the_ID() ,'sk_om_lexikonet_order_button', true );
            if(!empty($modal_button)):
            ?>
			    <button type="button" class="btn-submit" data-toggle="modal" data-target="#sk-modal-box"><?php echo $modal_button;?></button>
            <?php endif;?>
		</div>

</div><!-- End .sk-inner-page -->

<?php
$modal_content = get_post_meta( get_the_ID() ,'modal_content', true );
if(!empty($modal_content)):
?>
    <!-- Modal -->
    <div class="modal fade" id="sk-modal-box" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content sk-wcontact-form-popup">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <div class="modal-body">
            <?php echo do_shortcode($modal_content);?>
          </div>
        </div>

      </div><!-- End .modal-dialog -->
    </div><!-- End .modal -->
<?php endif;?>

<?php get_footer();?>
