<?php
/* Template Name: Company listing */
get_header();
/**/
?>
    <div class="sk-inner-page map">
        <a href="#" class="sk-mobile-filter visible-xs"><?php _e('Filter', 'skradderiforbundet'); ?><i class="fa fa-filter" aria-hidden="true"></i></a>
        <div class="sk-company-filters sk-filter">
            <div class="col-md-12">
                <div class="sk-c-gap">
                    <form name="company-map-filter" id="company-map-filter" autocomplete="off">
                        <div class="sk-c-select">
                            <select id="company-cat" class="selectpicker" name="cat_search">
                                <option value=""><?php _e('Kategori', 'skradderiforbundet'); ?></option>
                                <?php
                                /*Get company cats*/
                                switch_to_blog(2);
                                $company_cats = sk_get_tax_terms_by_sql('company_category', true, true);
                                if (!empty($company_cats) && is_array(($company_cats))) {
                                    foreach ($company_cats as $company_cat) {
                                        echo "<option value=" . $company_cat['term_id'] . ">" . $company_cat['name'] . "</option>";
                                    }
                                }
                                restore_current_blog();
                                /**/
                                ?>
                            </select>
                        </div>
                        <div class="sk-c-select">
                            <select id="company-sub-cat" class="selectpicker" name="sub_cat_search" title="<?php _e('Välj underkategori','skradderiforbundet');?>"></select>
                        </div>
                        <div class="sk-c-form-group">
                            <input type="text" id="place-search" name="place_search" placeholder="<?php _e('Ort', 'skradderiforbundet'); ?>">
                        </div>
                        <div class="sk-c-form-group">
                            <input type="text" id="company-search" name="company_search" placeholder="<?php _e('Sök här', 'skradderiforbundet'); ?>">
                        </div>
                        <button type="submit"><?php _e('Sök', 'skradderiforbundet'); ?></button>
                    </form>
                      
                </div>
            </div>
        </div>
        <!-- End .sk-company-filters -->
    
            <div class="map-container">
                <a class="map-filtericon" href="#"><i class="fa fa-bars"></i></a>
                <div class="companies-list-mode <?php if(wp_is_mobile()){ echo 'list-toggle';}?>">
                    <div class="company-inner-list">
                        <?php
                        $companies = sk_get_companies('list-mode');
                        if(true == $companies['have_companies']){
                            if( isset($companies['company_data']) && is_array($companies['company_data'])){
                                foreach($companies['company_data'] as $company){
                                    ?>
                                    <div class="single-company-listing" data-name="<?php echo $company['name'];?>">
                                        <a class="map-content-link" href="#">
                                            <div class="company-details">
                                                <div class="map-company-info">
                                                    <span><?php echo $company['name'];?></span>
                                                    <span><?php echo $company['address'];?></span>
                                                    <span><?php echo $company['city'];?></span>
                                                    <span><?php echo $company['phone'];?></span>
                                                </div>
                                            </div>
                                            <div class="map-image-container">
                                                <?php
                                                $image = $company['image'];
                                                if(!empty($image)){
                                                    echo "<img src=".$image.">";
                                                }
                                                ?>
                                            </div>
                                        </a>

                                        <div class="orgn-links orgn-email"><?php echo $company['email'];?></div>
                                        <div class="orgn-links orgn-site"><?php echo $company['website'];?></div>
                                        <div class="orgn-links orgn-more"><?php echo $company['link'];?></div>
                                    </div>
                                    <?php
                                }
                            }
                        }
                        ?>
                    </div>
                </div>
                <div id="company-listing-map" style="height: calc( 100vh - 134px ); background-color: grey;">
                </div>
            </div>

        <!-- End .map-container -->
    </div><!-- End .sk-inner-page -->
<?php
get_footer('map');