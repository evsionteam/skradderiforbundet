<?php
/*Template Name: Contact*/
get_header();
while(have_posts()):the_post();
?>
<div class="sk-inner-page contact-us">
	<div class="sk-c-double-section">
		<div class = "container">
				<div class = "sk-title">
					<?php the_title(); ?>
				</div>
				<div class = "sk-description">
					<?php the_content(); ?>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="post-address">
						<h2 class="sk-title"><?php the_field('sk_contact_post_title');?></h2>
						<div class="sk-description">
							<?php the_field('sk_contact_post_content'); ?>
						</div>
					</div>
					</div>
					<div class="col-sm-6">
						<div class="post-address">
						<h2 class="sk-title"><?php the_field('sk_contact_board_title'); ?></h2>
						<div class="sk-description">
							<?php the_field('sk_contact_board_content');?>
						</div>
					</div>
					</div>
				</div>
		</div><!-- End .container -->
	</div><!-- End .sk-c-double-section -->

	<div class="container">
		<div class="row">
			<div class="sk-branch-detail sk-equal-row">
                <?php
                $organization_contact_info = get_field('organization_contact_info');
                if(!empty($organization_contact_info)){
                    foreach((array)$organization_contact_info as $info){
			if(! is_array($info) ) {
				continue;
			}	
                        ?>
                        <div class="col-sm-6 sk-equal-col">
                            <div class="post-address">
                                <h2 class="sk-title"><?php echo $info['name'] ?></h2>
                                <div class="sk-description">
                                    <?php echo $info['contact_info'] ?>
                                    <?php echo $info['contact_button'] ?>
                                </div>
                            </div>
                        </div>
                    <?php
                    }
                }
                ?>
			</div><!-- End .sk-branch-detail -->
		</div><!-- End .row -->
	</div> <!-- End .container -->
</div><!-- End .sk-inner-page -->

<?php
$modal_content = get_post_meta( get_the_ID() ,'sk_contact_form', true );
if(!empty($modal_content)):
?>
<div class="modal fade" id="sk-modal-box" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content sk-wcontact-form-popup">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="modal-body">
                <div class="contact-form-wrapper">
                    <?php echo do_shortcode('[contact-form-7 id="397" title="Local Admin Contact Form"]');?>
                </div>
            </div>
        </div>

    </div><!-- End .modal-dialog -->
</div>
<?php endif;?>

<?php endwhile; get_footer(); ?>
