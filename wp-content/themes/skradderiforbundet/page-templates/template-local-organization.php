<?php
/*Template Name: Organization Template*/
get_header();
?>

    <div class="sk-inner-page about-us organization-lists">
        <div class="sk-inner-banner" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>)">
            <div class="sk-letter">
                <?php the_field('background_letter');?>
            </div>
            <div class="c-article">
                <div class="sk-title-des">
                    <h1 class="sk-title"><?php the_title(); ?></h1>
                    <div class="sk-description">
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>
            <a href="" class="sk-scroll-down" data-target="#sk-education-intro" data-animation-duration="500">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/build/img/sk-down-arrow.png" alt="down-arrow"/>
            </a>
        </div>
        <div class="clearfix"></div>

            <div class="row">
                <div class="sk-orgn-list">

                <?php
                switch_to_blog(2);
                $org_args = array(
                    'post_type' => 'ev_organization',
                    'posts_per_page' => -1,
                    'posts_status' => 'publish',
                );
                $organizations = new WP_Query($org_args);
                if($organizations->have_posts()):
                    $counter = 1;
                    while($organizations->have_posts()):$organizations->the_post();
                    ?>
                        <div class="orgn-description <?php if($counter % 2 == 0){ echo 'odd-bg'; }?>">
                            <div class="container">
                                <div class="col-sm-4 col-md-3">
                                    <div class="orgn-logo">
                                        <?php
                                        $org_logo = get_post_meta(get_the_ID(),'organization_logo',true);
                                        if(!empty($org_logo)){
                                            echo "<img src='".$org_logo."'/>";
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-8 col-md-9">
                                    <div class="orgn-detail">
                                        <h3><?php the_title();?></h3>
                                        <div class="orgn-detail-list">
                                            <?php the_excerpt();?>
                                            <div class="orgn-teams">
                                                <?php the_content();?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                    $counter++;
                    endwhile;wp_reset_postdata();
                    endif;
                restore_current_blog();
                ?>

                </div>
            </div>
  
    </div> <!-- End .sk-inner-page organisation-lists -->
  
<?php
get_footer();