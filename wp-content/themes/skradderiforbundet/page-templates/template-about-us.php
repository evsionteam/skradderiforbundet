<?php
/*Template Name: About Us*/
get_header();
if(has_post_thumbnail(get_the_ID())){
  $about_thumbnail_id = get_post_thumbnail_id( get_the_ID() );
  $about_image_src = wp_get_attachment_image_src($about_thumbnail_id, 'full');
} else {
  $about_image_src[0] = get_template_directory_uri() . '/assets/build/img/sk-member-banner-image.jpg';
}
$content_post = get_post(get_the_ID());
?>

<div class="sk-inner-page about-us">
 
      <div class="sk-inner-banner" style="background-image: url(<?php echo $about_image_src[0]; ?>)">
          <span class="sk-letter">
               <?php the_field('background_letter');?>
          </span>
          <div class="c-article">
              <div class="sk-title-des">
                  <h1 class="sk-title"><?php the_title(); ?></h1>
                  <div class="sk-description">
                    <?php echo apply_filters('the_content', $content_post->post_content); ?>
                  </div>
              </div>
          </div>
      </div><!-- End .sk-inner-banner -->

    <div class="col-md-12">
        <div class="sk-inner-intro">
          <h2 class="sk-title"> <?php if(!empty(get_post_meta( get_the_ID() ,'sk_about_wed_title', true ))) { echo get_post_meta( get_the_ID() ,'sk_about_wed_title', true ); } ?> </h2>
          <div class="sk-description">
           <?php if(!empty(get_post_meta( get_the_ID() ,'sk_about_wed_content', true ))) { echo wpautop(get_post_meta( get_the_ID() ,'sk_about_wed_content', true )); } ?>
          </div>
        </div><!-- End .sk-what-we-do -->
    </div> <!-- /.col-md-12 -->

    <div class="clearfix"></div>
    <div class="sk-c-double-section">
      <div class="container">

        <div class="sk-section-1">
          <h2 class="sk-title"> <?php if(!empty(get_post_meta( get_the_ID() ,'sk_about_purpose_title', true ))) { echo get_post_meta( get_the_ID() ,'sk_about_purpose_title', true ); } ?> </h2>
          <div class="sk-description">
           <?php if(!empty(get_post_meta( get_the_ID() ,'sk_about_purpose_content', true ))) { echo wpautop(get_post_meta( get_the_ID() ,'sk_about_purpose_content', true )); } ?>
          </div>
        </div><!-- End .sk-purpose-->
        <div class="sk-section-2">
          <h2 class="sk-title"> <?php if(!empty(get_post_meta( get_the_ID() ,'sk_about_history_title', true ))) { echo get_post_meta( get_the_ID() ,'sk_about_history_title', true ); } ?> </h2>
          <div class="sk-description">
           <?php if(!empty(get_post_meta( get_the_ID() ,'sk_about_history_content', true ))) { echo wpautop(get_post_meta( get_the_ID() ,'sk_about_history_content', true )); } ?>
          </div>
        </div><!-- End .sk-section-2 -->

      </div><!-- End .container -->
    </div><!-- End .sk-c-double-section -->

</div><!-- End .sk-inner-page -->


<?php
get_footer();
