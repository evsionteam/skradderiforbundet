<?php
/* Template Name: Course Page */
get_header();
while (have_posts()):the_post();
    ?>
    <div class="sk-inner-page about-us courses">
        <div class="sk-inner-banner" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>)">
            <div class="sk-letter">
                <?php the_field('background_letter');?>
            </div>
            <div class="c-article">
                <div class="sk-title-des">
                    <h1 class="sk-title"><?php the_title(); ?></h1>
                    <div class="sk-description">
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>
            <a href="" class="sk-scroll-down" data-target="#sk-education-intro" data-animation-duration="500">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/build/img/sk-down-arrow.png" alt="down-arrow"/>
            </a>
        </div>
        <div class="clearfix"></div>
        <div class="container">
            <div class="sk-inner-intro" id="sk-education-intro">
                <h2 class="sk-title">
                    <?php the_field('info_title');?>
                </h2>
                <div class="sk-description">
                    <?php the_field('info_content'); ?>
                </div>
            </div>
        </div>
        <!-- End .container -->
        <div class="sk-c-double-section">
            <div class="container">
                <div class="sk-section-1">
                    <h2 class="sk-title">
                        <?php the_field('training_title'); ?>
                    </h2>
                    <div class="sk-description">
                        <?php the_field('training_links');  ?>
                    </div>
                </div>
                <!-- End .sk-section-1 -->
                <div class="sk-section-2">
                    <h2 class="sk-title">
                        <?php the_field('further_training'); ?>
                    </h2>
                    <div class="sk-description">
                        <?php the_field('further_training_info'); ?>
                    </div>
                </div>
                <!-- End .sk-section-2 -->
            </div>
            <!-- End .container -->
        </div>
        <!-- End .sk-c-double-section -->
        <?php
        $args = array(
            'post_type' => 'ev_courses',
            'post_status' => 'publish',
            'posts_per_page' => -1
        );
        $educations = new WP_Query($args);
        if ($educations->have_posts()):
            ?>
            <div class="sk-course-n-eduction">
                <div class="container">
                    <h2 class="sk-title"><?php _e('Aktuella kurser & vidareutbildningar', 'skradderiforbundet'); ?></h2>

                    <div class="education-list-wrapper sk-table">
                        <table class="table table-condensed" id="sk-master-table">
                            <thead>
                            <tr>
                                <th><?php _e('Namn', 'skradderiforbundet'); ?></th>
                                <th data-breakpoints="xs"><?php _e('Kurshållare', 'skradderiforbundet'); ?></th>
                                <th data-breakpoints="xs"><?php _e('Plats', 'skradderiforbundet'); ?></th>
                                <th data-breakpoints="xs"><?php _e('Kurststart', 'skradderiforbundet'); ?></th>
                                <th data-breakpoints="xs"></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php while ($educations->have_posts()): $educations->the_post(); ?>
                                <tr>
                                    <td><?php the_title(); ?></td>
                                    <td data-breakpoints="xs"><?php echo get_post_meta(get_the_ID(), '_course_holder', true); ?></td>
                                    <td data-breakpoints="xs"><?php echo get_post_meta(get_the_ID(), '_training_place', true); ?></td>
                                    <td data-breakpoints="xs"><?php echo get_post_meta(get_the_ID(), '_course_start_date', true); ?></td>
                                    <td data-breakpoints="xs">
                                        <?php
                                        $link = get_permalink(get_the_ID());
                                        echo "<a href=$link>" . __('Visa', 'skradderiforbundet') . "</a>";
                                        ?>
                                    </td>
                                </tr>
                            <?php endwhile;
                            wp_reset_postdata(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div> <!-- End .sk-inner-page courses -->
<?php endwhile;
get_footer(); ?>
