<?php
/*Template Name: Master*/
get_header(); ?>
<div class="sk-inner-page master">
<?php

$user_args = array(
    'blog_id' => 2,
    'role'  => 'member',
);
$selected_master_value = $search_value = '';
$form_errors = new WP_Error;
$master_members = array();

if( isset($_GET['master_in']) ){

    $selected_master_value = $_GET['master_in'];
    $search_value = $_GET['search'];

    if( !empty($selected_master_value) && !empty($search_value) ){
        $user_args['meta_query'] = array(
            'relation' => 'AND',
            array(
                'key'     => 'master_in',
                'value'   => $selected_master_value,
                'compare' => 'LIKE'
            ),
            array(
                'relation' => 'OR',
                array(
                    'key'     => 'first_name',
                    'value'   => $search_value,
                    'compare' => 'LIKE'
                ),
                array(
                    'key'     => 'last_name',
                    'value'   => $search_value,
                    'compare' => 'LIKE'
                ),
            )
        );
    }elseif( !empty($selected_master_value) && empty($search_value) ){
        $user_args['meta_query'] = array(
            array(
                'key'     => 'master_in',
                'value'   => $selected_master_value,
                'compare' => 'LIKE'
            ),
        );
    }elseif( empty($selected_master_value) && !empty($search_value) ){
        $user_args['meta_query'] = array(
            'relation' => 'OR',
            array(
                'key'     => 'first_name',
                'value'   => $search_value,
                'compare' => 'LIKE'
            ),
            array(
                'key'     => 'last_name',
                'value'   => $search_value,
                'compare' => 'LIKE'
            ),
        );
    }else{
        $user_args['meta_query'] = array(
            array(
                'key'     => 'master_in',
                'value'   => '',
                'compare' => '!='
            ),
        );
    }
}else{
    $user_args['meta_query'] = array(
        array(
            'key'     => 'master_in',
            'value'   => '',
            'compare' => '!='
        ),
    );
}

/*Run the query only when there are no errors*/
if ( count($form_errors->get_error_messages()) < 1 ) {
    $user_query = new WP_User_Query($user_args);
    $master_members = $user_query->get_results();
}
/**/

if ( is_wp_error( $form_errors ) ) {
    echo '<div class="form-validation-errors">';
    foreach ( $form_errors->get_error_messages() as $error ) {
        echo "<p class='form-error alert alert-danger'>$error</p>";
    }
    echo '</div>';
}
?>
    <div class="sk-search-form sk-filter">
        <div class="col-md-12">
          <div class="sk-c-gap">
            <form>
                <div class="sk-search">
                    <div class="sk-form-group">
                        <div class="sk-c-select">
                            <select name="master_in" class="selectpicker">
                                <option value="" <?php selected('',$selected_master_value);?>><?php _e('&mdash;Välj kategori&mdash;','skradderiforbundet')?></option>
                                <option value="him" <?php selected('him',$selected_master_value);?>><?php _e('Herrskräddare','skradderiforbundet')?></option>
                                <option value="her" <?php selected('her',$selected_master_value);?>><?php _e('Damskräddare','skradderiforbundet')?></option>
                                <option value="dress" <?php selected('dress',$selected_master_value);?>><?php _e('Klänningsskräddare','skradderiforbundet')?></option>
                            </select>
                        </div>
                    </div>
                    <div class="sk-form-group">
                        <input type="text" name="search" placeholder="<?php _e('Sök här','skradderiforbundet')?>" value="<?php echo $search_value;?>"/>
                    </div>

                    <button type="submit"><?php _e('Sök','skradderiforbundet')?></button>
                </div>
                <!-- End .sk-search -->
            </form>
          </div>
        </div>
    </div>

    <?php if(!empty($master_members)):
        ?>
        <div class="col-md-12">
          <div class="sk-c-gap">
            <div class="sk-table">
                <table class="table table-condensed" id="sk-master-table" data-sorting="true" data-show-toggle="false">
                    <thead>
                        <tr>
                            <th><?php _e('Mästare', 'skradderiforbundet'); ?></th>
                            <th data-breakpoints="xs"><?php _e('Företag', 'skradderiforbundet'); ?></th>
                            <th data-breakpoints="xs"><?php _e('Ort', 'skradderiforbundet'); ?></th>
                            <th data-breakpoints="xs"><?php _e('Yrke', 'skradderiforbundet'); ?></th>
                            <th data-breakpoints="xs"><?php _e('Kategori', 'skradderiforbundet'); ?></th>
                            <th></th>
                            <th data-breakpoints="all"><?php _e('Email', 'skradderiforbundet'); ?></th>
                            <th data-breakpoints="all"><?php _e('Telefonnummer', 'skradderiforbundet'); ?></th>
                            <th data-breakpoints="all"><?php _e('Postnummer', 'skradderiforbundet'); ?></th>
                            <th data-breakpoints="all"><?php _e('Hemsida', 'skradderiforbundet'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($master_members as $master_member)
                        {
                            $member_id = $master_member->ID;
                            ?>
                            <tr>
                                <td>
                                    <?php
                                        $first_name = get_user_meta($member_id,'first_name',true);
                                        $last_name = get_user_meta($member_id,'last_name',true);
                                        echo $first_name.' '.$last_name;
                                    ?>
                                </td>
                                <td><?php echo get_user_meta($member_id,'company_name',true)?></td>
                                <td><?php echo get_user_meta($member_id,'member_place',true);?></td>
                                <td><?php echo get_user_meta($member_id,'member_profession',true);?></td>
                                <td>
                                    <?php
                                    $master_labels = array();
                                    $master_in = get_user_meta($member_id,'master_in',true);
                                    if(is_array($master_in)){
                                        $temp = array();
                                        foreach($master_in as $value){
                                            if('him' == $value){
                                                $temp[] = 'Herr';
                                            }elseif('her' == $value){
                                                $temp[] = 'Dam';
                                            }elseif('dress' == $value){
                                                $temp[] = 'Klänning';
                                            }
                                        }
                                        $master_labels = $temp;
                                        echo implode(', ',$master_labels);
                                    }
                                    ?>
                                </td>
                                <td>
                                    <a href="#" class="footable-toggle fooicon">
                                        <?php _e('Visa', 'skradderiforbundet'); ?>
                                    </a>
                                </td>
                                <td><?php echo $master_member->data->user_email;?></td>
                                <td><?php echo get_user_meta($member_id,'member_home_phone',true);?></td>
                                <td><?php echo get_user_meta($member_id,'member_post_number',true);?></td>
                                <td><?php echo get_user_meta($member_id,'company_website',true);?></td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
          </div>
        </div>

    <!-- End .container -->
    <?php
    else:
        ?>
        <div class="no-members">
            <h3><?php _e('Members Not Found','skradderiforbundet'); ?></h3>
        </div>
        <?php
    endif;
    ?>
    <div class="container">
        <div class="sk-master-info">
            <div class="sk-organisation-info"><?php the_field('link_text');?></div>
            <div class="logo-left">
                <?php
                $logo = get_field('logo');
                if(!empty($logo)){
                    $logo_link = get_field('logo_link')
                    ?>
                    <a href="<?php echo $logo_link;?>" target="_blank">
                        <img src="<?php echo $logo['url'];?>">
                    </a>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</div><!-- End sk-inner-page -->
<?php
get_footer();
