<?php
get_header();
if( ( isset($_GET['q']) && !empty($_GET['q']) ) ){
    switch_to_blog(2);
    if ( $post = get_page_by_path( $_GET['q'], OBJECT, 'ev_companies' ) ){
        $id = $post->ID;
    }
    else{
        $id = 0;
    }
    if( 0 != $id ){
        $company = get_post($id);
        ?>
        <div class="sk-inner-page sk-company-details">
           
            <div class="col-md-12">
                <div class="sk-inner-intro"> 
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="sk-company-logo"><?php the_post_thumbnail('full');?></div>
                        </div>
                        <div class="col-sm-9">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="sk-description table-responsive">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <th><?php _e('Företagets namn','skradderiforbundet')?></th>
                                            <td><?php echo $company->post_title;?></td>
                                        </tr>
                                        <tr>
                                            <th><?php _e('Adress','skradderiforbundet')?></th>
                                            <td><?php echo get_post_meta($id,'_company_address',true);?></td>
                                        </tr>
                                        <tr>
                                            <th><?php _e('Zip Code','skradderiforbundet')?></th>
                                            <td><?php echo get_post_meta($id,'_company_zip_code',true);?></td>
                                        </tr>
                                        <tr>
                                            <th><?php _e('City','skradderiforbundet')?></th>
                                            <td><?php echo get_post_meta($id,'_company_city',true);?></td>
                                        </tr>
                                        <tr>
                                            <th><?php _e('Telefonnummer','skradderiforbundet')?></th>
                                            <td><?php echo get_post_meta($id,'_company_phone',true);?></td>
                                        </tr>
                                        <tr>
                                            <th><?php _e('Email','skradderiforbundet')?></th>
                                            <td>
                                                <?php
                                                $email = get_post_meta($id,'_company_email',true);
                                                if(!empty($email)){
                                                    echo "<a href='mailto:$email'>".$email."</a>";
                                                }
                                                ?>
                                            </td>
                                        </tr>

                                        <?php
                                        $website = get_post_meta($id,'_company_website',true);
                                        if(!empty($website)){
                                            ?>
                                            <tr>
                                                <th><?php _e('Website','skradderiforbundet')?></th>
                                                <td>
                                                    <?php echo "<a href='".esc_url($website)."' target='_blank'>".$website."</a>";?>
                                                </td>
                                            </tr>
                                            <?php
                                        }

                                        $cat_arr = array();
                                        $categories = sk_get_post_terms_by_sql($id,'company_category');
                                        if( !empty($categories) && is_array($categories) ){
                                           foreach($categories as $cat){
                                               $cat_arr[] = $cat['name'];
                                           }
                                           if(!empty($cat_arr)){
                                               ?>
                                                <tr>
                                                    <th><?php _e('Category','skradderiforbundet')?></th>
                                                    <td>
                                                        <?php echo join(', ',$cat_arr);?>
                                                    </td>
                                                </tr>
                                               <?php
                                           }
                                        }
                                        ?>
                                        <tr>
                                            <th><?php _e('Information om företaget','skradderiforbundet')?></th>
                                            <td><?php echo apply_filters('the_content',$company->post_content);?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-sm-4"></div>
                        
                    </div>

                    </div>
                </div><!-- End .sk-what-we-do -->
            </div>
        </div>
        <?php
    }
    restore_current_blog();
}
get_footer();