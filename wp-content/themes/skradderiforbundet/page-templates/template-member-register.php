<?php
/*Template Name: User Register Template*/
get_header();
?>
<div class="sk-inner-page member-register about-us  ">

<?php
while (have_posts()):the_post();
    ?>
    <div class="sk-inner-page">
    <div id="primary" class="content-area main-content">
    <main id="main" class="site-main" role="main">
    
        <div class="sk-inner-banner" style="background-image: url(<?php if (has_post_thumbnail()) {
            the_post_thumbnail_url();
        } ?>) ">
            <span class="sk-letter">
                 <?php the_field('background_letter');?>
            </span>
            <div class="c-article">
                <div class="sk-title-des">
                    <h1 class="sk-title"><?php the_title(); ?></h1>
                    <div class="sk-description">
                      <p>
                          <?php echo get_the_content(); ?>
                      </p>
                    </div>
                </div>
            </div>
            <a href="" class="sk-scroll-down" data-target="#sk-member-register-form" data-animation-duration="500">
                <?php _e('Ansök','skradderiforbundet');?>
                <img src="<?php echo get_template_directory_uri(); ?>/assets/build/img/sk-down-arrow.png" alt="down-arrow"/>
            </a>
        </div>
        <div class=" sk-c-gap ">
            <div class="col-md-12">
                <div class="sk-inner-intro">
                    <h2 class="sk-title"><?php the_field('info_title');?></h2>
                    <div class="sk-description">
                        <?php the_field('info_text');?>
                    </div>
                </div>
            </div>
            <!-- /.col-md-12 -->



            <div class="clearfix"></div>
            <div class="sk-member-register-form clearfix" id="sk-member-register-form">
            <div class="sk-member-register-message"></div>
            <form name="register_member_form" id="register_member_form" method="post" enctype="multipart/form-data" autocomplete="off">
            <?php wp_nonce_field('register_member', 'register_member_nonce'); ?>

            <!-- PERSONAL INFORMATION -->
            <div class="sk-personal-info ">
                <div class="col-md-4 sk-c-title">
                    <h2><?php _e('Personuppgifter', 'skradderiforbundet'); ?></h2>
                </div>
                <!-- End .col-md-12 -->
                <div class="clearfix"></div>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-sm-6 col-md-4">
                            <div class="c-form-group">
                                <input type="text" id="first-name" name="first_name" class="form-control" required/>
                                <label for="first-name"><?php _e('Förnamn', 'skradderiforbundet'); ?>*</label>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="c-form-group">
                                <input type="text" id="last-name" name="last_name" class="form-control" required/>
                                <label for="last-name"><?php _e('Efternamn', 'skradderiforbundet'); ?>*</label>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="c-form-group">
                                <input type="text" id="member-birthdate" name="member_birthdate" class="form-control datepicker" required/>
                                <label for="member-birthdate"><?php _e('Födelsedatum', 'skradderiforbundet'); ?>*</label>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="c-form-group">
                                <input type="text" id="member-professional-title" name="member_profession" class="form-control" required/>
                                <label for="member-professional-title"><?php _e('Yrke', 'skradderiforbundet'); ?>*</label>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="c-form-group">
                                <input type="text" id="member-home-address" name="member_home_address" class="form-control" required/>
                                <label for="member-home-address"><?php _e('Hemadress', 'skradderiforbundet'); ?>*</label>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="c-form-group">
                                <input type="text" id="member-post-number" name="member_post_number" class="form-control" required/>
                                <label for="member-post-number"><?php _e('Postnummer', 'skradderiforbundet'); ?>*</label>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="c-form-group">
                                <input type="text" id="member-place" name="member_place" class="form-control" required/>
                                <label for="member-place"><?php _e('Stad', 'skradderiforbundet'); ?>*</label>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="c-form-group">
                                <input type="text" id="member-country" name="member_country" class="form-control" required/>
                                <label for="member-country"><?php _e('Land', 'skradderiforbundet'); ?>*</label>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="c-form-group">
                                <input type="text" id="member-home-phone" name="member_home_phone" class="form-control" required/>
                                <label for="member-home-phone"><?php _e('Telefonnummer', 'skradderiforbundet'); ?>*</label>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="c-form-group">
                                <input type="text" id="username" name="username" class="form-control" required/>
                                <label for="username"><?php _e('Användarnamn', 'skradderiforbundet'); ?>*</label>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="c-form-group">
                                <input type="email" id="email" name="email" class="form-control" required/>
                                <label for="email"><?php _e('E-mail', 'skradderiforbundet'); ?>*</label>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="sk-c-select">
                                <select name="member_organization" class="selectpicker" required>
                                    <option value=""><?php _e('Välj lokalförening', 'skradderiforbundet'); ?></option>
                                    <?php
                                    switch_to_blog(2);
                                    $org_args = array(
                                        'post_type' => 'ev_organization',
                                        'posts_per_page' => -1,
                                        'meta_query' => array(
                                            array(
                                                'key' => '_organization_admin',
                                                'value' => '',
                                                'compare' => '!=',
                                            ),
                                        ),
                                    );
                                    $organizations = new WP_Query($org_args);
                                    if ($organizations->have_posts()):
                                        while ($organizations->have_posts()):$organizations->the_post();
                                            echo "<option value=" . get_the_ID() . ">" . get_the_title() . "</option>";
                                        endwhile;
                                        wp_reset_postdata();
                                    endif;
                                    restore_current_blog();
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <!-- End .row -->
                </div>
                <!-- End .col-md-12 -->
            </div>
            <!-- End .sk-personal-info -->


            <!-- Certificate -->
            <div class="sk-member-certificate">
                <div class="col-md-4 sk-c-title">
                    <h2><?php _e('Gesällbrev', 'skradderiforbundet'); ?></h2>
                    <div class="sk-c-title-checkbox">
                        <input type="checkbox" id="member-certificate" class="slide-toggle-form-checkbox" name="has_certificate" data-toggle="#toggle-member-certificate"/>
                        <label for="member-certificate"></label>
                    </div>
                </div>
                <!-- End .col-md-12 -->
                <div class="clearfix"></div>

                <div class="col-md-12 sk-form-group-gap" hidden id="toggle-member-certificate">
                    <div class="row">
                        <div class="col-sm-6 col-md-3">
                            <div class="form-group sk-check-form">
                                <input type="checkbox" id="herr-certificate" name="certificate_in_man" class="form-control" disabled/>
                                <label for="herr-certificate"><?php _e('Herr', 'skradderiforbundet'); ?></label>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <div class="form-group sk-check-form">
                                <input type="checkbox" id="woman-certificate" name="certificate_in_woman" class="form-control" disabled/>
                                <label for="woman-certificate"><?php _e('Dam', 'skradderiforbundet'); ?></label>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <div class="form-group sk-check-form">
                                <input type="checkbox" id="dress-certificate" name="certificate_in_dress" class="form-control" disabled/>
                                <label for="dress-certificate"><?php _e('Klänning', 'skradderiforbundet'); ?></label>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <div class="c-form-group">
                                <input type="text" id="date-of-certificate" name="date_of_certificate" class="form-control" data-type="required" disabled/>
                                <label for="date-of-certificate"><?php _e('År för utfärdande', 'skradderiforbundet'); ?>*</label>
                            </div>
                        </div>
                    </div>
                    <!-- End .row -->
                </div>
                <!-- End .col-md-12 -->
            </div>
            <!-- End .certificate -->


            <!-- MASTER CRAFTMAN -->
            <div class="sk-master-craftman ">
                <div class="col-md-4 sk-c-title">
                    <h2><?php _e('Mästarbrev', 'skradderiforbundet'); ?></h2>
                    <div class="sk-c-title-checkbox">
                        <input type="checkbox" id="sk-master-craftman" class="slide-toggle-form-checkbox" name="has_master_craft" data-toggle="#toggle-master-craftman"/>
                        <label for="sk-master-craftman"></label>
                    </div>
                </div>
                <!-- End .col-md-12 -->
                <div class="clearfix"></div>

                <div class="col-md-12 sk-form-group-gap" hidden id="toggle-master-craftman">
                    <div class="row">
                        <div class="col-sm-6 col-md-3">
                            <div class="form-group sk-check-form">
                                <input type="checkbox" id="mastarbrev-herr" name="master_in_man" class="form-control" disabled/>
                                <label for="mastarbrev-herr"><?php _e('Herr', 'skradderiforbundet'); ?></label>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <div class="form-group sk-check-form">
                                <input type="checkbox" id="master-in-woman" name="master_in_woman" class="form-control" disabled/>
                                <label for="master-in-woman"><?php _e('Dam', 'skradderiforbundet'); ?></label>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <div class="form-group sk-check-form">
                                <input type="checkbox" id="membership-dress" name="master_in_dress" class="form-control" disabled/>
                                <label for="membership-dress"><?php _e('Klänning', 'skradderiforbundet'); ?></label>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <div class="c-form-group">
                                <input type="text" id="master-year" name="master_year" class="form-control" data-type="required" disabled/>
                                <label for="master-year"><?php _e('År för utfärdande', 'skradderiforbundet'); ?>*</label>
                            </div>
                        </div>
                    </div>
                    <!-- End .row -->
                </div>
                <!-- End .col-md-12 -->
            </div>
            <!-- End .master-craftman -->

            <!-- OWN BUSINESS -->
            <div class="sk-own-business">
                <div class="col-md-4 sk-c-title">
                    <h2><?php _e('Eget företag', 'skradderiforbundet'); ?></h2>
                    <div class="sk-c-title-checkbox">
                        <input type="checkbox" id="sk-own-business" class="slide-toggle-form-checkbox" name="has_business" data-toggle="#toggle-own-business"/>
                        <label for="sk-own-business"></label>
                    </div>
                </div>
                <!-- End .col-md-12 -->
                <div class="clearfix"></div>

                <div class="col-md-12 sk-form-group-gap " id="toggle-own-business" hidden>
                    <div class="row">
                        <div class="col-sm-6 col-md-4">
                            <div class="c-form-group">
                                <input type="text" id="company-name" name="company_name" data-type="required" class="form-control" disabled/>
                                <label for="company-name"><?php _e('Företagsnamn', 'skradderiforbundet'); ?>*</label>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="c-form-group">
                                <input type="text" id="company-phone" name="company_phone" data-type="required" class="form-control" disabled/>
                                <label for="company-phone"><?php _e('Telefon', 'skradderiforbundet'); ?>*</label>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="c-form-group">
                                <input type="text" id="company-start-date" name="company_start_date" data-type="required" class="form-control" disabled/>
                                <label for="company-start-date"><?php _e('Registreringsår', 'skradderiforbundet'); ?>*</label>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="c-form-group">
                                <input type="text" id="business-address" name="company_address" data-type="required" class="form-control" disabled/>
                                <label for="business-address"><?php _e('Adress', 'skradderiforbundet'); ?>*</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="c-form-group">
                                <input type="text" id="company-post-number" name="company_post_number" data-type="required" class="form-control" disabled/>
                                <label for="company-post-number"><?php _e('Postnummer', 'skradderiforbundet'); ?>*</label>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="c-form-group">
                                <input type="text" id="company-city" name="company_city" data-type="required" class="form-control" disabled/>
                                <label for="company-city"><?php _e('Stad', 'skradderiforbundet'); ?>*</label>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="sk-c-select">
                                <select name="company_nature[]" class="selectpicker" title="<?php _e('Verksamhetsområde', 'skradderiforbundet'); ?>" disabled multiple data-size="10">
                                    <option value="Barnkläder"><?php _e('Barnkläder', 'skradderiforbundet'); ?></option>
                                    <option value="Festklänning"><?php _e('Festklänning', 'skradderiforbundet'); ?></option>
                                    <option value="Kavaj"><?php _e('Kavaj', 'skradderiforbundet'); ?></option>
                                    <option value="Plissering"><?php _e('Plissering', 'skradderiforbundet'); ?></option>
                                    <option value="Blus"><?php _e('Blus', 'skradderiforbundet'); ?></option>
                                    <option value="Film"><?php _e('Film', 'skradderiforbundet'); ?></option>
                                    <option value="Kemtvätt"><?php _e('Kemtvätt', 'skradderiforbundet'); ?></option>
                                    <option value="Prästkläder"><?php _e('Prästkläder', 'skradderiforbundet'); ?></option>
                                    <option value="Brudklänning"><?php _e('Brudklänning', 'skradderiforbundet'); ?></option>
                                    <option value="Folkdräkter"><?php _e('Folkdräkter', 'skradderiforbundet'); ?></option>
                                    <option value="Kjol"><?php _e('Kjol', 'skradderiforbundet'); ?></option>
                                    <option value="Reparationer"><?php _e('Reparationer', 'skradderiforbundet'); ?></option>
                                    <option value="Byxor"><?php _e('Byxor', 'skradderiforbundet'); ?></option>
                                    <option value="Frack"><?php _e('Frack', 'skradderiforbundet'); ?></option>
                                    <option value="Scenkläder"><?php _e('Klädda knappar, spännen', 'skradderiforbundet'); ?></option>
                                    <option value="Cape"><?php _e('Cape', 'skradderiforbundet'); ?></option>
                                    <option value="Hatt"><?php _e('Hatt', 'skradderiforbundet'); ?></option>
                                    <option value="Klänning"><?php _e('Klänning', 'skradderiforbundet'); ?></option>
                                    <option value="Cummerband"><?php _e('Cummerband', 'skradderiforbundet'); ?></option>
                                    <option value="Hemtextilier"><?php _e('Hemtextilier', 'skradderiforbundet'); ?></option>
                                    <option value="Konståkningskläder"><?php _e('Konståkningskläder', 'skradderiforbundet'); ?></option>
                                    <option value="Smoking"><?php _e('Smoking', 'skradderiforbundet'); ?></option>
                                    <option value="Damskrädderi"><?php _e('Damskrädderi', 'skradderiforbundet'); ?></option>
                                    <option value="Herrkonfektion"><?php _e('Herrkonfektion', 'skradderiforbundet'); ?></option>
                                    <option value="Korsett"><?php _e('Korsett', 'skradderiforbundet'); ?></option>
                                    <option value="Syateljé"><?php _e('Syateljé', 'skradderiforbundet'); ?></option>
                                    <option value="Danskläder"><?php _e('Danskläder', 'skradderiforbundet'); ?></option>
                                    <option value="Herrskrädderi"><?php _e('Herrskrädderi', 'skradderiforbundet'); ?></option>
                                    <option value="Kostym"><?php _e('Kostym', 'skradderiforbundet'); ?></option>
                                    <option value="Teaterskräddare"><?php _e('Teaterskräddare', 'skradderiforbundet'); ?></option>
                                    <option value="Djur"><?php _e('Djur', 'skradderiforbundet'); ?></option>
                                    <option value="Historiska-kläder"><?php _e('Historiska kläder', 'skradderiforbundet'); ?></option>
                                    <option value="Krinolin"><?php _e('Krinolin', 'skradderiforbundet'); ?></option>
                                    <option value="Uniformer"><?php _e('Uniformer', 'skradderiforbundet'); ?></option>
                                    <option value="Doktorskragar"><?php _e('Doktorskragar', 'skradderiforbundet'); ?></option>
                                    <option value="Huvudbonad"><?php _e('Huvudbonad', 'skradderiforbundet'); ?></option>
                                    <option value="Lönsömnad"><?php _e('Lönsömnad', 'skradderiforbundet'); ?></option>
                                    <option value="Uthyrning"><?php _e('Uthyrning', 'skradderiforbundet'); ?></option>
                                    <option value="Dopklänning"><?php _e('Dopklänning', 'skradderiforbundet'); ?></option>
                                    <option value="Jaquette"><?php _e('Jaquette', 'skradderiforbundet'); ?></option>
                                    <option value="Maskerad"><?php _e('Maskerad', 'skradderiforbundet'); ?></option>
                                    <option value="Väst"><?php _e('Väst', 'skradderiforbundet'); ?></option>
                                    <option value="Dräkt"><?php _e('Dräkt', 'skradderiforbundet'); ?></option>
                                    <option value="Jeans"><?php _e('Jeans', 'skradderiforbundet'); ?></option>
                                    <option value="Måttbeställning"><?php _e('Måttbeställning', 'skradderiforbundet'); ?></option>
                                    <option value="Ändringar"><?php _e('Ändringar', 'skradderiforbundet'); ?></option>
                                    <option value="Fantasydräkter"><?php _e('Fantasydräkter', 'skradderiforbundet'); ?></option>
                                    <option value="Kappa"><?php _e('Kappa', 'skradderiforbundet'); ?></option>
                                    <option value="Mönsterkonstruktion"><?php _e('Mönsterkonstruktion', 'skradderiforbundet'); ?></option>
                                    <option value="Ändringsskrädderi"><?php _e('Ändringsskrädderi', 'skradderiforbundet'); ?></option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="form-group sk-check-form">
                                <input type="checkbox" id="f-tax" name="company_f_tax" class="form-control" disabled/>
                                <label for="f-tax"><?php _e('F-skatt', 'skradderiforbundet'); ?></label>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="form-group sk-check-form">
                                <input type="checkbox" id="company-vat" name="company_vat" class="form-control" disabled/>
                                <label for="company-vat"><?php _e('Moms', 'skradderiforbundet'); ?></label>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-sm-6 col-md-4">
                            <div class="c-form-group">
                                <input type="email" id="company-email" name="company_email" class="form-control" data-type="required" disabled/>
                                <label for="company-email"><?php _e('Företagsemail', 'skradderiforbundet'); ?>*</label>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="c-form-group">
                                <input type="text" id="company-website" name="company_website" class="form-control" disabled/>
                                <label for="company-website"><?php _e('Webbsida', 'skradderiforbundet'); ?></label>
                            </div>
                        </div>
                    </div>
                    <!-- End .row -->
                </div>
                <!-- End .col-md-12 -->
            </div>
            <!-- End .own-business-->


            <!-- Employee section -->
            <div class="sk-company-employee">
                <div class="col-md-4 sk-c-title">
                    <h2><?php _e('Anställd på företag', 'skradderiforbundet'); ?></h2>
                    <div class="sk-c-title-checkbox">
                        <input type="checkbox" id="sk-company-employee" class="slide-toggle-form-checkbox" name="is_employee" data-toggle="#toggle-company-employee"/>
                        <label for="sk-company-employee"></label>
                    </div>
                </div>
                <!-- End .col-md-12 -->
                <div class="clearfix"></div>

                <div class="col-md-12 sk-form-group-gap" id="toggle-company-employee" hidden>
                    <div class="row">
                        <div class="col-sm-6 col-md-4">
                            <div class="c-form-group">
                                <input type="text" id="employee-company-name" name="employee_company_name" data-type="required" class="form-control" disabled/>
                                <label for="employee-company-name"><?php _e('Företagsnamn', 'skradderiforbundet'); ?>*</label>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="c-form-group">
                                <input type="text" id="employee-company-phone" name="employee_company_phone" data-type="required" class="form-control" disabled/>
                                <label for="employee-company-phone"><?php _e('Företagstelefon', 'skradderiforbundet'); ?>*</label>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="c-form-group">
                                <input type="text" id="employee-company-address" name="employee_company_address" data-type="required" class="form-control" disabled/>
                                <label for="employee-company-address"><?php _e('Adress', 'skradderiforbundet'); ?>*</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="c-form-group">
                                <input type="text" id="employee-company-post-number" name="employee_company_post_number" data-type="required" class="form-control" disabled/>
                                <label for="employee-company-post-number"><?php _e('Postnummer', 'skradderiforbundet'); ?>*</label>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="c-form-group">
                                <input type="text" id="employee-company-city" name="employee_company_city" data-type="required" class="form-control" disabled/>
                                <label for="employee-company-city"><?php _e('Stad', 'skradderiforbundet'); ?>*</label>
                            </div>
                        </div>
                    </div>
                    <!-- End .row -->
                </div>
                <!-- End .col-md-12 -->
            </div>
            <!---->

            <!-- ACTIVE TEACHERS -->
            <div class="sk-active-teachers">
                <div class="col-md-4 sk-c-title">
                    <h2><?php _e('Aktiv lärare', 'skradderiforbundet'); ?></h2>

                    <div class="sk-c-title-checkbox">
                        <input type="checkbox" id="sk-active-teacher" class="slide-toggle-form-checkbox" name="has_school" data-toggle="#toggle-active-teachers"/>
                        <label for="sk-active-teacher"></label>
                    </div>
                </div>
                <!-- End .col-md-12 -->
                <div class="clearfix"></div>
                <div class="col-md-12 sk-form-group-gap" hidden id="toggle-active-teachers">
                    <div class="row">
                        <!-- NEW -->
                        <div class="col-sm-6 col-md-4">
                            <div class="c-form-group">
                                <input type="text" id="school" name="school_name" class="form-control" data-type="required" disabled/>
                                <label for="school"><?php _e('Skola', 'skradderiforbundet'); ?>*</label>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="c-form-group">
                                <input type="text" id="telephone_workspace" name="school_phone" class="form-control" data-type="required" disabled/>
                                <label for="telephone_workspace"><?php _e('Telefon arbetsplats', 'skradderiforbundet'); ?>*</label>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="c-form-group">
                                <input type="email" id="school-email" name="school_email" class="form-control" data-type="required" disabled/>
                                <label for="school-email"><?php _e('Email', 'skradderiforbundet'); ?>*</label>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="c-form-group">
                                <input type="text" id="school-address" name="school_address" class="form-control" data-type="required" disabled/>
                                <label for="school-address"><?php _e('Adress', 'skradderiforbundet'); ?>*</label>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="c-form-group">
                                <input type="text" id="school-post-number" name="school_post_number" class="form-control" data-type="required" disabled/>
                                <label for="school-post-number"><?php _e('Postnummer', 'skradderiforbundet'); ?>*</label>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="c-form-group">
                                <input type="text" id="school-ort" name="school_place" class="form-control" data-type="required" disabled/>
                                <label for="school-ort"><?php _e('Stad', 'skradderiforbundet'); ?>*</label>
                            </div>
                        </div>
                    </div>
                    <!-- End .row -->
                </div>
                <!-- End .col-md-12 -->
            </div>
            <!-- End .sk-active-teachers -->

            <!--Other Information-->
            <div class="sk-other-info">
                <div class="col-md-12 sk-form-group-gap">
                    <div class="c-form-group">
                        <textarea id="other-info" name="other_information" class="form-control" placeholder="" rows="6"  style="resize:none"></textarea>
                        <label for="school-ort"><?php _e('Övriga uppgifter du vill åberopa t.ex. anställning, utbildning, och meriter', 'skradderiforbundet'); ?></label>
                    </div>
                </div>
            </div>
            <!---->

            <!-- SUBMIT BUTTON -->
            <div class="col-md-12">
                <div class="form-group">
                    <input type="submit" name="register_member" value="<?php _e('SKICKA ANSÖKAN', 'ev-intranet'); ?>" class="btn-submit"/>
                    <span class="add-member-loader" style="display: none">
                        <i class="fa fa-spinner fa-spin fa-2x"></i>
                    </span>
                </div>
            </div>
            </form>
            </div>
            <!-- .sk-member-register-form -->
            
        </div>
    </main>
    </div>
    <!-- sk-inner-page -->
    </div> <!-- sk-inner-page -->
<?php
endwhile;
wp_reset_postdata(); ?>
</div>
<?php get_footer();
?>
