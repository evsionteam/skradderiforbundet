<?php
get_header();
global $sk_option;
if( ( isset($_GET['action']) && !empty($_GET['action']) ) && ( isset($_GET['key']) && !empty($_GET['key']) ) ){
    $output = '';

    /*member verify*/
    if( 'mr' == $_GET['action'] ){
        $user = get_users(
            array(
                'blog_id' => 2,
                'role' => 'member',
                'meta_key' => 'member_activation_key',
                'meta_value' => $_GET['key']
            )
        );
        if( !empty($user) ){
            if( is_array($user) ){
                $inactive_status = get_user_meta($user[0]->ID,'inactive_member',true);
                if( 1 == $inactive_status ){
                    $local_admin_id = get_user_meta($user[0]->ID,'_local_admin',true);
                    if(!empty($local_admin_id)){
                        $local_admin_data = get_userdata($local_admin_id);
                        if(!empty($local_admin_data)){
                            $local_admin_email = $local_admin_data->data->user_email;
                            if(!empty($local_admin_email)){

                                $aa_key = get_user_meta($user[0]->ID,'admin_activation_key',true);

                                $mail_template = file_get_contents( dirname(__DIR__).'/inc/email-template/admin-verification-email.html' );

                                /*Logo*/
                                global $sk_option ;
                                $logo = isset($sk_option['logo_image']) ? $sk_option['logo_image'] : '';
                                if(!empty($logo)){
                                    $logo = wp_get_attachment_image_url($logo);
                                    $logo = '<img editable="true" src="'.$logo.'" width="75" alt="" border="0" mc:edit="39">';
                                }
                                /**/

                                $mr_header_text = '';

                                $mr_body_text = __('The following user has verified his account.','skradderiforbundet').'<br/>';
                                $mr_body_text .= sprintf(__('Username: %s'), $user[0]->user_login).'<br/><br/>';
                                $mr_body_text .= __('You can find the attachment for detail info.','skradderiforbundet').'<br/><br/>';
                                $mr_body_text .= __('Visit the following link to approve the user.','skradderiforbundet').'<br/>';
                                $mr_body_text .= network_site_url("/verify-account/?action=vm&key=$aa_key").'<br/>';

                                $mr_footer_text = '';

                                $mail_template = str_replace('@logo@', $logo, $mail_template);
                                $mail_template = str_replace('@header_text@', $mr_header_text, $mail_template);
                                $mail_template = str_replace('@body_text@', $mr_body_text, $mail_template);
                                $mail_template = str_replace('@footer_text@', $mr_footer_text, $mail_template);

                                /*Send mail to local admin for approval*/
                                $upload_dir = wp_upload_dir();
                                $pdf = md5($user[0]->data->user_email);
                                $attachment_path = $upload_dir['basedir'].'/sites/2/users/'.$pdf.'.pdf';

                                $attachments = array($attachment_path);

                                $headers = array('Content-Type: text/html; charset=UTF-8','From: Skrädderiförbundet <skradderiforbundet@wordpress.org>');

                                wp_mail($local_admin_email, __('User Verification Email','skradderiforbundet'), $mail_template,$headers,$attachments);
                                /**/

                                /*Send the mail to super admins too*/
                                $super_admins = get_super_admins();
                                if(!empty($super_admins) && is_array($super_admins)){
                                    foreach($super_admins as $super_admin){
                                        $user_info = get_user_by( 'login', $super_admin );
                                        if(!empty($user_info)){
                                            $user_email = $user_info->data->user_email;
                                            if(!empty($user_email)){
                                                wp_mail($user_email, __('User Verification Email','skradderiforbundet'), $mail_template,$headers,$attachments);
                                            }
                                        }
                                    }
                                }
                                /**/

                                $output .= __('Tack för din ansökan.','skradderiforbundet').'<br/><br/>';
                                $output .= __('Den är nu skickad till den förening som du valt.','skradderiforbundet').'<br/><br/>';
                                $output .= __('De kommer att behandla den på deras ordinarie nästkommande styrelsemöte så det är möjligt att det kan dröja ett tag innan du får besked.När du blivit godkänd så kommer du att få ett inbetalningskort med medlemsavgiften. När den är betald så får du inloggningsuppgifter till förbundets hemsida.','skradderiforbundet');
                                delete_user_meta($user[0]->ID,'inactive_member');
                            }else{
                                $output .= __('Sorry, could not send the mail.','skradderiforbundet');
                            }
                        }
                    }
                }else{
                    $output .= __('Your account has already been verified.','skradderiforbundet');
                }
            }
        }else{
            $output .= __('Sorry, your account does not exist.','skradderiforbundet');
        }
    }

    /*admin verify*/
    if('vm' == $_GET['action']){
        $user = get_users(
            array(
                'blog_id' => 2,
                'role' => 'member',
                'meta_key' => 'admin_activation_key',
                'meta_value' => $_GET['key']
            )
        );
        if( !empty($user) ){
            if( is_array($user) ){
                /*only send mail to unapproved user*/
                $user_id = $user[0]->ID;
                $already_approved = get_user_meta($user_id,'approved_member',true);
                if(false == $already_approved){
                    /*Send mail to member after approval*/
                    global $sk_option;

                    $mail_template = file_get_contents( dirname(__DIR__).'/inc/email-template/member-verification-email.html' );

                    $contact_info = $fb_link = $twitter_link = $instagram_link = '';

                    /*Logo*/
                    $logo = $site_logo = $org_name = '';
                    $org_id =  get_user_meta($user_id,'_organization_id',true);
                    if(!empty($org_id)){
                        switch_to_blog(2);
                        $logo = get_post_meta($org_id,'organization_logo',true);
                        $org_name = get_the_title($org_id);

                        $contact_info = get_field('contact_info',$org_id);
                        $fb_link = get_field('facebook_link',$org_id);
                        $twitter_link = get_field('twitter_link',$org_id);
                        $instagram_link = get_field('instagram_link',$org_id);

                        restore_current_blog();
                    }


                    /*site logo*/
                    if( isset($sk_option['logo_image']) && !empty($sk_option['logo_image']) ){
                        $temp = wp_get_attachment_image_src($sk_option['logo_image']);
                        if(!empty($temp)){
                            $site_logo = $temp[0];
                            if(empty($logo)){
                                $logo = $temp[0];
                            }
                        }
                    }
                    /**/

                    $logo = '<img editable="true" src="'.$logo.'" width="120" alt="" border="0" mc:edit="40">';
                    $site_logo = '<img src="'.$site_logo.'" alt="site-logo" style="border-radius:50%;display:block;margin:0;padding:0;" width="50" class="image_target">';
                    /**/

                    $ma_header_text = isset($sk_option['ma_email_header']) ? wpautop($sk_option['ma_email_header']) : '';
                    $ma_header_text = str_replace('@username@', $user[0]->data->display_name, $ma_header_text);

                    $new_password =  wp_generate_password();
                    wp_set_password( $new_password, $user_id );

                    $login_details = sprintf(  __( 'Username: %s', 'skradderiforbundet' ),$user[0]->user_login ).'<br/>';
                    $login_details .= sprintf(  __( 'Password: %s', 'skradderiforbundet' ),$new_password );

                    $ma_body_text = isset($sk_option['ma_email_content']) ? wpautop($sk_option['ma_email_content']) : '';
                    $ma_body_text = str_replace('@login_details@', $login_details , $ma_body_text);
                    $ma_body_text = str_replace('@login_link@', "<a style='color:#000000;font-weight:bold' href='".get_site_url(2).'/login/'."'>".get_site_url(2).'/login/'."</a>" , $ma_body_text);

                    $ma_footer_text = isset($sk_option['ma_email_footer']) ? wpautop($sk_option['ma_email_footer']) : '';

                    $ma_body_text = str_replace('@fb_link@', "<a style='color:#000000;font-weight:bold' href='".$fb_link."'>".__('Facebook','skradderiforbundet')."</a>", $ma_body_text);
                    $ma_body_text = str_replace('@insta_link@', "<a style='color:#000000;font-weight:bold' href='".$instagram_link."'>".__('Instagram','skradderiforbundet')."</a>", $ma_body_text);

                    $mail_template = str_replace('@logo@', $logo, $mail_template);
                    $mail_template = str_replace('@organization_name@', $org_name, $mail_template);
                    $mail_template = str_replace('@header_text@', $ma_header_text, $mail_template);
                    $mail_template = str_replace('@body_text@', $ma_body_text, $mail_template);
                    $mail_template = str_replace('@site_logo@', $site_logo, $mail_template);
                    $mail_template = str_replace('@footer_text@', $ma_footer_text, $mail_template);

                    $fb_icon = get_template_directory_uri().'/assets/src/img/facebook.png';
                    $twitter_icon = get_template_directory_uri().'/assets/src/img/twitter.png';
                    $insta_icon = get_template_directory_uri().'/assets/src/img/instagram.png';

                    $mail_template = str_replace('@contact_info@', $contact_info, $mail_template);
                    $mail_template = str_replace('@fb_link@', $fb_link, $mail_template);
                    $mail_template = str_replace('@fb_icon@', $fb_icon, $mail_template);
                    $mail_template = str_replace('@twitter_link@', $twitter_link, $mail_template);
                    $mail_template = str_replace('@twitter_icon@', $twitter_icon, $mail_template);
                    $mail_template = str_replace('@insta_link@', $instagram_link, $mail_template);
                    $mail_template = str_replace('@insta_icon@', $insta_icon, $mail_template);

                    $upload_dir = wp_upload_dir();
                    $pdf = md5($user[0]->data->user_email);
                    $attachment_path = $upload_dir['basedir'].'/sites/2/users/'.$pdf.'.pdf';

                    $attachments = array($attachment_path);
                    $headers = array('Content-Type: text/html; charset=UTF-8','From: Skrädderiförbundet <skradderiforbundet@wordpress.org>');

                    wp_mail($user[0]->data->user_email, __('Account approved','skradderiforbundet'), $mail_template,$headers,$attachments);
                    /**/

                    $output .= __('Kontot är nu aktiverat och klart.','skradderiforbundet');
                    /*set approved status to true*/
                    update_user_meta($user[0]->ID,'approved_member',true);
                }else{
                    $output .= __('The account has already been activated.','skradderiforbundet');
                }
            }
        }else{
            $output .= __('Sorry, the account does not exist.','skradderiforbundet');
        }
    }?>
   
    <div class="container">
        <div class="verification-mesg"><span><?php echo $output; ?></span> </div>
    </div>
<?php } 
get_footer();