<?php
/* Template Name: Gallery Page */
get_header();
while (have_posts()):the_post();
?>
<div class="sk-gallery-section">
	<div class="container">
		<div class="row sk-gallery-wrapper">
            <?php
            switch_to_blog(2);
            $args = array(
                'post_type' => 'ev_image_gallery',
                'posts_per_page' => -1,
                'post_status' => 'publish',
            );
            $img_gallery = new WP_Query($args);
            if($img_gallery->have_posts()):
                while($img_gallery->have_posts()):$img_gallery->the_post();
                    if(has_post_thumbnail()):
                        $site_link = get_post_meta(get_the_ID(),'site_link',true);
                        $uploaded_by = get_post_meta(get_the_ID(),'submit_as',true);

                        $content = '<div class="modal-footer-content"><h6>'.$uploaded_by.'</h6><a href="'.$site_link.'" target="_blank">'.$site_link.'</a></div>';
                    ?>
                        <div class="col-md-3 col-sm-6 single-gallery-item">
                            <div class="sk-gallery-item">
                                <figure>
                                    <a href="<?php the_post_thumbnail_url();?>" class="gallery-link" data-toggle="lightbox" data-title="" data-gallery="sk-gallery" data-footer='<?php echo $content;?>'>
                                           <?php the_post_thumbnail('sk-archive-img-size',['alt' => get_the_title()]);?>
                                        <figcaption><span><?php the_title();?></span></figcaption>
                                    </a>
                                </figure>
                            </div>
                        </div>
                    <?php
                    endif;
                endwhile;
            endif;
            restore_current_blog();
            ?>
		</div>
	</div>
</div>
<?php
endwhile;
get_footer();
