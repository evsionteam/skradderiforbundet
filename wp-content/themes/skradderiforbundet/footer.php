<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package skradderiforbundet
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-md-3 c-section">
					<?php dynamic_sidebar('footer-1');?>
				</div>
				<div class="col-sm-6 col-md-3 c-section">
					<?php dynamic_sidebar('footer-2');?>
				</div>
				<div class="clearfix visible-sm"></div>
				<div class="col-sm-6 col-md-3 c-section">
					<?php dynamic_sidebar('footer-3');?>
				</div>

				<div class="col-sm-6 col-md-3 c-section">
					<?php dynamic_sidebar('footer-4');?>
				</div>

			</div>
		</div>


	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
