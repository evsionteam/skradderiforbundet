<?php
/**
 * skradderiforbundet functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package skradderiforbundet
 */

//hide admin panel
show_admin_bar(false);

if ( ! function_exists( 'skradderiforbundet_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function skradderiforbundet_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on skradderiforbundet, use a find and replace
	 * to change 'skradderiforbundet' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'skradderiforbundet', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'left_nav' => __( 'Left Navigation', 'skradderiforbundet' ),
		'right_nav' => __( 'Right Navigation', 'skradderiforbundet' )
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

    add_image_size( 'sk-archive-img-size', 415, 220, true );

// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif;
add_action( 'after_setup_theme', 'skradderiforbundet_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function skradderiforbundet_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'skradderiforbundet_content_width', 640 );
}
add_action( 'after_setup_theme', 'skradderiforbundet_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function skradderiforbundet_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'skradderiforbundet' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'skradderiforbundet' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer 1', 'skradderiforbundet' ),
		'id'            => 'footer-1',
		'description'   => esc_html__( 'Add widgets here.', 'skradderiforbundet' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer 2', 'skradderiforbundet' ),
		'id'            => 'footer-2',
		'description'   => esc_html__( 'Add widgets here.', 'skradderiforbundet' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer 3', 'skradderiforbundet' ),
		'id'            => 'footer-3',
		'description'   => esc_html__( 'Add widgets here.', 'skradderiforbundet' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer 4', 'skradderiforbundet' ),
		'id'            => 'footer-4',
		'description'   => esc_html__( 'Add widgets here.', 'skradderiforbundet' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'skradderiforbundet_widgets_init' );

require get_template_directory() . '/inc/admin/posts-restriction.php';

require get_template_directory() . '/inc/template-tags.php';
require get_template_directory() . '/inc/extras.php';
require get_template_directory() . '/inc/script-loader.php';
require get_template_directory() . '/inc/member-register.php';
require get_template_directory() . '/inc/member-pdf/pdf.php';
require get_template_directory() . '/inc/companies-list.php';
require get_template_directory() . '/inc/customizer.php';
require get_template_directory() . '/inc/ajax-load-more.php';
require get_template_directory() . '/inc/ajax-load-words.php';
require get_template_directory() . '/inc/ajax-home-load-more.php';
require get_template_directory() . '/inc/Mobile_Detect.php';

require get_template_directory() . '/inc/post-types/logo-slider.php';
require get_template_directory() . '/inc/post-types/news.php';
require get_template_directory() . '/inc/post-types/slider.php';
require get_template_directory() . '/inc/post-types/home-slider.php';
require get_template_directory() . '/inc/post-types/course.php';
require get_template_directory() . '/inc/post-types/world-congress.php';
require get_template_directory() . '/inc/post-types/exhibitions.php';
require get_template_directory() . '/inc/post-types/dictionary.php';
require get_template_directory() . '/inc/post-types/modemuseum.php';

require get_template_directory() . '/inc/about-meta.php';
require get_template_directory() . '/inc/lexikonet-meta.php';

require get_template_directory() . '/inc/post-expirator.php';
require get_template_directory() . '/inc/post-order-meta.php';

// Add class in body
add_filter('body_class', 'remove_overflow_hidden_class');
function remove_overflow_hidden_class($classes) {
        $classes[] = 'overflow-hidden';
        return $classes;
}

/*excerpt support for page*/
function sk_add_excerpt_support_for_pages() {
    add_post_type_support( 'page', 'excerpt' );
}
add_action( 'init', 'sk_add_excerpt_support_for_pages' );

/*Member verification template*/
add_action('init','sk_member_verification_template',20 );
function sk_member_verification_template(){
    if (strpos($_SERVER['REQUEST_URI'], 'verify-account') !== false) {
        $load = locate_template('page-templates/template-verify.php', true);
        if ($load) {
            exit();
        }
    }
};

/*Company Listing template*/
add_action('init','sk_company_listing_template',20 );
function sk_company_listing_template(){
    if (strpos($_SERVER['REQUEST_URI'], 'hitta-skradderi') !== false) {
        if (strpos($_SERVER['REQUEST_URI'], 'q') !== false) {
            $load = locate_template('page-templates/template-company-details.php', true);
            if ($load) {
                exit();
            }
        }
    }
};

add_action('init','sk_customizer_return' );
function sk_customizer_return(){
	global $sk_option ;
	$sk_option = get_theme_mod('sk_option');
}


function sk_posts_filter_query( $query ){

    if(is_admin() || !$query->is_main_query()){
        return $query;
    }

    if( ($query->is_archive( 'sk_news' ) ) || ($query->is_archive( 'ev_exhibitions' ) ) ){

        $query->set('posts_per_page',6);

        /*date filter*/
        if(isset($_GET['date']) && !empty($_GET['date'])){
            $date_values = explode('-',$_GET['date']);
            if( !empty($date_values) && is_array($date_values) ){
                $year = $date_values[0];
                $month = $date_values[1];
                $date_query = array(
                    array(
                        'year'  => $year,
                        'month' => $month,
                    ),
                );
                $query->set( 'date_query', $date_query );
            }
        }
        /**/

        /*search filter*/
        if(isset($_GET['search']) && !empty($_GET['search'])){
            $query->set( 's', $_GET['search'] );
        }
        /**/


        /*Order By featured Post*/
        //$query->set( 'meta_key', '_is_featured_post' );
        //$query->set( 'orderby', 'meta_value_num' );
        //$query->set( 'order', 'DESC' );
        /**/

        /*Ignore Expired Posts*/
        $meta_query = array(
            'relation' => 'OR',
            array(
                'key'     => 'expires_in',
                'value'   => '',
                'compare' => '=',
            ),
            array(
                'key'     => 'expires_in',
                'compare' => 'NOT EXISTS',
            ),
            array(
                'key'     => 'expires_in',
                'value'   => date("Y-m-d"),
                'type'    => 'DATE',
                'compare' => '>',
            ),
        );
        $query->set( 'meta_query', $meta_query );
        /**/

	}

	if( ($query->is_archive( 'sk_course' ) ) && ( isset($_GET['course_category']) && !empty($_GET['course_category']) ) ){
	     $taxquery = array(
	        array(
	            'taxonomy' => 'course_cat',
	            'field' => 'slug',
	            'terms' => $_GET['course_category']
	        )
	    );
	    $query->set( 'tax_query', $taxquery );
	}
}
add_action( 'pre_get_posts', 'sk_posts_filter_query' );

/*Custom query to get the post type archive dates*/
function sk_get_post_type_archive_dates($post_type = 'post'){

    $archive_dates = array();
    global $wpdb, $wp_locale;

    $archive_date_query = "SELECT YEAR(post_date) AS `year`, MONTH(post_date) AS `month`, count(ID) as posts FROM $wpdb->posts  WHERE post_type = '$post_type' AND post_status = 'publish' GROUP BY YEAR(post_date), MONTH(post_date) ORDER BY post_date DESC ";
    $archive_date_results = $wpdb->get_results( $archive_date_query );

    if(!empty($archive_date_results)){
        foreach ( (array) $archive_date_results as $result ) {
            $text = sprintf( __( '%1$s %2$d' ), $wp_locale->get_month( $result->month ), $result->year );
            $archive_dates[$result->year.'-'.$result->month] = $text;
        }
    }

    return $archive_dates;
}

/*Remove Default Post types*/
add_action('admin_menu','sk_remove_default_post_type');
function sk_remove_default_post_type() {
    remove_menu_page('edit.php');
}

/*Send me the mail copy for contact form 7*/
add_filter( 'wpcf7_additional_mail', 'sk_wpcf7_send_mail_to_self', 10, 2 );
function sk_wpcf7_send_mail_to_self( $additional_mail, $cf ) {
    $cf = WPCF7_Submission::get_instance();
    $posted_data = $cf->get_posted_data();
    $send_me_copy = $posted_data['sendmecopy'];
    if(is_array($send_me_copy)){
        foreach ($send_me_copy as $key => $value) {
            if (empty($value)) {
                unset($send_me_copy[$key]);
            }
        }
    }

    if (empty($send_me_copy)) {
        $additional_mail = array();
    }

    return $additional_mail;
}
/**/

/*Dynamic email address for contact form 7*/
/*make sure to create [hidden contact-form-email id:sk-contact-form-email] in the form and the button for popup like*/
/* <button type="button" class="btn-submit contact-page-form" data-toggle="modal" data-target="#contact-form" data-email="cloudrajup@gmail.com">Kontakta oss</button>*/
add_action('wpcf7_before_send_mail', 'sk_wpcf7_before_send_mail');
function sk_wpcf7_before_send_mail($form){

    if (!isset($_POST['contact-form-email'])) {
        return;
    }

    $new_mail_address = $_POST['contact-form-email'];
    $properties = $form->get_properties();
    $properties['mail']['recipient'] = $new_mail_address;
    $form->set_properties($properties);
}
/**/


function get_image_sizes() {
    global $_wp_additional_image_sizes;

    $sizes = array();

    foreach ( get_intermediate_image_sizes() as $_size ) {
        if ( in_array( $_size, array('thumbnail', 'medium', 'medium_large', 'large') ) ) {
            $sizes[ $_size ]['width']  = get_option( "{$_size}_size_w" );
            $sizes[ $_size ]['height'] = get_option( "{$_size}_size_h" );
            $sizes[ $_size ]['crop']   = (bool) get_option( "{$_size}_crop" );
        } elseif ( isset( $_wp_additional_image_sizes[ $_size ] ) ) {
            $sizes[ $_size ] = array(
                'width'  => $_wp_additional_image_sizes[ $_size ]['width'],
                'height' => $_wp_additional_image_sizes[ $_size ]['height'],
                'crop'   => $_wp_additional_image_sizes[ $_size ]['crop'],
            );
        }
    }

    return $sizes;
}

function get_image_size( $size ) {
    $sizes = get_image_sizes();

    if ( isset( $sizes[ $size ] ) ) {
        return $sizes[ $size ];
    }

    return false;
}

/* Mandrill SMTP */
//add_action( 'phpmailer_init', 'sk_phpmailer_settings' );
function sk_phpmailer_settings( $phpmailer ) {
    $phpmailer->isSMTP();
    $phpmailer->Host = 'smtp.mandrillapp.com';
    $phpmailer->SMTPAuth = true; // Force it to use Username and Password to authenticate
    $phpmailer->Port = 587;
    $phpmailer->Username = 'Skradderiforbundet';
    $phpmailer->Password = 'm8g9unWW5MIoKx9ml9DbTQ';
    //$phpmailer->From = "abc<wordpress@202.166.198.46>";
    //$phpmailer->FromName = "Skradderiforbundet";
}

/*Change mail headers to take html for email*/
//add_filter( 'wp_mail', 'my_wp_mail_filter2' );
function my_wp_mail_filter2( $args ) {
    $args['headers'] = array('content-type: text/html; charset=UTF-8','From: Skrädderiförbundet <skradderiforbundet@wordpress.org>');
    return $args;
}


add_filter( 'wp_mail_content_type','sk_wp_mail_filter' );
function sk_wp_mail_filter( $content_type ) {
    return 'text/html; charset=UTF-8';
}


/*Change default loss password template*/
add_filter( 'retrieve_password_message', 'sk_change_lost_pass_template',10,4 );
function sk_change_lost_pass_template($message, $key, $user_login, $user_data){

    $original_message = nl2br(htmlentities($message));

    $message = file_get_contents( __DIR__.'/inc/email-template/password-lost-email.html' );

    $logo = '';
    global $sk_option;
    if( isset($sk_option['logo_image']) && !empty($sk_option['logo_image']) ){
        $temp = wp_get_attachment_image_src($sk_option['logo_image'],'full');
        if(!empty($temp)){
            if(empty($logo)){
                $logo = $temp[0];
            }
        }
    }
    $logo = '<img editable="true" src="'.$logo.'" width="120" alt="" border="0" mc:edit="40">';

    $message = str_replace('@logo@', $logo, $message);
    $message = str_replace( '@body_text@'  ,$original_message , $message);

    return $message;
}

//check if role exist before removing it. Remove member role
/*if( get_role('local_admin') ){
    remove_role( 'local_admin' );
}*/

/*add new role member*/
/*$result = add_role(
    'local_admin',
    __( 'Local Admin' ),
    array(
        'upload_files' => true,
        'edit_posts' => true,
        'delete_posts' => true,
        'read' => true,
    )
);
if ( null !== $result ) {
    echo 'Yay! New role created!';
}
else {
    echo 'Oh... the role already exists.';
}
die;*/