<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package skradderiforbundet
 */

get_header(); 
$course_category = '';
if(isset($_GET['course_category'])){ 
    $course_category = $_GET['course_category'];
} 
?>

<div class="sk-inner-page">

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		  <div class="sk-news-wrapper clearfix">
			<?php
	          $news_category_args = array(
	            'hide_empty' => 0,
                'taxonomy' => 'course_cat',
	        );
	        $news_taxonomy = get_terms($news_category_args); ?>

	        <?php $args = array(
				'type'            => 'monthly',
				'limit'           => '',
				'format'          => 'option', 
				'before'          => '',
				'after'           => '',
				'show_post_count' => false,
				'echo'            => 1,
				'order'           => 'DESC',
			    'post_type'     => 'sk_course'
			);
		?>
				<div class="sk-title-n-filter clearfix">
					<div class="col-md-12">
						<h1 class="visible-xs">News</h1>
						<div class="category-filter">
                    		<form action="" method="get">

								<div class="sk-form-group hidden-xs">
									<select name="course_category" class = "term-select">
										<option value="">Kategori</option>
										<?php foreach ($news_taxonomy as $category) { ?>
	                                <option value="<?php echo $category->slug; ?>" <?php selected( $category->slug, $course_category ); ?>><?php echo $category->name ?></option>
	                            <?php } ?>
									</select>
								</div><!-- .sk-form-group -->

								<div class="sk-form-group">
									<select name="date" class = "date-select">
										<option value="">Datum</option>
  											<?php wp_get_archives($args); ?>
									</select>

								</div><!-- .sk-form-group -->
							</form>
						</div>
				</div><!-- .col-md-12 -->
			</div><!-- .sk-title-n-filter -->

				<div class="col-md-12">
					<?php
					if ( have_posts() ) : ?>

					<?php
					/* Start the Loop */
					while ( have_posts() ) : the_post();

					/*
					* Include the Post-Format-specific template for the content.
					* If you want to override this in a child theme, then include a file
					* called content-___.php (where ___ is the Post Format name) and that will be used instead.
					*/
					get_template_part( 'template-parts/content', get_post_format() );

				endwhile;

				the_posts_navigation();

					else :

						get_template_part( 'template-parts/content', 'none' );

					endif; ?>

				</div><!-- .col-md-12 -->
			</div> <!-- .sk-new -->

			<div class="clearfix"></div>
		  <div id="sk-load-more-news" class = "sk-news-load-more">
    <div class="sk-arrow-n-text">
      <a href="#">
        <span><?php _e('ALDRE ART KLAR','skradderiforbundet')?></span>
        <i class="fa fa-angle-down fa-3x"></i>
      </a>
      <span class = "load-more-spinner">
        <i class="fa fa-spinner fa-spin"></i>
      </span>
    </div>
  </div><!-- #sk-load-more-news -->

		</main><!-- #main -->
	</div><!-- #primary -->

</div><!-- .inner-page news-archive -->

<?php
//get_sidebar();
get_footer();
