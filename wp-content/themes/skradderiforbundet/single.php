<?php
get_header(); ?>
    <div class="sk-c-double-section">
        <div class="container">
            <div class="col-md-12">
                <div id="primary" class="content-area">
                    <main id="main" class="site-main" role="main">
                        <?php
                        while (have_posts()) : the_post();
                            get_template_part('template-parts/content', get_post_format());
                            the_post_navigation();
                        endwhile; // End of the loop.
                        ?>
                        <div class="sk-archive-link">
                            <?php
                            $post_type = get_post_type(get_the_ID());
                            if ( 'ev_world_congress' != $post_type ) {
                                $post_type_obj = get_post_type_object($post_type);
                                ?>
                                <a class="see-all" href="<?php echo get_post_type_archive_link($post_type); ?>">
                                    <?php printf(esc_html__('Se alla %s', 'skradderiforbundet'), $post_type_obj->label); ?>
                                </a>
                            <?php } ?>
                        </div>
                    </main>
                </div>
            </div>
        </div>
    </div>
<?php
get_footer();
