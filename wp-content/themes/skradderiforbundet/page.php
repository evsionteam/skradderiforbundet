<?php
get_header();
while (have_posts()):the_post();
    ?>
    <div class="sk-inner-page about-us courses">
        <div class="sk-inner-banner" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>)">
            <div class="sk-letter">
               <?php the_field('background_letter');?>
            </div>
            <div class="c-article">
                <div class="sk-title-des">
                    <h1 class="sk-title"><?php the_title(); ?></h1>
                    <div class="sk-description">
                        <?php the_excerpt(); ?>
                    </div>
                </div>
            </div>
            <a href="" class="sk-scroll-down" data-target="#content-section" data-animation-duration="500">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/build/img/sk-down-arrow.png" alt="down-arrow"/>
            </a>
        </div>
        <div class="clearfix"></div>
        <div id="content-section" class="container">
            <div class="sk-inner-intro">
                <?php the_content(); ?>
            </div>
        </div>
    </div>
<?php endwhile;
get_footer(); ?>
