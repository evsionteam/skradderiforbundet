<?php
    if (strpos($_SERVER['REQUEST_URI'], 'wp-activate') === false) {
        if( !is_user_logged_in() && !is_page('login')){
            $redirect_url = get_site_url().'/login';
            wp_redirect($redirect_url);
            exit;
        }
    }
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <?php
    wp_head();
    global $ev_intranet;
    ?>
</head>
<body <?php body_class(); ?>>
<div id="page" class="site">
    <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'ev-intranet' ); ?></a>
    <header id="masthead" class="site-header" role="banner">
        <div class="above-container">
            <div class="row">
                <div class="col-xs-6 col-md-2">
                    <div class="site-branding">
                        <a class="site-logo" href="<?php echo site_url();?>">
                            <img src="<?php echo $ev_intranet['logo-image']['url'];?>" alt="site-logo"/>
                        </a>
                    </div><!-- .site-branding -->
                </div><!-- col-xs-6 -->
                <div class="col-xs-6 col-md-10">
                    <div class="nav-wrap">
                        <a class="mobile-menu" href="#menu"><i class="fa fa-bars"></i></a>
                        <nav id="site-navigation" class="main-navigation" role="navigation">
                            
                                <?php wp_nav_menu( array( 'theme_location' => 'primary' )); ?>
                                
                        
                        </nav><!-- #site-navigation -->
                        <div class="staff-login-menu">
                            <a class="user-icon" href="#"><i class="fa fa-user-circle" aria-hidden="true"></i></a>
                            <?php wp_nav_menu( array( 'theme_location' => 'login_menu',  'container' => false, 'menu_class' => 'login-menu' ) ); ?>
                        </div>
                    </div>
                </div>

            </div>
            
        </div><!-- .container -->
    </header><!-- #masthead -->
    <nav id="menu" style="display:none">
        <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'mobile-menu' ) ); ?>
    </nav>
    <div id="content" class="site-content">