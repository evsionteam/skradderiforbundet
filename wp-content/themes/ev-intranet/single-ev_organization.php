<?php
get_header();
while(have_posts()):the_post();
?>

<div id="primary" class="content-area main-content">
    <main id="main" class="site-main" role="main">
        <?php get_template_part('template-parts/content','inner');?>
        <div class="above-container">
            <div class="row">
                <?php require get_template_directory() . '/inc/breadcrumbs.php'; ?>
            </div>
            <div class="c-row">
                <?php
                $ppp = '';
                $found_posts = 0 ;
                $organization_admin = get_post_meta(get_the_ID(),'_organization_admin',true);
                if(!empty($organization_admin)):
                ?>
                    <?php
                    if ( strpos($_SERVER['REQUEST_URI'], "list") !== false){
                        $wrapper_class = 'news-grid-wrapper new-class';
                    }else{
                        $wrapper_class = 'news-grid-wrapper';
                    }
                    ?>
                    <div class="<?php echo $wrapper_class;?>">
                        <?php
                        global $wp_query;
                        $ppp = 10;

                        $args = array(
                            'post_type' => array( 'ev_articles', 'ev_news', 'ev_courses','ev_exhibitions' ),
                            'post_status' => 'publish',
                            'posts_per_page' => $ppp,
                            'author' => $organization_admin,
                        );

                        if( isset($_GET['search']) && !empty($_GET['search']) ){
                            $args['s'] =  $_GET['search'];
                        }

                        $query = new WP_Query($args);
                        $found_posts = $query->found_posts;

                        $load_text = ($found_posts > $ppp) ? 'Load More' : 'Inga fler inlägg';
                        $load_class = ($found_posts > $ppp) ? 'posts-exists' : 'no-posts';

                        while ( $query->have_posts() ) : $query->the_post();
                            item_load_posts();
                        endwhile;wp_reset_postdata();
                        ?>
                    </div>
                <?php endif;?>
            </div><!--/.row -->
            <?php if(!empty($organization_admin) && $found_posts > $ppp):?>
                <div id="load-more-posts" class="organization-page-load-more" data-id="<?php echo get_queried_object_id();?>">
                    <div class="container">
                        <div id="loading-text" class="<?php echo $load_class;?>">
                        <a href="#">
                            <span class="loader-text"><?php echo $load_text;?></span>
                        </a>
                        <span class="rosen-loader" style="display:none">
                            <i class="fa fa-spinner fa-spin fa-2x"></i>
                        </span> <!-- rosen-loader -->
                        </div>
                    </div><!-- container -->
                </div><!-- load-more-posts-news-->
            <?php endif;?>
        </div><!-- container -->
    </main><!-- #main -->
</div><!-- #primary -->
<?php
endwhile;get_footer('organization');
