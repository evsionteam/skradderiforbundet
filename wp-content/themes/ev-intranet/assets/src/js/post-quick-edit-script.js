/*
 * Post Bulk Edit Script
 * Hooks into the inline post editor functionality to extend it to our custom metadata
 */
jQuery(document).ready(function($){
    /*Prepopulating our quick-edit post info*/
    var $inline_editor = inlineEditPost.edit;
    inlineEditPost.edit = function(id){

        /*call old copy*/
        $inline_editor.apply( this, arguments);

        /*our custom functionality below*/
        var post_id = 0;
        if( typeof(id) == 'object'){
            post_id = parseInt(this.getId(id));
        }

        /*if we have our post*/
        if(post_id != 0){
            /*find our row*/
            var $row = $('#edit-' + post_id);
            /*get the order number*/
            var $order = $( '#sk_post_order-' + post_id ).text();
            /*post the order*/
            $row.find('input[name="sk_post_order"]').val($order);
        }
    }
});