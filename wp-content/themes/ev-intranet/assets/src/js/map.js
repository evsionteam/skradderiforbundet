var map;
var marker;

function initialize() {
    var mapOptions = {
        center: new google.maps.LatLng(59.329323,18.068581),
        zoom: 16,
        mapTypeId: 'roadmap'
    };

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    createMarker(59.329323,18.068581);

    /*Detects click on Map*/
    google.maps.event.addListener(map, "click", function(event) {

        /*Get lat lng coordinates*/
        var lat = event.latLng.lat().toFixed(6);
        var lng = event.latLng.lng().toFixed(6);

        /*Call createMarker() function to create a marker on the map*/
        createMarker(lat, lng);

        /*getCoords() function inserts lat and lng values into text boxes*/
        getCoords(lat, lng);

    });
    /**/
}

if( jQuery('#map-canvas').length ){
    google.maps.event.addDomListener(window, 'load', initialize);
}


/*Creates the marker*/
function createMarker(lat, lng) {

    /*If marker exist already clear the marker.*/
    if (marker) {
        marker.setMap(null);
        marker = "";
    }

    /*Set marker variable with new location*/
    marker = new google.maps.Marker({
        position: new google.maps.LatLng(lat, lng),
        draggable: true,
        map: map
    });
    marker.position = marker.getPosition();

    /*center the map*/
    map.panTo(marker.getPosition());

    /*This event detects the drag movement of the marker.The event is fired when left button is released.*/
    google.maps.event.addListener(marker, 'dragend', function() {

        /*Updates lat and lng position of the marker.*/
        marker.position = marker.getPosition();

        /*Get lat and lng coordinates.*/
        var lat = marker.position.lat().toFixed(6);
        var lng = marker.position.lng().toFixed(6);

        /*Update lat and lng values into text boxes.*/
        getCoords(lat, lng);

    });
}

/*Update text box lat and lng value*/
function getCoords(lat, lng) {
    var coords_lat = document.getElementById('latitude');
    coords_lat.value = lat;
    var coords_lng = document.getElementById('longitude');
    coords_lng.value = lng;
}

/*Update map on lat long textfield change too*/
jQuery('#longitude').on('input',function(){
    var lng = jQuery(this).val();
    var lat = jQuery('#latitude').val();
    if( lat && lng ){
        createMarker(lat,lng);
    }
});
jQuery('#latitude').on('input',function(){
    var lat = jQuery(this).val();
    var lng = jQuery('#longitude').val();
    if( lat && lng ){
        createMarker(lat,lng);
    }
});
/**/