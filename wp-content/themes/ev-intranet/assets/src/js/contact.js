jQuery(document).ready(function($){
    jQuery('#ev_contact_form').on('submit', function(e){
        e.preventDefault();

        var $this = jQuery(this),
            nonce = $this.find('#contact_form_nonce').val(),
            formdata = false;

        var form_values = jQuery(this).serialize();

        formdata = new FormData();

        var files_data = jQuery('#attachment');
        jQuery.each(jQuery(files_data), function(i, obj) {
            jQuery.each(obj.files, function(j, file) {
                formdata.append('files[' + j + ']', file);
            });
        });

        formdata.append('form_values', form_values);
        formdata.append('action', 'ev_contact_form_message');
        formdata.append('nonce', nonce);

        jQuery.ajax({
            url: evAjax.ajaxurl,
            type: 'POST',
            data: formdata,
            dataType: 'json',
            processData: false,
            contentType: false,
            beforeSend: function() {
                jQuery('.contact-form-loader').css('display','inline-block');
                jQuery('.contact-message').empty();
            },
            success: function(data) {
                jQuery('.contact-form-loader').css('display','none');
                if (data.status) {
                    jQuery('.contact-message').html('<div class="alert alert-success">' + data.message + '</div>');
                } else {
                    jQuery('.contact-message').html('<div class="alert alert-danger">' + data.message +'</div>');
                }
                var headerHeight = $(".site-header").height();
                jQuery('html, body').animate({
                    scrollTop: $(".contact-form-wrapper").offset().top - headerHeight
                }, 1000);
            }
        });

    });
});