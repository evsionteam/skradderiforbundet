jQuery(document).ready(function($){

    var on_load = 'yes';
    jQuery("#category").on('change',function(e){
        var cat = jQuery(this).val();
        var posts_cats = jQuery(this).attr('data-cats');
        if(cat){
            jQuery.ajax({
                url: evAjax.ajaxurl,
                type: 'POST',
                data : {
                    action : 'ev_get_sub_cats',
                    cat: cat
                },
                dataType: 'json',
                beforeSend: function() {
                    jQuery('.load-more-spinner').css('display','inline');
                },
                success: function(response) {
                    jQuery('.load-more-spinner').css('display','none');
                    jQuery('#sub-cats').html('').selectpicker('refresh');
                    if( true == response.success ){
                        if(response.data){
                            jQuery.each( response.data, function( index, value ){
                                var selected = '';
                                if ( 'yes' == on_load && posts_cats.indexOf(index) > -1){
                                    selected = 'selected="selected"';
                                }
                                jQuery('#sub-cats').append('<option value="'+index+'" '+selected+'>'+value+'</option>').selectpicker('refresh');
                            });
                        }
                    }
                    on_load = 'no';
                }
            });
        }else{
            jQuery('#sub-cats').html('').selectpicker('refresh');
        }
    });

    jQuery('#upload_company_form').on('submit', function(e){
        e.preventDefault();

        var $this = jQuery(this),
            nonce = $this.find('#company_submit_nonce').val(),
            formdata = false;

        var form_values = jQuery(this).serialize();

        formdata = new FormData();

        var files_data = jQuery('#file-image');
        jQuery.each(jQuery(files_data), function(i, obj) {
            jQuery.each(obj.files, function(j, file) {
                formdata.append('files[' + j + ']', file);
            });
        });

        var form_action = jQuery(this).attr('data-action');
        formdata.append('form_values', form_values);
        if('update' == form_action){
            formdata.append('action', 'ev_update_company');
        }else{
            formdata.append('action', 'ev_upload_company');
        }

        formdata.append('nonce', nonce);

        jQuery.ajax({
            url: evAjax.ajaxurl,
            type: 'POST',
            data: formdata,
            dataType: 'json',
            processData: false,
            contentType: false,
            beforeSend: function() {
                jQuery('.add-company-loader').css('display','inline-block');
                jQuery('.company-message').empty();
            },
            success: function(data) {
                jQuery('.add-company-loader').css('display','none');
                if (data.status) {
                    jQuery('.company-message').html('<div class="alert alert-success">' + data.message + '</div>');
                } else {
                    jQuery('.company-message').html('<div class="alert alert-danger">' + data.message +'</div>');
                }
                var headerHeight = $(".site-header").height();
                jQuery('html, body').animate({
                    scrollTop: $(".company-upload-form-wrapper").offset().top - headerHeight
                }, 1000);
            }
        });

    });
});

jQuery(window).load( function(){
    jQuery("#category").trigger('change');
});