jQuery(document).ready(function($){
    /*Image upload*/
    jQuery('#upload_form').on('submit', function(e){
        e.preventDefault();

        var $this = jQuery(this),
            nonce = $this.find('#image_submit_nonce').val(),
            images_wrap = jQuery('#images_wrap'),
            status = jQuery('#status'),
            loader = jQuery('.add-image-loader'),
            formdata = false;

        if ( $this.find('#file-image').val() == '' ) {
            alert('Please select an image to upload.');
            return;
        }

        formdata = new FormData();

        var files_data = jQuery('#file-image');

        jQuery.each(jQuery(files_data), function(i, obj) {
            jQuery.each(obj.files, function(j, file) {
                formdata.append('files[' + j + ']', file);
            });
        });

        var post_title = jQuery('#post_title').val();
        formdata.append('post_title', post_title);

        var site_link = jQuery('#site-link').val();
        formdata.append('site_link', site_link);

        var submit_as = jQuery('#submit-as').val();
        formdata.append('submit_as', submit_as);

        /*Our AJAX identifier*/
        formdata.append('action', 'ev_upload_image');

        formdata.append('nonce', nonce);

        jQuery.ajax({
            url: evAjax.ajaxurl,
            type: 'POST',
            data: formdata,
            dataType: 'json',
            processData: false,
            contentType: false,
            beforeSend: function() {
                status.text('');
                loader.css('display','inline-block');
            },
            success: function(data) {
                loader.css('display','none');
                if (data.status) {
                    images_wrap.append(data.message);
                    status.fadeIn().html('<div class="alert alert-success">Image Uploaded Successfully.</div>');
                } else {
                    status.fadeIn().html('<div class="alert alert-danger">' + data.message +'</div>');
                }
            }
        });

    });
    /**/

    /*Remove Image*/
    jQuery('.remove-image').on('click',function(e){

        e.preventDefault();

        var _that = jQuery(this),
            post_id = jQuery(this).attr('data-id'),
            nonce = jQuery(this).attr('data-nonce'),
            loader = jQuery(this).closest('.member-image').find('.remove-image-loader'),
            status = jQuery('#status');

        if(post_id){
            jQuery.ajax({
                type : 'post',
                url : evAjax.ajaxurl,
                data : {
                    action : 'remove_image',
                    post_id : post_id,
                    nonce : nonce
                },
                dataType:'json',
                beforeSend: function() {
                    _that.closest('.member-image').css('opacity',0.5);
                    loader.css('display','inline-block');
                },
                success : function( data ) {
                    loader.css('display','none');
                    if (data.status) {
                        _that.closest('.member-image').remove();
                        status.fadeIn().html('<div class="alert alert-success">' + data.message +'</div>');
                    } else {
                        status.fadeIn().html('<div class="alert alert-danger">' + data.message +'</div>');
                    }
                }
            });
        }
    });
    /**/
});
