jQuery(document).ready(function(){

    var page = 1;
    var offset_multiplier = 10;
    var loading_from;
    var org_id;

    var $grid = jQuery('body .news-grid-wrapper').imagesLoaded( function() {
        $grid.masonry({
            columnWidth: 1,
            itemSelector: '.grid-item'
        });
    });

    /*Get search values*/
    var search = getParameterByName('q');
    if(!search){
        search = getParameterByName('search');
    }

    jQuery('#load-more-posts a').on('click',function(e){
        e.preventDefault();

        if(jQuery('.home-page-load-more').length){
            loading_from = 'home';
        }

        if(jQuery('.news-page-load-more').length){
            loading_from = 'news';
        }

        if(jQuery('.article-page-load-more').length){
            loading_from = 'article';
        }

        if(jQuery('.organization-page-load-more').length){
            loading_from = 'organization';
            org_id = jQuery('.organization-page-load-more').attr('data-id');
        }

        if(jQuery('.course-page-load-more').length){
            loading_from = 'course';
            offset_multiplier = 9;
        }

        if(jQuery('.exhibition-page-load-more').length){
            loading_from = 'exhibition';
            offset_multiplier = 9;
        }

        if(jQuery('.congress-page-load-more').length){
            loading_from = 'congress';
            offset_multiplier = 9;
        }

        jQuery.ajax({
            type : 'post',
            url : evAjax.ajaxurl,
            data : {
                action : 'load_more_posts',
                loading_from : loading_from,
                offset: (page * offset_multiplier),
                url: window.location.href,
                search: search,
                org_id: org_id
            },
            dataType:'json',
            beforeSend: function() {
                jQuery('.rosen-loader').css('display','inline-block');
            },
            success : function( response ) {
                page++;
                if(true == response.more_post){
                    if('course' == loading_from){
                        jQuery('.category-section').append(response.data);
                    }else{
                        var $content = jQuery( response.data.join(''));
                        jQuery('body .news-grid-wrapper').imagesLoaded( function() {
                            jQuery('body .news-grid-wrapper').append( $content ).masonry( 'appended', $content );
                        });
                    }
                }
                else{
                    jQuery('#loading-text').html( response.data );
                }
                jQuery('.rosen-loader').hide();
            }
        });
    });
});

function getParameterByName(name, url) {
    if (!url) {
        url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}