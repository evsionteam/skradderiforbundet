jQuery(document).ready(function($){
    jQuery('#edit_member_form').on('submit', function(e){
        e.preventDefault();

        var $this = jQuery(this),
            nonce = $this.find('#edit_member_nonce').val(),
            form_values = jQuery(this).serialize();

        jQuery.ajax({
            url: evAjax.ajaxurl,
            type: 'POST',
            data: {
                action : 'update_member_profile',
                nonce : nonce,
                form_data : form_values
            },
            dataType: 'json',
            beforeSend: function() {
                jQuery('.update-member-loader').css('display','inline-block');
                jQuery('.member-message').empty();
            },
            success: function(data) {
                jQuery('.update-member-loader').css('display','none');
                if (data.status) {
                    jQuery('.member-message').html('<div class="alert alert-success">' + data.message + '</div>');
                } else {
                    jQuery('.member-message').html('<div class="alert alert-danger">' + data.message +'</div>');
                }
                var headerHeight = $(".site-header").height();
                jQuery('html, body').animate({
                    scrollTop: $(".member-profile").offset().top - headerHeight
                }, 1000);
            }
        });

    });
});