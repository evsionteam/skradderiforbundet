jQuery(document).ready(function($){
    $('#role').on('change',function(){
        if($('#noconfirmation').is(':checked')){
            var role = $(this).val();
            if( 'local_admin' === role || 'member' === role){
                $('.organization-info-table').css('display','block');
                }
            else{
                $('.organization-info-table').css('display','none');
            }
        }
    });
    $('#noconfirmation').click(function(){
        if (this.checked) {
            var role = $('#role').val();
            if( 'local_admin' === role || 'member' === role){
                $('.organization-info-table').css('display','block');
            }
        }
        else{
            $('.organization-info-table').css('display','none');
        }
    });

    var role = $('#role').val();
    if('local_admin' === role || 'member' === role){
        $('.organization-info-table').css('display','block');
    }
    else{
        $('.organization-info-table').css('display','none');
    }


    /*Approve member*/
    jQuery('.member-status').on('click',function(e){
        e.preventDefault();
        var self = jQuery(this);
        var user_id = self.data('uid');
        jQuery.ajax({
            type : 'post',
            url : evAdminAjax.ajaxurl,
            data : {
                action : 'ev_set_member_status',
                id : user_id
            },
            beforeSend: function() {
                self.find('.loading-img').css('display','inline');
            },
            success : function( response ) {
                self.find('.loading-img').css('display','none');
                self.text('Approved');
            }
        });
    });
    /**/
});