jQuery(document).ready(function($) {

    //For image upload
    // Instantiates the variable that holds the media library frame.
    var ev_intranet_image_frame;

    // Runs when the image button is clicked.
    $('#upload_image').on('click',function(e){

        // Prevents the default action from occuring.
        e.preventDefault();

        // If the frame already exists, re-open it.
        if ( ev_intranet_image_frame ) {
            ev_intranet_image_frame.open();
            return;
        }

        // Sets up the media library frame
        ev_intranet_image_frame = wp.media.frames.ev_intranet_image_frame = wp.media({
            title: meta_image.title,
            button: { text:  'Use this Image' },
            library: { type: 'image' }
        });

        // Runs when an image is selected.
        ev_intranet_image_frame.on('select', function(){

            // Grabs the attachment selection and creates a JSON representation of the model.
            var media_attachment = ev_intranet_image_frame.state().get('selection').first().toJSON();

            // Sends the attachment URL to our custom image input field.
            $('#org-logo').val(media_attachment.url);
            $('#image_preview p').html('<img style="max-width:100px;" src="'+media_attachment.url+'">');
        });

        // Opens the media library frame.
        ev_intranet_image_frame.open();
    });

    $('#delete_image').on('click',function(){
        $('#org-logo').attr('value','');
        $('#image_preview p').empty();
    });
});