
//binding uploadFile function to the window.. here 'this' is the window
this.uploadFile = function( target ){
    document.getElementById("file-name").innerHTML = target.files[0].name;
};

jQuery(document).ready(function($){
	jQuery(".image_description").remove();
	jQuery(".clear").remove();
	
    var images = document.getElementsByTagName("img");
    DisableToolTip(images);

    var source = document.getElementsByTagName("source");
    DisableToolTip(source);
	
   
	


    var modalLayout  = "";

    modalLayout     += "        <div class='modal fade' tabindex='-1' role='dialog' aria-labelledby='localVideoModalLabel'>";
    modalLayout     += "            <div class='modal-dialog' >";
    modalLayout     += "                <div class='modal-content'>";
    modalLayout     += "                    <div class='modal-body'>";
    modalLayout     += "                        <button type='button' class='close' data-dismiss='modal' aria-label='Close'>";
    modalLayout     += "                            <span aria-hidden='true'>&times;</span>";
    modalLayout     += "                        </button>";
    modalLayout     += "                        <div class='video-container'></div>";
    modalLayout     += "                        <div class='modal-caption'>";
    modalLayout     += "                            <h3></h3>";
    modalLayout     += "                            <span class='date'></span>";
    modalLayout     += "                        </div>";
    modalLayout     += "                    </div>";
    modalLayout     += "                </div>";
    modalLayout     += "            </div>";
    modalLayout     += "        </div>";

    var modalObj = jQuery(modalLayout).attr('id','localVideo');
    jQuery('body').append(modalObj);

    var $videoModal = jQuery('#localVideo');
    var video = null;

    jQuery('.play-video').on('click',function(event){
        event.preventDefault();

        var children = jQuery(this).children('.video-block');
        var mediaType = children.attr('data-type');
        var mediaSrc = children.attr('data-src');
        var mediaTitle = children.attr('data-title');
        var mediaDate = children.attr('data-date');

        $videoModal.find('.modal-caption > h3').text(mediaTitle);
        $videoModal.find('.modal-caption > .date').text(mediaDate);

        if( mediaType == 'local' ){
            var source = document.createElement('source');
            source.src = mediaSrc;
            source.type = 'video/mp4';

            video = document.createElement('video');
            video.appendChild(source);
            video.controls = true;
        }else if( mediaType == 'embed' ){
            video = mediaSrc;
        }

        $videoModal.modal();
    });

    $videoModal.on('show.bs.modal', function (event) {
        jQuery(event.target).find('.modal-body > .video-container').html(video);
        jQuery(event.target).find('.modal-body > .video-container video').trigger('play');
    });

    $videoModal.on('hidden.bs.modal', function (event) {
        jQuery(event.target).find('.modal-body > .video-container').html('');
    });

   /* $('.user-icon').click(function(e){
        e.preventDefault();
        $('.login-menu').slideToggle(300);
    }); */

    /*jQuery('.play-icon').click(function() {
		var $video   = $(this).children().prev(),
		$caption = $(this).children();
        $video.trigger('play');
		$caption.fadeOut();
		$video.attr('controls', '');
    });

	jQuery("video").on("pause", function (e) {
		jQuery(this).removeAttr('controls');
		jQuery(this).next().fadeIn();
	});*/

    jQuery('[data-toggle="tooltip"]').tooltip();

    jQuery('.ev-table').footable();

    /*wholesaler filter*/
    jQuery('#wholesaler-filter select[name="city"], #wholesaler-filter select[name="product"]').on('change',function(e){
        e.preventDefault();
        jQuery('#wholesaler-filter').submit();
    });
    /**/

    /*member filter*/
    jQuery('#member-filter select[name="organization"]').on('change',function(e){
        e.preventDefault();
        jQuery('#member-filter').submit();
    });
    /**/

    jQuery('.datepicker').datepicker({
        dateFormat : 'yy-mm-dd'
    });

    /*footable wholesaler*/
    jQuery('.sk-cm-table table tbody tr').on('click',function(){
       jQuery(this).find('.footable-toggle').toggleClass('fooicon-plus fooicon-minus');
    });
    /**/

    /*prettyphoto*/
    jQuery("a[rel^='prettyPhoto']").prettyPhoto({
            deeplinking: false,
            social_tools: false
    });
    jQuery("a[rel^='prettyPhoto[pp_gal]']").prettyPhoto({
        deeplinking: false,
        social_tools: false
    });
    /**/

    /* footable download icon click */
    
    jQuery('.fa-download').on('click', function(){
        jQuery(this).parent().trigger('click');
    });

});



//dynamic content height

jQuery(window).load(function(){
    dynamicContentHeight();
    addClassClearfix();
    loader();
    jQuery('#menu').show();
    jQuery('nav#menu').mmenu({
        extensions  : [ 'effect-slide-menu', 'pageshadow' ],
        navbar      : {
            title       : 'Menu'
        },
        offCanvas: {
            position: "right"
        }
    });

    footableIconPositionChange();
});

/* temp solution */
function footableIconPositionChange(){
    jQuery('.table-wholesaler .footable-toggle').remove();
    jQuery('.table-wholesaler tbody tr').each(function(){
        jQuery('td.footable-last-visible', this).append('<span class="footable-toggle fooicon fooicon-plus"></span>');
    });
};

function dynamicContentHeight(){
	var	$content = $('#content'),
	    windowHeight = $(window).height(),
   	    // headerHeight = $('#masthead').outerHeight(),
    	footerHeight = $('#colophon').outerHeight(),
    	remainHeight = windowHeight- footerHeight;
    	$content.css("min-height", remainHeight+"px");
}

function DisableToolTip(elements) {
    for (var i = 0; i < elements.length; i++) {
        var element = elements[i];
        element.onmouseover = function() {
            this.setAttribute("uz_title", this.title);
            this.title = "";
        };
        element.onmouseout = function() {
            this.title = this.getAttribute("uz_title");
        };
    }
}

function addClassClearfix(){
    var $imageContainer = $('.nextgen_pro_grid_album .image_container'),
    counter = 1;
    $imageContainer.each( function( index ){
        if( counter % 3 == 0){
            $(this).after("<div class='clearfix visible-lg '></div>");  
        }
        if(counter % 2 == 0){
            $(this).after("<div class='clearfix hidden-lg'></div>");
        }
        counter ++;
    });
}

function loader(){
  var $loader = jQuery( '.loader-overlay' ),
      $body = jQuery( 'body');
      $loader.fadeOut(600, function(){ this.remove(); });
      $body.css( 'overflow','initial' );
}

$(function () {
  $('[data-toggle="popover"]').popover()
})
