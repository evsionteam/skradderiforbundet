<?php

require_once __DIR__.'/tcpdf/config/tcpdf_config.php';
require_once __DIR__.'/tcpdf/tcpdf.php';

// Extend the TCPDF class to create custom Header and Footer
class SK_PDF extends TCPDF {
    public $isLastPage = false;

    //Page header
    public function Header() {
        // get the current page break margin
        $bMargin = $this->getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = $this->AutoPageBreak;
        // auto-page-break status
        $this->SetAutoPageBreak($auto_page_break, $bMargin);
        // set the starting point for the page content
        $this->setPageMark();
    }

    /*Page footer*/
    public function Footer() {
        /*Only on last page*/
        if($this->isLastPage) {
            $footer_text = '<div style="text-align: center; font-size: 12px;">Kontakt ordförande: Peter Magnusson, 040-91 06 50, e-mail: peter_magnusson@comhem.se</div>';
            $this->SetY(-15);
            $this->SetFont('helvetica', 'I', 8);
            $this->writeHTMLCell(0, 0, '', '', $footer_text, 0, 0, false, "L", true);
        }
    }

    public function lastPage($resetmargins=false) {
        $this->setPage($this->getNumPages(), $resetmargins);
        $this->isLastPage = true;
    }
}

function sk_save_user_info_pdf( $user_info ) {

    global $sk_option ;

    $heading = __('User Info','supporterskalet');

    /**
     *  moved class `SK_PDF` from here
     */  
    


    $pdf = new SK_PDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

    $pdf->SetCreator(get_bloginfo('name'));
    $pdf->SetAuthor(get_bloginfo('name'));
    $pdf->SetTitle($heading);
    $pdf->SetSubject($heading);    

    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    // convert TTF font to TCPDF format and store it on the fonts folder
   
    $pdf->SetFont('merriweather', 'BI', 14, '', false);
    $pdf->SetFont('merriweatherb', 'BI', 14, '', false); 

    $pdf->SetMargins(10, 0, 10);   

    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }

    $pdf->setFontSubsetting(true);

    $pdf->setTextShadow(array('enabled'=>false, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

    $header = file_get_contents( __DIR__.'/parts/header.html' );
    $body   = file_get_contents( __DIR__.'/parts/body.html' );
    $footer = file_get_contents( __DIR__.'/parts/footer.html' );

    $pdf_logo = '';

    $logo = isset( $sk_option['logo_image'] ) ? $sk_option['logo_image'] : '';
    if(!empty($logo)) {     
        switch_to_blog(1);
        $logo_link = wp_get_attachment_image_url( $logo,'full' );
        restore_current_blog();
        $pdf_logo = '<img width="100" src="'.$logo_link.'">';
    }

    $body = str_replace('@logo@', $pdf_logo, $body);
    $body = str_replace('@date@', date("Y-m-d"), $body);

    $user_info_label = array(
        'first_name'            => __( 'Förnamn:', 'skradderiforbundet' ),
        'last_name'             => __( 'Efternamn:', 'skradderiforbundet' ),
        'member_birthdate'      => __( 'Födelsedatum:', 'skradderiforbundet' ),
        'member_profession'     => __( 'Yrke:', 'skradderiforbundet' ),
        'member_home_address'   => __( 'Hemadress:', 'skradderiforbundet' ),
        'member_post_number'    => __( 'Postnummer:', 'skradderiforbundet' ),
        'member_place'          => __( 'Stad:', 'skradderiforbundet' ),
        'member_country'        => __( 'Land:', 'skradderiforbundet' ),
        'member_home_phone'     => __( 'Telefonnummer:', 'skradderiforbundet' ),
        'email'                 => __( 'Email:', 'skradderiforbundet' ),
        'member_organization'   => __( 'Lokalförening:', 'skradderiforbundet' ),
    );

    $company_info_label = array(
        'company_name'          => __( 'Företagsnamn:', 'skradderiforbundet' ),
        'company_phone'         => __( 'Telefon:', 'skradderiforbundet' ),
        'company_start_date'    => __( 'Registreringsår:', 'skradderiforbundet' ),
        'company_address'       => __( 'Adress:', 'skradderiforbundet' ),
        'company_post_number'   => __( 'Postnummer:', 'skradderiforbundet' ),
        'company_city'          => __( 'Stad:', 'skradderiforbundet' ),
        'company_nature'        => __( 'Verksamhetsområde:', 'skradderiforbundet' ),
        'company_f_tax'         => __( 'F-skatt:', 'skradderiforbundet' ),
        'company_vat'           => __( 'Moms:', 'skradderiforbundet' ),
        'company_email'         => __( 'Företagsemail:', 'skradderiforbundet' ),
        'company_website'       => __( 'Webbsida:', 'skradderiforbundet' ),
    );

    $employed_info_label = array(
        'employee_company_name'         => __( 'Företagsnamn:', 'skradderiforbundet' ),
        'employee_company_phone'        => __( 'Företagstelefon:', 'skradderiforbundet' ),
        'employee_company_address'      => __( 'Adress:', 'skradderiforbundet' ),
        'employee_company_post_number'  => __( 'Postnummer:', 'skradderiforbundet' ),
        'employee_company_city'         => __( 'Stad:', 'skradderiforbundet' ),
    );

    $school_info_label = array(
        'school_name'           => __( 'Skola:', 'skradderiforbundet' ),
        'school_phone'          => __( 'Telefon arbetsplats:', 'skradderiforbundet' ),
        'school_email'          => __( 'Email:', 'skradderiforbundet' ),
        'school_address'        => __( 'Adress:', 'skradderiforbundet' ),
        'school_post_number'    => __( 'Postnummer:', 'skradderiforbundet' ),
        'school_place'          => __( 'Stad:', 'skradderiforbundet' ),
    );


    $user_info_table = $certificate_info_table = $master_info_table = $company_info_table = $employed_info_table = $school_info_table = '';
    $user_info_label_value_pair = $company_info_label_value_pair = $employed_info_label_value_pair = $school_info_label_value_pair = array();

    $company_info_heading = $employed_info_heading = $school_info_heading = $message_heading = '';
    if ( isset( $user_info['has_business'] ) ) {
        $company_info_heading = '<h3 style="font-weight: bold; font-size:15px;">'.__('Eget företag','skradderiforbundet').'</h3>
                                <table>
                                    <tr>
                                        <td>
                                            <div style="line-height: 1;"></div>
                                        </td>
                                    </tr>
                                </table>';
    }
    if ( isset( $user_info['is_employee'] ) ) {
        $employed_info_heading = '<h3 style="font-weight: bold; font-size:15px;">'.__('Anställd på företag','skradderiforbundet').'</h3>
                                <table>
                                    <tr>
                                        <td>
                                            <div style="line-height: 1;"></div>
                                        </td>
                                    </tr>
                                </table>';
    }
    if ( isset( $user_info['has_school'] ) ) {
        $school_info_heading = '<h3 style="font-weight: bold; font-size:15px;">'.__('Aktiv lärare','skradderiforbundet').'</h3>
                                <table>
                                    <tr>
                                        <td>
                                            <div style="line-height: 1;"></div>
                                        </td>
                                    </tr>
                                </table>';
    }
    if ( isset( $user_info['other_information'] ) ) {
        $message_heading = '<h3 style="font-weight: bold; font-size:15px;">'.__('Övriga upplysningar','skradderiforbundet').'</h3>
                            <table>
                                <tr>
                                    <td>
                                        <div style="line-height: 0;"></div>
                                    </td>
                                </tr>
                            </table>';
    }

    /*get organization name by id and set the name in array*/
    if ( isset( $user_info['member_organization'] ) ) {
        switch_to_blog(2);
        $org_name = get_the_title( $user_info['member_organization'] );
        restore_current_blog();
        $user_info['member_organization'] = $org_name;
    }
    /**/

    /*set tax and vat text of company*/
    if ( isset( $user_info['has_business'] ) ) {
        if ( isset( $user_info['company_f_tax'] ) ) {
          $user_info['company_f_tax'] = __('Ja','skradderiforbundet');
        } else {
            $user_info['company_f_tax'] = __('Nej','skradderiforbundet');
        }

        if ( isset( $user_info['company_vat'] ) ) {
            $user_info['company_vat'] = __('Ja','skradderiforbundet');
        } else {
            $user_info['company_vat'] = __('Nej','skradderiforbundet');
        }
    }
   

    /*convert company nature array*/
    if ( isset( $user_info['company_nature'] ) &&  is_array( $user_info['company_nature'] )  ) {
        $user_info['company_nature'] = implode( ',', $user_info['company_nature'] ); 
    }   

    /*match the keys in both arrays to determine the label and value pair*/
    if ( !empty( $user_info ) ) {
        foreach ( $user_info as $key => $val ) {
            if ( !empty( $val ) ) {
                /*Personal info*/
                if ( array_key_exists( $key, $user_info_label ) ) {
                    $temp                         = array();
                    $temp['label']                = $user_info_label[$key];
                    $temp['value']                = $val;
                    $user_info_label_value_pair[] = $temp;
                }

                
                /*Company info*/
                if ( array_key_exists( $key, $company_info_label ) ) {
                    $temp                               = array();
                    $temp['label']                      = $company_info_label[$key];
                    $temp['value']                      = $val;
                    $company_info_label_value_pair[]    = $temp;
                }
                
                /*Employed info*/
                if ( array_key_exists( $key, $employed_info_label ) ) {
                    $temp                               = array();
                    $temp['label']                      = $employed_info_label[$key];
                    $temp['value']                      = $val;
                    $employed_info_label_value_pair[]   = $temp;
                }
               
                /*School info*/
                if ( array_key_exists( $key, $school_info_label ) ) {
                    $temp                           = array();
                    $temp['label']                  = $school_info_label[$key];
                    $temp['value']                  = $val;
                    $school_info_label_value_pair[] = $temp;
                }
               
            }
        }
    }
    

    /*Personal info*/
    if ( !empty( $user_info_label_value_pair ) ) {
        $numItems = count( $user_info_label_value_pair );
        $j = 1;
        $user_info_table .= '<tr>';
        foreach( $user_info_label_value_pair as $info ) {
            $user_info_table .= '<td>
                                <p class="title-text" style="font-family:merriweatherb;">'.$info['label'].'</p>
                                <p class="text">'.$info['value'].'</p>
                                <div style="line-height: 0.6;"></div>
    		                    </td>';
            if ( $j != 1  && $j % 3 == 0 ){
                if ( $j < $numItems ) {
                    /*after each 3rd info*/
                    $user_info_table .= '</tr><tr>';
                } else {
                    /*only if is the 3rd info but not last array item*/
                    if( $j != $numItems ){
                        $user_info_table .= '</tr>';
                    }
                }
            }
            /*if last array item */
            if( $j == $numItems ){
                $user_info_table .= '</tr>';
            }
            $j++;
        }
    }
    

    /*certificate info*/
    if ( isset( $user_info['has_certificate'] ) ) {
        $doc = $user_info['date_of_certificate'];
        $certificate_value = array();
        if ( isset($user_info['certificate_in_man'] ) ){
            $certificate_value[] = 'Herr';
        }

        if ( isset($user_info['certificate_in_woman'] ) ){
            $certificate_value[] = 'Dam';
        }

        if ( isset($user_info['certificate_in_dress'] ) ){
            $certificate_value[] = 'Klänning';
        }

        $certificate_info_table = '<tr><td>
                                    <p class="title-text" style="font-family:merriweatherb;">' . __( 'Gesällbrev', 'skradderiforbundet') . '</p>
                                    <p class="text">' . implode( ",", $certificate_value ) . '</p>
                                    <div style="line-height: 1;"></div>
                                    </td>
                                    <td colspan="2">
                                    <p class="title-text" align="left" style="font-family:merriweatherb;">' . __( 'År för utfärdande:', 'skradderiforbundet') . '</p>
                                    <p class="text" align="left">' . $doc . '</p>
                                    <div style="line-height: 1;"></div>
                                    </td></tr>';
    }
    

    /*master info*/
    if ( isset( $user_info['has_master_craft'] ) ) {
        
        $master_year = $user_info['master_year'];
        $master_value = array();

        if( isset( $user_info['master_in_man'] ) ){
            $master_value[] = 'Herr';
        }

        if( isset( $user_info['master_in_woman'] ) ){
            $master_value[] = 'Dam';
        }

        if( isset( $user_info['master_in_dress'] ) ){
            $master_value[] = 'Klänning';
        }

        $master_info_table = '<tr><td>
                                    <p class="title-text" style="font-family:merriweatherb;">' . __( 'Mästarbrev', 'skradderiforbundet') . '</p>
                                    <p class="text">' . implode( ",", $master_value ) . '</p>
                                    <div style="line-height: 1;"></div>
                                    </td>
                                    <td colspan="2">
                                    <p class="title-text" align="left" style="font-family:merriweatherb;">' . __( 'År för utfärdande:', 'skradderiforbundet' ) . '</p>
                                    <p class="text" align="left">' . $master_year . '</p>
                                    <div style="line-height: 1;"></div>
                                    </td></tr>';
    }
   

    /*Company info*/
    if ( !empty( $company_info_label_value_pair ) ) {
        $numItems = count( $company_info_label_value_pair );
        $j = 1;
        $company_info_table .= '<tr>';

        foreach ( $company_info_label_value_pair as $info ) {
            $company_info_table .= '<td>
                                <p class="title-text" style="font-family:merriweatherb;">' . $info['label'] . '</p>
                                <p class="text">' . $info['value'] . '</p>
                                <div style="line-height: 0.6;"></div>
    		                    </td>';

            if ( $j != 1  && $j % 3 == 0 ) {
                if( $j < $numItems ){
                    /*after each 3rd info*/
                    $company_info_table .= '</tr><tr>';
                } else {
                    /*only if is the 3rd info but not last array item*/
                    if($j != $numItems){
                        $company_info_table .= '</tr>';
                    }
                }
            }
            /*if last array item */
            if ( $j == $numItems ) {
                $company_info_table .= '</tr>';
            }
            $j++;
        }
    }
   

    /*Employed info*/
    if ( !empty( $employed_info_label_value_pair ) ) {
        $numItems = count( $employed_info_label_value_pair );
        $j        = 1;
        $employed_info_table .= '<tr>';

        foreach ( $employed_info_label_value_pair as $info ) {
            $employed_info_table .= '<td>
                                <p class="title-text" style="font-family:merriweatherb;">'.$info['label'].'</p>
                                <p class="text">'.$info['value'].'</p>
                                <div style="line-height: 0.6;"></div>
    		                    </td>';
            if ( $j != 1  && $j % 3 == 0 ){
                if( $j < $numItems ){
                    /*after each 3rd info*/
                    $employed_info_table .= '</tr><tr>';
                } else {
                    /*only if is the 3rd info but not last array item*/
                    if( $j != $numItems ) {
                        $employed_info_table .= '</tr>';
                    }
                }
            }
            /*if last array item */
            if( $j == $numItems ){
                $employed_info_table .= '</tr>';
            }
            $j++;
        }
    }
 

    /*School Info*/
    if ( !empty( $school_info_label_value_pair ) ) {
        $numItems = count( $school_info_label_value_pair );
        $j = 1;
        $school_info_table .= '<tr>';
        foreach( $school_info_label_value_pair as $info ) {
            $school_info_table .= '<td>
                                <p class="title-text" style="font-family:merriweatherb;">'.$info['label'].'</p>
                                <p class="text">'.$info['value'].'</p>
                                <div style="line-height: 0.6;"></div>
    		                    </td>';
            if( $j != 1  && $j % 3 == 0 ) {
                if( $j < $numItems ){
                    /*after each 3rd info*/
                    $school_info_table .= '</tr><tr>';
                } else {
                    /*only if is the 3rd info but not last array item*/
                    if( $j != $numItems ) {
                        $school_info_table .= '</tr>';
                    }
                }
            }
            /*if last array item */
            if( $j == $numItems ){
                $school_info_table .= '</tr>';
            }
            $j++;
        }
    }   
   
    $body = str_replace( '@user_info@', $user_info_table, $body );
    $body = str_replace( '@certificate_info@', $certificate_info_table, $body );
    $body = str_replace( '@master_info@', $master_info_table, $body );
    $body = str_replace( '@company_info_heading@', $company_info_heading, $body );
    $body = str_replace( '@company_info@', $company_info_table, $body );
    $body = str_replace( '@employed_info_heading@', $employed_info_heading, $body );
    $body = str_replace( '@employed_info@', $employed_info_table, $body );
    $body = str_replace( '@school_info_heading@', $school_info_heading, $body );
    $body = str_replace( '@school_info@', $school_info_table, $body );
    $body = str_replace( '@message_heading@', $message_heading, $body );
    $body = str_replace( '@message@', $user_info['other_information'], $body );

    $html = $header . $body . $footer;

    $pdf->AddPage();
    $pdf->writeHTMLCell( 0, 0, '', '', $html, 0, 1, 0, true, '', true );

    $upload_dir = wp_upload_dir();
    $path = $upload_dir['basedir'].'/users/';

    if( !file_exists( $path ) ){
        wp_mkdir_p( $path );
    }

    $rand = md5( $user_info['email'] );

    $pdf->Output( $path . $rand . '.pdf', 'F' );

    return $path . $rand . '.pdf';
    
}



