<?php

/*Get sub categories*/
add_action( 'wp_ajax_ev_get_sub_cats', 'ev_get_sub_cats_callback' );
add_action( 'wp_ajax_nopriv_ev_get_sub_cats', 'ev_get_sub_cats_callback' );
function ev_get_sub_cats_callback(){
    $data = $sub_cats = array();
    $parent_cats = $_POST['cat'];
    if(!empty($parent_cats)){
        foreach($parent_cats as $parent_cat){
            $sub_cats = get_term_children( $parent_cat,'company_category' );
            if(!empty($sub_cats)){
                foreach ( $sub_cats as $sub_cat ) {
                    $term = get_term_by( 'id', $sub_cat, 'company_category' );
                    $data[$sub_cat] = $term->name;
                }
            }
        }
        if(!empty($data)){
            wp_send_json_success( $data );
        }else{
            wp_send_json_error();
        }
    }else{
        wp_send_json_error();
    }
    die();
}


/*Insert Company*/
add_action( 'wp_ajax_ev_upload_company', 'ev_upload_company_callback' );
add_action( 'wp_ajax_nopriv_ev_upload_company', 'ev_upload_company_callback' );
function ev_upload_company_callback() {
    $data = array();
    $data['status'] = false;

    $file_error = false;

    if( isset( $_POST['nonce'] ) && wp_verify_nonce( $_POST['nonce'], 'submit_company' ) ){
        $params = array();
        parse_str($_POST['form_values'], $params);

        global $current_user;
        if(!in_array('member',$current_user->roles)){
            $data['status'] = false;
            $data['message'] = __("Ledsen, men du måste logga in som en medlem för att kunna fortsätta.",'ev-intranet');
        }else{
            if( empty($params['post_title']) || empty($params['address']) || empty($params['zip_code']) || empty($params['city']) || empty($params['phone']) || empty($params['longitude']) || empty($params['latitude']) ){
                $data['message'] = __('Please fill all the required fields.','ev-intranet');
            }else{
                $files = $_FILES['files'];
                if( !empty($files)){
                    if ( $files['size'][0] > 3145728 ) {
                        $data['message'] = __('Image is too large. It must be less than 3M!','ev-intranet');
                        $file_error = true;
                    }
                    if( $files['type'][0] != 'image/jpeg' && $files['type'][0] != 'image/png'){
                        $data['message'] = __('Please select appropriate file type.','ev-intranet');
                        $file_error = true;
                    }
                }

                /*check if company already exist in same location */
                $args = array(
                    'post_type' => 'ev_companies',
                    'meta_query' => array(
                        'relation' => 'AND',
                        array(
                            'key'     => '_company_latitude',
                            'value'   => $params['latitude'],
                        ),
                        array(
                            'key'     => '_company_longitude',
                            'value'   => $params['longitude'],
                        ),
                    ),
                );
                $company = new WP_Query($args);
                if($company->have_posts()){
                    $data['message'] = __('Sorry, A Company already exist on same location !!!','ev-intranet');
                }else{

                    /*check if member has already added a company*/
                    $args = array(
                        'post_type' => 'ev_companies',
                        'author' => get_current_user_id()
                    );
                    $company = new WP_Query($args);
                    if($company->have_posts()){
                        $data['message'] = __('Sorry, You have already added a company !!!','ev-intranet');
                    }else{

                        $post_data = array(
                            'post_title' => sanitize_text_field($params['post_title']),
                            'post_content' => sanitize_text_field($params['description']),
                            'post_status' => 'pending',
                            'post_type' => 'ev_companies'
                        );

                        /*insert post ( and attachment if any )*/
                        $post_id = wp_insert_post( $post_data );

                        if($post_id){

                            /*add extra info in post meta*/
                            add_post_meta($post_id,'_company_phone',sanitize_text_field($params['phone']));
                            add_post_meta($post_id,'_company_email',sanitize_text_field($params['email']));
                            add_post_meta($post_id,'_company_address',sanitize_text_field($params['address']));
                            add_post_meta($post_id,'_company_zip_code',sanitize_text_field($params['zip_code']));
                            add_post_meta($post_id,'_company_city',sanitize_text_field($params['city']));
                            add_post_meta($post_id,'_company_latitude',sanitize_text_field($params['latitude']));
                            add_post_meta($post_id,'_company_longitude',sanitize_text_field($params['longitude']));
                            add_post_meta($post_id,'_company_website',sanitize_text_field($params['website']));
                            /**/

                            /*set category and sub categories*/
                            $allowed_cats = array();
                            $categories = get_terms( array(
                                'taxonomy' => 'company_category',
                                'hide_empty' => false,
                            ) );
                            if(!empty($categories)){
                                foreach($categories as $cat){
                                    $allowed_cats[] = $cat->term_id;
                                }
                            }

                            $cats = array();
                            if(!empty($params['category'])){
                                foreach($params['category'] as $category){
                                    $cats[] = (int)$category;
                                }
                            }
                            if(!empty($params['sub_cats'])){
                                foreach($params['sub_cats'] as $sub_cat){
                                    $cats[] = (int)$sub_cat;
                                }
                            }


                            /*Only allow to insert categories present in database*/
                            $filtered_cats = array_intersect($cats, $allowed_cats);
                            /**/

                            if(!empty($filtered_cats)){
                                wp_set_object_terms( $post_id, $filtered_cats, 'company_category' );
                            }
                            /**/

                            /*set featured image*/
                            if(!empty($files)){
                                if(false == $file_error){
                                    $file = array(
                                        'name'      => $files['name'][0],
                                        'type'      => $files['type'][0],
                                        'tmp_name'  => $files['tmp_name'][0],
                                        'error'     => $files['error'][0],
                                        'size'      => $files['size'][0]
                                    );
                                    ev_upload_image_fn( $file, $post_id, true );
                                }
                            }
                            /**/

                            /*Notify admin*/
                            $to = get_option('admin_email');
                            if(!empty($to)){

                                $post_edit_link = get_edit_post_link($post_id);
                                $logo = $banner_message = '';

                                global $ev_intranet;
                                /*site logo*/
                                if( isset($ev_intranet['logo-image']) && !empty($ev_intranet['logo-image']) ){
                                    $logo = '<img src="'.$ev_intranet['logo-image']['url'].'" width="120" alt="" border="0" mc:edit="40">';
                                }
                                /**/

                                $subject = __('New Company Added to site','ev-intranet');
                                $banner_message = __('Ett nytt företag har lagts till!<br/> Logga in genom att klicka på denna länk och publicera företaget. ','ev-intranet');
                                $banner_message .= '<br/>';
                                $banner_message .= '<a href="'.$post_edit_link.'" style="color:#000000;">'.$post_edit_link.'</a>';


                                $body = file_get_contents( __DIR__.'/email-template/admin-notify-image-upload.htm');

                                $body = str_replace( '{BannerImage}', $logo, $body );
                                $body = str_replace( '{shortMessage}', $banner_message, $body );

                                $headers = array('Content-Type: text/html; charset=UTF-8','From: Skrädderiförbundet <skradderiforbundet@wordpress.org>');
                                wp_mail( $to, $subject, $body, $headers );
                                //wp_mail( 'cloudrubal@gmail.com', $subject, $body, $headers );

                            }
                            /**/

                            $data['status'] = true;
                            $data['message'] = __('Company added successfully!','ev-intranet');
                        }else{
                            $data['status'] = false;
                            $data['message'] = __('Unable to add Company!','ev-intranet');
                        }
                        /**/
                    }
                }
            }
        }
    }
    echo json_encode($data);
    die();
}


/*Update Company*/
add_action( 'wp_ajax_ev_update_company', 'ev_update_company_callback' );
add_action( 'wp_ajax_nopriv_ev_update_company', 'ev_update_company_callback' );
function ev_update_company_callback() {
    $data = array();
    $data['status'] = false;

    $file_error = false;

    if( isset( $_POST['nonce'] ) && wp_verify_nonce( $_POST['nonce'], 'submit_company' ) ){
        $params = array();
        parse_str($_POST['form_values'], $params);

        global $current_user;
        if(!in_array('member',$current_user->roles)){
            $data['status'] = false;
            $data['message'] = __("Ledsen, men du måste logga in som en medlem för att kunna fortsätta.",'ev-intranet');
        }else{
            if( empty($params['post_title']) || empty($params['address']) || empty($params['zip_code']) || empty($params['city']) || empty($params['phone']) || empty($params['longitude']) || empty($params['latitude']) ){
                $data['message'] = __('Please fill all the required fields.','ev-intranet');
            }else{
                $files = $_FILES['files'];
                if( !empty($files)){
                    if ( $files['size'][0] > 3145728 ) {
                        $data['message'] = __('Image is too large. It must be less than 3M!','ev-intranet');
                        $file_error = true;
                    }
                    if( $files['type'][0] != 'image/jpeg' && $files['type'][0] != 'image/png'){
                        $data['message'] = __('Please select appropriate file type.','ev-intranet');
                        $file_error = true;
                    }
                }

                /*check if company already exist in same location */
                $args = array(
                    'post_type' => 'ev_companies',
                    'author' => -get_current_user_id(),
                    'meta_query' => array(
                        'relation' => 'AND',
                        array(
                            'key'     => '_company_latitude',
                            'value'   => $params['latitude'],
                        ),
                        array(
                            'key'     => '_company_longitude',
                            'value'   => $params['longitude'],
                        ),
                    ),
                );
                $company = new WP_Query($args);
                if($company->have_posts()){
                    $data['message'] = __('Sorry, A Company already exist on same location !!!','ev-intranet');
                }else{

                    $args = array(
                        'post_type' => 'ev_companies',
                        'author' => get_current_user_id()
                    );
                    $company = new WP_Query($args);
                    $company_id = '';
                    if($company->have_posts()){
                        while($company->have_posts()):$company->the_post();
                        $company_id = get_the_ID();
                        endwhile;wp_reset_postdata();
                    }

                    if(!empty($company_id)){

                        $post_data = array(
                            'ID'=> $company_id,
                            'post_title' => sanitize_text_field($params['post_title']),
                            'post_content' => sanitize_text_field($params['description']),
                            'post_status' => 'pending',
                            'post_type' => 'ev_companies'
                        );

                        /*update post ( and attachment if any )*/
                        wp_update_post( $post_data );

                        /*add extra info in post meta*/
                        update_post_meta($company_id,'_company_phone',sanitize_text_field($params['phone']));
                        update_post_meta($company_id,'_company_email',sanitize_text_field($params['email']));
                        update_post_meta($company_id,'_company_address',sanitize_text_field($params['address']));
                        update_post_meta($company_id,'_company_zip_code',sanitize_text_field($params['zip_code']));
                        update_post_meta($company_id,'_company_city',sanitize_text_field($params['city']));
                        update_post_meta($company_id,'_company_latitude',sanitize_text_field($params['latitude']));
                        update_post_meta($company_id,'_company_longitude',sanitize_text_field($params['longitude']));
                        update_post_meta($company_id,'_company_website',sanitize_text_field($params['website']));
                        /**/

                        /*set category and sub categories*/
                        $allowed_cats = array();
                        $categories = get_terms( array(
                            'taxonomy' => 'company_category',
                            'hide_empty' => false,
                        ) );
                        if(!empty($categories)){
                            foreach($categories as $cat){
                                $allowed_cats[] = $cat->term_id;
                            }
                        }

                        $cats = array();
                        if(!empty($params['category'])){
                            foreach($params['category'] as $category){
                                $cats[] = (int)$category;
                            }
                        }
                        if(!empty($params['sub_cats'])){
                            foreach($params['sub_cats'] as $sub_cat){
                                $cats[] = (int)$sub_cat;
                            }
                        }


                        /*Only allow to insert categories present in database*/
                        $filtered_cats = array_intersect($cats, $allowed_cats);
                        /**/

                        if(!empty($filtered_cats)){
                            wp_set_object_terms( $company_id, $filtered_cats, 'company_category' );
                        }
                        /**/

                        /*set featured image*/
                        if(!empty($files)){
                            if(false == $file_error){
                                $file = array(
                                    'name'      => $files['name'][0],
                                    'type'      => $files['type'][0],
                                    'tmp_name'  => $files['tmp_name'][0],
                                    'error'     => $files['error'][0],
                                    'size'      => $files['size'][0]
                                );
                                ev_upload_image_fn( $file, $company_id, true );
                            }
                        }
                        /**/



                        /*Notify admin*/
                        //$to = get_option('admin_email');

                        /**/

                        $data['status'] = true;
                        $data['message'] = __('Company updated successfully!','ev-intranet');
                    }else{
                        $data['status'] = false;
                        $data['message'] = __('Unable to update Company!','ev-intranet');
                    }
                        /**/
                }
            }
        }
    }
    echo json_encode($data);
    die();
}