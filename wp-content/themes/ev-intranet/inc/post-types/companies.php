<?php
add_action( 'init', 'ev_companies_init' );
function ev_companies_init() {

    $companies_labels = array(
        'name'               => _x( 'Företag', 'post type general name', 'ev-intranet' ),
        'singular_name'      => _x( 'Företag', 'post type singular name', 'ev-intranet' ),
        'menu_name'          => _x( 'Företag', 'admin menu', 'ev-intranet' ),
        'name_admin_bar'     => _x( 'Företag', 'add new on admin bar', 'ev-intranet' ),
        'add_new'            => _x( 'Lägg till nytt', 'Företag', 'ev-intranet' ),
        'add_new_item'       => __( 'Lägg till nytt Företag', 'ev-intranet' ),
        'new_item'           => __( 'New Företag', 'ev-intranet' ),
        'edit_item'          => __( 'Edit Företag', 'ev-intranet' ),
        'view_item'          => __( 'View Företag', 'ev-intranet' ),
        'all_items'          => __( 'Alla företag', 'ev-intranet' ),
        'search_items'       => __( 'Search Företag', 'ev-intranet' ),
        'parent_item_colon'  => __( 'Parent Företag:', 'ev-intranet' ),
        'not_found'          => __( 'No Företag found.', 'ev-intranet' ),
        'not_found_in_trash' => __( 'No Företag found in Trash.', 'ev-intranet' )
    );

    $companies_args = array(
        'labels'             => $companies_labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'company' ),
        'taxonomies'         => array( 'company_category' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'menu_icon'          => 'dashicons-location',
        'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt', 'author' )
    );

    register_post_type( 'ev_companies', $companies_args );

}

add_action( 'init', 'create_company_taxonomies', 0 );

function create_company_taxonomies() {
    $cat_labels = array(
        'name'              => _x( 'Kategorier', 'taxonomy general name', 'ev-intranet' ),
        'singular_name'     => _x( 'Kategorier', 'taxonomy singular name', 'ev-intranet' ),
        'search_items'      => __( 'Search Kategorier', 'ev-intranet' ),
        'all_items'         => __( 'Alla kategorier', 'ev-intranet' ),
        'parent_item'       => __( 'Parent Kategorier', 'ev-intranet' ),
        'parent_item_colon' => __( 'Parent Kategorier:', 'ev-intranet' ),
        'edit_item'         => __( 'Edit Kategorier', 'ev-intranet' ),
        'update_item'       => __( 'Update Kategorier', 'ev-intranet' ),
        'add_new_item'      => __( 'Lägg till ny kategori', 'ev-intranet' ),
        'new_item_name'     => __( 'New Kategorier Name', 'ev-intranet' ),
        'menu_name'         => __( 'Kategorier', 'ev-intranet' ),
    );

    $cat_args = array(
        'hierarchical'      => true,
        'labels'            => $cat_labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'company-category' ),
    );

    register_taxonomy( 'company_category', array( 'ev_companies' ), $cat_args );

}

add_action('init','add_categories_to_company');
function add_categories_to_company(){
    register_taxonomy_for_object_type('company_category', 'ev_companies');
}

function ev_add_companies_meta_box() {
    add_meta_box(
        'ev-copany-meta-box',
        __( 'Inställningar','ev-intranet' ),
        'render_company_meta_box',
        'ev_companies',
        'normal',
        'default'
    );
}
add_action( 'add_meta_boxes_ev_companies', 'ev_add_companies_meta_box' );

function render_company_meta_box($post){
    wp_nonce_field( basename( __FILE__ ), 'ev_compnay_nonce' );
    $phone = get_post_meta($post->ID,'_company_phone',true);
    $email = get_post_meta($post->ID,'_company_email',true);
    $address = get_post_meta($post->ID,'_company_address',true);
    $zip_code = get_post_meta($post->ID,'_company_zip_code',true);
    $city = get_post_meta($post->ID,'_company_city',true);
    $latitude = get_post_meta($post->ID,'_company_latitude',true);
    $longitude = get_post_meta($post->ID,'_company_longitude',true);
    $website = get_post_meta($post->ID,'_company_website',true);
    ?>
    <div>
        <p><label for="phone"><?php _e( 'Telefon:', 'ev-intranet' )?></label></p>
        <p><input type="text" name="phone" id="phone" value="<?php echo $phone;?>" size="30" /></p>
    </div>
    <div>
        <p><label for="email"><?php _e( 'Email:', 'ev-intranet' )?></label></p>
        <p><input type="text" name="email" id="email" value="<?php echo $email;?>" size="30" /></p>
    </div>
    <div>
        <p><label for="address-info"><?php _e( 'Adress:', 'ev-intranet' )?></label></p>
        <p>
            <textarea id="address-info" name="address" rows="5" cols="50"><?php echo $address;?></textarea>
        </p>
    </div>
    <div>
        <p><label for="zip-code"><?php _e( 'Postnummer:', 'ev-intranet' )?></label></p>
        <p><input type="text" name="zip_code" id="zip-code" value="<?php echo $zip_code;?>" size="30" /></p>
    </div>
    <div>
        <p><label for="website"><?php _e( 'Website:', 'ev-intranet' )?></label></p>
        <p><input type="text" name="website" id="website" value="<?php echo $website;?>" size="30" /></p>
    </div>
    <div>
        <p><label for="city"><?php _e( 'Stad:', 'ev-intranet' )?></label></p>
        <p><input type="text" name="city" id="city" value="<?php echo $city;?>" size="30" /></p>
    </div>
    <div>
        <p><label for="latitude"><?php _e( 'Latitud:', 'ev-intranet' )?></label></p>
        <p><input type="text" name="latitude" id="latitude" value="<?php echo $latitude;?>" size="30" /></p>
    </div>
    <div>
        <p><label for="longitude"><?php _e( 'Longitud:', 'ev-intranet' )?></label></p>
        <p><input type="text" name="longitude" id="longitude" value="<?php echo $longitude;?>" size="30" /></p>
    </div>
<?php
}

function ev_company_meta_save( $post_id ) {

    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ 'ev_company_nonce' ] ) && wp_verify_nonce( $_POST[ 'ev_company_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
    if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
        return;
    }

    if(isset($_POST['phone'])){
        update_post_meta($post_id,'_company_phone',sanitize_text_field($_POST['phone']));
    }

    if(isset($_POST['email'])){
        update_post_meta($post_id,'_company_email',sanitize_text_field($_POST['email']));
    }

    if(isset($_POST['address'])){
        update_post_meta($post_id,'_company_address',sanitize_text_field($_POST['address']));
    }

    if(isset($_POST['zip_code'])){
        update_post_meta($post_id,'_company_zip_code',sanitize_text_field($_POST['zip_code']));
    }

    if(isset($_POST['city'])){
        update_post_meta($post_id,'_company_city',sanitize_text_field($_POST['city']));
    }

    if(isset($_POST['latitude'])){
        update_post_meta($post_id,'_company_latitude',sanitize_text_field($_POST['latitude']));
    }

    if(isset($_POST['longitude'])){
        update_post_meta($post_id,'_company_longitude',sanitize_text_field($_POST['longitude']));
    }

    if(isset($_POST['website'])){
        update_post_meta($post_id,'_company_website',sanitize_text_field($_POST['website']));
    }
}
add_action( 'save_post_ev_companies', 'ev_company_meta_save' );