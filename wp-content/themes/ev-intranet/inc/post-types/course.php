<?php
add_action('init', 'ev_courses_init');
function ev_courses_init()
{
    $courses_labels = array(
        'name' => _x('Kurser', 'post type general name', 'ev-intranet'),
        'singular_name' => _x('Kurser', 'post type singular name', 'ev-intranet'),
        'menu_name' => _x('Kurser', 'admin menu', 'ev-intranet'),
        'name_admin_bar' => _x('Kurser', 'add new on admin bar', 'ev-intranet'),
        'add_new' => _x('Lägg till ny', 'Kurser', 'ev-intranet'),
        'add_new_item' => __('Lägg till ny Kurser', 'ev-intranet'),
        'new_item' => __('New Kurser', 'ev-intranet'),
        'edit_item' => __('Edit Kurser', 'ev-intranet'),
        'view_item' => __('View Kurser', 'ev-intranet'),
        'all_items' => __('All Kurser', 'ev-intranet'),
        'search_items' => __('Search Kurser', 'ev-intranet'),
        'parent_item_colon' => __('Parent Kurser:', 'ev-intranet'),
        'not_found' => __('No Kurser found.', 'ev-intranet'),
        'not_found_in_trash' => __('No Kurser found in Trash.', 'ev-intranet')
    );

    $courses_args = array(
        'labels' => $courses_labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'kurser'),
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'menu_icon' => 'dashicons-book-alt',
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'author')
    );

    register_post_type('ev_courses', $courses_args);
}

function ev_add_courses_meta_box()
{
    add_meta_box(
        'ev-course-meta-box',
        __('Kurs inställningar', 'ev-intranet'),
        'render_course_meta_box',
        'ev_courses',
        'normal',
        'default'
    );
}

add_action('add_meta_boxes_ev_courses', 'ev_add_courses_meta_box');

function render_course_meta_box($post)
{
    wp_nonce_field(basename(__FILE__), 'ev_course_nonce');

    $course_info = get_post_meta($post->ID);

    $course_start_date = isset($course_info['_course_start_date'][0]) ? $course_info['_course_start_date'][0] : '';
    $course_end_date = isset($course_info['_course_end_date'][0]) ? $course_info['_course_end_date'][0] : '';
    $course_time = isset($course_info['course_time'][0]) ? $course_info['course_time'][0] : '';
    $course_holder = isset($course_info['_course_holder'][0]) ? $course_info['_course_holder'][0] : '';
    $training_place = isset($course_info['_training_place'][0]) ? $course_info['_training_place'][0] : '';
    $course_link = isset($course_info['_course_link'][0]) ? $course_info['_course_link'][0] : '';
    $contact_info = isset($course_info['_course_contact_info'][0]) ? $course_info['_course_contact_info'][0] : '';
    $selected_design = isset($course_info['intranet_news_design_select_meta'][0]) ? $course_info['intranet_news_design_select_meta'][0] : '';

    ?>
    <div>
        <p><label for="start-date"><?php _e('Startdatum:', 'ev-intranet') ?></label></p>

        <p><input name="course_start_date" id="start-date" class="datepicker" type="text" value="<?php echo $course_start_date; ?>"/></p>
    </div>

    <div>
        <p><label for="end-date"><?php _e('Slutdatum:', 'ev-intranet') ?></label></p>

        <p><input name="course_end_date" id="end-date" class="datepicker" type="text" value="<?php echo $course_end_date; ?>"/></p>
    </div>

    <div>
        <p><label for="course-time"><?php _e('Time:', 'ev-intranet') ?></label></p>
        <p><textarea id="course-time" name="course_time" rows="5" cols="50"><?php echo $course_time; ?></textarea></p>
    </div>

    <div>
        <p><label for="course_holder"><?php _e('Kurshållare:', 'ev-intranet') ?></label></p>

        <p><input name="course_holder" id="course_holder" type="text" value="<?php echo $course_holder; ?>"/></p>
    </div>

    <div>
        <p><label for="training_place"><?php _e('Plats:', 'ev-intranet') ?></label></p>

        <p><input name="training_place" id="training_place" type="text" value="<?php echo $training_place; ?>"/></p>
    </div>

    <div>
        <p><label for="course_link"><?php _e('Kurslänk:', 'ev-intranet') ?></label></p>

        <p><input name="course_link" id="course_link" type="text" value="<?php echo $course_link; ?>" size="50"/></p>
    </div>

    <div>
        <p><label for="contact-info"><?php _e('Kontaktinformation:', 'ev-intranet') ?></label></p>

        <p>
            <textarea id="contact-info" name="contact_info" rows="5" cols="50"><?php echo $contact_info; ?></textarea>
        </p>
    </div>

    <div>
        <p><label for="intranet_news_design_select"><?php _e( 'Design:', 'ev-intranet' )?></label></p>
            <p>
                <select name="intranet_news_design_select" id="intranet_news_design_select">
                <?php
                $designs = array(
                    'design_1' => __('1 Kolumn','ev-intranet'),
                    'design_2' => __('2 kolumner','ev-intranet'),
                    'design_3' => __('3 kolumner','ev-intranet'),
                );
                if(!empty($designs)){
                    foreach($designs as $design => $name){
                        ?>
                        <option value="<?php echo $design;?>" <?php selected($design,$selected_design);?>>
                            <?php echo $name;?>
                        </option>
                    <?php
                    }
                }
                ?>
            </select>
        </p>
    </div>

<?php
}

function ev_course_meta_save($post_id)
{

    $is_autosave = wp_is_post_autosave($post_id);
    $is_revision = wp_is_post_revision($post_id);
    $is_valid_nonce = (isset($_POST['ev_course_nonce']) && wp_verify_nonce($_POST['ev_course_nonce'], basename(__FILE__))) ? 'true' : 'false';
    if ($is_autosave || $is_revision || !$is_valid_nonce) {
        return;
    }

    /*insert the data in main site too*/
    if (isset($_POST['post_title'])) {

        global $sitepress;
        $post_lang = isset($_POST['icl_post_language']) ? $_POST['icl_post_language'] : '';

        /*save meta values*/
        ev_update_course_post_meta_values($post_id, $_POST);
        /**/

        /*thumbnail url*/
        $thumb_url = '';
        $thumb_id = isset($_POST['_thumbnail_id']) ? $_POST['_thumbnail_id'] : '';
        if( !empty($thumb_id) && -1 != $thumb_id ){
            $thumb_url = wp_get_attachment_url( $thumb_id );
        }

        $public_mirrored_post = get_post_meta($post_id, '_public_mirrored_post', true);

        /*only create new post if it is not already inserted in the main site else update the post*/
        if (empty($public_mirrored_post)) {

            /*remove action to avoid infinite loops*/
            remove_action('save_post_ev_courses', 'ev_course_meta_save');
            /**/

            /*insert this new post in the main site too*/
            $insert_data = array(
                'post_title' => isset($_POST['post_title']) ? $_POST['post_title'] : '',
                'post_content' => isset($_POST['content']) ? $_POST['content'] : '',
                'post_excerpt' => isset($_POST['post_excerpt']) ? $_POST['post_excerpt'] : '',
                'post_status' => isset($_POST['post_status']) ? $_POST['post_status'] : 'publish',
                'post_type' => isset($_POST['post_type']) ? $_POST['post_type'] : 'ev_courses',
            );

            switch_to_blog(1);

            $new_post_id = wp_insert_post($insert_data);

            /* Save post meta Values */
            if ($new_post_id) {

                /*set the language to work with wpml*/
                $sitepress->set_element_language_details($new_post_id, 'post_ev_courses', '', $post_lang);
                /**/

                /*set current post id as a flag in newly inserted post*/
                update_post_meta($new_post_id, '_intranet_main_post', $post_id);

                /*save meta values*/
                ev_update_course_post_meta_values($new_post_id, $_POST);
                /**/

                /*set thumbnail url*/
                if( !is_wp_error($thumb_url) ){
                    update_post_meta($new_post_id, '_thumbnail_url', $thumb_url);
                }
                /**/
            }

            restore_current_blog();
            /**/

            /*set the newly inserted post id as a flag in current post*/
            if ($new_post_id) {
                update_post_meta($post_id, '_public_mirrored_post', $new_post_id);
            }
            /**/

            /*reattach the removed hook*/
            add_action('save_post_ev_courses', 'ev_course_meta_save');
            /**/

        } else {

            /*remove action to avoid infinite loops*/
            remove_action('save_post_ev_courses', 'ev_course_meta_save');
            /**/

            /*Update post in public site*/
            $post_data = array(
                'ID' => $public_mirrored_post,
                'post_title' => isset($_POST['post_title']) ? $_POST['post_title'] : '',
                'post_content' => isset($_POST['content']) ? $_POST['content'] : '',
                'post_excerpt' => isset($_POST['post_excerpt']) ? $_POST['post_excerpt'] : '',
                'post_name' => isset($_POST['post_name']) ? $_POST['post_name'] : '',
            );

            switch_to_blog(1);

            wp_update_post($post_data);

            /*update meta values*/
            ev_update_course_post_meta_values($public_mirrored_post, $_POST);
            /**/

            /*set thumbnail url*/
            if( !is_wp_error($thumb_url) ){
                update_post_meta($public_mirrored_post, '_thumbnail_url', $thumb_url);
            }
            /**/

            restore_current_blog();
            /**/

            /*reattach the removed hook*/
            add_action('save_post_ev_courses', 'ev_course_meta_save');
            /**/

        }
        /**/
    }
}

add_action('save_post_ev_courses', 'ev_course_meta_save');


/*update post meta values*/
function ev_update_course_post_meta_values($post_id, $data)
{

    $course_start_date = isset($data['course_start_date']) ? $data['course_start_date'] : '';
    $course_end_date = isset($data['course_end_date']) ? $data['course_end_date'] : '';
    $course_time = isset($data['course_time']) ? $data['course_time'] : '';
    $contact_info = isset($data['contact_info']) ? $data['contact_info'] : '';
    $course_holder = isset($data['course_holder']) ? $data['course_holder'] : '';
    $training_place = isset($data['training_place']) ? $data['training_place'] : '';
    $course_link = isset($data['course_link']) ? $data['course_link'] : '';
    $course_expire_date = isset($data['sk_post_expiry_date']) ? $data['sk_post_expiry_date'] : '';
    $design = isset($data['intranet_news_design_select']) ? $data['intranet_news_design_select'] : '';

    update_post_meta($post_id, '_course_start_date', $course_start_date);
    update_post_meta($post_id, '_course_end_date', $course_end_date);
    update_post_meta($post_id, 'course_time', $course_time);
    update_post_meta($post_id, '_course_contact_info', $contact_info);
    update_post_meta($post_id, '_course_holder', $course_holder);
    update_post_meta($post_id, '_training_place', $training_place);
    update_post_meta($post_id, '_course_link', $course_link);
    update_post_meta($post_id, 'expires_in', $course_expire_date);
    update_post_meta($post_id, 'intranet_news_design_select_meta', $design);

    if ( isset( $data['sk_post_order'] ) ) {
        if(!empty($data['sk_post_order'])){
            update_post_meta( $post_id, 'sk_post_order', (int)$data['sk_post_order'] );
        }else{
            /*Storing a huge number so that the key exists on each post with higher value*/
            update_post_meta( $post_id, 'sk_post_order', 999999 );
        }
    }
}

/**/

/* Delete Main site post too on deletion*/
add_action('before_delete_post', 'ev_course_delete_action');
function ev_course_delete_action($post_id)
{

    global $post_type;
    if ($post_type != 'ev_courses') return;

    $public_mirrored_post = get_post_meta($post_id, '_public_mirrored_post', true);
    if (!empty($public_mirrored_post)) {
        switch_to_blog(1);
        wp_delete_post($public_mirrored_post);
        restore_current_blog();
    }
}