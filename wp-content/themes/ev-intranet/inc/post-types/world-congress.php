<?php
add_action( 'init', 'ev_world_congress_init' );
function ev_world_congress_init() {

    $world_congress_labels = array(
        'name'               => _x( 'Världskongressen', 'post type general name', 'ev-intranet' ),
        'singular_name'      => _x( 'Världskongressen', 'post type singular name', 'ev-intranet' ),
        'menu_name'          => _x( 'Världskongressen', 'admin menu', 'ev-intranet' ),
        'name_admin_bar'     => _x( 'Världskongressen', 'add new on admin bar', 'ev-intranet' ),
        'add_new'            => _x( 'Lägg till nytt', 'Världskongressen', 'ev-intranet' ),
        'add_new_item'       => __( 'Lägg till nytt Världskongressen', 'ev-intranet' ),
        'new_item'           => __( 'New Världskongressen', 'ev-intranet' ),
        'edit_item'          => __( 'Edit Världskongressen', 'ev-intranet' ),
        'view_item'          => __( 'View Världskongressen', 'ev-intranet' ),
        'all_items'          => __( 'Alla inlägg', 'ev-intranet' ),
        'search_items'       => __( 'Search Världskongressen', 'ev-intranet' ),
        'parent_item_colon'  => __( 'Parent Världskongressen:', 'ev-intranet' ),
        'not_found'          => __( 'No Världskongressen found.', 'ev-intranet' ),
        'not_found_in_trash' => __( 'No Världskongressen found in Trash.', 'ev-intranet' )
    );

    $world_congress_args = array(
        'labels'             => $world_congress_labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'world-congress' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'menu_icon'          => 'dashicons-tickets-alt',
        'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt','author' )
    );

    register_post_type( 'ev_world_congress', $world_congress_args );
}


function ev_add_wc_meta_box()
{
    add_meta_box(
        'ev-wc-meta-box',
        __('Design Option', 'ev-intranet'),
        'render_wc_meta_box',
        'ev_world_congress',
        'normal',
        'default'
    );
}

add_action('add_meta_boxes_ev_world_congress', 'ev_add_wc_meta_box');

function render_wc_meta_box($post)
{
    wp_nonce_field(basename(__FILE__), 'ev_wc_nonce');

    $course_info = get_post_meta($post->ID);
    $selected_design = isset($course_info['intranet_news_design_select_meta'][0]) ? $course_info['intranet_news_design_select_meta'][0] : '';
    ?>
    <div>
        <p><label for="intranet_news_design_select"><?php _e( 'Design:', 'ev-intranet' )?></label></p>
        <p>
            <select name="intranet_news_design_select" id="intranet_news_design_select">
                <?php
                $designs = array(
                    'design_1' => __('1 Kolumn','ev-intranet'),
                    'design_2' => __('2 kolumner','ev-intranet'),
                    'design_3' => __('3 kolumner','ev-intranet'),
                );
                if(!empty($designs)){
                    foreach($designs as $design => $name){
                        ?>
                        <option value="<?php echo $design;?>" <?php selected($design,$selected_design);?>>
                            <?php echo $name;?>
                        </option>
                    <?php
                    }
                }
                ?>
            </select>
        </p>
    </div>
<?php
}

function ev_world_congress_meta_save($post_id)
{

    $is_autosave = wp_is_post_autosave($post_id);
    $is_revision = wp_is_post_revision($post_id);
    $is_valid_nonce = (isset($_POST['ev_wc_nonce']) && wp_verify_nonce($_POST['ev_wc_nonce'], basename(__FILE__))) ? 'true' : 'false';

    if ( $is_autosave || $is_revision || !$is_valid_nonce) {
        return;
    }

    /*insert the data in main site too*/
    if (isset($_POST['post_title'])) {

        global $sitepress;
        $post_lang = isset($_POST['icl_post_language']) ? $_POST['icl_post_language'] : '';

        ev_update_world_congress_post_meta_values($post_id,$_POST);

        /*thumbnail url*/
        $thumb_url = '';
        $thumb_id = isset($_POST['_thumbnail_id']) ? $_POST['_thumbnail_id'] : '';
        if( !empty($thumb_id) && -1 != $thumb_id ){
            $thumb_url = wp_get_attachment_url( $thumb_id );
        }

        $public_mirrored_post = get_post_meta($post_id, '_public_mirrored_post', true);

        /*only create new post if it is not already inserted in the main site else update the post*/
        if (empty($public_mirrored_post)) {

            /*remove action to avoid infinite loops*/
            remove_action('save_post_ev_world_congress', 'ev_world_congress_meta_save');
            /**/

            /*insert this new post in the main site too*/
            $insert_data = array(
                'post_title' => isset($_POST['post_title']) ? $_POST['post_title'] : '',
                'post_content' => isset($_POST['content']) ? $_POST['content'] : '',
                'post_excerpt' => isset($_POST['post_excerpt']) ? $_POST['post_excerpt'] : '',
                'post_status' => isset($_POST['post_status']) ? $_POST['post_status'] : 'publish',
                'post_type' => isset($_POST['post_type']) ? $_POST['post_type'] : 'ev_world_congress',
            );

            switch_to_blog(1);

            $new_post_id = wp_insert_post($insert_data);

            /* Save post meta Values */
            if ($new_post_id) {

                /*set the language to work with wpml*/
                $sitepress->set_element_language_details($new_post_id, 'post_ev_world_congress', '', $post_lang);
                /**/

                /*set current post id as a flag in newly inserted post*/
                update_post_meta($new_post_id, '_intranet_main_post', $post_id);

                /*feature post and expire status in pubic site*/
                ev_update_world_congress_post_meta_values($new_post_id,$_POST);
                /**/

                /*set thumbnail url*/
                if( !is_wp_error($thumb_url) ){
                    update_post_meta($new_post_id, '_thumbnail_url', $thumb_url);
                }
                /**/
            }

            restore_current_blog();
            /**/

            /*set the newly inserted post id as a flag in current post*/
            if ($new_post_id) {
                update_post_meta($post_id, '_public_mirrored_post', $new_post_id);
            }
            /**/

            /*reattach the removed hook*/
            add_action('save_post_ev_world_congress', 'ev_world_congress_meta_save');
            /**/

        } else {

            /*remove action to avoid infinite loops*/
            remove_action('save_post_ev_world_congress', 'ev_world_congress_meta_save');
            /**/

            /*Update post in public site*/

            /*Set date manually*/
            $mm = $_POST['mm'];
            $jj = $_POST['jj'];
            $aa = $_POST['aa'];
            $hh = $_POST['hh'];
            $mn = $_POST['mn'];
            $ss = $_POST['ss'];
            $postdate = $aa.'-'.$mm.'-'.$jj.' '.$hh.':'.$mn.':'.$ss ;
            /**/

            $post_data = array(
                'ID' => $public_mirrored_post,
                'post_title' => isset($_POST['post_title']) ? $_POST['post_title'] : '',
                'post_content' => isset($_POST['content']) ? $_POST['content'] : '',
                'post_date' => $postdate,
                'post_excerpt' => isset($_POST['post_excerpt']) ? $_POST['post_excerpt'] : '',
                'post_name' => isset($_POST['post_name']) ? $_POST['post_name'] : '',
            );

            switch_to_blog(1);

            wp_update_post($post_data);

            /*set thumbnail url*/
            if( !is_wp_error($thumb_url) ){
                update_post_meta($public_mirrored_post, '_thumbnail_url', $thumb_url);
            }
            /**/

            /*feature post and expire status in pubic site*/
            ev_update_world_congress_post_meta_values($public_mirrored_post,$_POST);
            /**/

            restore_current_blog();
            /**/

            /*reattach the removed hook*/
            add_action('save_post_ev_world_congress', 'ev_world_congress_meta_save');
            /**/

        }
        /**/
    }
}
add_action('save_post_ev_world_congress', 'ev_world_congress_meta_save');


/*update post meta values*/
function ev_update_world_congress_post_meta_values($post_id, $data)
{
    $expire_date = isset($data['sk_post_expiry_date']) ? $data['sk_post_expiry_date'] : '';
    $design = isset($data['intranet_news_design_select']) ? $data['intranet_news_design_select'] : '';
    update_post_meta($post_id, 'intranet_news_design_select_meta', $design);

    update_post_meta($post_id, 'expires_in', $expire_date);

    if ( isset( $data['sk_post_order'] ) ) {
        if(!empty($data['sk_post_order'])){
            update_post_meta( $post_id, 'sk_post_order', (int)$data['sk_post_order'] );
        }else{
            /*Storing a huge number so that the key exists on each post with higher value*/
            update_post_meta( $post_id, 'sk_post_order', 999999 );
        }
    }

}
/**/

/* Delete Main site post too on deletion*/
add_action('before_delete_post', 'ev_world_congress_delete_action');
function ev_world_congress_delete_action($post_id)
{

    global $post_type;
    if ($post_type != 'ev_world_congress') return;

    $public_mirrored_post = get_post_meta($post_id, '_public_mirrored_post', true);
    if (!empty($public_mirrored_post)) {
        switch_to_blog(1);
        wp_delete_post($public_mirrored_post);
        restore_current_blog();
    }
}