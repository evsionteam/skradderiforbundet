<?php
add_action( 'init', 'ev_image_gallery_init' );
function ev_image_gallery_init() {

    $image_gallery_labels = array(
        'name'               => _x( 'Fotogalleri', 'post type general name', 'ev-intranet' ),
        'singular_name'      => _x( 'Fotogalleri', 'post type singular name', 'ev-intranet' ),
        'menu_name'          => _x( 'Fotogalleri', 'admin menu', 'ev-intranet' ),
        'name_admin_bar'     => _x( 'Fotogalleri', 'add new on admin bar', 'ev-intranet' ),
        'add_new'            => _x( 'Lägg till nytt', 'Fotogalleri', 'ev-intranet' ),
        'add_new_item'       => __( 'Lägg till nytt Image', 'ev-intranet' ),
        'new_item'           => __( 'New Image', 'ev-intranet' ),
        'edit_item'          => __( 'Edit Image ', 'ev-intranet' ),
        'view_item'          => __( 'View Image ', 'ev-intranet' ),
        'all_items'          => __( 'Alla bilder ', 'ev-intranet' ),
        'search_items'       => __( 'Search Image ', 'ev-intranet' ),
        'parent_item_colon'  => __( 'Parent Image:', 'ev-intranet' ),
        'not_found'          => __( 'No Image found.', 'ev-intranet' ),
        'not_found_in_trash' => __( 'No Image found in Trash.', 'ev-intranet' )
    );

    $image_gallery_args = array(
        'labels'             => $image_gallery_labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'image-gallery' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'menu_icon'          => 'dashicons-format-gallery',
        'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt','author' )
    );

    register_post_type( 'ev_image_gallery', $image_gallery_args );
}


function ev_add_image_gallery_meta_box() {
    add_meta_box(
        'ev-gallery-meta-box',
        __( 'Foto detaljer','ev-intranet' ),
        'render_img_gal_meta_box',
        'ev_image_gallery',
        'normal',
        'default'
    );
}
add_action( 'add_meta_boxes_ev_image_gallery', 'ev_add_image_gallery_meta_box' );

function render_img_gal_meta_box($post){
    wp_nonce_field( basename( __FILE__ ), 'ev_img_gal_nonce' );
    $website_link = get_post_meta($post->ID,'site_link',true);
    ?>
    <div>
        <p><label for="site-link"><?php _e( 'Länk:', 'ev-intranet' )?></label></p>
        <p><input type="text" name="site_link" id="site-link" value="<?php echo $website_link;?>" size="50" /></p>
    </div>
    <?php
}

function ev_image_gallery_meta_save( $post_id ) {

    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ 'ev_img_gal_nonce' ] ) && wp_verify_nonce( $_POST[ 'ev_img_gal_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
    if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
        return;
    }

    if(isset($_POST['site_link'])){
        update_post_meta($post_id,'site_link',sanitize_text_field($_POST['site_link']));
    }
}
add_action( 'save_post_ev_image_gallery', 'ev_image_gallery_meta_save' );