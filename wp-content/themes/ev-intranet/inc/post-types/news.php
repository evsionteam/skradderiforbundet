<?php
add_action( 'init', 'ev_news_init' );
function ev_news_init() {

    $news_labels = array(
        'name'               => _x( 'Nyheter', 'post type general name', 'ev-intranet' ),
        'singular_name'      => _x( 'Nyheter', 'post type singular name', 'ev-intranet' ),
        'menu_name'          => _x( 'Nyheter', 'admin menu', 'ev-intranet' ),
        'name_admin_bar'     => _x( 'Nyheter', 'add new on admin bar', 'ev-intranet' ),
        'add_new'            => _x( 'Lägg till nyhet', 'Nyheter', 'ev-intranet' ),
        'add_new_item'       => __( 'Lägg till nyhet Nyheter', 'ev-intranet' ),
        'new_item'           => __( 'New Nyheter', 'ev-intranet' ),
        'edit_item'          => __( 'Edit Nyheter', 'ev-intranet' ),
        'view_item'          => __( 'View Nyheter', 'ev-intranet' ),
        'all_items'          => __( 'Alla nyheter', 'ev-intranet' ),
        'search_items'       => __( 'Search news', 'ev-intranet' ),
        'parent_item_colon'  => __( 'Parent news:', 'ev-intranet' ),
        'not_found'          => __( 'No Nyheter found.', 'ev-intranet' ),
        'not_found_in_trash' => __( 'No news found in Trash.', 'ev-intranet' )
    );

    $news_args = array(
        'labels'             => $news_labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'nyheter' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'menu_icon'          => 'dashicons-clipboard',
        'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt', 'author' )
    );

    register_post_type( 'ev_news', $news_args );

}

function ev_add_news_meta_box() {
    add_meta_box(
        'ev-news-meta-box',
        __( 'Nyheter inställningar','ev-intranet' ),
        'render_news_meta_box',
        'ev_news',
        'normal',
        'default'
    );
}
add_action( 'add_meta_boxes_ev_news', 'ev_add_news_meta_box' );

function render_news_meta_box($post){
    $selected_design = get_post_meta($post->ID, 'intranet_news_design_select_meta', true);
    wp_nonce_field( basename( __FILE__ ), 'ev_news_nonce' );
    ?>
    <p>
        <label for="intranet_news_design_select"><?php _e( 'Design:', 'ev-intranet' )?></label>
        <select name="intranet_news_design_select" id="intranet_news_design_select">
            <?php
            $designs = array(
                'design_1' => __('1 Kolumn','ev-intranet'),
                'design_2' => __('2 kolumner','ev-intranet'),
                'design_3' => __('3 kolumner','ev-intranet'),
            );
            if(!empty($designs)){
                foreach($designs as $design => $name){
                    ?>
                    <option value="<?php echo $design;?>" <?php selected($design,$selected_design);?>>
                        <?php echo $name;?>
                    </option>
                <?php
                }
            }
            ?>
        </select>
    </p>
<?php
}

function ev_news_meta_save( $post_id ) {

    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ 'ev_news_nonce' ] ) && wp_verify_nonce( $_POST[ 'ev_news_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
    if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
        return;
    }

    if(isset($_POST["intranet_news_design_select"])){
        update_post_meta($post_id, 'intranet_news_design_select_meta', $_POST["intranet_news_design_select"]);
    }

    if ( isset( $_POST['sk_post_order'] ) ) {
        if(!empty($_POST['sk_post_order'])){
            update_post_meta( $post_id, 'sk_post_order', (int)$_POST['sk_post_order'] );
        }else{
            /*Storing a huge number so that the key exists on each post with higher value*/
            update_post_meta( $post_id, 'sk_post_order', 999999 );
        }
    }
}
add_action( 'save_post_ev_news', 'ev_news_meta_save' );
