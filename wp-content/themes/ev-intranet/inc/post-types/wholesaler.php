<?php
add_action( 'init', 'ev_wholesaler_init' );
function ev_wholesaler_init() {

    $wholesaler_labels = array(
        'name'               => _x( 'Leverantörer', 'post type general name', 'ev-intranet' ),
        'singular_name'      => _x( 'Leverantörer', 'post type singular name', 'ev-intranet' ),
        'menu_name'          => _x( 'Leverantörer', 'admin menu', 'ev-intranet' ),
        'name_admin_bar'     => _x( 'Leverantörer', 'add new on admin bar', 'ev-intranet' ),
        'add_new'            => _x( 'Lägg till ny', 'Leverantörer', 'ev-intranet' ),
        'add_new_item'       => __( 'Lägg till ny Leverantörer', 'ev-intranet' ),
        'new_item'           => __( 'New Leverantörer', 'ev-intranet' ),
        'edit_item'          => __( 'Edit Leverantörer', 'ev-intranet' ),
        'view_item'          => __( 'View Leverantörer', 'ev-intranet' ),
        'all_items'          => __( 'Alla leverantörer', 'ev-intranet' ),
        'search_items'       => __( 'Search Leverantörer', 'ev-intranet' ),
        'parent_item_colon'  => __( 'Parent Leverantörer:', 'ev-intranet' ),
        'not_found'          => __( 'No Leverantörer found.', 'ev-intranet' ),
        'not_found_in_trash' => __( 'No Leverantörer found in Trash.', 'ev-intranet' )
    );

    $wholesaler_args = array(
        'labels'             => $wholesaler_labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'wholesaler' ),
        'taxonomies'         => array( 'wholesaler_city', 'wholesaler_product' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'menu_icon'          => 'dashicons-businessman',
        'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt' )
    );

    register_post_type( 'ev_wholesaler', $wholesaler_args );
}

add_action( 'init', 'create_wholesaler_taxonomies', 0 );
function create_wholesaler_taxonomies() {
    $city_labels = array(
        'name'              => _x( 'Stad', 'taxonomy general name', 'ev-intranet' ),
        'singular_name'     => _x( 'Stad', 'taxonomy singular name', 'ev-intranet' ),
        'search_items'      => __( 'Search Stad', 'ev-intranet' ),
        'all_items'         => __( 'Alla städer', 'ev-intranet' ),
        'parent_item'       => __( 'Parent Stad', 'ev-intranet' ),
        'parent_item_colon' => __( 'Parent Stad:', 'ev-intranet' ),
        'edit_item'         => __( 'Edit Stad', 'ev-intranet' ),
        'update_item'       => __( 'Update Stad', 'ev-intranet' ),
        'add_new_item'      => __( 'Lägg till ny Stad', 'ev-intranet' ),
        'new_item_name'     => __( 'New Stad Name', 'ev-intranet' ),
        'menu_name'         => __( 'Städer', 'ev-intranet' ),
    );

    $city_args = array(
        'hierarchical'      => true,
        'labels'            => $city_labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'wholesaler-city' ),
    );

    register_taxonomy( 'wholesaler_city', array( 'ev_wholesaler' ), $city_args );

    $product_labels = array(
        'name'              => _x( 'Produkt', 'taxonomy general name', 'ev-intranet' ),
        'singular_name'     => _x( 'Produkt', 'taxonomy singular name', 'ev-intranet' ),
        'search_items'      => __( 'Search Produkt', 'ev-intranet' ),
        'all_items'         => __( 'Alla produkter', 'ev-intranet' ),
        'parent_item'       => __( 'Parent Produkt', 'ev-intranet' ),
        'parent_item_colon' => __( 'Parent Produkt:', 'ev-intranet' ),
        'edit_item'         => __( 'Edit Produkt', 'ev-intranet' ),
        'update_item'       => __( 'Update Produkt', 'ev-intranet' ),
        'add_new_item'      => __( 'Lägg till ny Produkt', 'ev-intranet' ),
        'new_item_name'     => __( 'New Produkt Name', 'ev-intranet' ),
        'menu_name'         => __( 'Produkter', 'ev-intranet' ),
    );

    $product_args = array(
        'hierarchical'      => true,
        'labels'            => $product_labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'wholesaler-product' ),
    );

    register_taxonomy( 'wholesaler_product', array( 'ev_wholesaler' ), $product_args );
}

add_action('init','add_categories_to_wholesaler');
function add_categories_to_wholesaler(){
    register_taxonomy_for_object_type('wholesaler_city', 'ev_wholesaler');
    register_taxonomy_for_object_type('wholesaler_product', 'ev_wholesaler');
}

function ev_add_wholesaler_meta_box() {
    add_meta_box(
        'ev-wholesaler-meta-box',
        __( 'Leverantörer detaljer','ev-intranet' ),
        'render_wholesaler_meta_box',
        'ev_wholesaler',
        'normal',
        'default'
    );
}
add_action( 'add_meta_boxes_ev_wholesaler', 'ev_add_wholesaler_meta_box' );

function render_wholesaler_meta_box($post){
    wp_nonce_field( basename( __FILE__ ), 'ev_wholesaler_nonce' );
    $phone = get_post_meta($post->ID,'_wholesaler_phone',true);
    $address = get_post_meta($post->ID,'_wholesaler_address',true);
    $site = get_post_meta($post->ID,'_wholesaler_site',true);
    ?>
    <div>
        <p><label for="phone"><?php _e( 'Telefon:', 'ev-intranet' )?></label></p>
        <p><input type="text" name="phone" id="phone" value="<?php echo $phone;?>" size="30" /></p>
    </div>
    <div>
        <p><label for="address"><?php _e( 'Adress:', 'ev-intranet' )?></label></p>
        <p><input type="text" name="address" id="address" value="<?php echo $address;?>" size="50" /></p>
    </div>
    <div>
        <p><label for="site"><?php _e( 'Länk:', 'ev-intranet' )?></label></p>
        <p><input type="text" name="site" id="site" value="<?php echo $site;?>" size="80" /></p>
    </div>
<?php
}

function ev_wholesaler_meta_save( $post_id ) {

    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ 'ev_wholesaler_nonce' ] ) && wp_verify_nonce( $_POST[ 'ev_wholesaler_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
    if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
        return;
    }

    if(isset($_POST['phone'])){
        update_post_meta($post_id,'_wholesaler_phone',sanitize_text_field($_POST['phone']));
    }

    if(isset($_POST['site'])){
        update_post_meta($post_id,'_wholesaler_site',sanitize_text_field($_POST['site']));
    }

    if(isset($_POST['address'])){
        update_post_meta($post_id,'_wholesaler_address',sanitize_text_field($_POST['address']));
    }
}
add_action( 'save_post_ev_wholesaler', 'ev_wholesaler_meta_save' );