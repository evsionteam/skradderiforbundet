<?php
add_action( 'init', 'ev_blacklisted_companies_init' );
function ev_blacklisted_companies_init() {

    $bl_labels = array(
        'name'               => _x( 'Svarta listan', 'post type general name', 'ev-intranet' ),
        'singular_name'      => _x( 'Svarta listan', 'post type singular name', 'ev-intranet' ),
        'menu_name'          => _x( 'Svarta listan', 'admin menu', 'ev-intranet' ),
        'name_admin_bar'     => _x( 'Svarta listan', 'add new on admin bar', 'ev-intranet' ),
        'add_new'            => _x( 'Lägg till nytt', 'Svarta listan', 'ev-intranet' ),
        'add_new_item'       => __( 'Lägg till nytt Svarta listan', 'ev-intranet' ),
        'new_item'           => __( 'New Svarta listan', 'ev-intranet' ),
        'edit_item'          => __( 'Edit Svarta listan', 'ev-intranet' ),
        'view_item'          => __( 'View Svarta listan', 'ev-intranet' ),
        'all_items'          => __( 'Alla företag', 'ev-intranet' ),
        'search_items'       => __( 'Search Svarta listan', 'ev-intranet' ),
        'parent_item_colon'  => __( 'Parent Svarta listan:', 'ev-intranet' ),
        'not_found'          => __( 'No Svarta listan found.', 'ev-intranet' ),
        'not_found_in_trash' => __( 'No Svarta listan found in Trash.', 'ev-intranet' )
    );

    $bl_args = array(
        'labels'             => $bl_labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'blacklisted-company' ),
        'taxonomies'         => array( 'bl_company_city', 'bl_company_product' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'menu_icon'          => 'dashicons-businessman',
        'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt' )
    );

    register_post_type( 'ev_bl_companies', $bl_args );
}

add_action( 'init', 'create_bl_taxonomies', 0 );
function create_bl_taxonomies() {
    $city_labels = array(
        'name'              => _x( 'Stad', 'taxonomy general name', 'ev-intranet' ),
        'singular_name'     => _x( 'Stad', 'taxonomy singular name', 'ev-intranet' ),
        'search_items'      => __( 'Search Stad', 'ev-intranet' ),
        'all_items'         => __( 'Alla städer', 'ev-intranet' ),
        'parent_item'       => __( 'Parent Stad', 'ev-intranet' ),
        'parent_item_colon' => __( 'Parent Stad:', 'ev-intranet' ),
        'edit_item'         => __( 'Edit Stad', 'ev-intranet' ),
        'update_item'       => __( 'Update Stad', 'ev-intranet' ),
        'add_new_item'      => __( 'Lägg till nytt Stad', 'ev-intranet' ),
        'new_item_name'     => __( 'New Stad Name', 'ev-intranet' ),
        'menu_name'         => __( 'Städer', 'ev-intranet' ),
    );

    $city_args = array(
        'hierarchical'      => true,
        'labels'            => $city_labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'bl-company-city' ),
    );

    register_taxonomy( 'bl_company_city', array( 'ev_bl_companies' ), $city_args );

    $product_labels = array(
        'name'              => _x( 'Produkt', 'taxonomy general name', 'ev-intranet' ),
        'singular_name'     => _x( 'Produkt', 'taxonomy singular name', 'ev-intranet' ),
        'search_items'      => __( 'Search Produkt', 'ev-intranet' ),
        'all_items'         => __( 'Alla produkter', 'ev-intranet' ),
        'parent_item'       => __( 'Parent Produkt', 'ev-intranet' ),
        'parent_item_colon' => __( 'Parent Produkt:', 'ev-intranet' ),
        'edit_item'         => __( 'Edit Produkt', 'ev-intranet' ),
        'update_item'       => __( 'Update Produkt', 'ev-intranet' ),
        'add_new_item'      => __( 'Lägg till nytt Produkt', 'ev-intranet' ),
        'new_item_name'     => __( 'New Produkt Name', 'ev-intranet' ),
        'menu_name'         => __( 'Produkter', 'ev-intranet' ),
    );

    $product_args = array(
        'hierarchical'      => true,
        'labels'            => $product_labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'bl-company-product' ),
    );

    register_taxonomy( 'bl_company_product', array( 'ev_bl_companies' ), $product_args );
}

add_action('init','add_categories_to_bl_company');
function add_categories_to_bl_company(){
    register_taxonomy_for_object_type('bl_company_city', 'ev_bl_companies');
    register_taxonomy_for_object_type('bl_company_product', 'ev_bl_companies');
}