<?php
add_action( 'init', 'ev_exhibitions_init' );
function ev_exhibitions_init() {

    $exhibitions_labels = array(
        'name'               => _x( 'Utställningar', 'post type general name', 'ev-intranet' ),
        'singular_name'      => _x( 'Utställningar', 'post type singular name', 'ev-intranet' ),
        'menu_name'          => _x( 'Utställningar', 'admin menu', 'ev-intranet' ),
        'name_admin_bar'     => _x( 'Utställningar', 'add new on admin bar', 'ev-intranet' ),
        'add_new'            => _x( 'Lägg till ny', 'Utställningar', 'ev-intranet' ),
        'add_new_item'       => __( 'Lägg till ny Utställningar', 'ev-intranet' ),
        'new_item'           => __( 'New Utställningar', 'ev-intranet' ),
        'edit_item'          => __( 'Edit Utställningar', 'ev-intranet' ),
        'view_item'          => __( 'View Utställningar', 'ev-intranet' ),
        'all_items'          => __( 'Alla utställningar', 'ev-intranet' ),
        'search_items'       => __( 'Search Utställningar', 'ev-intranet' ),
        'parent_item_colon'  => __( 'Parent Utställningar:', 'ev-intranet' ),
        'not_found'          => __( 'No Utställningar found.', 'ev-intranet' ),
        'not_found_in_trash' => __( 'No Utställningar found in Trash.', 'ev-intranet' )
    );

    $exhibitions_args = array(
        'labels'             => $exhibitions_labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'utstallningar' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'menu_icon'          => 'dashicons-groups',
        'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt','author' )
    );

    register_post_type( 'ev_exhibitions', $exhibitions_args );
}


function ev_exhibitions_meta_save( $post_id ) {

    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ 'ev_exhibitions_nonce' ] ) && wp_verify_nonce( $_POST[ 'ev_exhibitions_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
    if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
        return;
    }

    if ( isset( $_POST['sk_post_order'] ) ) {
        if(!empty($_POST['sk_post_order'])){
            update_post_meta( $post_id, 'sk_post_order', (int)$_POST['sk_post_order'] );
        }else{
            /*Storing a huge number so that the key exists on each post with higher value*/
            update_post_meta( $post_id, 'sk_post_order', 999999 );
        }
    }
}
add_action( 'save_post_ev_exhibitions', 'ev_exhibitions_meta_save' );

