<?php
add_action( 'init', 'ev_marketing_materials_init' );
function ev_marketing_materials_init() {

    $mm_labels = array(
        'name'               => _x( 'Logotyper', 'post type general name', 'ev-intranet' ),
        'singular_name'      => _x( 'Marketing Material', 'post type singular name', 'ev-intranet' ),
        'menu_name'          => _x( 'Logotyper', 'admin menu', 'ev-intranet' ),
        'name_admin_bar'     => _x( 'Marketing Material', 'add new on admin bar', 'ev-intranet' ),
        'add_new'            => _x( 'Lägg till ny', 'Marketing Material', 'ev-intranet' ),
        'add_new_item'       => __( 'Lägg till ny Marketing Material', 'ev-intranet' ),
        'new_item'           => __( 'New Marketing Material', 'ev-intranet' ),
        'edit_item'          => __( 'Redigera logotyp', 'ev-intranet' ),
        'view_item'          => __( 'View Marketing Material', 'ev-intranet' ),
        'all_items'          => __( 'Alla logotyper', 'ev-intranet' ),
        'search_items'       => __( 'Search Logotyper', 'ev-intranet' ),
        'parent_item_colon'  => __( 'Parent Logotyper:', 'ev-intranet' ),
        'not_found'          => __( 'No Logotyper found.', 'ev-intranet' ),
        'not_found_in_trash' => __( 'No Logotyper found in Trash.', 'ev-intranet' )
    );

    $mm_args = array(
        'labels'             => $mm_labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'marketing-material' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'menu_icon'          => 'dashicons-layout',
        'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt' )
    );

    register_post_type( 'ev_mm', $mm_args );
}