<?php
add_action( 'init', 'ev_organization_init' );
function ev_organization_init() {

    $organization_labels = array(
        'name'               => _x( 'Organisationer', 'post type general name', 'ev-intranet' ),
        'singular_name'      => _x( 'Organisationer', 'post type singular name', 'ev-intranet' ),
        'menu_name'          => _x( 'Organisationer', 'admin menu', 'ev-intranet' ),
        'name_admin_bar'     => _x( 'Organisationer', 'add new on admin bar', 'ev-intranet' ),
        'add_new'            => _x( 'Lägg till ny', 'Organisationer', 'ev-intranet' ),
        'add_new_item'       => __( 'Lägg till ny Organisationer', 'ev-intranet' ),
        'new_item'           => __( 'New Organisationer', 'ev-intranet' ),
        'edit_item'          => __( 'Edit Organisationer', 'ev-intranet' ),
        'view_item'          => __( 'View Organisationer', 'ev-intranet' ),
        'all_items'          => __( 'Alla lokalföreningar', 'ev-intranet' ),
        'search_items'       => __( 'Search Organisationer', 'ev-intranet' ),
        'parent_item_colon'  => __( 'Parent Organisationer:', 'ev-intranet' ),
        'not_found'          => __( 'No Organisationer found.', 'ev-intranet' ),
        'not_found_in_trash' => __( 'No Organisationer found in Trash.', 'ev-intranet' )
    );

    $organization_args = array(
        'labels'             => $organization_labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'organization' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'menu_icon'          => 'dashicons-building',
        'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt' )
    );

    register_post_type( 'ev_organization', $organization_args );

}

function ev_add_organization_meta_box() {
    add_meta_box(
        'ev-organization-meta-box',
        __( 'Inställningar','ev-intranet' ),
        'render_organization_meta_box',
        'ev_organization',
        'normal',
        'default'
    );
}
add_action( 'add_meta_boxes_ev_organization', 'ev_add_organization_meta_box' );

function render_organization_meta_box($post){
    $organization_admin = get_post_meta($post->ID,'_organization_admin',true);
    $org_logo = get_post_meta( $post->ID, 'organization_logo', true);
    $fb_feed = get_post_meta($post->ID,'_fb_feed',true);
    $insta_feed = get_post_meta($post->ID,'_insta_feed',true);
    $btn_text = get_post_meta($post->ID,'_btn_text',true);
    $btn_link = get_post_meta($post->ID,'_btn_link',true);
    wp_nonce_field( basename( __FILE__ ), 'ev_org_nonce' );
    ?>
    <div>
        <label for="org-admin"><?php _e( 'Föreningens admin', 'ev-intranet' )?></label>
        <div>
            <select name="organization_admin" id="org-admin">
                <option value=""><?php _e( '&mdash; Select &mdash;', 'ev-intranet' )?></option>
                <?php
                $local_admins = get_users( array(
                    'role' => 'local_admin'
                ) );
                if(!empty($local_admins)){
                    foreach($local_admins as $local_admin){
                        ?>
                        <option value="<?php echo $local_admin->ID;?>" <?php selected($local_admin->ID,$organization_admin);?>>
                            <?php echo $local_admin->data->user_nicename;?>
                        </option>
                    <?php
                    }
                }
                ?>
            </select>
        </div>
    </div>
    <div id="image_field">
        <label for="org-logo"><?php _e( 'Föreningens logotyp', 'ev-intranet' )?></label><br/>
        <div id="image_preview">
            <p class="description">
                <img style="max-width:100px;" src="<?php echo $org_logo; ?>" />
            </p>
        </div>
        <input type="hidden" id="org-logo" name="organization_logo" value="<?php echo $org_logo; ?>" />
        <input id="upload_image" type="button" class="button" value="<?php _e( 'Ladda upp bild', 'ev-intranet' ); ?>" />
        <?php if ( '' != $org_logo): ?>
            <input id="delete_image" type="button" class="button" value="<?php _e( 'Radera bild', 'ev-intranet' ); ?>" />
        <?php endif; ?>
    </div>
    <div>
        <label for="fb-feed"><?php _e( 'Facebook flöde', 'ev-intranet' )?></label>
        <div>
            <textarea id="fb-feed" name="fb_feed" rows="10" cols="100"><?php echo $fb_feed;?></textarea>
        </div>
    </div>
    <div>
        <label for="insta-feed"><?php _e( 'Instagram flöde', 'ev-intranet' )?></label>
        <div>
            <textarea id="insta-feed" name="insta_feed" rows="1" cols="100"><?php echo $insta_feed;?></textarea>
        </div>
    </div>
    <div>
        <p><label for="btn-text"><?php _e( 'Knapp text:', 'ev-intranet' )?></label></p>
        <p><input name="btn_text" id="btn-text" type="text" value="<?php echo $btn_text;?>"  size="30"/></p>
    </div>
    <div>
        <p><label for="btn-link"><?php _e( 'Knapp länk:', 'ev-intranet' )?></label></p>
        <p><input name="btn_link" id="btn-link" type="text" value="<?php echo $btn_link;?>" size="80"/></p>
    </div>
<?php
}

function ev_organization_meta_save( $post_id ) {

    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ 'ev_org_nonce' ] ) && wp_verify_nonce( $_POST[ 'ev_org_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
    if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
        return;
    }

    if( isset( $_POST[ 'organization_admin' ] ) ) {
        update_post_meta( $post_id, '_organization_admin', $_POST[ 'organization_admin' ] );
        if(!empty($_POST[ 'organization_admin' ])){
            update_user_meta($_POST[ 'organization_admin' ],'_organization_id',$post_id);
        }
    }

    if( isset( $_POST[ 'organization_logo' ] ) ) {
        update_post_meta( $post_id, 'organization_logo', $_POST[ 'organization_logo' ] );
    }

    if( isset( $_POST[ 'fb_feed' ] ) ) {
        update_post_meta( $post_id, '_fb_feed', $_POST[ 'fb_feed' ] );
    }

    if( isset( $_POST[ 'insta_feed' ] ) ) {
        update_post_meta( $post_id, '_insta_feed', $_POST[ 'insta_feed' ] );
    }

    if( isset( $_POST[ 'btn_text' ] ) ) {
        update_post_meta( $post_id, '_btn_text', sanitize_text_field($_POST[ 'btn_text' ]) );
    }

    if( isset( $_POST[ 'btn_link' ] ) ) {
        update_post_meta( $post_id, '_btn_link', sanitize_text_field($_POST[ 'btn_link' ]) );
    }
}
add_action( 'save_post_ev_organization', 'ev_organization_meta_save' );

/*add organization admin info in admin columns*/
function ev_add_organization_admin_columns( $columns ) {
    $columns = array(
        'cb' => '<input type="checkbox" />',
        'title' => 'Title',
        'organization_admin' => __('Local Admin','ev-intranet'),
        'date' => 'Date'
    );
    return $columns;
}
function ev_add_organization_admin_columns_data( $column, $post_id ) {
    switch ( $column ) {
        case 'organization_admin':
            $organization_admin = get_post_meta($post_id,'_organization_admin',true);
            if(!empty($organization_admin)){
                $organization_admin_data = get_userdata( $organization_admin );
                if(!empty($organization_admin_data)){
                    echo "<p><a href='".get_edit_user_link( $organization_admin)."'>".$organization_admin_data->data->user_nicename."</a></p>";
                }
            }
            break;
    }
}
if ( function_exists( 'add_theme_support' ) ) {
    add_filter( 'manage_ev_organization_posts_columns' , 'ev_add_organization_admin_columns' );
    add_action( 'manage_ev_organization_posts_custom_column' , 'ev_add_organization_admin_columns_data', 10, 2 );
}