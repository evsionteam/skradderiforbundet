<?php
add_action( 'init', 'ev_articles_init' );
function ev_articles_init() {

    $articles_labels = array(
        'name'               => _x( 'Artiklar', 'post type general name', 'ev-intranet' ),
        'singular_name'      => _x( 'Artiklar', 'post type singular name', 'ev-intranet' ),
        'menu_name'          => _x( 'Artiklar', 'admin menu', 'ev-intranet' ),
        'name_admin_bar'     => _x( 'Artiklar', 'add new on admin bar', 'ev-intranet' ),
        'add_new'            => _x( 'Lägg till ny', 'Artiklar', 'ev-intranet' ),
        'add_new_item'       => __( 'Lägg till ny Artiklar', 'ev-intranet' ),
        'new_item'           => __( 'New Artiklar', 'ev-intranet' ),
        'edit_item'          => __( 'Edit Artiklar', 'ev-intranet' ),
        'view_item'          => __( 'View Artiklar', 'ev-intranet' ),
        'all_items'          => __( 'Alla artiklar', 'ev-intranet' ),
        'search_items'       => __( 'Search Artiklar', 'ev-intranet' ),
        'parent_item_colon'  => __( 'Parent Artiklar:', 'ev-intranet' ),
        'not_found'          => __( 'No Artiklar found.', 'ev-intranet' ),
        'not_found_in_trash' => __( 'No Artiklar found in Trash.', 'ev-intranet' )
    );

    $articles_args = array(
        'labels'             => $articles_labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'article' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'menu_icon'          => 'dashicons-clipboard',
        'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt','author' )
    );

    register_post_type( 'ev_articles', $articles_args );

}

function ev_add_articles_meta_box() {
    add_meta_box(
        'ev-articles-meta-box',
        __( 'Inställningar','ev-intranet' ),
        'render_article_meta_box',
        'ev_articles',
        'normal',
        'default'
    );
}
add_action( 'add_meta_boxes_ev_articles', 'ev_add_articles_meta_box' );

function render_article_meta_box($post){
    $selected_design = get_post_meta($post->ID, 'intranet_news_design_select_meta', true);
    wp_nonce_field( basename( __FILE__ ), 'ev_article_nonce' );
    ?>
    <p>
        <label for="intranet_news_design_select"><?php _e( 'Design:', 'ev-intranet' )?></label>
        <select name="intranet_news_design_select" id="intranet_news_design_select">
            <?php
            $designs = array(
                'design_1' => __('1 Kolumn','ev-intranet'),
                'design_2' => __('2 kolumner','ev-intranet'),
                'design_3' => __('3 kolumner','ev-intranet'),
            );
            if(!empty($designs)){
                foreach($designs as $design => $name){
                    ?>
                    <option value="<?php echo $design;?>" <?php selected($design,$selected_design);?>>
                        <?php echo $name;?>
                    </option>
                <?php
                }
            }
            ?>
        </select>
    </p>
<?php
}

function ev_articles_meta_save( $post_id ) {

    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ 'ev_article_nonce' ] ) && wp_verify_nonce( $_POST[ 'ev_article_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
    if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
        return;
    }

    if(isset($_POST["intranet_news_design_select"])){
        update_post_meta($post_id, 'intranet_news_design_select_meta', $_POST["intranet_news_design_select"]);
    }

    if ( isset( $_POST['sk_post_order'] ) ) {
        if(!empty($_POST['sk_post_order'])){
            update_post_meta( $post_id, 'sk_post_order', (int)$_POST['sk_post_order'] );
        }else{
            /*Storing a huge number so that the key exists on each post with higher value*/
            update_post_meta( $post_id, 'sk_post_order', 999999 );
        }
    }
}
add_action( 'save_post_ev_articles', 'ev_articles_meta_save' );
