<?php
$search_query = $style = '';
if( isset($_GET['q']) && !empty($_GET['q']) ):
    $search_query = $_GET['q'];
endif;
if( isset($_GET['search']) && !empty($_GET['search']) ):
    $search_query = $_GET['search'];
endif;

$style = 'Rutnät';
if( isset($_GET['style']) && !empty($_GET['style']) ):
    if('list' == $_GET['style']){
        $style = 'Listvy';
    }
endif;

$retrive_style = (isset($_GET['style']) && !empty($_GET['style']))?$_GET['style']:'grid';

?>
	<div class="filter clearfix">
		<div class="col-sm-5"></div>
		<div class="col-sm-7">
			<div class="grid-filter">
				<div class="search-form">
					<form action="<?php ?>" id="searchform" method="get">
						<div class="search-box">
	            			<input type="search" id="" class="search-field" placeholder="<?php _e('Sök vad du vill…','ev-intranet');?>"
								value="<?php echo $search_query;?>"
								name="<?php if(is_home() || is_front_page()) { echo 'q'; } else { echo 'search';}?>" />
								<?php if( isset($_GET['style']) && !empty($_GET['style']) ): ?>
								<input type="hidden" name="style" value="<?php echo $_GET['style']; ?>" />
								<?php endif; ?>
				            
				            	<button type="submit" value="" id="searchsubmit">
									<svg class="icon icon-search">
										<path class="path1" d="M15.504 13.616l-3.79-3.223c-0.392-0.353-0.811-0.514-1.149-0.499 0.895-1.048 1.435-2.407 1.435-3.893 0-3.314-2.686-6-6-6s-6 2.686-6 6 2.686 6 6 6c1.486 0 2.845-0.54 3.893-1.435-0.016 0.338 0.146 0.757 0.499 1.149l3.223 3.79c0.552 0.613 1.453 0.665 2.003 0.115s0.498-1.452-0.115-2.003zM6 10c-2.209 0-4-1.791-4-4s1.791-4 4-4 4 1.791 4 4-1.791 4-4 4z"></path>
									</svg>
								</button>
	        			</div>
    				</form>
				</div><!-- search-form -->
				
				<div class="grid-option">
					<span class="grid-text"><?php echo $style;?>:</span>

					<a href = "<?php echo add_query_arg( 'style','grid');?>" <?php echo ( $retrive_style == 'grid')?'class="active"':''; ?> ><span class="grid-view"><i class="fa fa-th" aria-hidden="true"></i></span></a>
					<a href = "<?php echo add_query_arg( 'style' , 'list');?>" <?php echo ( $retrive_style == 'list')?'class="active"':''; ?> ><span class="list-view"><i class="fa fa-bars" aria-hidden="true"></i></span></a>
				
				</div><!-- grid-option -->
			</div><!-- grid-filter -->
		</div>
	</div><!-- filter -->

