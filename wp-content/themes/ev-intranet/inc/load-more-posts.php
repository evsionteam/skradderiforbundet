<?php
function load_more_posts(){

    /*required for course only*/
    $counter = 1;
    /**/

    $output['more_post'] = false;
    $output['data'] = '';
    $offset = $_POST['offset'];

    $loading_fn = 'item_load_posts';

    $args = array(
        'posts_per_page' => 10,
        'offset' => $offset,
    );

    $loading_from = $_POST['loading_from'];

    if(!empty($loading_from)){
        if('home' == $loading_from){
            $args['post_type'] = array( 'ev_news', 'ev_articles', 'ev_courses','ev_exhibitions' );
        }
        if('news' == $loading_from){
            $args['post_type'] = array( 'ev_news' );
        }
        if('article' == $loading_from){
            $args['post_type'] = array( 'ev_articles' );
        }
        if('organization' == $loading_from){
            if(!empty($_POST['org_id'])){
                $organization_admin = get_post_meta($_POST['org_id'],'_organization_admin',true);
                if(!empty($organization_admin)):
                    $args['post_type'] = array( 'ev_news', 'ev_articles', 'ev_courses','ev_exhibitions' );
                    $args['author'] = $organization_admin;
                endif;
            }
        }
        if('course' == $loading_from){
            $args['post_type'] = array( 'ev_courses' );
            $args['posts_per_page'] = 9;
            $loading_fn = 'item_load_course';
        }
        if('congress' == $loading_from){
            $args['post_type'] = array( 'ev_world_congress' );
            $args['posts_per_page'] = 9;
        }

        if('exhibition' == $loading_from){
            $args['post_type'] = array( 'ev_exhibitions' );
            $args['posts_per_page'] = 9;
        }
    }

    if(isset($_POST['search']) && !empty($_POST['search'])){
        $args['s'] =  $_POST['search'];
    }

    $query = new WP_Query($args);
    if ($query->have_posts()):
        $output['more_post'] = true;
        while ($query->have_posts()):$query->the_post();
            ob_start();
            $loading_fn($counter);
            $output['data'][] = ob_get_clean();
        endwhile;wp_reset_query();
    else:
        $output['more_post'] = false;
        $output['data'] = __('Inga fler inlägg','ev-intranet');
    endif;

    echo json_encode($output);
    exit;
}
add_action('wp_ajax_nopriv_load_more_posts', 'load_more_posts');
add_action('wp_ajax_load_more_posts', 'load_more_posts');

