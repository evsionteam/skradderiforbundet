<?php
class EV_USERS_ROUTES extends WP_REST_Controller{

    public function register_routes(){
        $namespace = 'ev';
        register_rest_route( $namespace, '/user/(?P<id>\d+)', array(
            array(
                'methods' => WP_REST_Server::READABLE,
                'callback' => array( $this, 'get' ),
                'args' => array(
                    'id' => array(
                        'validate_callback' => function ( $param, $request, $key ) {
                                return is_numeric( $param );
                            }
                    )
                ),
            )
        ));
    }

    public function get($request){
        $data = $request->get_params();
        $user_id = $data['id'];
        $member_info = get_userdata($user_id);
        if(!empty($member_info)){
            $member_email = $member_info->data->user_email;
            $upload_dir = wp_upload_dir();
            $pdf = md5($member_email);
            if(!empty($pdf)){
                $attachment_path = $upload_dir['basedir'].'/users/'.$pdf.'.pdf';
                header('Content-type: application/pdf');
                header('Content-Disposition: attachment; filename="'.md5(time()).'.pdf"');
                readfile($attachment_path);
            }
        }
        return true;
    }
}

add_action('rest_api_init', 'ev_register_webhook_route');
function ev_register_webhook_route(){
    $users = new EV_USERS_ROUTES();
    $users->register_routes();
}