<?php

/*Notify members upon saving*/
add_action( 'post_submitbox_misc_actions', 'ev_member_notification_fn');
function ev_member_notification_fn() {
    global $post;
    if ( in_array(get_post_type($post), array('ev_articles','ev_courses','ev_exhibitions','ev_news','ev_world_congress' )) ) {
        echo '<div class="misc-pub-section misc-pub-notify" style="border-top: 1px solid #eee;">';
        wp_nonce_field( basename( __FILE__ ), 'notify_member_nonce' );
        $notify_member = get_post_meta( $post->ID, 'notify_member', true );
        ?>
        <input type="checkbox" name="notify_member" id="notify-member" value="1" <?php checked( $notify_member, '1' ); ?> />
        <label for="notify-member"><?php _e( 'Notify Member','ev-intranet' )?></label><br/>
        <?php echo '</div>';
    }
}

add_action( 'save_post', 'ev_notify_member' );
function ev_notify_member( $post_id ) {

    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ 'notify_member_nonce' ] ) && wp_verify_nonce( $_POST[ 'notify_member_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';

    if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
        return;
    }

    if( isset( $_POST[ 'notify_member' ] ) ) {
        update_post_meta( $post_id, 'notify_member', $_POST[ 'notify_member' ] );

        /*send mail to all the active members*/
        global $current_user;
        $members_mail = array();

        $member_args = array(
            'role' => 'member',
            'meta_query' => array(
                array(
                    'key'     => 'approved_member',
                    'value'   => 1,
                    'compare' => '='
                ),
            )
        );
        if(in_array('local_admin',$current_user->roles)){
            $member_args['meta_query']['relation'] = 'AND';
            $member_args['meta_query'][] = array(
                'key'     => '_local_admin',
                'value'   => $current_user->ID,
                'compare' => '='
            );
        }
        $members = get_users($member_args);
        if(!empty($members)){
            foreach($members as $member){
                $members_mail[] = $member->data->user_email;
            }
        }


        if(!empty($members_mail)){

            global $ev_intranet;
            $logo = '';

            if(in_array('local_admin',$current_user->roles)){
                $org_id =  get_user_meta($current_user->ID,'_organization_id',true);
                if(!empty($org_id)){
                    $logo = get_post_meta($org_id,'organization_logo',true);
                    if(!empty($logo)){
                        $logo = '<img src="'.$logo.'" width="120" alt="" border="0" mc:edit="40">';
                    }
                }
            }else{
                /*site logo*/
                if( isset($ev_intranet['logo-image']) && !empty($ev_intranet['logo-image']) ){
                    $logo = '<img src="'.$ev_intranet['logo-image']['url'].'" width="120" alt="" border="0" mc:edit="40">';
                }
                /**/
            }

            $subject = ( isset($ev_intranet["notify-mail-subject"]) && !empty($ev_intranet["notify-mail-subject"]) ) ? $ev_intranet["notify-mail-subject"] : 'New Post on the site';
            $banner_message = ( isset($ev_intranet["notify-mail-message"]) && !empty($ev_intranet["notify-mail-message"]) ) ? $ev_intranet["notify-mail-message"] : get_the_title();
            $heading = ( isset($ev_intranet["notify-mail-heading"]) && !empty($ev_intranet["notify-mail-heading"]) ) ? $ev_intranet["notify-mail-heading"] : '';
            $content = ( isset($ev_intranet["notify-mail-content"]) && !empty($ev_intranet["notify-mail-content"]) ) ? $ev_intranet["notify-mail-content"] : '';
            $btn_text = ( isset($ev_intranet["notify-mail-btn-text"]) && !empty($ev_intranet["notify-mail-btn-text"]) ) ? $ev_intranet["notify-mail-btn-text"] : 'Read More';

            $heading = str_replace( '{postTitle}', get_the_title(), $heading );
            $content = str_replace( '{postContent}', wp_trim_words( $_POST['content'], 40, '...' ), $content );

            $body = file_get_contents( __DIR__.'/email-template/notify.htm');

            $body = str_replace( '{BannerImage}', $logo, $body );
            $body = str_replace( '{shortMessage}', $banner_message, $body );
            $body = str_replace( '{postTitle}', $heading, $body );
            $body = str_replace( '{postContent}', $content, $body );
            $body = str_replace( '{ButtonText}', $btn_text, $body );
            $body = str_replace( '{ButtonUrl}', get_the_permalink(get_the_ID()), $body );

            $headers = array('Content-Type: text/html; charset=UTF-8','From: Skrädderiförbundet <skradderiforbundet@wordpress.org>');
            foreach($members_mail as $to){
                wp_mail( $to, $subject, $body, $headers );
            }
        }
        /**/
    } else {
        update_post_meta( $post_id, 'notify_member', 0 );
    }
}