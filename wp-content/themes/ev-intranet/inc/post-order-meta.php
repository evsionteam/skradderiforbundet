<?php

if(is_user_logged_in()){
    global $current_user;
    if(!in_array('administrator',$current_user->roles)){
        return;
    }
}

/*Display post order in admin column*/
$post_types = array( 'ev_world_congress','ev_courses','ev_articles','ev_news','ev_exhibitions' );
foreach( $post_types as $post_type ){

    /*Adding custom column*/
    add_filter( 'manage_'.$post_type.'_posts_columns' , 'sk_add_post_order_column_head' );
    add_action( 'manage_'.$post_type.'_posts_custom_column' , 'sk_add_post_order_column_content', 10, 2 );
    /**/

    /*Making the custom column sortable*/
    add_filter( 'manage_edit-'.$post_type.'_sortable_columns', 'ev_sortable_post_order_post_column' );
    add_action( 'pre_get_posts', 'ev_sort_ft_post_orderby' );
    /**/
}
function sk_add_post_order_column_head( $columns ){
    $columns['sk_post_order_column'] = __( 'Order', 'ev-intranet' );
    return $columns;
}
function sk_add_post_order_column_content( $column, $post_id ){

    if ( 'sk_post_order_column' != $column ){
        return;
    }
    $order = get_post_meta( $post_id, 'sk_post_order', true );
    if( 999999 == $order ){
        $order = 'N/A';
    }
    echo '<div id="sk_post_order-' . $post_id . '">' . $order . '</div>';

}

function ev_sortable_post_order_post_column($sortable_columns ){
    $sortable_columns[ 'sk_post_order_column' ] = 'sk_post_order';
    return $sortable_columns;
}
function ev_sort_ft_post_orderby( $query ) {

    if( ! is_admin() ){
        return;
    }

    if ( $query->is_main_query() && ( $orderby = $query->get( 'orderby' ) ) ) {
        if( 'sk_post_order' == $orderby ) {
            $query->set('meta_key','sk_post_order');
            $query->set( 'orderby', 'meta_value_num' );
        }
    }

}
/**/

/*Add the custom column in bulk and quick edit options*/
//add_action( 'bulk_edit_custom_box', 'rachel_carden_add_to_bulk_quick_edit_custom_box', 10, 2 );
add_action( 'quick_edit_custom_box', 'sk_add_to_bulk_quick_edit_custom_box', 10, 2 );
function sk_add_to_bulk_quick_edit_custom_box( $column_name, $post_type ) {
    switch ( $post_type ) {
        case 'ev_courses':
        case 'ev_world_congress':
        case 'ev_articles':
        case 'ev_news':
        case 'ev_exhibitions':
            switch( $column_name ) {
                case 'sk_post_order_column':
                    ?>
                    <fieldset class="inline-edit-col-right">
                        <div class="inline-edit-col">
                            <label>
                                <span class="title"><?php _e('Order','ev-intranet')?></span>
                                <input type="text" name="sk_post_order" value="" />
                            </label>
                        </div>
                    </fieldset>
                    <?php
                    break;
            }
        break;
    }
}
