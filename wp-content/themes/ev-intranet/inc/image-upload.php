<?php
add_action( 'wp_ajax_ev_upload_image', 'ev_upload_image_callback' );
add_action( 'wp_ajax_nopriv_ev_upload_image', 'ev_upload_image_callback' );
function ev_upload_image_callback() {

    $data = array();
    $attachment_id = '';
    if( isset( $_POST['nonce'] ) && wp_verify_nonce( $_POST['nonce'], 'submit_content' ) ){
        global $current_user;
        if(!in_array('member',$current_user->roles)){
            $data['status'] = false;
            $data['message'] = __("Ledsen, men du måste logga in som en medlem för att kunna fortsätta.",'ev-intranet');
        }else{
            $files = $_FILES['files'];
            if ( empty($files) ) {
                $data['status'] = false;
                $data['message'] = __('Please select an image to upload!','ev-intranet');
            }

            if(empty($_POST['post_title'])){
                $data['status'] = false;
                $data['message'] = __('Please enter title!','ev-intranet');
            }

            if(empty($_POST['submit_as'])){
                $data['status'] = false;
                $data['message'] = __('Please enter Upload as value!','ev-intranet');
            }

            if( !empty($files) && !empty($_POST['post_title']) && !empty($_POST['submit_as'])){

                if ( $files['size'][0] > 512000 ) {
                    $data['size'] = $files['size'][0];
                    $data['status'] = false;
                    $data['message'] = __('Bilden är tyvärr större än 500kb. Förminska bilden och ladda upp igen. Om du inte vet hur man förminskar, maila oss bilden webredaktor@skradderiforbundet.se så hjälper vi dig','ev-intranet');
                } else {
                    if( $files['type'][0] == 'image/jpeg' || $files['type'][0] == 'image/png'){
                        $data['message'] = '';
                        $post_data = array(
                            'post_title' => sanitize_text_field($_POST['post_title']),
                            'post_status' => 'pending',
                            'post_type' => 'ev_image_gallery'
                        );

                        /*insert post and attachment*/
                        $post_id = wp_insert_post( $post_data );
                        if($post_id){

                            /*save meta values*/
                            update_post_meta($post_id,'site_link',sanitize_text_field($_POST['site_link']));
                            $submit_value = '';
                            $submit_as = $_POST['submit_as'];
                            if('person' == $submit_as){
                                $submit_value = get_user_meta(get_current_user_id(),'first_name',true);
                                $submit_value .= ' '.get_user_meta(get_current_user_id(),'last_name',true);
                            }elseif('company' == $submit_as){
                                $submit_value = get_user_meta(get_current_user_id(),'company_name',true);
                            }
                            update_post_meta($post_id,'submit_as',$submit_value);
                            /**/

                            $file = array(
                                'name'      => $files['name'][0],
                                'type'      => $files['type'][0],
                                'tmp_name'  => $files['tmp_name'][0],
                                'error'     => $files['error'][0],
                                'size'      => $files['size'][0]
                            );
                            $attachment_id = ev_upload_image_fn( $file, $post_id, true );


                            /*Notify admin*/
                            $to = get_option('admin_email');
                            if(!empty($to)){

                                $post_edit_link = get_edit_post_link($post_id);
                                $logo = $banner_message = '';

                                global $ev_intranet;
                                /*site logo*/
                                if( isset($ev_intranet['logo-image']) && !empty($ev_intranet['logo-image']) ){
                                    $logo = '<img src="'.$ev_intranet['logo-image']['url'].'" width="120" alt="" border="0" mc:edit="40">';
                                }
                                /**/

                                $subject = __('New Image Uploaded to site','ev-intranet');
                                $banner_message = __('Ett nytt foto har laddats upp!<br/> Logga in genom att klicka på denna länk och publicera fotot. ','ev-intranet');
                                $banner_message .= '<br/>';
                                $banner_message .= '<a href="'.$post_edit_link.'" style="color:#000000;">'.$post_edit_link.'</a>';


                                $body = file_get_contents( __DIR__.'/email-template/admin-notify-image-upload.htm');

                                $body = str_replace( '{BannerImage}', $logo, $body );
                                $body = str_replace( '{shortMessage}', $banner_message, $body );

                                $headers = array('Content-Type: text/html; charset=UTF-8','From: Skrädderiförbundet <skradderiforbundet@wordpress.org>');
                                wp_mail( $to, $subject, $body, $headers );
                                //wp_mail( 'cloudrubal@gmail.com', $subject, $body, $headers );

                            }
                            /**/

                        }
                        /**/

                        if ( is_numeric($attachment_id) ) {
                            $img_thumb = wp_get_attachment_image_src( $attachment_id, 'thumbnail' );
                            $data['status'] = true;
                            $data['message'] .=
                                '<div id="attachment-'.$attachment_id.' "class="sk-small-thumbnail">
                            <img src="'.$img_thumb[0].'" alt="" />
                        </div>';
                        }

                        if( ! $attachment_id ){
                            $data['status'] = false;
                            $data['message'] = __('An error has occurred. Your image was not added.','ev-intranet');
                        }
                    }else{
                        $data['status'] = false;
                        $data['message'] = __('Please select proper image type.','ev-intranet');
                    }

                }
            }
        }
    } else {
        $data['status'] = false;
        $data['message'] = __('Nonce verify failed.','ev-intranet');
    }
    echo json_encode($data);
    die();
}

function ev_upload_image_fn( $file_handler, $post_id = 0 , $set_as_featured = false ) {

    $attach_id = '';
    if ($_FILES[$file_handler]['error'] !== UPLOAD_ERR_OK) __return_false();

    require_once(ABSPATH . "wp-admin" . '/includes/image.php');
    require_once(ABSPATH . "wp-admin" . '/includes/file.php');
    require_once(ABSPATH . "wp-admin" . '/includes/media.php');

    $_FILES = array('ev_image_upload' => $file_handler);
    foreach ($_FILES as $file => $array) {
        $attach_id = media_handle_upload( $file, $post_id );
    }
    if ( is_wp_error( $attach_id ) ) {
        return '';
    } else {
        if( $set_as_featured == true ) {
            set_post_thumbnail( $post_id, $attach_id );
        }
        return $attach_id;
    }
}


/*Remove Image*/
add_action( 'wp_ajax_remove_image', 'ev_remove_image_callback' );
add_action( 'wp_ajax_nopriv_remove_image', 'ev_remove_image_callback' );
function ev_remove_image_callback() {

    $data = array();
    $permission = check_ajax_referer( 'delete_image_nonce', 'nonce', false );
    if( $permission == false ){
        $data['status'] = false;
        $data['message'] = __('You don\'t have privilege.','ev-intranet');
    }else{
       $post_id = $_POST['post_id'];
       $current_user_id = get_current_user_id();
        if( !empty($post_id) && !empty($current_user_id) ){
            $post_author = get_post_field( 'post_author', $post_id );
            if($post_author == $current_user_id){
                /*Delete post and attachment*/
                wp_delete_attachment( get_post_thumbnail_id( $post_id ), true );
                wp_delete_post($post_id,true);
                /**/
                $data['status'] = true;
                $data['message'] = __('Successfully removed the image.','ev-intranet');
            }else{
                $data['status'] = false;
                $data['message'] = __('You don\'t have privilege.','ev-intranet');
            }
        } 
    }
    
    echo json_encode($data);
    die();
}