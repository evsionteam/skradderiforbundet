<?php
add_action( 'wp_ajax_ev_contact_form_message', 'ev_contact_form_message' );
add_action( 'wp_ajax_nopriv_ev_contact_form_message', 'ev_contact_form_message' );
function ev_contact_form_message() {
    $data = $attachments = array();
    $data['status'] = false;

    if( isset( $_POST['nonce'] ) && wp_verify_nonce( $_POST['nonce'], 'submit_contact_form' ) ){
        $params = array();
        parse_str($_POST['form_values'], $params);

        global $current_user;
        if(!in_array('member',$current_user->roles)){
            $data['status'] = false;
            $data['message'] = __("Ledsen, men du måste logga in som en medlem för att kunna fortsätta.",'ev-intranet');
        }else{
            if( empty($params['subject']) || empty($params['description']) || empty($params['topic']) ){
                $data['message'] = __('Please fill all the required fields.','ev-intranet');
            }else{
                $files = $_FILES['files'];
                if( !empty($files)){
                    if ( $files['size'][0] > 3145728 ) {
                        $data['message'] = __('Attachment is too large. It must be less than 3M!','ev-intranet');
                        die;
                    }

                    $temp = explode(".", $files["name"][0]);
                    $extension = end($temp);
                    $newname= $files["tmp_name"][0].".".$extension;
                    rename($files["tmp_name"][0],$newname);
                    $attachments = array( $newname );
                }

                $headers = 'From: Skradderiforbundet <skradderiforbundet@wordpress.org>' . "\r\n";

                $message = "Regarding:" . "\r\n\r\n";
                $message .= $params['topic'] . "\r\n\r\n";
                $message .= "Link: ".$params['website'] . "\r\n\r\n";
                $message .= $params['description'];

                $local_admin_id = get_user_meta(get_current_user_id(),'_local_admin',true);
                if(!empty($local_admin_id)){
                    $user_info = get_userdata($local_admin_id);
                    $to = $user_info->data->user_email;
                    if(wp_mail($to, $params['subject'] , $message  , $headers, $attachments)){
                        $data['status'] = true;
                        $data['message'] = __('Message sent successfully!','ev-intranet');
                    }else{
                        $data['status'] = false;
                        $data['message'] = __('Unable to Send the message!','ev-intranet');
                    }
                }else{
                    $data['status'] = false;
                    $data['message'] = __('Some error occurred!','ev-intranet');
                }
                /**/
            }
        }
    }
    echo json_encode($data);
    die();
}