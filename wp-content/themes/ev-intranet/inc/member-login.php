<?php
function member_login_form_fn() {
    if(!is_user_logged_in()) {
        ob_start();
        member_login_form();
        $output = ob_get_clean();
    } else {
        $output = "<div class='form-error alert alert-danger'>".__('Du är redan inloggad.','ev-intranet')."</div>";
    }
    return $output;
}
add_shortcode('member_login_form', 'member_login_form_fn');

add_action('init','ev_member_login_validation');
function ev_member_login_validation(){

    if ( isset($_POST['submit_login']) && wp_verify_nonce($_POST['member_login_nonce'], 'member-login-nonce') ) {

        $username = $_POST['username'];
        $password = $_POST['password'];

        global $reg_errors;
        $reg_errors = new WP_Error;

        if ( empty( $username ) || empty( $password ) ) {
            $reg_errors->add('field', __('Vänligen fyll i alla nödvändiga fält.','ev-intranet'));
        }

        /*Check user info from the user name*/
        if(!empty($username)){
            if (filter_var($username, FILTER_VALIDATE_EMAIL)) {
                $user = get_user_by('email', $username);
            } else {
                $user = get_user_by('login', $username);
            }
            if(!$user) {
                $reg_errors->add('empty_username', __('Ogiltigt användarnamn eller email.','ev-intranet'));
            }
        }

        /*Check the user's login with their password and member status*/
        if( !empty($password) && !empty($user) ){
            if(!wp_check_password($password, $user->user_pass, $user->ID)) {
                /*if the password is incorrect for the specified user*/
                $reg_errors->add('incorrect_password', __('Ogiltigt lösenord.','ev-intranet'));
            }else{
                $approved_member = get_user_meta($user->ID,'approved_member',true);
                if(false == $approved_member){
                    $reg_errors->add('not_approved', __('Sorry, your account has not been approved yet.','ev-intranet'));
                }
            }
        }

        /*Sanitize form input fields*/
        $username	= 	sanitize_user($username);
        $password 	= 	sanitize_text_field($password);

        if ( count($reg_errors->get_error_messages()) < 1 ) {
            $creds = array('user_login' => $username, 'user_password' => $password);
            $user = wp_signon( $creds );
            if ( is_wp_error($user) ){
                echo $user->get_error_message();
            }else{
                wp_redirect( site_url() );
                exit;
            }
        }
    }

}

function member_login_form() {
    global $reg_errors;
    if ( is_wp_error( $reg_errors ) ) {
        echo '<div class="form-validation-errors">';
        foreach ( $reg_errors->get_error_messages() as $error ) {
            echo "<p class='form-error alert alert-danger'>$error</p>";
        }
        echo '</div>';
    }
    ?>
    <div class="staff-form login-form">
        <h3 class="staff_header"><?php _e('Logga in'); ?></h3>
        <form action="<?php echo $_SERVER['REQUEST_URI'];?>" method="post">
            <fieldset>
                <div class="form-group">
                    <label for="username"><?php _e('Anställnings ID / Email ','ev-intranet'); ?>*</label>
                    <input name="username" id="username" type="text" required value="<?php echo isset($_POST['username']) ? $_POST['username'] : '';?>"/>
                </div>
                <div class="form-group">
                    <label for="password"><?php _e('Lösenord','ev-intranet'); ?>*</label>
                    <input name="password" id="password"  type="password" required/>
                </div>
                <div class="form-group">
                    <input type="hidden" name="member_login_nonce" value="<?php echo wp_create_nonce('member-login-nonce'); ?>"/>
                    <input type="submit" name="submit_login" value="<?php _e('Logga in','ev-intranet'); ?>"/>
                    <div class="register-here">
                        <span>
                            <?php _e('Har du inte ett konto?','ev-intranet');?>
                        </span>
                        <a href='<?php echo network_site_url('/bli-medlem/')?>'><?php _e('Registrera ditt konto här','ev-intranet')?></a>
                    </div>
                    <div class="lost-password">
                        <a href='<?php echo esc_url( wp_lostpassword_url() ); ?>'><?php _e('Glömt lösenordet?','ev-intranet')?></a>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
    <?php
}