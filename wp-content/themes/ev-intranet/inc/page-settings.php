<?php
function ev_add_page_meta_box() {
    add_meta_box(
        'ev-page-meta-box',
        __( 'Page Settings','ev-intranet' ),
        'render_page_meta_box',
        'page',
        'normal',
        'default'
    );
}
add_action( 'add_meta_boxes_page', 'ev_add_page_meta_box' );

function render_page_meta_box($post){
    wp_nonce_field( basename( __FILE__ ), 'ev_page_nonce' );
    $btn_text = get_post_meta($post->ID,'_btn_text',true);
    $btn_link = get_post_meta($post->ID,'_btn_link',true);
    ?>
    <div>
        <p><label for="btn-text"><?php _e( 'Knapp text:', 'ev-intranet' )?></label></p>
        <p><input name="btn_text" id="btn-text" type="text" value="<?php echo $btn_text;?>"  size="30"/></p>
    </div>
    <div>
        <p><label for="btn-link"><?php _e( 'Knapp länk:', 'ev-intranet' )?></label></p>
        <p><input name="btn_link" id="btn-link" type="text" value="<?php echo $btn_link;?>" size="80"/></p>
    </div>
<?php
}

function ev_page_meta_save( $post_id ) {

    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ 'ev_page_nonce' ] ) && wp_verify_nonce( $_POST[ 'ev_page_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
    if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
        return;
    }

    if( isset( $_POST[ 'btn_text' ] ) ) {
        update_post_meta( $post_id, '_btn_text', sanitize_text_field($_POST[ 'btn_text' ]) );
    }
    if( isset( $_POST[ 'btn_link' ] ) ) {
        update_post_meta( $post_id, '_btn_link', sanitize_text_field($_POST[ 'btn_link' ]) );
    }
}
add_action( 'save_post_page', 'ev_page_meta_save' );