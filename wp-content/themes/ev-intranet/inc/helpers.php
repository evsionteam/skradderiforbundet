<?php

if ( ! function_exists( 'ev_intranet_posted_on' ) ) :
	function ev_intranet_posted_on() {
		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
		}

		$time_string = sprintf( $time_string,
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() ),
			esc_attr( get_the_modified_date( 'c' ) ),
			esc_html( get_the_modified_date() )
		);

		$posted_on = sprintf(
			esc_html_x( 'Posted on %s', 'post date', 'ev-intranet' ),
			'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
		);

		$byline = sprintf(
			esc_html_x( 'by %s', 'post author', 'ev-intranet' ),
			'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
		);

		echo '<span class="posted-on">' . $posted_on . '</span><span class="byline"> ' . $byline . '</span>'; // WPCS: XSS OK.
	}
endif;

if ( ! function_exists( 'ev_intranet_entry_footer' ) ) :
	function ev_intranet_entry_footer() {
		if ( 'post' === get_post_type() ) {
			$categories_list = get_the_category_list( esc_html__( ', ', 'ev-intranet' ) );
			if ( $categories_list && ev_intranet_categorized_blog() ) {
				printf( '<span class="cat-links">' . esc_html__( 'Posted in %1$s', 'ev-intranet' ) . '</span>', $categories_list );
			}

			$tags_list = get_the_tag_list( '', esc_html__( ', ', 'ev-intranet' ) );
			if ( $tags_list ) {
				printf( '<span class="tags-links">' . esc_html__( 'Tagged %1$s', 'ev-intranet' ) . '</span>', $tags_list ); // 
			}
		}

		if ( ! is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
			echo '<span class="comments-link">';
			comments_popup_link( sprintf( wp_kses( __( 'Leave a Comment<span class="screen-reader-text"> on %s</span>', 'ev-intranet' ), array( 'span' => array( 'class' => array() ) ) ), get_the_title() ) );
			echo '</span>';
		}

		edit_post_link(
			sprintf(
				esc_html__( 'Edit %s', 'ev-intranet' ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			),
			'<span class="edit-link">',
			'</span>'
		);
	}
endif;

function ev_intranet_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'ev_intranet_categories' ) ) ) {
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,
			'number'     => 2,
		) );

		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'ev_intranet_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		return true;
	} else {

		return false;
	}
}

function ev_intranet_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	delete_transient( 'ev_intranet_categories' );
}
add_action( 'edit_category', 'ev_intranet_category_transient_flusher' );
add_action( 'save_post',     'ev_intranet_category_transient_flusher' );


set_theme_package();
function set_theme_package(){

	$theme_dir = get_template_directory();

	/* Load Project Settings */
	$package_file = $theme_dir.'/package.json';
	$project_name = "evision Intranet";

	if(file_exists($package_file)){
		$package = file_get_contents($package_file);	
		$settings = json_decode($package,true);
		if( isset($settings['project']) && !empty($settings['project']) ){
			$project_name = $settings['project'];
		}
	}
	define('PROJECT_NAME',$project_name);

	/* Load CPT */
	if ($handle = opendir($theme_dir.'/inc/post-types/')) {
	    while (false !== ($entry = readdir($handle))) {
	        if ($entry != "." && $entry != "..") {
	            require_once $theme_dir.'/inc/post-types/'.$entry;
	        }
	    }

	    closedir($handle);
	}

}



add_filter( 'get_the_excerpt', 'control_size_of_exerpt',10,2);
function control_size_of_exerpt($post_excerpt, $post=null ){
	if(!$post){
		return $post_excerpt;
	}
	if(is_post_type_archive($post->post_type) && strlen($post_excerpt) > 86 ){
		$post_excerpt = substr( $post_excerpt, 0, 86 ). ' ...';
	}

	return $post_excerpt;
}

/* Removes VC shortcode coming from monitor site */
function ev_get_safe_content($content){
	$content = str_replace('vc_column_text', '', $content);
	$content = str_replace('vc_column', '', $content);
	$content = str_replace('vc_row', '', $content);
	$content = str_replace('[]', '', $content);
	$content = str_replace('[/]', '', $content);
	return $content;
}


/* Add additional parameter in oEmbeded URL */
add_filter('oembed_result', 'append_additional_param_in_oembed_video',10,3);
function append_additional_param_in_oembed_video($html, $url, $args) {
    return str_replace("?feature=oembed", "?feature=oembed&autoplay=1", $html);
}
