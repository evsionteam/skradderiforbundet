<?php
/* Add organization info for members and local admins while user Registration */
add_action( 'user_new_form', 'sk_add_org_settings' );
function sk_add_org_settings($status) {
    if('add-new-user' == $status){

        /*fetch organizations*/
        $all_organizations = array();
        $args = array(
            'post_type' => 'ev_organization',
            'posts_per_page' => -1,
            'posts_status' => 'publish',
        );
        $organization = new WP_Query($args);
        if($organization->have_posts()):
            while($organization->have_posts()):$organization->the_post();
                $all_organizations[get_the_ID()] = get_the_title();
            endwhile;wp_reset_postdata();
        endif;
        /**/

        /*only show if there are organizations*/
        if(!empty($all_organizations)){
            ?>
            <h3 class="organization-info-table" style="display: none"><?php _e('Additional Settings','ev-intranet')?></h3>
            <table class="form-table organization-info-table" hidden>
                <tbody>
                    <tr>
                        <th><label for="organization"><?php _e( 'Organization', 'ev-intranet' ); ?></label></th>
                        <td>
                            <select name="organization" id="organization">
                                <option value=""><?php _e( '&mdash; Select &mdash;', 'supporterskalet' ); ?></option>
                                <?php foreach ($all_organizations as $organization_id => $organization_name): ?>
                                    <option value="<?php echo $organization_id; ?>"><?php echo $organization_name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                </tbody>
            </table>
            <?php
        }
    }
}

add_action( 'user_register', 'sk_save_org_settings' );
function sk_save_org_settings( $user_id ) {
    if(isset($_POST['organization'])){
        update_user_meta( $user_id, '_organization_id', $_POST['organization'] );
        /*change the organization admin meta value too*/
        if(!empty($_POST[ 'organization' ])){
            if( isset($_POST['role']) && !empty($_POST['role'])){
                if('local_admin' == $_POST['role']){
                    update_post_meta($_POST[ 'organization' ],'_organization_admin',$user_id);
                }
                if('member' == $_POST['role']){
                    $organization_admin = get_post_meta($_POST[ 'organization' ],'_organization_admin',true);
                    if(!empty($organization_admin)){
                        update_user_meta($user_id, '_local_admin', $organization_admin);
                    }
                }
            }
        }
    }
}


add_action( 'show_user_profile', 'sk_show_user_extra_profile_fields' );
add_action( 'edit_user_profile', 'sk_show_user_extra_profile_fields' );
function sk_show_user_extra_profile_fields( $user ) {

    /*fetch organizations*/
    $all_organizations = array();
    $args = array(
        'post_type' => 'ev_organization',
        'posts_per_page' => -1,
        'posts_status' => 'publish',
    );
    $organization = new WP_Query($args);
    if($organization->have_posts()):
        while($organization->have_posts()):$organization->the_post();
            $all_organizations[get_the_ID()] = get_the_title();
        endwhile;wp_reset_postdata();
    endif;
    /**/

    /*only show if there are organizations*/
    if(!empty($all_organizations)){
        $selected_organization =  get_the_author_meta( '_organization_id', $user->ID  );
        ?>
        <h2 class="organization-info-table" style="display: none"><?php _e('Additional Info','ev-intranet')?></h2>
        <table class="form-table organization-info-table" hidden>
            <tbody>
            <tr>
                <th><label for="organization"><?php _e( 'Organization', 'ev-intranet' ); ?></label></th>
                <td>
                    <select name="organization" id="organization">
                        <option value=""><?php _e( '&mdash; Select &mdash;', 'supporterskalet' ); ?></option>
                        <?php foreach ($all_organizations as $organization_id => $organization_name): ?>
                            <option value="<?php echo $organization_id; ?>" <?php selected($organization_id,$selected_organization);?>><?php echo $organization_name; ?></option>
                        <?php endforeach; ?>
                    </select>
                </td>
            </tr>


            <!--Member Details-->
            <?php
            if(in_array('member',$user->roles)):

                $member_info = get_user_meta($user->ID);

                $member_birthdate = isset($member_info['member_birthdate']) ? $member_info['member_birthdate'][0] : '';
                $member_profession = isset($member_info['member_profession']) ? $member_info['member_profession'][0] : '';
                $member_home_address = isset($member_info['member_home_address']) ? $member_info['member_home_address'][0] : '';
                $member_post_number = isset($member_info['member_post_number']) ? $member_info['member_post_number'][0] : '';
                $member_place = isset($member_info['member_place']) ? $member_info['member_place'][0] : '';
                $member_country = isset($member_info['member_country']) ? $member_info['member_country'][0] : '';
                $member_home_phone = isset($member_info['member_home_phone']) ? $member_info['member_home_phone'][0] : '';

                $date_of_certificate = isset($member_info['date_of_certificate']) ? $member_info['date_of_certificate'][0] : '' ;
                $master_in_year = isset($member_info['master_year']) ? $member_info['master_year'][0] : '' ;

                $company_name = isset($member_info['company_name']) ? $member_info['company_name'][0] : '';
                $company_phone = isset($member_info['company_phone']) ? $member_info['company_phone'][0] : '';
                $company_start_date = isset($member_info['company_start_date']) ? $member_info['company_start_date'][0] : '';
                $company_address = isset($member_info['company_address']) ? $member_info['company_address'][0] : '';
                $company_post_number = isset($member_info['company_post_number']) ? $member_info['company_post_number'][0] : '';
                $company_city = isset($member_info['company_city']) ? $member_info['company_city'][0] : '';
                $registered_for_tax = isset($member_info['company_f_tax']) ? $member_info['company_f_tax'][0] : '';
                $registered_for_vat = isset($member_info['company_vat']) ? $member_info['company_vat'][0] : '';
                $company_email = isset($member_info['company_email']) ? $member_info['company_email'][0] : '';
                $company_website = isset($member_info['company_website']) ? $member_info['company_website'][0] : '';

                $employee_company_name = isset($member_info['employee_company_name']) ? $member_info['employee_company_name'][0] : '';
                $employee_company_phone = isset($member_info['employee_company_phone']) ? $member_info['employee_company_phone'][0] : '';
                $employee_company_address = isset($member_info['employee_company_address']) ? $member_info['employee_company_address'][0] : '';
                $employee_company_post_number = isset($member_info['employee_company_post_number']) ? $member_info['employee_company_post_number'][0] : '';
                $employee_company_city = isset($member_info['employee_company_city']) ? $member_info['employee_company_city'][0] : '';

                $school_name = isset($member_info['school_name']) ? $member_info['school_name'][0] : '';
                $school_phone = isset($member_info['school_phone']) ? $member_info['school_phone'][0] : '';
                $school_email = isset($member_info['school_email']) ? $member_info['school_email'][0] : '';
                $school_address = isset($member_info['school_address']) ? $member_info['school_address'][0] : '';
                $school_post_number = isset($member_info['school_post_number']) ? $member_info['school_post_number'][0] : '';
                $school_place = isset($member_info['school_place']) ? $member_info['school_place'][0] : '';

                $master_in = get_user_meta($user->ID,'master_in',true);
                if(empty($master_in)){
                    $master_in = array();
                }

                $certificate_in = get_user_meta($user->ID,'certificate_in',true);
                if(empty($certificate_in)){
                    $certificate_in = array();
                }

                $company_nature = get_user_meta($user->ID,'company_nature',true);
                if(empty($company_nature)){
                    $company_nature = array();
                }

                ?>
                <tr>
                    <th><h2><?php _e('Personuppgifter', 'ev-intranet'); ?></h2></th>
                </tr>
                <tr>
                    <th><label for="member-birthdate"><?php _e( 'Födelsedatum', 'ev-intranet' ); ?></label></th>
                    <td>
                        <input type="text" id="member-birthdate" name="member_birthdate" size="50" value="<?php echo $member_birthdate;?>">
                    </td>
                </tr>
                <tr>
                    <th><label for="member-profession"><?php _e( 'Yrke', 'ev-intranet' ); ?></label></th>
                    <td>
                        <input type="text" id="member-profession" name="member_profession" size="50" value="<?php echo $member_profession;?>">
                    </td>
                </tr>
                <tr>
                    <th><label for="member-home-address"><?php _e( 'Hemadress', 'ev-intranet' ); ?></label></th>
                    <td>
                        <input type="text" id="member-home-address" name="member_home_address" size="50" value="<?php echo $member_home_address;?>">
                    </td>
                </tr>
                <tr>
                    <th><label for="member-post-number"><?php _e( 'Postnummer', 'ev-intranet' ); ?></label></th>
                    <td>
                        <input type="text" id="member-post-number" name="member_post_number" size="50" value="<?php echo $member_post_number;?>">
                    </td>
                </tr>
                <tr>
                    <th><label for="member-place"><?php _e( 'Stad', 'ev-intranet' ); ?></label></th>
                    <td>
                        <input type="text" id="member-place" name="member_place" size="50" value="<?php echo $member_place;?>">
                    </td>
                </tr>
                <tr>
                    <th><label for="member-country"><?php _e( 'Land', 'ev-intranet' ); ?></label></th>
                    <td>
                        <input type="text" id="member-country" name="member_country" size="50" value="<?php echo $member_country;?>">
                    </td>
                </tr>
                <tr>
                    <th><label for="member_home_phone"><?php _e( 'Telefonnummer', 'ev-intranet' ); ?></label></th>
                    <td>
                        <input type="text" id="member_home_phone" name="member_home_phone" size="50" value="<?php echo $member_home_phone;?>">
                    </td>
                </tr>
                <tr>
                    <th><h2><?php _e('Gesällbrev', 'ev-intranet'); ?></h2></th>
                </tr>
                <tr>
                    <th><label for="certificate_in"><?php _e( 'Gesällbrev', 'ev-intranet' ); ?></label></th>
                    <td>
                        <input type="checkbox" id="herr-certificate" name="certificate_in_man" class="form-control" <?php if(in_array('him',$certificate_in)){ echo 'checked="checked"'; } ?>/>
                        <label for="herr-certificate"><?php _e('Herr', 'ev-intranet'); ?></label>
                        <input type="checkbox" id="woman-certificate" name="certificate_in_woman" class="form-control" <?php if(in_array('her',$certificate_in)){ echo 'checked="checked"'; } ?>/>
                        <label for="woman-certificate"><?php _e('Dam', 'ev-intranet'); ?></label>
                        <input type="checkbox" id="dress-certificate" name="certificate_in_dress" class="form-control" <?php if(in_array('dress',$certificate_in)){ echo 'checked="checked"'; } ?>/>
                        <label for="dress-certificate"><?php _e('Klänning', 'ev-intranet'); ?></label>
                        <br/>
                        <label for="date_of_certificate"><?php _e( 'År för utfärdande', 'ev-intranet' ); ?></label>
                        <br/>
                        <input type="text" id="date_of_certificate" name="date_of_certificate" size="50" value="<?php echo $date_of_certificate;?>">
                    </td>
                </tr>
                <tr>
                    <th><h2><?php _e('Mästarbrev', 'ev-intranet'); ?></h2></th>
                </tr>
                <tr>
                    <th><label for="master_in"><?php _e( 'Mästarbrev', 'ev-intranet' ); ?></label></th>
                    <td>
                        <input type="checkbox" id="mastarbrev-herr" name="master_in_man" class="form-control" <?php if(in_array('him',$master_in)){ echo 'checked="checked"'; } ?>/>
                        <label for="mastarbrev-herr"><?php _e('Herr', 'ev-intranet'); ?></label>
                        <input type="checkbox" id="master-in-woman" name="master_in_woman" class="form-control" <?php if(in_array('her',$master_in)){ echo 'checked="checked"'; } ?>/>
                        <label for="master-in-woman"><?php _e('Dam', 'ev-intranet'); ?></label>
                        <input type="checkbox" id="membership-dress" name="master_in_dress" class="form-control" <?php if(in_array('dress',$master_in)){ echo 'checked="checked"'; } ?>/>
                        <label for="membership-dress"><?php _e('Klänning', 'ev-intranet'); ?></label>
                        <br/>
                        <label for="master-year"><?php _e( 'År för utfärdande', 'ev-intranet' ); ?></label>
                        <br/>
                        <input type="text" id="master-year" name="master_year" size="50" value="<?php echo $master_in_year;?>">
                    </td>
                </tr>
                <tr>
                    <th><h2><?php _e('Eget företag', 'ev-intranet'); ?></h2></th>
                </tr>
                <tr>
                    <th><label for="company_name"><?php _e( 'Företagsnamn', 'ev-intranet' ); ?></label></th>
                    <td>
                        <input type="text" id="company_name" name="company_name" size="50" value="<?php echo $company_name;?>">
                    </td>
                </tr>
                <tr>
                    <th><label for="company_phone"><?php _e( 'Telefon', 'ev-intranet' ); ?></label></th>
                    <td>
                        <input type="text" id="company_phone" name="company_phone" size="50" value="<?php echo $company_phone;?>">
                    </td>
                </tr>
                <tr>
                    <th><label for="company_start_date"><?php _e( 'Registreringsår', 'ev-intranet' ); ?></label></th>
                    <td>
                        <input type="text" id="company_start_date" name="company_start_date" size="50" value="<?php echo $company_start_date;?>">
                    </td>
                </tr>
                <tr>
                    <th><label for="company_address"><?php _e( 'Adress', 'ev-intranet' ); ?></label></th>
                    <td>
                        <input type="text" id="company_address" name="company_address" size="50" value="<?php echo $company_address;?>">
                    </td>
                </tr>
                <tr>
                    <th><label for="company_post_number"><?php _e( 'Postnummer', 'ev-intranet' ); ?></label></th>
                    <td>
                        <input type="text" id="company_post_number" name="company_post_number" size="50" value="<?php echo $company_post_number;?>">
                    </td>
                </tr>
                <tr>
                    <th><label for="company_nature"><?php _e( 'Verksamhetsområde', 'ev-intranet' ); ?></label></th>
                    <td>
                        <input type="text" id="company_nature" name="company_nature" size="50" value="<?php echo implode(',',$company_nature);?>">
                    </td>
                </tr>
                <tr>
                    <th><label for="company_city"><?php _e( 'Stad', 'ev-intranet' ); ?></label></th>
                    <td>
                        <input type="text" id="company_city" name="company_city" size="50" value="<?php echo $company_city;?>">
                    </td>
                </tr>
                <tr>
                    <th><label for="company_f_tax"><?php _e( 'Registrated F-TAX', 'ev-intranet' ); ?></label></th>
                    <td>
                        <input type="checkbox" id="company_f_tax" name="company_f_tax" class="form-control" value="yes" <?php checked( $registered_for_tax, 'yes' ); ?>/>
                        <label for="company_f_tax"><?php _e('Yes', 'ev-intranet'); ?></label>
                    </td>
                </tr>
                <tr>
                    <th><label for="company_vat"><?php _e( 'VAT Resistrated', 'ev-intranet' ); ?></label></th>
                    <td>
                        <input type="checkbox" id="company_vat" name="company_vat" class="form-control" value="yes" <?php checked( $registered_for_vat, 'yes' ); ?>/>
                        <label for="company_vat"><?php _e('Yes', 'ev-intranet'); ?></label>
                    </td>
                </tr>
                <tr>
                    <th><label for="company_email"><?php _e( 'Företagsemail', 'ev-intranet' ); ?></label></th>
                    <td>
                        <input type="text" id="company_email" name="company_email" size="50" value="<?php echo $company_email;?>">
                    </td>
                </tr>
                <tr>
                    <th><label for="company_website"><?php _e( 'Webbsida', 'ev-intranet' ); ?></label></th>
                    <td>
                        <input type="text" id="company_website" name="company_website" size="50" value="<?php echo $company_website;?>">
                    </td>
                </tr>

                <tr>
                    <th><h2><?php _e('Anställd på företag', 'ev-intranet'); ?></h2></th>
                </tr>
                <tr>
                    <th><label for="employee_company_name"><?php _e( 'Företagsnamn', 'ev-intranet' ); ?></label></th>
                    <td>
                        <input type="text" id="employee_company_name" name="employee_company_name" size="50" value="<?php echo $employee_company_name;?>">
                    </td>
                </tr>
                <tr>
                    <th><label for="employee_company_phone"><?php _e( 'Företagstelefon', 'ev-intranet' ); ?></label></th>
                    <td>
                        <input type="text" id="employee_company_phone" name="employee_company_phone" size="50" value="<?php echo $employee_company_phone;?>">
                    </td>
                </tr>
                <tr>
                    <th><label for="employee_company_address"><?php _e( 'Adress', 'ev-intranet' ); ?></label></th>
                    <td>
                        <input type="text" id="employee_company_address" name="employee_company_address" size="50" value="<?php echo $employee_company_address;?>">
                    </td>
                </tr>
                <tr>
                    <th><label for="employee_company_post_number"><?php _e( 'Postnummer', 'ev-intranet' ); ?></label></th>
                    <td>
                        <input type="text" id="employee_company_post_number" name="employee_company_post_number" size="50" value="<?php echo $employee_company_post_number;?>">
                    </td>
                </tr>
                <tr>
                    <th><label for="employee_company_city"><?php _e( 'Stad', 'ev-intranet' ); ?></label></th>
                    <td>
                        <input type="text" id="employee_company_city" name="employee_company_city" size="50" value="<?php echo $employee_company_city;?>">
                    </td>
                </tr>

                <tr>
                    <th><h2><?php _e('Aktiv lärare', 'ev-intranet'); ?></h2></th>
                </tr>
                <tr>
                    <th><label for="school_name"><?php _e( 'Skola', 'ev-intranet' ); ?></label></th>
                    <td>
                        <input type="text" id="school_name" name="school_name" size="50" value="<?php echo $school_name;?>">
                    </td>
                </tr>
                <tr>
                    <th><label for="school_phone"><?php _e( 'Telefon arbetsplats', 'ev-intranet' ); ?></label></th>
                    <td>
                        <input type="text" id="school_phone" name="school_phone" size="50" value="<?php echo $school_phone;?>">
                    </td>
                </tr>
                <tr>
                    <th><label for="school_email"><?php _e( 'Email', 'ev-intranet' ); ?></label></th>
                    <td>
                        <input type="text" id="school_email" name="school_email" size="50" value="<?php echo $school_email;?>">
                    </td>
                </tr>
                <tr>
                    <th><label for="school_address"><?php _e( 'Adress', 'ev-intranet' ); ?></label></th>
                    <td>
                        <input type="text" id="school_address" name="school_address" size="50" value="<?php echo $school_address;?>">
                    </td>
                </tr>
                <tr>
                    <th><label for="school_post_number"><?php _e( 'Postnummer', 'ev-intranet' ); ?></label></th>
                    <td>
                        <input type="text" id="school_post_number" name="school_post_number" size="50" value="<?php echo $school_post_number;?>">
                    </td>
                </tr>
                <tr>
                    <th><label for="school_place"><?php _e( 'Stad', 'ev-intranet' ); ?></label></th>
                    <td>
                        <input type="text" id="school_place" name="school_place" size="50" value="<?php echo $school_place;?>">
                    </td>
                </tr>
            <?php endif;?>
            <!---->

            </tbody>
        </table>
    <?php
    }
}


add_action( 'personal_options_update', 'sk_save_user_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'sk_save_user_extra_profile_fields' );
function sk_save_user_extra_profile_fields( $user_id ) {

    if ( !current_user_can( 'edit_user', $user_id ) ){
        return false;
    }

    if(isset($_POST['organization'])){
        update_user_meta( $user_id, '_organization_id', $_POST['organization'] );
        /*change the organization admin meta value too*/
        if(!empty($_POST[ 'organization' ])){
            $current_user = get_userdata($user_id);
            if(in_array('local_admin',$current_user->roles)){
                update_post_meta($_POST[ 'organization' ],'_organization_admin',$user_id);
            }
            if(in_array('member',$current_user->roles)){

                /*Personal Info*/
                $organization_admin = get_post_meta($_POST[ 'organization' ],'_organization_admin',true);
                if(!empty($organization_admin)){
                    update_user_meta($user_id, '_local_admin', $organization_admin);
                }
                if(isset($_POST['member_birthdate'])){
                    update_user_meta($user_id, 'member_birthdate', $_POST['member_birthdate']);
                }
                if(isset($_POST['member_profession'])){
                    update_user_meta($user_id, 'member_profession', $_POST['member_profession']);
                }
                if(isset($_POST['member_home_address'])){
                    update_user_meta($user_id, 'member_home_address', $_POST['member_home_address']);
                }
                if(isset($_POST['member_post_number'])){
                    update_user_meta($user_id, 'member_post_number', $_POST['member_post_number']);
                }
                if(isset($_POST['member_place'])){
                    update_user_meta($user_id, 'member_place', $_POST['member_place']);
                }
                if(isset($_POST['member_country'])){
                    update_user_meta($user_id, 'member_country', $_POST['member_country']);
                }
                if(isset($_POST['member_home_phone'])){
                    update_user_meta($user_id, 'member_home_phone', $_POST['member_home_phone']);
                }
                /**/

                /*Certificate Info*/
                if(isset($_POST['date_of_certificate'])){
                    update_user_meta($user_id, 'date_of_certificate', $_POST['date_of_certificate']);
                }
                $certificate_value = array();
                if( isset($_POST['certificate_in_man']) ){
                    $certificate_value[] = 'him';
                }
                if( isset($_POST['certificate_in_woman']) ){
                    $certificate_value[] = 'her';
                }
                if( isset($_POST['certificate_in_dress']) ){
                    $certificate_value[] = 'dress';
                }
                if(!empty($certificate_value)){
                    update_user_meta($user_id,'certificate_in',$certificate_value);
                }
                /**/


                /*Master Info*/
                if(isset($_POST['master_year'])){
                    update_user_meta($user_id, 'master_year', $_POST['master_year']);
                }
                $master_value = array();
                if( isset($_POST['master_in_man']) ){
                    $master_value[] = 'him';
                }
                if( isset($_POST['master_in_woman']) ){
                    $master_value[] = 'her';
                }
                if( isset($_POST['master_in_dress']) ){
                    $master_value[] = 'dress';
                }
                if(!empty($master_value)){
                    update_user_meta($user_id,'master_in',$master_value);
                }
                /**/

                /*Company Info*/
                if(isset($_POST['company_name'])){
                    update_user_meta($user_id, 'company_name', $_POST['company_name']);
                }
                if(isset($_POST['company_phone'])){
                    update_user_meta($user_id, 'company_phone', $_POST['company_phone']);
                }
                if(isset($_POST['company_start_date'])){
                    update_user_meta($user_id, 'company_start_date', $_POST['company_start_date']);
                }
                if(isset($_POST['company_address'])){
                    update_user_meta($user_id, 'company_address', $_POST['company_address']);
                }
                if(isset($_POST['company_post_number'])){
                    update_user_meta($user_id, 'company_post_number', $_POST['company_post_number']);
                }
                if(isset($_POST['company_city'])){
                    update_user_meta($user_id, 'company_city', $_POST['company_city']);
                }
                if(isset($_POST['company_nature'])){
                    update_user_meta($user_id, 'company_nature', $_POST['company_nature']);
                }

                if(isset($_POST['company_f_tax'])){
                    update_user_meta($user_id,'company_f_tax','yes');
                }else{
                    update_user_meta($user_id,'company_f_tax','no');
                }

                if(isset($_POST['company_vat'])){
                    update_user_meta($user_id,'company_vat','yes');
                }else{
                    update_user_meta($user_id,'company_vat','no');
                }

                if(isset($_POST['company_email'])){
                    update_user_meta($user_id, 'company_email', $_POST['company_email']);
                }
                if(isset($_POST['company_website'])){
                    update_user_meta($user_id, 'company_website', $_POST['company_website']);
                }
                /**/

                /*Employee Info*/
                $employee_company_name = isset($_POST['employee_company_name']) ? $_POST['employee_company_name']: '';
                $employee_company_phone = isset($_POST['employee_company_phone']) ? $_POST['employee_company_phone']: '';
                $employee_company_address = isset($_POST['employee_company_address']) ? $_POST['employee_company_address']: '';
                $employee_company_post_number = isset($_POST['employee_company_post_number']) ? $_POST['employee_company_post_number']: '';
                $employee_company_city = isset($_POST['employee_company_city']) ? $_POST['employee_company_city']: '';

                update_user_meta($user_id, 'employee_company_name', $employee_company_name);
                update_user_meta($user_id, 'employee_company_phone', $employee_company_phone);
                update_user_meta($user_id, 'employee_company_address', $employee_company_address);
                update_user_meta($user_id, 'employee_company_post_number', $employee_company_post_number);
                update_user_meta($user_id, 'employee_company_city', $employee_company_city);
                /**/

                /*School Info*/
                if(isset($_POST['school_name'])){
                    update_user_meta($user_id, 'school_name', $_POST['school_name']);
                }
                if(isset($_POST['school_phone'])){
                    update_user_meta($user_id, 'school_phone', $_POST['school_phone']);
                }
                if(isset($_POST['school_email'])){
                    update_user_meta($user_id, 'school_email', $_POST['school_email']);
                }
                if(isset($_POST['school_address'])){
                    update_user_meta($user_id, 'school_address', $_POST['school_address']);
                }
                if(isset($_POST['school_post_number'])){
                    update_user_meta($user_id, 'school_post_number', $_POST['school_post_number']);
                }
                if(isset($_POST['school_place'])){
                    update_user_meta($user_id, 'school_place', $_POST['school_place']);
                }
                /**/

            }
        }
    }

}