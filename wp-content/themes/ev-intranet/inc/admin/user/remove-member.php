<?php
/* allow local admin to delete only their associated members*/
function ev_delete_members( $user_id ) {
    $current_user = wp_get_current_user();
    if(in_array('local_admin',$current_user->roles)){
        $local_admin = get_user_meta($user_id,'_local_admin',true);
        if(!empty($local_admin)){
            if($local_admin != $current_user->ID){
                wp_die(
                    '<h1>' . __( 'Cheatin&#8217; uh?','ev-intranet' ) . '</h1>' .
                    '<p>' . __( 'Sorry, you are not allowed this action.','ev-intranet' ) . '</p>',
                    403
                );
            }
        }else{
            wp_die(
                '<h1>' . __( 'Cheatin&#8217; uh?','ev-intranet' ) . '</h1>' .
                '<p>' . __( 'Sorry, you are not allowed this action.','ev-intranet' ) . '</p>',
                403
            );
        }
    }
}
add_action( 'remove_user_from_blog', 'ev_delete_members' );