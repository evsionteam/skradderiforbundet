<?php
if ( isset($_REQUEST['action']) && 'createmember' == $_REQUEST['action'] ) {
    check_admin_referer( 'create-member', '_wpnonce_create-member' );

    if ( ! current_user_can( 'create_members' ) ) {
        wp_die(
            '<h1>' . __( 'Cheatin&#8217; uh?' ) . '</h1>' .
            '<p>' . __( 'Sorry, you are not allowed to create members.' ) . '</p>',
            403
        );
    }

    /*validate the inputted fields*/
    ev_member_registration_validation(
        $_POST['user_login'],
        wp_unslash( $_POST['email'] )
    );

    $username	= 	sanitize_user($_POST['user_login']);
    $email 		= 	sanitize_email($_POST['email']);
    $fname 		= 	sanitize_text_field($_POST['first_name']);
    $lname 		= 	sanitize_text_field($_POST['last_name']);

    /*Register only when no WP_error is found*/
    ev_member_complete_registration(
        $username,
        $email,
        $fname,
        $lname
    );

}

/**
* Create add member page for local admin only
*/
add_action('admin_menu', 'ev_add_member_page');
function ev_add_member_page() {
    global $current_user;
    /*show this page settings for local staff only*/
    if(in_array('local_admin',$current_user->roles)){
        add_submenu_page(
            'users.php',
            __( 'Add Member', 'ev-intranet' ),
            __( 'Add Member', 'ev-intranet' ),
            'create_members',
            'add-member',
            'add_members_callback'
        );
    }
}

function add_members_callback() {
    ?>
    <div class="wrap">
        <h2><?php _e( 'Add New Member','ev-intranet' ); ?></h2>
        <?php
        /*Show errors if any*/
        global $reg_errors,$create_msg;
        if ( isset($reg_errors) && is_wp_error( $reg_errors ) && !empty($reg_errors)) {
            foreach ( $reg_errors->get_error_messages() as $error ) {
                echo "<div id='message' class='error notice is-dismissible'><p>$error</p></div>";
            }
        }
        if(isset($create_msg) && !empty($create_msg)){
            echo $create_msg;
        }
        ?>
        <form method="post" name="createmember" id="createuser" class="validate" novalidate="novalidate">
            <input name="action" type="hidden" value="createmember" />
            <?php wp_nonce_field( 'create-member', '_wpnonce_create-member' ); ?>
            <?php
            $creating = isset( $_POST['createmember'] );
            $new_user_login = $creating && isset( $_POST['user_login'] ) ? wp_unslash( $_POST['user_login'] ) : '';
            $new_user_email = $creating && isset( $_POST['email'] ) ? wp_unslash( $_POST['email'] ) : '';
            $new_user_fname = $creating && isset( $_POST['first_name'] ) ? wp_unslash( $_POST['first_name'] ) : '';
            $new_user_lname = $creating && isset( $_POST['last_name'] ) ? wp_unslash( $_POST['last_name'] ) : '';

            ?>
            <table class="form-table">
                <tr class="form-field form-required">
                    <th scope="row"><label for="user_login"><?php _e('Username','ev-intranet'); ?> <span class="description"><?php _e('(required)'); ?></span></label></th>
                    <td><input name="user_login" type="text" id="user_login" value="<?php echo esc_attr( $new_user_login ); ?>" aria-required="true" autocapitalize="none" autocorrect="off" maxlength="60" /></td>
                </tr>
                <tr class="form-field form-required">
                    <th scope="row"><label for="email"><?php _e('Email','ev-intranet'); ?> <span class="description"><?php _e('(required)'); ?></span></label></th>
                    <td><input name="email" type="email" id="email" value="<?php echo esc_attr( $new_user_email ); ?>" /></td>
                </tr>
                <tr class="form-field">
                    <th scope="row"><label for="first_name"><?php _e('First Name','ev-intranet'); ?></label></th>
                    <td><input name="first_name" type="text" id="first_name" value="<?php echo esc_attr( $new_user_fname ); ?>" /></td>
                </tr>
                <tr class="form-field">
                    <th scope="row"><label for="last_name"><?php _e('Last Name','ev-intranet'); ?></label></th>
                    <td><input name="last_name" type="text" id="last_name" value="<?php echo esc_attr( $new_user_lname ); ?>" /></td>
                </tr>

            </table>
            <?php submit_button( __( 'Add New Member','ev-intranet' ), 'primary', 'createmember', true, array( 'id' => 'createmembersub' ) ); ?>
        </form>
    </div>
<?php
}


function ev_member_registration_validation( $username, $email) {
    global $reg_errors;
    $reg_errors = new WP_Error;

    if ( empty( $username ) || empty( $email ) ) {
        $reg_errors->add('field', __('Please fill all required fields.','ev-intranet'));
    }

    if(!empty($username)){
        if ( username_exists( $username ) ){
            $reg_errors->add('user_name', __('Sorry, the username already exist!','ev-intranet'));
        }

        if ( !validate_username( $username ) ) {
            $reg_errors->add('username_invalid', __('Sorry, the username is invalid.','ev-intranet'));
        }
    }

    if(!empty($email)){
        if ( !is_email( $email ) ) {
            $reg_errors->add('email_invalid', __('Email address is not valid.','ev-intranet'));
        }

        if ( email_exists( $email ) ) {
            $reg_errors->add('email', __('Email address already exist.','ev-intranet'));
        }
    }
}

function ev_member_complete_registration($username,$email,$fname,$lname){
    global $reg_errors,$create_msg;
    if ( count($reg_errors->get_error_messages()) < 1 ) {
        $user_data = array(
            'user_login'	    => 	$username,
            'user_email' 	    => 	$email,
            'first_name' 	    => 	$fname,
            'last_name' 	    => 	$lname,
            'user_pass' 	    => 	wp_generate_password(),
            'user_registered'	=>  date('Y-m-d H:i:s'),
            'role'				=> 'member'
        );
        global $current_user;
        $user_id = wp_insert_user( $user_data );
        if($user_id) {

            /*add current admin id and organization id in registered user meta*/
            add_user_meta($user_id,'_local_admin',$current_user->ID);
            $organization_id = get_user_meta($current_user->ID,'_organization_id',true);
            if(!empty($organization_id)){
                add_user_meta($user_id,'_organization_id',$organization_id);
            }

            /*send an email to the admin and currently registered user alerting them of the registration*/
            wp_new_user_notification($user_id,NULL,'both');
            $create_msg = '<div id="message" class="updated notice is-dismissible"><p>' . __( 'New member created successfully.','ev-intranet' ) . '</p></div>';
        }else{
            $create_msg = '<div id="message" class="error notice is-dismissible"><p>' . __( 'Unable to create member. Please try again.','ev-intranet' ) . '</p></div>';
        }
    }
}