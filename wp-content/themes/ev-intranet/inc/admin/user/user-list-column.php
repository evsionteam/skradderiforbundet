<?php
/*show user organization and managed by options in user list admin columns */
function ev_user_organization( $column ) {
    $current_user = wp_get_current_user();
    if(in_array('local_admin',$current_user->roles)){
        unset($column['role']);
        unset($column['posts']);
    }else{
        $column['user_organization'] = __('Organization','ev-intranet');
        $column['managed_by'] = __('Managed By','ev-intranet');
    }
    $column['user_status'] = __('Status','ev-intranet');
    $column['user_pdf'] = __('PDF','ev-intranet');
    return $column;
}
add_filter( 'manage_users_columns', 'ev_user_organization' );

function ev_display_user_organization( $val, $column_name, $user_id ) {

    $user_data = get_userdata($user_id);
    $roles = $user_data->roles;

    $role_arr = array();

    foreach($roles as $role){
        $role_arr[] = $role;
    }

    if(in_array('local_admin',$role_arr)){
        $output = '-';
        switch ($column_name) {
            case 'user_organization':
                $organization = get_user_meta($user_id,'_organization_id',true);
                if(!empty($organization)){
                    $output = '<a href="'.get_edit_post_link($organization).'">'.get_the_title($organization).'</a>';
                }
                return $output;
                break;
            default:
        }
    }
    if(in_array('member',$role_arr)){
        $output = '-';
        switch ($column_name) {
            case 'managed_by':
                $local_admin = get_user_meta($user_id,'_local_admin',true);
                if(!empty($local_admin)){
                    $local_admin_data = get_userdata( $local_admin );
                    if(!empty($local_admin_data)){
                        $output = "<p><a href='".get_edit_user_link( $local_admin)."'>".$local_admin_data->data->user_nicename."</a></p>";
                    }
                }
                return $output;
                break;
            case 'user_status':
                $user_status = get_user_meta($user_id,'inactive_member',true);
                if(true == $user_status){
                    $output = __('Unverified','ev-intranet');
                }else{
                    $user_status = get_user_meta($user_id,'approved_member',true);
                    if(true == $user_status){
                        $output = __('Approved','ev-intranet');
                    }else{
                        $output = '<div class="member-status" data-uid="'.$user_id.'"><a href="#" class="button">';
                        $output .= '<div class="status-action">'.__('Approve','skradderiforbundet').'</div>';
                        $output .= '<img class="loading-img" src="'.get_template_directory_uri().'/assets/src/img/ajax-loader.gif" style="display:none"/>';
                        $output .= '</a></div>';
                    }
                }
                return $output;
                break;
            case 'user_pdf':

                $upload_dir = wp_upload_dir();
                $pdf = md5($user_data->data->user_email);
                $attachment_path = $upload_dir['baseurl'].'/users/'.$pdf.'.pdf';

                $output = '<div class="member-pdf"><a href="'.$attachment_path.'" class="button" target="_blank">';
                $output .= '<div class="status-action">'.__('Download','skradderiforbundet').'</div>';
                $output .= '</a></div>';

                return $output;
                break;
            default:
        }
    }
    return $val;
}
add_filter( 'manage_users_custom_column', 'ev_display_user_organization', 10, 3 );
/**/

/*make sortable by status*/
function ev_status_column_sortable($columns) {
    $columns['user_status'] = 'user_status';
    return $columns;
}
add_filter( 'manage_users_sortable_columns', 'ev_status_column_sortable' );
function sk_sort_by_user_status( $query ) {
    if ( 'user_status' == $query->get( 'orderby' ) ) {
        $query->set( 'orderby', 'meta_value_num' );
        $query->set( 'meta_key', 'approved_member' );
    }
}
add_action( 'pre_get_users', 'sk_sort_by_user_status' );
/**/


/*Ajax callback for approving member*/
function ev_set_member_status() {
    $user_id = $_POST['id'];
    $user_data = get_userdata($user_id);

    if(!empty($user_id)){
        $current_user = wp_get_current_user();

        if(in_array('local_admin',$current_user->roles)){
            /*Allow local admin to only approve their associated members*/
            $local_admin = get_user_meta($user_id,'_local_admin',true);
            if($local_admin == $current_user->ID){
                update_user_meta($user_id,'approved_member',true);
            }
        }else{
            update_user_meta($user_id,'approved_member',true);
        }


        /*Send mail to member after approval*/
        $mail_template = file_get_contents( dirname(dirname(__DIR__)).'/email-template/member-verification-email.html' );

        /*Logo*/
        $logo = $site_logo = $org_name = '';
        $org_id =  get_user_meta($user_id,'_organization_id',true);
        if(!empty($org_id)){
            $logo = get_post_meta($org_id,'organization_logo',true);
            $org_name = get_the_title($org_id);
        }

        $new_password =  wp_generate_password();
        wp_set_password( $new_password, $user_id );

        /*Fetch info from public site*/
        switch_to_blog(1);

        $sk_option = get_theme_mod('sk_option');

        /*site logo*/
        if( isset($sk_option['logo_image']) && !empty($sk_option['logo_image']) ){
            $temp = wp_get_attachment_image_src($sk_option['logo_image']);
            if(!empty($temp)){
                $site_logo = $temp[0];
                if(empty($logo)){
                    $logo = $temp[0];
                }
            }
        }
        /**/


        $ma_header_text = isset($sk_option['ma_email_header']) ? wpautop($sk_option['ma_email_header']) : '';
        $ma_header_text = str_replace('@username@', $user_data->data->display_name, $ma_header_text);

        $ma_body_text = isset($sk_option['ma_email_content']) ? wpautop($sk_option['ma_email_content']) : '';
        $ma_footer_text = isset($sk_option['ma_email_footer']) ? wpautop($sk_option['ma_email_footer']) : '';

        restore_current_blog();
        /**/

        $logo = '<img editable="true" src="'.$logo.'" width="120" alt="" border="0" mc:edit="40">';
        $site_logo = '<img src="'.$site_logo.'" alt="site-logo" style="border-radius:50%;display:block;margin:0;padding:0;" width="50" class="image_target">';


        $login_details = sprintf(  __( 'Username: %s', 'skradderiforbundet' ),$user_data->data->user_login ).'<br/>';
        $login_details .= sprintf(  __( 'Password: %s', 'skradderiforbundet' ),$new_password );

        $ma_body_text = str_replace('@login_details@', $login_details , $ma_body_text);
        $ma_body_text = str_replace('@login_link@', "<a style='color:#000000;font-weight:bold' href='".get_site_url().'/login/'."'>".get_site_url().'/login/'."</a>" , $ma_body_text);

        $contact_info = get_field('contact_info',$org_id);
        $fb_link = get_field('facebook_link',$org_id);
        $twitter_link = get_field('twitter_link',$org_id);
        $instagram_link = get_field('instagram_link',$org_id);

        $ma_body_text = str_replace('@fb_link@', "<a style='color:#000000;font-weight:bold' href='".$fb_link."'>".__('Facebook','skradderiforbundet')."</a>", $ma_body_text);
        $ma_body_text = str_replace('@insta_link@', "<a style='color:#000000;font-weight:bold' href='".$instagram_link."'>".__('Instagram','skradderiforbundet')."</a>", $ma_body_text);

        $mail_template = str_replace('@logo@', $logo, $mail_template);
        $mail_template = str_replace('@organization_name@', $org_name, $mail_template);
        $mail_template = str_replace('@header_text@', $ma_header_text, $mail_template);
        $mail_template = str_replace('@body_text@', $ma_body_text, $mail_template);
        $mail_template = str_replace('@site_logo@', $site_logo, $mail_template);
        $mail_template = str_replace('@footer_text@', $ma_footer_text, $mail_template);

        $fb_icon = get_template_directory_uri().'/assets/src/img/facebook.png';
        $twitter_icon = get_template_directory_uri().'/assets/src/img/twitter.png';
        $insta_icon = get_template_directory_uri().'/assets/src/img/instagram.png';

        $mail_template = str_replace('@contact_info@', $contact_info, $mail_template);
        $mail_template = str_replace('@fb_link@', $fb_link, $mail_template);
        $mail_template = str_replace('@fb_icon@', $fb_icon, $mail_template);
        $mail_template = str_replace('@twitter_link@', $twitter_link, $mail_template);
        $mail_template = str_replace('@twitter_icon@', $twitter_icon, $mail_template);
        $mail_template = str_replace('@insta_link@', $instagram_link, $mail_template);
        $mail_template = str_replace('@insta_icon@', $insta_icon, $mail_template);


        $upload_dir = wp_upload_dir();
        $pdf = md5($user_data->data->user_email);
        $attachment_path = $upload_dir['basedir'].'/users/'.$pdf.'.pdf';

        $attachments = array($attachment_path);
        $headers = array('Content-Type: text/html; charset=UTF-8','From: Skradderiforbundet <skradderiforbundet@wordpress.org>');

        wp_mail($user_data->data->user_email, __('Account approved','skradderiforbundet'), $mail_template,$headers,$attachments);
        /**/

    }
    die();
}
add_action( 'wp_ajax_ev_set_member_status', 'ev_set_member_status' );


/*Show only current local admin members*/
add_filter( 'users_list_table_query_args', 'ev_show_only_related_members' );
function ev_show_only_related_members($args){
    $current_user = wp_get_current_user();
    if(in_array('local_admin',$current_user->roles)){
        $args = array(
            'meta_key' => '_local_admin',
            'meta_value' => $current_user->ID
        );
    }
    return $args;
}

/*hide the user count for local admin*/
add_filter( 'views_users','ev_show_only_related_members_count' );
function ev_show_only_related_members_count($views){
    $current_user = wp_get_current_user();
    if(in_array('local_admin',$current_user->roles)){
        $views = array();
    }
    return $views;
}

