<?php
/*show only administrator and local admins to set as author for certain post types*/
add_action( 'load-post.php',     'ev_load_user_dropdown_filter' );
add_action( 'load-post-new.php', 'ev_load_user_dropdown_filter' );
function ev_load_user_dropdown_filter() {
    $screen = get_current_screen();
    if(!empty($screen->post_type)){
        if('ev_courses' === $screen->post_type || 'ev_articles' === $screen->post_type || 'ev_exhibitions' === $screen->post_type || 'ev_news' === $screen->post_type || 'ev_world_congress' === $screen->post_type || 'ev_modemuseum' === $screen->post_type ){
            add_filter( 'wp_dropdown_users_args', 'ev_show_local_admin_only_in_author_box', 10, 2 );
        }
        if( 'ev_companies' === $screen->post_type || 'ev_image_gallery' === $screen->post_type ){
            add_filter( 'wp_dropdown_users_args', 'ev_show_members_only_in_author_box', 10, 2 );
        }
    }
}
function ev_show_local_admin_only_in_author_box( $args, $r ) {
    global $post, $current_user;
    if ( 'post_author_override' === $r['name'] && ( 'ev_courses' === $post->post_type ||  'ev_articles' === $post->post_type ||  'ev_exhibitions' === $post->post_type ||  'ev_news' === $post->post_type ||  'ev_world_congress' === $post->post_type || 'ev_modemuseum' === $post->post_type  ) ) {
        $args['who']      = '';
        $args['role__in'] = array('local_admin','administrator');

        /*If is local admin, then only show current user in the author box*/
        if(in_array('local_admin',$current_user->roles)){
            $args['include'] = $current_user->ID;
        }
        /**/
    }
    return $args;
}

function ev_show_members_only_in_author_box( $args, $r ) {
    global $post, $current_user;
    if ( 'post_author_override' === $r['name'] && ( 'ev_companies' === $post->post_type || 'ev_image_gallery' === $post->post_type ) ) {
        $args['who']      = '';
        $args['role__in'] = array('member');

        /*If is local admin, then only show associated members in the author box*/
        if(in_array('local_admin',$current_user->roles)){
            $args['meta_key'] = '_local_admin';
            $args['meta_value'] = $current_user->ID;
        }
        /**/
    }
    return $args;
}

/*show only current author post for local admin and member also remove post count*/
function ev_posts_for_current_author($query) {
    global $pagenow,$current_user;
    if( 'edit.php' != $pagenow || !$query->is_admin || current_user_can( 'administrator' ) )
        return $query;

    if( current_user_can( 'local_admin' ) || current_user_can( 'member' ) ) {
        global $post_type;
        if( 'ev_companies' != $post_type){
            $query->set('author', $current_user->ID );
        }else{
            /*Show only associated members posts*/
            $associated_members_ids = array();
            $associated_members = get_users(
                array(
                    'meta_key'     => '_local_admin',
                    'meta_value'   => $current_user->ID,
                )
            );
            if(!empty($associated_members)){
                foreach($associated_members as $associated_member){
                    $associated_members_ids = $associated_member->ID;
                }
            }
            if(!empty($associated_members_ids)){
                $query->set('author', $associated_members_ids );
            }
            /**/
        }
        add_filter('views_edit-ev_exhibitions', 'ev_hide_post_counts');
        add_filter('views_edit-page', 'ev_hide_post_counts');
        add_filter('views_edit-ev_courses', 'ev_hide_post_counts');
        add_filter('views_edit-ev_companies', 'ev_hide_post_counts');
        add_filter('views_edit-ev_articles', 'ev_hide_post_counts');
        add_filter('views_edit-ev_mm', 'ev_hide_post_counts');
        add_filter('views_edit-ev_news', 'ev_hide_post_counts');
        add_filter('views_edit-ev_world_congress', 'ev_hide_post_counts');
        add_filter('views_edit-ev_wholesaler', 'ev_hide_post_counts');
        add_filter('views_edit-ev_modemuseum', 'ev_hide_post_counts');
    }
    return $query;
}
add_filter('pre_get_posts', 'ev_posts_for_current_author');

/*hide post counts for member and local admin*/
function ev_hide_post_counts($views){
    $views = array();
    return $views;
}

/*Restrict user to edit his post only*/
add_action( 'load-post.php', 'ev_restrict_user_post_edit' );
function ev_restrict_user_post_edit(){
    if(is_admin()){
        add_filter('pre_get_posts', 'ev_restrict_local_admin_post_edit');
    }
}
function ev_restrict_local_admin_post_edit($query){
    global $post, $current_user;
    $screen = get_current_screen();
    if(in_array('local_admin',$current_user->roles)){
        if( 'ev_companies' != $screen->post_type  ){
            if( $post->post_author != $current_user->ID){
                wp_die(
                    '<h1>' . __( 'Cheatin&#8217; uh?','ev-intranet' ) . '</h1>' .
                    '<p>' . __( 'Sorry, you are not allowed this action.','ev-intranet' ) . '</p>',
                    403
                );
            }
        }else{
            $local_admin = get_user_meta($post->post_author,'_local_admin',true);
            if($local_admin != $current_user->ID){
                wp_die(
                    '<h1>' . __( 'Cheatin&#8217; uh?','ev-intranet' ) . '</h1>' .
                    '<p>' . __( 'Sorry, you are not allowed this action.','ev-intranet' ) . '</p>',
                    403
                );
            }
        }
    }
    return $query;
}