<?php
function sk_add_expiry_date_metabox() {
    $post_types = array('ev_courses','ev_world_congress','ev_articles','ev_news','ev_exhibitions');
    foreach($post_types as $post_type){
        add_meta_box(
            'sk_pub_site_metabox',
            __( 'Publika inställningar', 'skradderiforbundet'),
            'sk_pub_site_metabox_callback',
            $post_type,
            'side',
            'high'
        );
    }
}
add_action( 'add_meta_boxes', 'sk_add_expiry_date_metabox' );

function sk_pub_site_metabox_callback($post){
    wp_nonce_field( basename( __FILE__ ), 'sk_exp_nonce' );
    $expire_date = get_post_meta($post->ID,'expires_in',true);
    $sk_post_order = get_post_meta($post->ID,'sk_post_order',true);
    if( 999999 == $sk_post_order){
        $sk_post_order = '';
    }
    ?>
    <div>
        <label for="sk_post_expiry_date"><?php _e('Avpubliceringsdatum', 'ev-intranet' ); ?></label><br/>
        <input type="text" id="sk_post_expiry_date" class="datepicker" name="sk_post_expiry_date" value="<?php echo $expire_date;?>" / >
    </div>
    <div>
        <p><label for="sk-post-order"><?php _e('Order', 'ev-intranet' ); ?></label></p>
        <input type="text" id="sk-post-order" name="sk_post_order" value="<?php echo $sk_post_order;?>"/>
    </div>
<?php
}