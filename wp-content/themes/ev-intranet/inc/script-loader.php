<?php
add_action( 'wp_enqueue_scripts', 'ev_intranet_scripts' );
function ev_intranet_scripts() {
	$suffix = '.min';
	if( defined('SCRIPT_DEBUG') && SCRIPT_DEBUG ){
		$suffix = '';
	}
	
	$assets_url = get_stylesheet_directory_uri().'/assets/build/';

	if( is_home() || is_front_page() ){
        wp_enqueue_style( 'rosenlundsakeri', $assets_url . 'css/homepage-above-fold' . $suffix . '.css', array() );
    } else {
        wp_enqueue_style( 'rosenlundsakeri', $assets_url . 'css/inner-above-fold' . $suffix . '.css', array() );
    }

	//wp_enqueue_style( 'ev-intranet-style', $assets_url.'css/main'.$suffix.'.css');

	$main_css = $assets_url.'css/main'.$suffix.'.css';
    $script = "
       var inlineNode = document.getElementById('rosenlundsakeri-inline-css');
       var link  = document.createElement('link');
       link.rel  = 'stylesheet';
       link.type = 'text/css';
       link.href = '".$main_css."';
       link.media = 'all';
       inlineNode.parentNode.insertBefore(link, inlineNode);
    ";

    wp_enqueue_script('jquery-ui-core');
    wp_enqueue_script('jquery-ui-datepicker');
    wp_enqueue_style('jquery-ui-css', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');

	wp_enqueue_script( 'ev-intranet', $assets_url.'js/script'.$suffix.'.js', array('jquery'), null, true );
    wp_enqueue_script('masonry');
	wp_localize_script( 'ev-intranet', 'evAjax' ,array(
            'ajaxurl' => admin_url( 'admin-ajax.php' )
        )
    );

 	wp_enqueue_script('google-maps', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDIuunzpLhUeoYDivVb08zklnJgOVsLRvM');
  		
	$webfont = "WebFontConfig = {
					google: { families: [ 
						'Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i', 
						'Merriweather+Sans:400,400i,700,700i,800,800i',
					] }
				};
				(function() {
					var wf = document.createElement('script');
					wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
					wf.type = 'text/javascript';
					wf.async = 'true';
					var s = document.getElementsByTagName('script')[0];
					s.parentNode.insertBefore(wf, s);
				})();

				(function(d) {
				    var config = {
				      kitId: 'bct1apf',
				      scriptTimeout: 3000,
				      async: true
				    },
				    h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,'')+' wf-inactive';},config.scriptTimeout),tk=d.createElement('script'),f=false,s=d.getElementsByTagName('script')[0],a;h.className+=' wf-loading';tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!='complete'&&a!='loaded')return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
				  })(document);";

	wp_add_inline_script('ev-intranet', $webfont );
	wp_add_inline_script('ev-intranet', $script );

	$theme_stylesheet_handle = 'rosenlundsakeri';
	$custom_script_handle    = 'ev-intranet';


    global $ev_intranet;
    if( isset($ev_intranet['site-color']) && !empty($ev_intranet['site-color']) ){
        $color = $ev_intranet['site-color'];
        $custom_css = "
       .c-btn {
            background: $color;
        }
        .site-content .main-content .grid-item .image-link .category-name {
            background: $color;
        }
        .filter .grid-filter .grid-option a.active {
            color: $color;
        }
        .site-content .main-content .grid-item .image-link .image-content .img-overlay-text .news-title span {
            color: $color;
        }
        .site-header .nav-wrap .main-navigation .menu-main-menu-container > ul > li ul.sub-menu li a:hover {
            background: $color;
        }
        .single-content-wrap .single-full-image .single-content-overlay .img-overlay-text .news-title span {
            color: $color;
        }
        .site-content .inner-wrap .post-nav .nav-pagination a {
            background-color: $color;
        }
        .sk-cm-table.sk-foo-table thead {
            background-color: $color;
        }
        .svarta-listan .inner-banner-content .left-text-wrap .photo-gall-btn:hover {
            background-color: $color;
        }
        .staff-login-menu ul.login-menu li:hover {
            background: $color;
        }
        form.skrad-form .sk-checkbox-style label.control-checkbox .check-indicator {
            border: 1px $color solid;
        }
        form.skrad-form .sk-checkbox-style label.control-checkbox input:checked ~ .check-indicator {
            background: $color;
        }
        .modal-design .modal-dialog .modal-content .modal-body form .cat-submit-btn input[type='submit'] {
            background-color: $color;
        }
        .sk-image-gallry-sec .sk-member-image-wrapper .sk-image-thumnail .sk-image-info .remove-image {
            background: $color;
        }
        .sk-archive-link .see-all {
            background-color: $color;
        }
        ";
       wp_add_inline_style( $theme_stylesheet_handle, $custom_css );
    }

	wp_add_inline_style( $theme_stylesheet_handle,'body{
		opacity: 0
	}');
	
	$preloader = 'function preloader( param ){
		
		this.time = param.time;

		this.init = function(){

			var time = this.time;

			jQuery(window).load( function(){
				jQuery( "body" ).animate({
					opacity : 1
				},time);
			})
		}
	} jQuery(document).ready(function(){ new preloader({ time: 400 }).init(); });';

    wp_add_inline_script( $custom_script_handle,$preloader );
}

add_action( 'admin_enqueue_scripts', 'ev_intranet__admin_scripts' );
function ev_intranet__admin_scripts() {
	$assets_url = get_stylesheet_directory_uri().'/assets/build/';
	$suffix = '.min';
	if( defined('SCRIPT_DEBUG') && SCRIPT_DEBUG ){
		$suffix = '';
	}

    /*Datepicker for course*/
    $screen = get_current_screen();
    if ( $screen->id == 'ev_courses' || $screen->id == 'ev_world_congress' ) {
        wp_enqueue_script('jquery-ui-core');
        wp_enqueue_script('jquery-ui-datepicker');
        wp_enqueue_style('jquery-ui-css', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
    }

    wp_enqueue_script( 'ev-quick-edit-js', get_template_directory_uri() . '/assets/src/js/post-quick-edit-script.js', array( 'jquery', 'inline-edit-post' ), '', true );

    wp_enqueue_media();
    wp_register_script( 'ev-intranet-admin', $assets_url.'js/admin-script'.$suffix.'.js', array( 'jquery' ),null,true );
    wp_localize_script( 'ev-intranet-admin', 'meta_image',
        array(
            'title' => __( 'Choose or Upload an Video', 'ev-intranet' ),
            'button' => __( 'Use this Video', 'ev-intranet' ),
            'document_title' => __( 'Choose or Upload an Document', 'ev-intranet' ),
            'document_button' => __( 'Use this Document', 'ev-intranet' ),
        )
    );
    wp_localize_script( 'ev-intranet-admin', 'evAdminAjax' ,array(
            'ajaxurl' => admin_url( 'admin-ajax.php' )
        )
    );
	wp_enqueue_script( 'ev-intranet-admin');
}

function ev_intranet_print_admin_scripts() {

    $screen = get_current_screen();
    if ( $screen->id == 'ev_courses' || $screen->id == 'ev_world_congress' ) {
        ?>
        <script type="text/javascript">
            jQuery('.datepicker').datepicker({
                dateFormat : 'yy-mm-dd',
                changeMonth: true
            });
        </script>
    <?php
    }
}
add_action( 'admin_print_footer_scripts', 'ev_intranet_print_admin_scripts' );