<?php
add_action( 'wp_ajax_update_member_profile', 'update_member_profile_callback' );
add_action( 'wp_ajax_nopriv_update_member_profile', 'update_member_profile_callback' );
function update_member_profile_callback() {
    $data = array();
    check_ajax_referer( 'edit_member', 'nonce' );
    $params = array();
    parse_str($_POST['form_data'], $params);

    global $current_user;

    if(!in_array('member',$current_user->roles)){
        $data['status'] = false;
        $data['message'] = __("Ledsen, men du måste logga in som en medlem för att kunna fortsätta.",'ev-intranet');
    }else{
        $user_id = get_current_user_id();

        $allowed = array(
            'first_name',
            'last_name',
            'member_birthdate',
            'member_profession',
            'member_home_address',
            'member_post_number',
            'member_place',
            'member_country',
            'member_home_phone',
            'company_name',
            'company_phone',
            'company_start_date',
            'company_address',
            'company_post_number',
            'company_city',
            'company_nature',
            'company_f_tax',
            'company_vat',
            'company_email',
            'company_website',
            'employee_company_name',
            'employee_company_phone',
            'employee_company_address',
            'employee_company_post_number',
            'employee_company_city',
            'school_name',
            'school_phone',
            'school_email',
            'school_address',
            'school_post_number',
            'school_place',
        );

        /*filter only allowed data*/
        $filtered_data = array_intersect_key($params, array_flip($allowed));
        foreach( $filtered_data as $key => $value ){
           update_user_meta( $user_id, $key, $value);
        }

        /*Certificate Info*/
        if(isset($params['date_of_certificate'])){
            update_user_meta($user_id, 'date_of_certificate', $params['date_of_certificate']);
        }
        $certificate_value = array();
        if( isset($params['certificate_in_man']) ){
            $certificate_value[] = 'him';
        }
        if( isset($params['certificate_in_woman']) ){
            $certificate_value[] = 'her';
        }
        if( isset($params['certificate_in_dress']) ){
            $certificate_value[] = 'dress';
        }
        if(!empty($certificate_value)){
            update_user_meta($user_id,'certificate_in',$certificate_value);
        }
        /**/


        /*Master Info*/
        if(isset($params['master_year'])){
            update_user_meta($user_id, 'master_year', $params['master_year']);
        }
        $master_value = array();
        if( isset($params['master_in_man']) ){
            $master_value[] = 'him';
        }
        if( isset($params['master_in_woman']) ){
            $master_value[] = 'her';
        }
        if( isset($params['master_in_dress']) ){
            $master_value[] = 'dress';
        }
        if(!empty($master_value)){
            update_user_meta($user_id,'master_in',$master_value);
        }
        /**/

        if(!empty($params['password'])){
            if($params['password'] !== $params['retype_password']){
                $data['status'] = false;
                $data['message'] = __("Sorry, The passwords didn't match !!!",'ev-intranet');
            }else{
                if(strlen( $params['password'] ) <= 6){
                    $data['status'] = false;
                    $data['message'] = __("Sorry, Password length must be greater than 6 characters !!!",'ev-intranet');
                }else{
                    wp_set_password( $params['password'], $user_id );
                }
            }
        }

        if(!isset($data['status'])){
            $data['status'] = true;
            $data['message'] = __("Profile Updated Successfully !!!",'ev-intranet');
        }
    }
    echo json_encode($data);
    die();
}