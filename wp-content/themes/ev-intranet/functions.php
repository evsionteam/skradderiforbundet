<?php
if ( ! function_exists( 'ev_intranet_setup' ) ) :

function ev_intranet_setup() {

	load_theme_textdomain( 'ev-intranet', get_template_directory() . '/languages' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );
	register_nav_menus( array(
		'primary'   => esc_html__( 'Primary', 'ev-intranet' ),
		'login_menu' => esc_html__( 'Logged In Menu', 'ev-intranet' ),
		'top_left' => esc_html__( 'Top Left', 'ev-intranet' ),
		'top_right' => esc_html__( 'Top Right', 'ev-intranet' ),
		'footer-1'  => esc_html__( 'Footer 1', 'ev-intranet' ),
		'footer-2'  => esc_html__( 'Footer 2', 'ev-intranet' ),
		'footer-3'  => esc_html__( 'Footer 3', 'ev-intranet' ),
	) );

	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );
	add_image_size( 'news-col-1', 1348, 600, true );
	add_image_size( 'news-col-3', 420, 229, true );
    add_image_size( 'news-col-2', 875, 229, true );
    //add_image_size( 'sk-archive-img-size', 415, 285, true );
    add_image_size( 'sk-archive-img-size', 415, 220, true );
}
endif;
add_action( 'after_setup_theme', 'ev_intranet_setup' );


function ev_intranet_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'ev_intranet_content_width', 640 );
}
add_action( 'after_setup_theme', 'ev_intranet_content_width', 0 );


function ev_intranet_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'ev-intranet' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'ev-intranet' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer 1', 'ev-intranet' ),
		'id'            => 'footer-1',
		'description'   => esc_html__( 'Add widgets here.', 'ev-intranet' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="footer-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer 2', 'ev-intranet' ),
		'id'            => 'footer-2',
		'description'   => esc_html__( 'Add widgets here.', 'ev-intranet' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="footer-title">',
    'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer 3', 'ev-intranet' ),
		'id'            => 'footer-3',
		'description'   => esc_html__( 'Add widgets here.', 'ev-intranet' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="footer-title">',
    'after_title'   => '</h3>',
	) );
    register_sidebar( array(
        'name'          => esc_html__( 'Footer 4', 'ev-intranet' ),
        'id'            => 'footer-4',
        'description'   => esc_html__( 'Add widgets here.', 'ev-intranet' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3 class="footer-title">',
        'after_title'   => '</h3>',
    ) );
}
add_action( 'widgets_init', 'ev_intranet_widgets_init' );

require get_template_directory() . '/inc/developer.php';
require get_template_directory() . '/inc/helpers.php';
require get_template_directory() . '/inc/script-loader.php';
require get_template_directory() . '/inc/admin/admin.php';
require get_template_directory() . '/inc/maid.php';
require get_template_directory() . '/inc/load-more-posts.php';
require get_template_directory() . '/inc/favicon.php';
require get_template_directory() . '/inc/page-settings.php';
require get_template_directory() . '/inc/member-login.php';
require get_template_directory() . '/inc/image-upload.php';
require get_template_directory() . '/inc/company-upload.php';
require get_template_directory() . '/inc/update-member.php';
require get_template_directory() . '/inc/contact-form-submission.php';

require get_template_directory() . '/inc/post-expirator.php';
require get_template_directory() . '/inc/post-order-meta.php';
require get_template_directory() . '/inc/notification.php';

require get_template_directory() . '/inc/admin/user/add-member.php';
require get_template_directory() . '/inc/admin/user/remove-member.php';
require get_template_directory() . '/inc/admin/user/user-register-edit.php';
require get_template_directory() . '/inc/admin/user/user-list-column.php';
require get_template_directory() . '/inc/admin/user/filter-post-author-list.php';

function archive_search_pgp($query) {

	if(is_admin() || !$query->is_main_query()){
        return $query;
    }

    if ( $query->is_archive() && $query->is_main_query() && !is_admin() ) {
        $query->set( 'posts_per_page', 10 );
    }

    $query_var = get_query_var('search');

    if ( is_post_type_archive( 'ev_courses' ) ) {
        $query->set( 'posts_per_page', 9 );
        $query->set('post_type', array( 'ev_courses') );
        if(!empty($query_var)){
            $query->set( 's', $query_var );
        }
    }
    elseif ( is_post_type_archive( 'ev_world_congress' ) ) {
        $query->set( 'posts_per_page', 9 );
        $query->set('post_type', array( 'ev_world_congress') );
        if(!empty($query_var)){
            $query->set( 's', $query_var );
        }
    }
    elseif ( is_post_type_archive( 'ev_modemuseum' ) ) {
        $query->set( 'posts_per_page', 9 );
        $query->set('post_type', array( 'ev_modemuseum') );
        if(!empty($query_var)){
            $query->set( 's', $query_var );
        }
    }
    elseif ( is_post_type_archive( 'job' ) && !empty($query_var) ) {
     	$query->set('post_type', array( 'job') );
        $query->set( 's', $query_var );
    }
    elseif ( is_post_type_archive( 'ev_news' ) && !empty($query_var) ) {
        $query->set('post_type', array( 'ev_news') );
        $query->set( 's', $query_var );
    }
    elseif ( is_post_type_archive( 'ev_articles' ) && !empty($query_var) ) {
        $query->set('post_type', array( 'ev_articles') );
        $query->set( 's', $query_var );
    }

    return $query;
}
add_action('pre_get_posts', 'archive_search_pgp');

/*load posts*/
function item_load_posts($counter = ''){
       global $ev_intranet;
       $url = $image[0] = '';
       if(isset($_POST['url']) && !empty($_POST['url'])){
           $url = $_POST['url'];
       }
       if ( strpos($_SERVER['REQUEST_URI'], "list") !== false || strpos($url, "list") !== false ){
           echo '<div class = "news-list-item clearfix">';
           if(has_post_thumbnail()){
               $image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'news-col-3');
           }else{
               if( isset($ev_intranet['default-placeholder-image']) && !empty($ev_intranet['default-placeholder-image'])){
                   $img_id = $ev_intranet['default-placeholder-image']['id'];
                   $image = wp_get_attachment_image_src( $img_id, 'news-col-3');
               }
           }
           echo '<div class="news-list-image"><a href="'.get_the_permalink().'"><img src ="'.$image[0].'"></a></div>';
           ?>
           <div class="news-list-content clearfix">
               <?php
               $logo =  ev_get_organization_logo(get_the_author_meta('ID'));
               if(!empty($logo)){
                   echo '<div class="organization-logo">';
                   echo "<img src=".$logo.">";
                   echo "</div>";
               }
               ?>

                <div class="img-overlay-text">
                   <div class="news-title">
                       <a href = "<?php the_permalink();?>"><?php the_title(); ?><span class="news-date">
                                <?php echo get_the_date('dmy');?>
                          </span>
                       </a>
                   </div>
                   <div class="restro-sub-title">
                       <a href="<?php the_permalink();?>">
                            <?php echo wp_trim_words( get_the_excerpt(), 8, '...' ); ?>
                       </a>
                   </div>
                </div>
           </div>
           <?php
           echo '</div>';/*news list item*/
       } else {
           $design_select = get_post_meta( get_the_ID() ,'intranet_news_design_select_meta', true );
           switch ($design_select) {
               case "design_1":
                   echo '<div class = "grid-item col-md-12 c-padding full-width-image">';
                   $image_size = 'news-col-1';
                   $wrapper_class = 'full-image-container';
                   break;
               case "design_2":
                   echo '<div class = "grid-item col-md-8 col-sm-6 c-padding news-lists">';
                   $image_size = 'news-col-2';
                   $wrapper_class = 'image-thumbnail-wrap';
                   break;
               case "design_3":
                   echo '<div class = "grid-item col-md-4 col-sm-6 c-padding news-lists">';
                   $image_size = 'news-col-3';
                   $wrapper_class = 'image-thumbnail-wrap';
                   break;
               default:
                   echo '<div class = "grid-item col-md-4 col-sm-6 c-padding news-lists">';
                   $image_size = 'news-col-3';
                   $wrapper_class = 'image-thumbnail-wrap';
           }
           echo '<a href="'.get_the_permalink().'" class="image-link">';
               if(has_post_thumbnail()){
                   $image = wp_get_attachment_image_src( get_post_thumbnail_id(), $image_size);
               }else{
                   if( isset($ev_intranet['default-placeholder-image']) && !empty($ev_intranet['default-placeholder-image'])){
                       $img_id = $ev_intranet['default-placeholder-image']['id'];
                       $image = wp_get_attachment_image_src( $img_id, $image_size);
                   }
               }
               echo '<div class="'.$wrapper_class.'" style="background-image:url('.$image[0].')">';
               ?>
               <div class="category-name">
                   <?php
                   $post_type =get_post_type(get_the_ID());
                   $obj = get_post_type_object( $post_type );
                   echo $obj->labels->singular_name;
                   ?>
               </div>
               <div class="image-content clearfix">
                   <?php
                   $logo =  ev_get_organization_logo(get_the_author_meta('ID'));
                   if(!empty($logo)){
                       echo '<div class="organization-logo">';
                       echo "<img src=".$logo.">";
                       echo "</div>";
                   }
                   ?>
                   
                  <div class="img-overlay-text">
                     <div class="news-title">
                         <span><?php the_title();?></span>
                         <span class="news-date"><?php echo get_the_date('dmy');?></span>
                     </div>
                     <div class="restro-sub-title"><?php echo wp_trim_words( get_the_excerpt(), 8, '...' ); ?></div>
                  </div>
               </div>
               <?php
               echo '</div>';/*background image close*/
           echo '</a>';
           echo '</div>';/*grid item close*/
       }
}
/**/

/*load course*/
function item_load_course( &$counter = '' ){

    global $ev_intranet;
    $url = $wrapper_start = $image_content = '';
    $wrapper_end = '</div>';

    if(isset($_POST['url']) && !empty($_POST['url'])){
        $url = $_POST['url'];
    }
    if ( strpos($_SERVER['REQUEST_URI'], "list") !== false || strpos($url, "list") !== false ){
        $wrapper_start = '<div class="full-width-content">';
    } else {
        $wrapper_start = '<div class="col-sm-4 c-padding">';
        $image_id = get_post_thumbnail_id( get_the_ID() );
        if(!empty($image_id)){
            $image = wp_get_attachment_image_src( $image_id, 'news-col-3');
            $image_content = '<div class="cat-image"><a href="'.get_the_permalink().'"><img src="'.$image[0].'"></a></div>';
        }else{
            if( isset($ev_intranet['default-placeholder-image']) && !empty($ev_intranet['default-placeholder-image'])){
                $img_id = $ev_intranet['default-placeholder-image']['id'];
                if(!empty($img_id)){
                    $image = wp_get_attachment_image_src( $img_id, 'news-col-3');
                    $image_content = '<div class="cat-image"><a href="'.get_the_permalink().'"><img src="'.$image[0].'"></a></div>';
                }
            }
        }
    }
    echo $wrapper_start;
    ?>
        <div class="category-list">
            <?php echo $image_content;?>
            <div class="category-content">
                <?php
                $logo =  ev_get_organization_logo(get_the_author_meta('ID'));
                if(!empty($logo)){
                    echo '<div class="organization-logo">';
                    echo "<img src=".$logo.">";
                    echo "</div>";
                }
                ?>

                <div class="company-cat-name-wrap clearfix">
                    <div class="company-cat-name">
                        <h2><a href="<?php the_permalink();?>"><?php the_title(); ?></a></h2>
                        <p><?php echo wp_trim_words( get_the_excerpt(), 8, '...' ); ?></p>
                    </div>
                    
                    <div class="contact-info">
                        <?php
                        $contact_info = get_post_meta( get_the_ID() ,'_course_contact_info', true );
                        if(!empty($contact_info)){
                            echo "<strong>".__('Kursansvarig','ev-intranet')."</strong>";
                            echo "<div>$contact_info</div>";
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    <?php
    echo $wrapper_end;

    if(!empty($counter)){
        if ( $counter % 3 == 0){
            echo '<div class="clearfix hidden-xs"></div>';
        }
        $counter ++;
    }
}
/**/

/*Limit admin access for member. Check for ajax call too.*/
function ev_no_admin_access_for_member(){
    global $current_user;
    $user_roles = $current_user->roles;
    if(in_array('member',$user_roles) ){
        if (strpos($_SERVER['REQUEST_URI'], '/wp-admin/admin-ajax.php') === false){
            wp_redirect( home_url( '/' ) );
            exit;
        }
    }
}
add_action( 'admin_init', 'ev_no_admin_access_for_member', 100 );

/*only login the approved member*/
add_filter('authenticate', 'member_check_login', 100, 3);
function member_check_login($user, $username, $password) {
    if(!empty($user)){
        if (!empty($username)) {
            if(property_exists($user,'roles')){
                if( in_array('member', $user->roles)){
                    $user_status = get_user_meta($user->ID,'approved_member',true);
                    if( true == $user_status ){
                        return $user;
                    }else{
                        return null;
                    }
                }
            }
        }
    }
    return $user;
}

/*add logout link in logged in menu*/
add_filter( 'wp_nav_menu_items', 'ev_add_logout_link', 10, 2 );
function ev_add_logout_link( $items, $args ) {
    if (is_user_logged_in() && $args->theme_location == 'login_menu') {
        $items .= '<li><a href="'.wp_logout_url(get_permalink()).'">'.__('Logga ut','ev-intranet').'</a></li>';
    }
    return $items;
}

/*set the post featured image to newly create post*/
function ev_set_created_post_ft_image($thumbnail_url,$created_post_id){

    $wp_upload_dir = wp_upload_dir();
    $image_data = file_get_contents($thumbnail_url);
    $filename   = basename($thumbnail_url);
    $filetype = wp_check_filetype( $filename, null );

    if( wp_mkdir_p( $wp_upload_dir['path'] ) ) {
        $file = $wp_upload_dir['path'] . '/' . $filename;
    } else {
        $file = $wp_upload_dir['basedir'] . '/' . $filename;
    }

    file_put_contents( $file, $image_data );

    $attachment = array(
        'post_mime_type' => $filetype['type'],
        'post_title'     => preg_replace( '/\.[^.]+$/', '', $filename ),
        'post_content'   => '',
        'post_status'    => 'inherit'
    );

    $attach_id = wp_insert_attachment( $attachment, $file, $created_post_id );

    require_once( ABSPATH . 'wp-admin/includes/image.php' );

    $attach_data = wp_generate_attachment_metadata( $attach_id, $file );
    wp_update_attachment_metadata( $attach_id, $attach_data );

    set_post_thumbnail( $created_post_id, $attach_id );
}
/**/

/*Get logo of an organization by user id */
function ev_get_organization_logo($user_id){
    $logo = '';
    $user_data = get_userdata($user_id);
    if(!empty($user_data)){
        if( in_array('administrator',$user_data->roles) ){
            global $ev_intranet;
            if(isset($ev_intranet['logo-image'])){
                $logo = $ev_intranet['logo-image']['url'];
            }
        }else{
            $user_organization = get_user_meta($user_id,'_organization_id',true);
            if(!empty($user_organization)){
                $logo = get_post_meta($user_organization,'organization_logo',true);
            }
        }
    }
    return $logo;
}

/*only show current user attachments*/
add_filter( 'ajax_query_attachments_args', 'show_current_user_attachments', 10, 1 );
function show_current_user_attachments( $query = array() ) {
    $user_id = get_current_user_id();
    if( $user_id ) {
        $user_data = get_userdata($user_id);
        if(!empty($user_data)){
            if(in_array('administrator',$user_data->roles)){
                return $query;
            }
            $query['author'] = $user_id;
        }
    }
    return $query;
}

function ev_add_query_vars_filter( $vars ){
    $vars[] = "city";
    $vars[] = "product";
    return $vars;
}
add_filter( 'query_vars', 'ev_add_query_vars_filter' );

function ev_add_excerpt_support_for_pages() {
    add_post_type_support( 'page', 'excerpt' );
}
add_action( 'init', 'ev_add_excerpt_support_for_pages' );


/*Remove Default Post types*/
add_action('admin_menu','ev_remove_default_post_type');
function ev_remove_default_post_type() {
    remove_menu_page('edit.php');
}

/*Allow additional file types to upload*/
add_filter( 'upload_mimes', 'ev_mime_types');
function ev_mime_types( $mime_types ) {
    $mime_types['eps'] = 'application/postscript';
    $mime_types['ai'] = 'application/postscript';
    $mime_types['bin'] = 'application/octet-stream';
    $mime_types['dms'] = 'application/octet-stream';
    $mime_types['lha'] = 'application/octet-stream';
    $mime_types['lzh'] = 'application/octet-stream';
    $mime_types['exe'] = 'application/octet-stream';
    $mime_types['class'] = 'application/octet-stream';
    $mime_types['so'] = 'application/octet-stream';
    $mime_types['dll'] = 'application/octet-stream';
    $mime_types['img'] = 'application/octet-stream';
    $mime_types['iso'] = 'application/octet-stream';
    return $mime_types;
}

//check if role exist before removing it. Remove local_admin role
/*if( get_role('local_admin') ){
    remove_role( 'local_admin' );
}*/

/*add new role local_admin*/
/*$result = add_role(
    'local_admin',
    __( 'Local Admin' ),
    array(
        'moderate_comments'  => true,
        //'manage_categories' => true,
        'manage_links' => true,
        'upload_files' => true,
        'unfiltered_html' => true,
        'edit_posts' => true,
        'edit_published_posts' => true,
        'publish_posts' => true,
        'delete_posts' => true,
        'delete_published_posts' => true,
        'delete_private_posts' => true,
        'edit_private_posts' => true,
        'read_private_posts' => true,
        'delete_private_pages' => true,
        'edit_private_pages' => true,
        'read_private_pages' => true,
        'edit_pages' => true,
        'edit_published_pages' => true,
        'publish_pages' => true,
        'delete_pages' => true,
        'delete_published_pages' => true,
        'read' => true,
        'level_7' => 1,
        'level_6' => 1,
        'level_5' => 1,
        'level_4' => 1,
        'level_3' => 1,
        'level_2' => 1,
        'level_1' => 1,
        'level_0' => 1,
        'create_users' => false,
        'list_users' => true,
        'promote_users' => false,
        'remove_users' => true,
        'create_members' => true,
    )
);
if ( null !== $result ) {
    echo 'Yay! New role created!';
}
else {
    echo 'Oh... the role already exists.';
}
die;*/

function add_theme_caps() {
    // gets the author role
    $role = get_role( 'local_admin' );

    // This only works, because it accesses the class instance.
    // would allow the author to edit others' posts for current theme only
    $role->add_cap( 'edit_others_posts' );
}
//add_action( 'admin_init', 'add_theme_caps');

/* Mandrill SMTP */
//add_action( 'phpmailer_init', 'ev_phpmailer_settings' );
function ev_phpmailer_settings( $phpmailer ) {
    $phpmailer->isSMTP();
    $phpmailer->Host = 'smtp.mandrillapp.com';
    $phpmailer->SMTPAuth = true; // Force it to use Username and Password to authenticate
    $phpmailer->Port = 587;
    $phpmailer->Username = 'Skradderiforbundet';
    $phpmailer->Password = 'm8g9unWW5MIoKx9ml9DbTQ';
    //$phpmailer->From = "no-reply@skradderiforbundet.com";
    //$phpmailer->From = "abc<wordpress@202.166.198.46>";
    //$phpmailer->FromName = "Skradderiforbundet";
}

//check if role exist before removing it. Remove member role
/*if( get_role('member') ){
    remove_role( 'member' );
}*/

/*add new role member*/
/*$result = add_role(
    'member',
    __( 'Member' ),
    array(
        'upload_files' => true,
        'edit_posts' => true,
        'edit_published_posts' => true,
        'publish_posts' => true,
        'delete_posts' => true,
        'delete_published_posts' => true,
        'read' => true,
        'level_2' => 1,
        'level_1' => 1,
        'level_0' => 1,
    )
);
if ( null !== $result ) {
    echo 'Yay! New role created!';
}
else {
    echo 'Oh... the role already exists.';
}
die;*/