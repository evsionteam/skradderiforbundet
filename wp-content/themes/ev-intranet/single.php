<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package EVision_Intranet
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="inner-wrap">
			<div class="above-container">
				<?php
				while ( have_posts() ) : the_post();

					get_template_part( 'template-parts/content', 'image' );

				endwhile; // End of the loop.
				?>

				<div class="post-nav">
				    <div class="prev-next-post-nav nav-pagination"><?php next_post_link('%link','<i class = "fa fa-long-arrow-left"></i> &nbsp;Tidigare' ) ?></div>
				    <div class="prev-next-post-nav nav-pagination"><?php previous_post_link( '%link','Nästa &nbsp;<i class = "fa fa-long-arrow-right"></i>' ) ?></div>
				</div>
                <div class="sk-archive-link">
                    <?php
                    $post_type = get_post_type(get_the_ID());
                    $post_type_obj = get_post_type_object( $post_type );
                    ?>
                    <a class="see-all" href="<?php echo get_post_type_archive_link($post_type);?>">
                        <?php printf( esc_html__( 'Se alla %s', 'skradderiforbundet' ), $post_type_obj->label );?>
                    </a>
                </div>
			</div>
			</div><!-- inner-wrap -->
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
