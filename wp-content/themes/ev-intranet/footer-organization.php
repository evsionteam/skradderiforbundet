<?php global $ev_intranet;?>
</div><!-- #content -->
<div class="footer-info-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="sk-footer-info">
                    <?php
                        while(have_posts()):the_post();
                        $content = get_the_content();
                        if(!empty($content)){
                            echo "<h2>".get_the_title()."</h2>";
                            echo wpautop($content);
                        }
                        endwhile;wp_reset_postdata();
                    ?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="sk-footer-info">
                    <?php
                    $insta_feed = get_post_meta(get_the_ID(),'_insta_feed',true);
                    if(!empty($insta_feed)){
                        echo '<h2>'.__('Instagram','ev-intranet').'</h2>';
                        echo do_shortcode($insta_feed);
                    }
                    ?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="sk-footer-info">
                    <?php
                    $fb_feed = get_post_meta(get_the_ID(),'_fb_feed',true);
                    if(!empty($fb_feed)){
                        echo '<h2>'.__('Facebook','ev-intranet').'</h2>';
                        echo wpautop($fb_feed);
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<footer id="colophon" class="site-footer" role="contentinfo">
    <div class="footer-widget">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    <?php dynamic_sidebar('footer-1');?>
                </div>
                <div class="col-sm-6 col-md-3">
                    <?php dynamic_sidebar('footer-2');?>
                </div>
                <div class="col-sm-6 col-md-3">
                    <?php dynamic_sidebar('footer-3');?>
                </div>
                <div class="col-sm-6 col-md-3">
                    <?php dynamic_sidebar('footer-4');?>
                </div>
            </div><!-- row -->
        </div>
    </div><!-- footer-widget -->
</footer><!-- #colophon -->
</div><!-- #page -->
<?php wp_footer(); ?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
</body>
</html>