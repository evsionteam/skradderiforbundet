<?php get_header(); ?>
	<div id="primary" class="content-area main-content">
		<main id="main" class="site-main" role="main">
			<div class="above-container">
				<div class="row">
					<?php require get_template_directory() . '/inc/breadcrumbs.php'; ?>
				</div>
				<div class="c-row">
                    <?php
                    $wrapper_class = '';
                    if( strpos($_SERVER['REQUEST_URI'], "list") !== false){
                        $wrapper_class = 'news-grid-wrapper new-class';
                    }else{
                        $wrapper_class = 'news-grid-wrapper';
                    }
                    ?>
					<div class="<?php echo $wrapper_class;?>">
                        <?php
                        $counter = 1;

                        global $wp_query;
                        $ppp = get_query_var('posts_per_page');
                        $found_posts = $wp_query->found_posts;

                        $load_text = ($found_posts > $ppp) ? 'Load More' : 'Inga fler inlägg';
                        $load_class = ($found_posts > $ppp) ? 'posts-exists' : 'no-posts';

                        while ( have_posts() ) : the_post();
                            item_load_posts();
                        endwhile;wp_reset_postdata();
                        ?>
                    </div><!-- category-section  -->
                </div><!-- c-row -->

                <?php if($found_posts > $ppp):?>
                    <div id="load-more-posts" class="course-page-load-more">
                        <div class="container">
                            <div id="loading-text" class="<?php echo $load_class;?>">
                                <a href="#">
                                    <span class="loader-text"><?php echo $load_text;?></span>
                                </a>
                                <span class="rosen-loader" style="display:none">
                                    <i class="fa fa-spinner fa-spin fa-2x"></i>
                                </span> <!-- rosen-loader -->
                            </div>
                        </div><!-- container -->
                    </div><!-- load-more-posts-news-->
                <?php endif;?>
			</div><!-- container -->
		</main><!-- #main -->
	</div><!-- #primary -->
    
<?php
get_footer();