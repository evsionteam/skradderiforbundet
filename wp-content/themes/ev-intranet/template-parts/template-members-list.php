<?php
/*Template Name: Members List Template*/
get_header();
while(have_posts()):the_post();

    $selected_org = $search_text = $org_id = '';
    $user_args = array(
        'role'  => 'member',
    );

    if(isset($_GET['organization'])){
        $selected_org = $_GET['organization'];
        if(!empty($selected_org)){
            if ( $post = get_page_by_path( $selected_org, OBJECT, 'ev_organization' ) ){
                $org_id = $post->ID;
            }
        }
    }

    if(isset($_GET['search'])){
        $search_text = $_GET['search'];
    }

    if( !empty($selected_org) && !empty($search_text) ){
        if(!empty($org_id)){
            $user_args['meta_query'] = array(
                'relation' => 'AND',
                array(
                    'key'     => '_organization_id',
                    'value'   => $org_id,
                    'compare' => '='
                ),
                array(
                    'relation' => 'AND',
                    array(
                        'key' => 'first_name',
                        'value' => $search_text,
                        'compare' => 'LIKE',
                    ),
                    array(
                        'key' => 'last_name',
                        'value' => $search_text,
                        'compare' => 'LIKE',
                    ),
                ),
            );
        }
    }elseif( !empty($selected_org) && empty($search_text) ){
        if(!empty($org_id)){
            $user_args['meta_query'] = array(
                array(
                    'key'     => '_organization_id',
                    'value'   => $org_id,
                    'compare' => '='
                ),
            );
        }
    }elseif( empty($selected_org) && !empty($search_text) ){
        $user_args['meta_query'] = array(
            'relation' => 'OR',
            array(
                'key'     => 'first_name',
                'value'   => $search_text,
                'compare' => 'LIKE',
            ),
            array(
                'key'     => 'last_name',
                'value'   => $search_text,
                'compare' => 'LIKE',
            ),
        );
    }else{

    }

    $user_query = new WP_User_Query($user_args);
    $members = $user_query->get_results();

    ?>
    <div id="primary" class="content-area main-content">
        <main id="main" class="site-main" role="main">
            <?php get_template_part('template-parts/content','inner');?>
                <div class="container">
                    <div class="row">
                        <div class="cm-padding members-wrapper sk-cm-table">
                                <div class="col-sm-12">
                                    <div class="ev-select-filters filter clearfix">
                                        <form name="member-filter" id="member-filter" class="skrad-form" action="<?php echo site_url('/medlemsregister');?>" autocomplete="off">
                                            <div class="select-box">
                                                <select name="organization" id="organization" class="selectpicker">
                                                    <option value=""><?php _e('Välj förening','ev-intranet');?></option>
                                                    <?php
                                                    $org_args = array(
                                                        'post_type' => 'ev_organization',
                                                        'posts_per_page' => -1,
                                                        'post_status' => 'publish'
                                                    );
                                                    $organizations = new WP_Query($org_args);
                                                    if($organizations->have_posts()):
                                                        while($organizations->have_posts()):$organizations->the_post();
                                                        global $post;
                                                        ?>
                                                            <option value="<?php echo $post->post_name;?>" <?php selected($post->post_name,$selected_org)?>><?php the_title();?></option>
                                                        <?php
                                                        endwhile;wp_reset_postdata();
                                                    endif;
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="searchbox select-box">
                                                <input type="search" class="search-input" placeholder="<?php  _e('Sök medlem…','ev-intranet');?>" name="search" value="<?php echo $search_text;?>">
                                                <button type="submit" class="ev-filter-search"><i class="fa fa-search"></i></button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <?php if(!empty($members)):?>
                                    <div class="sk-cm-table sk-foo-table table-responsive">
                                        <table class="ev-table table table-striped" data-sorting="true" data-show-toggle="false">
                                            <thead>
                                            <tr>
                                                <th><?php _e('Namn','ev-intranet');?></th>
                                                <th data-breakpoints="xs"><?php _e('Förening','ev-intranet');?></th>
                                                <th data-breakpoints="xs"><?php _e('Företag','ev-intranet');?></th>
                                                <th data-breakpoints="xs"><?php _e('Telefon','ev-intranet');?></th>
                                                <th data-breakpoints="xs"><?php _e('Email','ev-intranet');?></th>
                                                <th data-breakpoints="all"><?php _e('Adress','ev-intranet');?></th>
                                                <th data-breakpoints="all"><?php _e('Zip Code','ev-intranet');?></th>
                                                <th data-breakpoints="all"><?php _e('City','ev-intranet');?></th>
                                                <th data-breakpoints="all"><?php _e('Website','ev-intranet');?></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            foreach($members as $member):
                                                $member_id = $member->ID;
                                                $member_info = get_userdata($member_id);
                                                ?>
                                                <tr>
                                                    <td>
                                                        <?php
                                                        $first_name = get_user_meta($member_id,'first_name',true);
                                                        $last_name = get_user_meta($member_id,'last_name',true);
                                                        echo $first_name.' '.$last_name;
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        $org_id = get_user_meta($member_id,'_organization_id',true);
                                                        if(!empty($org_id)){
                                                            echo get_the_title($org_id);
                                                        }
                                                        ?>
                                                    </td>
                                                    <td><?php echo get_user_meta($member_id,'company_name',true)?></td>
                                                    <td><?php echo get_user_meta($member_id,'member_home_phone',true)?></td>
                                                    <td><?php echo $member_info->data->user_email;?>
                                                    <span class="footable-toggle fooicon fooicon-plus"></span></td>
                                                    <td><?php echo get_user_meta($member_id,'member_home_address',true)?></td>
                                                    <td><?php echo get_user_meta($member_id,'member_post_number',true)?></td>
                                                    <td><?php echo get_user_meta($member_id,'member_place',true)?></td>
                                                    <td><a href="<?php echo esc_url(get_user_meta($member_id,'company_website',true));?>" target="_blank"><?php echo get_user_meta($member_id,'company_website',true)?></a></td>
                                                </tr>
                                            <?php endforeach;?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <?php
                                    else:
                                        ?>
                                        <div class="no-members">
                                            <h3><?php _e('No Members Found','ev-intranet'); ?></h3>
                                        </div>
                                    <?php
                                    endif;
                                    ?>
                                </div>
                        </div>
                    </div>
                </div>
        </main>
    </div>
<?php
endwhile; wp_reset_postdata();
get_footer();
?>	