<?php
/*Template Name: Video Page*/
?>

<?php get_header(); ?>
<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<div class="inner-wrap">
			<div class = "above-container">
				<div class="c-row">

					<?php 
						$counter = 1;
						$args = array('post_type' => array( 'video' ));
				        $query = new WP_Query($args);
				        while ($query->have_posts()) {
					        $query->the_post();
					        
							$video = get_post_meta( get_the_ID(), 'video_upload', true);
							$data_type = 'local';

							if(!$video){
						 		$youtube_video = get_post_meta( get_the_ID(), 'youtube_video_link', true);
								if($youtube_video){
	                                $youtube_video_link = "https://www.youtube.com/watch?v=$youtube_video";
									$video = htmlspecialchars(wp_oembed_get($youtube_video_link, array( 'autoplay' => 1 ) ));

									$data_type = 'embed';
								}
						 	}
					
						 	if(has_post_thumbnail()){
								$image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ),'news-col-3');
								$image = $image[0];
						 	}else{
						 		$image = "https://placeholdit.imgix.net/~text?txtsize=1&bg=0a3e7c&txtclr=ffffff&txt=Rosendlundsakeri&w=378&h=220";
						 	}
					?>

					 	<div class = "col-sm-4 c-padding">
	                        <a href="#" class="play-video">
	                        <div class="video-block" id="custom-video-<?php echo get_the_ID();?>" data-src="<?php echo $video; ?>" data-type="<?php echo $data_type; ?>" data-title="<?php echo get_the_title(); ?>" data-date="<?php echo get_the_date('d-m-y'); ?>" style="background-image: url(<?php echo $image; ?>);" >
									<div class="video-caption">

											<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="50px" height="50px" viewBox="0 0 85.5 83" enable-background="new 0 0 85.5 83" xml:space="preserve">
												<path fill="#FFFFFF" d="M43,80.5c-5.15,0-10.172-0.999-14.925-2.969c-4.755-1.969-9.012-4.813-12.652-8.453
													c-3.64-3.641-6.484-7.897-8.454-12.652C4.999,51.671,4,46.65,4,41.5c0-5.149,0.999-10.17,2.969-14.925
													c1.969-4.755,4.813-9.012,8.454-12.652c3.638-3.639,7.895-6.483,12.652-8.454C32.83,3.5,37.852,2.5,43,2.5
													c5.148,0,10.169,0.999,14.925,2.969c4.758,1.971,9.015,4.815,12.652,8.454c3.64,3.641,6.484,7.897,8.454,12.652
													C81.001,31.33,82,36.352,82,41.5c0,5.149-0.999,10.171-2.969,14.925c-1.97,4.755-4.813,9.012-8.454,12.652
													c-3.64,3.64-7.897,6.483-12.652,8.453C53.171,79.501,48.15,80.5,43,80.5z M43,4.5c-4.884,0-9.648,0.948-14.159,2.817
													c-4.514,1.87-8.553,4.568-12.004,8.02c-3.454,3.454-6.152,7.493-8.02,12.004C6.948,31.852,6,36.616,6,41.5
													c0,4.885,0.948,9.649,2.817,14.16c1.869,4.512,4.567,8.55,8.02,12.004c3.454,3.453,7.492,6.151,12.004,8.02
													C33.35,77.553,38.114,78.5,43,78.5c4.886,0,9.65-0.947,14.159-2.816c4.512-1.868,8.55-4.566,12.004-8.02
													c3.453-3.454,6.151-7.492,8.02-12.004C79.052,51.15,80,46.386,80,41.5c0-4.885-0.948-9.649-2.817-14.159
													c-1.868-4.511-4.566-8.55-8.02-12.004c-3.451-3.452-7.489-6.15-12.004-8.02C52.648,5.448,47.884,4.5,43,4.5z"/>
												<path fill="#FFFFFF" d="M34.5,56.82V24.732l27.877,16.54L34.5,56.82z M36.5,28.244v25.171l21.867-12.197L36.5,28.244z"/>
											</svg>
								 		<h3><?php the_title(); ?></h3>
								 		<span class="date"><?php echo get_the_date('d-m-y');?></span>
							 		</div>
						 	</div><!-- vide0-block -->
	                        </a>
	                    </div>
						<?php
							if( $counter % 3 == 0){
								echo "<div class='clearfix'></div>";
							}
							$counter++;
						} ?>

				</div>
			</div><!-- .container -->	
		</div><!-- inner-wrap -->
	</main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>
