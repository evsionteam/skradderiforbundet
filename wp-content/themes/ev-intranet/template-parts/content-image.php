<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package EVision_Intranet
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="single-content-wrap">
        <?php $post_thumbnail_id = get_post_thumbnail_id( get_the_ID() );
        $image = wp_get_attachment_image_src( $post_thumbnail_id, 'full');
        if(empty($image)){
            global $ev_intranet;
            if( isset($ev_intranet['default-placeholder-image']) && !empty($ev_intranet['default-placeholder-image'])){
                $img_id = $ev_intranet['default-placeholder-image']['id'];
                if(!empty($img_id)){
                    $image = wp_get_attachment_image_src( $img_id, 'full');
                }
            }
        }
        ?>
        <div class="single-full-image" style="background-image:url(<?php echo $image[0];?>)">
            <div class="single-content-overlay">
                <div class="organization-logo">
                    <?php
                    $logo =  ev_get_organization_logo(get_the_author_meta('ID'));
                    if(!empty($logo)){
                        echo "<img src=".$logo.">";
                    }
                    ?>
                </div>
                <div class="img-overlay-text">
                     <div class="news-title">
                         <span><?php the_title(); ?></span>
                         <span class="news-date"><?php echo get_the_date('d m y');?></span>
                     </div>
                     <!--<div class="restro-sub-title">Lorem ipsum dolor sit amet, consectetuer adipiscing elit....</div>-->
                </div>
            </div>
        </div>
        <div class="single-text-content">
           <div class="text-content">
                <?php the_content(); ?>
               <?php if(is_singular('ev_courses')):?>
                   <div class="contact-info">
                       <table>
                           <tbody>
                           <tr>
                               <th><?php _e('Startdatum','skradderiforbundet')?></th>
                               <td><?php echo get_post_meta(get_the_ID(),'_course_start_date',true);?></td>
                           </tr>
                           <tr>
                               <th><?php _e('Slutdatum','skradderiforbundet')?></th>
                               <td><?php echo get_post_meta(get_the_ID(),'_course_end_date',true);?></td>
                           </tr>
                           <tr>
                               <th><?php _e('Time:','skradderiforbundet')?></th>
                               <td><?php echo get_post_meta(get_the_ID(),'course_time',true);?></td>
                           </tr>
                           <tr>
                               <th><?php _e('Kurshållare','skradderiforbundet')?></th>
                               <td><?php echo get_post_meta(get_the_ID(),'_course_holder',true);?></td>
                           </tr>
                           <tr>
                               <th><?php _e('Plats','skradderiforbundet')?></th>
                               <td><?php echo get_post_meta(get_the_ID(),'_training_place',true);?></td>
                           </tr>
                           <tr>
                               <th><?php _e('Website','skradderiforbundet')?></th>
                               <td><a href="<?php echo esc_url(get_post_meta(get_the_ID(),'_course_link',true));?>" target="_blank"><?php echo get_post_meta(get_the_ID(),'_course_link',true);?></a></td>
                           </tr>
                           <tr>
                               <th><?php _e('Kontaktinformation','skradderiforbundet')?></th>
                               <td><?php echo get_post_meta(get_the_ID(),'_course_contact_info',true);?></td>
                           </tr>
                           </tbody>
                       </table>
                   </div>
               <?php endif;?>
           </div>
        </div><!-- single-text-content -->
    </div><!-- single-content-wrap -->
</article><!-- #post-## -->