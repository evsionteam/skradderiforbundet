<?php
/*Template Name: Upload Company Template*/
get_header();
while(have_posts()):the_post();
    $action = 'save';
    $btn_text = __('Publicera','ev-intranet');

    $cats_arr = array();
    $name = $content = $address = $zip_code = $city = $phone = $email = $lat = $long = $website = '';

    $args = array(
        'post_type' => 'ev_companies',
        'author' => get_current_user_id(),
        'post_status' => array( 'pending', 'draft', 'publish' )
    );
    $company = new WP_Query($args);
    if($company->have_posts()):
        $action = 'update';
        $btn_text = __('Update','ev-intranet');
        while($company->have_posts()):$company->the_post();
            $name = get_the_title();
            $content = get_the_content();

            $company_data = get_post_meta(get_the_ID());

            $address = isset($company_data['_company_address']) ? $company_data['_company_address'][0] : '';
            $zip_code= isset($company_data['_company_zip_code']) ? $company_data['_company_zip_code'][0] : '';
            $city = isset($company_data['_company_city']) ? $company_data['_company_city'][0] : '';
            $phone = isset($company_data['_company_phone']) ? $company_data['_company_phone'][0] : '';
            $email = isset($company_data['_company_email']) ? $company_data['_company_email'][0] : '';
            $lat = isset($company_data['_company_latitude']) ? $company_data['_company_latitude'][0] : '';
            $long = isset($company_data['_company_longitude']) ? $company_data['_company_longitude'][0] : '';
            $website = isset($company_data['_company_website']) ? $company_data['_company_website'][0] : '';

            $categories = get_the_terms(get_the_ID(),'company_category');
            if( !empty($categories) && is_array($categories) ){
                foreach($categories as $cat){
                    $cats_arr[] = $cat->term_id;
                }
            }

        endwhile;wp_reset_postdata();
    endif;
?>


<div id="primary" class="content-area main-content">
    <main id="main" class="site-main" role="main">
        <?php get_template_part('template-parts/content','inner');?>
            <div class = "container">
                <div class="row">
                    <div class="cm-padding company-upload-form-wrapper">
                        <div class="company-message"></div>
                        <form name="upload_company_form" id="upload_company_form" class="skrad-form" method="post" enctype="multipart/form-data" data-action="<?php echo $action;?>">
                            <?php wp_nonce_field( 'submit_company', 'company_submit_nonce' ); ?>
                            <div class="col-md-3">
                                <div class="form-group">
                            		<label for="post_title"><?php _e('Företagets namn','ev-intranet');?></label>
                                	<input type="text" id="post_title" name="post_title" value="<?php echo $name;?>" required/>
                            	</div>
                            </div>
                        	<div class="col-md-3">
                                <div class="form-group">
                            		<label for="address"><?php _e('Adress','ev-intranet');?></label>
                                	<input type="text" id="address" name="address" value="<?php echo $address;?>" required/>
                            	</div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="zip-code"><?php _e('Zip Code','ev-intranet');?></label>
                                    <input type="text" id="zip-code" name="zip_code" value="<?php echo $zip_code;?>" required/>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="city"><?php _e('City','ev-intranet');?></label>
                                    <input type="text" id="city" name="city" value="<?php echo $city;?>" required/>
                                </div>
                            </div>
                        	<div class="col-md-3">
                                <div class="form-group">
                            		<label for="phone"><?php _e('Telefonnummer','ev-intranet');?></label>
                                	<input type="text" id="phone" name="phone" value="<?php echo $phone;?>" required/>
                            	</div>
                            </div>
                        	<div class="col-md-3">
                                <div class="form-group">
                            		<label for="email"><?php _e('Email','ev-intranet');?></label>
                                	<input type="email" id="email" name="email" value="<?php echo $email;?>"/>
                            	</div>
                            </div>
                        	<div class="col-md-3">
                                <div class="form-group latitud-link">
                            		<label for="longitude"><?php _e('Longitud','ev-intranet') ?>
                                        <a data-toggle="tooltip" data-placement="right" title="<?php _e('Vi behöver longitud och latitud för din adress. Klicka dig till din adress på kartan nedan, eller besök http://www.latlong.net/, för att få fram de rätta uppgifterna.','ev-intranet');?>">
                                            <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                        </a>      
                                    </label>
                                	<input type="text" id="longitude" name="longitude" required value="<?php echo $long;?>"/>
                                        <a class="info-link" href="http://www.latlong.net/" target="_blank">
                                            <?php _e('Ta reda på din longitud/latitude','ev-intranet');?>
                                        </a>
                            	</div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                            		<label for="latitude"><?php _e('Latitud','ev-intranet');?>
                                        <a data-toggle="tooltip" data-placement="right" title="<?php _e('Vi behöver longitud och latitud för din adress. Klicka dig till din adress på kartan nedan, eller besök http://www.latlong.net/, för att få fram de rätta uppgifterna.','ev-intranet');?>">
                                            <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                        </a>      
                                    </label>
                                	<input type="text" id="latitude" name="latitude" required value="<?php echo $lat;?>"/>
                            	</div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="website"><?php _e('Website','ev-intranet');?></label>
                                    <input type="text" id="website" name="website" value="<?php echo $website;?>"/>
                                </div>
                            </div>
                        	<div class="col-md-3">
                                <div class="form-group">
                            		<label for="category"><?php _e('Kategori','ev-intranet');?></label>
                                    <div class="select-box">
                                        <select id="category" name="category[]" class="selectpicker" data-cats="<?php echo join(',',$cats_arr);?>" title="<?php _e('Välj kategori','ev-intranet');?>" multiple>
                                            <?php
                                            $company_categories = get_terms(
                                                array(
                                                    'taxonomy' => 'company_category',
                                                    'parent' => 0,
                                                    'hide_empty' => 0,
                                                )
                                            );
                                            if(!empty($company_categories)):
                                                foreach($company_categories as $company_category){
                                                    $selected = in_array($company_category->term_id,$cats_arr) ? "selected='selected'" : '';
                                                    ?>
                                                    <option value="<?php echo $company_category->term_id?>" <?php echo $selected;?>><?php echo $company_category->name?></option>
                                                <?php
                                                }
                                            endif;
                                            ?>
                                        </select>
                                    </div>
                                    <div class="cat-submit-form">
                                        <a href="#" data-toggle="modal" data-target="#sendCatInfo">
                                        <?php _e('Skicka in ny kategori','ev-intranet');?>
                                        </a>
                                    </div>
                            	</div>
                            </div>
                        	<div class="col-md-3">
                                <div class="form-group">
                            		<label for="sub-cats">
                                        <?php _e('Sub Kategori','ev-intranet');?>
                                        <span class="load-more-spinner" style="display: none">
                                            <i class="fa fa-spinner fa-spin"></i>
                                        </span>
                                    </label>
                                    <div class="select-box">
                                        <select id="sub-cats" name="sub_cats[]" class="selectpicker" title="<?php _e('Välj Sub kategori','ev-intranet');?>" multiple></select>
                                    </div>
                            	</div>
                            </div>
                        	<div class="col-md-3">
                                <div class="form-group">
                            		<label for="image"><?php _e('Logotyp','ev-intranet');?>
                                        <a data-toggle="tooltip" data-placement="right" title="Tooltip on Right">
                                            <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                        </a>      
                                    </label>
                            		
                                	<div class="choose-file">
                                       <input type="file" name="image" id="file-image" accept="image/*" class="form-control files-data form-control" onchange="uploadFile(this)"/>
                                       <label for="file-image">
                                           <span id="file-name" class="file-box"><?php _e('Ladda upp fil (.jpg / .png)','ev-intranet')?></span>
                                           <span class="file-btn"><i class="fa fa-arrow-circle-o-up"></i></span>
                                       </label>
                                   </div>
                            	</div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                            		<label for="description"><?php _e('Information om företaget','ev-intranet');?></label>
                                	<textarea id="description" name="description" rows="6"><?php echo $content;?></textarea>
                                </div>
                                <div class="form-group">
                                    <div id="map-canvas" style="width: 100%;height: 500px;background-color: grey;"></div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" name="submit_company" class="btn btn-primary c-btn btn-upload <?php echo $action;?>">
                                        <?php echo $btn_text;?>
                                    </button>
                                    <span class="add-company-loader" style="display: none">
                                        <i class="fa fa-spinner fa-spin fa-2x"></i>
                                    </span>
                                </div>
                            </div>
                        </form>


                    </div>
                    
                </div>
            </div>
    </main>
</div>


<!--Popup Modal-->
<div class="modal fade modal-design" id="sendCatInfo" tabindex="-1" role="dialog" aria-labelledby="catSubmission">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="catSubmission">
                    <?php _e('Skicka in ett förslag på en kategori och så lägger vi till den.','ev-intranet')?>
                </h4>
            </div>
            <div class="modal-body">
                <?php
                global $ev_intranet;
                if(isset($ev_intranet['category-suggestion-form'])){
                    echo do_shortcode($ev_intranet['category-suggestion-form']);
                }
                ?>
            </div>
        </div>
    </div>
</div>
<!---->

<?php 
endwhile; wp_reset_postdata();
get_footer();
?>	