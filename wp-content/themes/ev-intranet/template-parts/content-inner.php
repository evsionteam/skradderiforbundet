<div class="c-inner-banner">
    <div class="above-container">
        <div class="c-row">
            <div class="col-md-10 col-md-offset-1">
                <div class="inner-banner-content">
                    <div class="c-row">
                        <div class="col-sm-7 c-padding">
                            <div class="left-text-wrap">
                                <h3>
                                    <?php
                                    if(is_front_page()):
                                        global $current_user;
                                        printf( esc_html__( 'Välkommen %1$s', 'ev-intranet' ), $current_user->data->display_name );
                                    else:
                                        the_title();
                                    endif;
                                    ?>
                                </h3>
                                <p>
                                    <?php
                                        global $post;
                                        echo wpautop($post->post_excerpt);
                                    ?>
                                </p>
                                <?php
                                $btn_text = get_post_meta($post->ID,'_btn_text',true);
                                $btn_link = get_post_meta($post->ID,'_btn_link',true);
                                if(!empty($btn_link) && !empty($btn_text)){
                                    echo '<a class="c-btn photo-gall-btn" href="'.$btn_link.'">'.$btn_text.'</a>';
                                }
                                ?>
                            </div><!-- left-text-wrap -->
                        </div>
                        <div class="col-sm-5 c-padding">
                            <?php
                            $style = '';
                            if(has_post_thumbnail()):
                                $style = 'style="background-image:url('.get_the_post_thumbnail_url(get_the_ID(),'full').')"';
                            endif;
                            ?>
                            <div class="rgt-banner-image" <?php echo $style;?> ></div>
                        </div>
                    </div>
                </div><!-- inner-banner-content -->
            </div>
        </div>
    </div>
</div><!-- c-inner-banner -->