<?php
/*Template Name: Contact Template*/
get_header();
global $ev_intranet;
while(have_posts()):the_post();
    ?>
    <div id="primary" class="content-area main-content">
        <main id="main" class="site-main" role="main">
            <?php get_template_part('template-parts/content','inner');?>
                <div class = "container">
                    <div class="row">
                        <div class="cm-padding contact-form-wrapper">
                            <div class="contact-message"></div>
                            <form name="ev_contact_form" id="ev_contact_form" class="skrad-form" method="post" enctype="multipart/form-data">
                                <?php wp_nonce_field( 'submit_contact_form', 'contact_form_nonce' ); ?>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="topic"><?php _e('Välj typ av tips','ev-intranet');?>
                                        </label>
                                        <div class="select-box">
                                            <select id="topic" name="topic" class="selectpicker" required>
                                                <option value=""><?php _e('Välj typ','ev-intranet');?></option>
                                                <?php
                                                if(isset($ev_intranet['contact-form-topic']) && !empty($ev_intranet['contact-form-topic'])){
                                                    if(is_array($ev_intranet['contact-form-topic'])){
                                                        foreach($ev_intranet['contact-form-topic'] as $topic){
                                                            echo "<option>".$topic."</option>";
                                                        }
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="subject"><?php _e('Ämne','ev-intranet');?></label>
                                        <input type="text" id="subject" name="subject" required/>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="website"><?php _e('Länk','ev-intranet');?></label>
                                        <input type="text" id="website" name="website"/>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="image"><?php _e('Bilagor','ev-intranet');?>
                                            <a data-toggle="tooltip" data-placement="right" title="Tooltip on Right">
                                                <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                            </a>
                                        </label>
                                        <div class="choose-file">
                                            <input type="file" name="attachment" id="attachment" class="form-control files-data form-control" onchange="uploadFile(this)"/>
                                            <label for="attachment">
                                                <span id="file-name" class="file-box"><?php _e('Ladda upp fil','ev-intranet');?></span>
                                                <span class="file-btn"><i class="fa fa-arrow-circle-o-up"></i></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="description"><?php _e('Meddelande','ev-intranet');?></label>
                                        <textarea id="description" name="description" rows="6" required></textarea>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" name="submit_contact_form" class="btn btn-primary c-btn btn-upload">
                                            <?php _e('SKICKA IN','ev-intranet');?>
                                        </button>
                                        <span class="contact-form-loader" style="display: none">
                                            <i class="fa fa-spinner fa-spin fa-2x"></i>
                                        </span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
        </main>
    </div>
<?php
endwhile; wp_reset_postdata();
get_footer();
?>	