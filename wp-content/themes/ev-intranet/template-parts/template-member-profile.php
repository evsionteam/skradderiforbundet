<?php
/*Template Name: Member Profile Template*/
get_header();
while(have_posts()):the_post();
    global $current_user;
    $user_id = $current_user->ID;
    $user_meta = get_user_meta($user_id);

    $master_in = get_user_meta($user_id,'master_in',true);
    if(empty($master_in)){
        $master_in = array();
    }

    $certificate_in = get_user_meta($user_id,'certificate_in',true);
    if(empty($certificate_in)){
        $certificate_in = array();
    }

    $company_nature = get_user_meta($user_id,'company_nature',true);
    if(!empty($company_nature) && is_array($company_nature)){
        $company_nature = implode(',',$company_nature);
    }

    ?>
    <div id="primary" class="content-area main-content">
        <main id="main" class="site-main" role="main">
            <?php get_template_part('template-parts/content','inner');?>
                <div class = "container">
                    <div class="row">
                        <div class="cm-padding member-profile">
                            <div class="member-message"></div>
                            <form name="edit_member_form" id="edit_member_form" class="skrad-form" method="post" class="skrad-form" enctype="multipart/form-data">
                                <?php wp_nonce_field( 'edit_member', 'edit_member_nonce' ); ?>

                                <div class="personal-detail-section clearfix">
                                    <!--Details-->
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="first-name"><?php _e('Förnamn', 'skradderiforbundet'); ?></label>
                                            <input type="text" id="first-name" name="first_name" value="<?php echo isset($user_meta['first_name'][0]) ? $user_meta['first_name'][0] : '' ;?>"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="last-name"><?php _e('Efternamn', 'skradderiforbundet'); ?></label>
                                            <input type="text" id="last-name" name="last_name" value="<?php echo isset($user_meta['last_name'][0]) ? $user_meta['last_name'][0] : '' ;?>"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="member-birthdate"><?php _e('Födelsedatum', 'skradderiforbundet'); ?></label>
                                            <input type="text" id="member-birthdate" name="member_birthdate" value="<?php echo isset($user_meta['member_birthdate'][0]) ? $user_meta['member_birthdate'][0] : '' ;?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="member-profession"><?php _e('Yrke','ev-intranet');?></label>
                                            <input type="text" id="member-profession" name="member_profession" value="<?php echo isset($user_meta['member_profession'][0]) ? $user_meta['member_profession'][0] : '' ;?>"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="member-home-address"><?php _e('Hemadress','ev-intranet');?></label>
                                            <input type="text" id="member-home-address" name="member_home_address" value="<?php echo isset($user_meta['member_home_address'][0]) ? $user_meta['member_home_address'][0] : '' ;?>"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="member-post-number"><?php _e('Postnummer','ev-intranet');?></label>
                                            <input type="text" id="member-post-number" name="member_post_number" value="<?php echo isset($user_meta['member_post_number'][0]) ? $user_meta['member_post_number'][0] : '' ;?>"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="member-place"><?php _e('Stad', 'skradderiforbundet'); ?></label>
                                            <input type="text" id="member-place" name="member_place" value="<?php echo isset($user_meta['member_place'][0]) ? $user_meta['member_place'][0] : '' ;?>"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="member-country"><?php _e('Land', 'skradderiforbundet'); ?></label>
                                            <input type="text" id="member-country" name="member_country" value="<?php echo isset($user_meta['member_country'][0]) ? $user_meta['member_country'][0] : '' ;?>"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="member-home-phone"><?php _e('Telefonnummer', 'skradderiforbundet'); ?></label>
                                            <input type="text" id="member-home-phone" name="member_home_phone" value="<?php echo isset($user_meta['member_home_phone'][0]) ? $user_meta['member_home_phone'][0] : '' ;?>"/>
                                        </div>
                                    </div>

                                </div>
                               
                                <!---->

                                <!--Company Section-->
                                <div class="employment-section clearfix">
                                    <h2><?php _e('Är du egen företagare?','ev-intranet');?></h2>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="company-name"><?php _e('Företagsnamn', 'skradderiforbundet'); ?></label>
                                            <input type="text" id="company-name" name="company_name" data-type="required" value="<?php echo isset($user_meta['company_name'][0]) ? $user_meta['company_name'][0] : '' ;?>"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="company-phone"><?php _e('Telefon','ev-intranet');?></label>
                                            <input type="text" id="company-phone" name="company_phone" value="<?php echo isset($user_meta['company_phone'][0]) ? $user_meta['company_phone'][0] : '' ;?>" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="company-start-date"><?php _e('Registreringsår','ev-intranet');?></label>
                                            <input type="text" id="company-start-date" name="company_start_date" value="<?php echo isset($user_meta['company_start_date'][0]) ? $user_meta['company_start_date'][0] : '' ;?>" class="datepicker"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="company-address"><?php _e('Adress','ev-intranet');?></label>
                                            <input type="text" id="company-address" name="company_address" value="<?php echo isset($user_meta['company_address'][0]) ? $user_meta['company_address'][0] : '' ;?>" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="company-post-number"><?php _e('Postnummer','ev-intranet');?></label>
                                            <input type="text" id="company-post-number" name="company_post_number" value="<?php echo isset($user_meta['company_post_number'][0]) ? $user_meta['company_post_number'][0] : '' ;?>" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="company-city"><?php _e('Stad', 'skradderiforbundet'); ?></label>
                                            <input type="text" id="company-city" name="company_city" value="<?php echo isset($user_meta['company_city'][0]) ? $user_meta['company_city'][0] : '' ;?>"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="company-nature"><?php _e('Verksamhetsområde','ev-intranet');?></label>
                                            <input type="text" id="company-nature" name="company_nature" value="<?php echo $company_nature;?>" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="company-f-tax"><?php _e('Har företaget F-skattsedel?','ev-intranet');?></label>
                                            <div class="select-box">
                                                <select name="company_f_tax" id="company-f-tax" class="selectpicker">
                                                    <option value=""><?php _e('Välj JA / NEJ','ev-intranet');?></option>
                                                    <option value="yes" <?php isset($user_meta['company_f_tax'][0]) ? selected('yes',$user_meta['company_f_tax'][0]) : '' ;?> ><?php _e('JA','ev-intranet');?></option>
                                                    <option value="no" <?php isset($user_meta['company_f_tax'][0]) ? selected('no',$user_meta['company_f_tax'][0]) : '' ;?> ><?php _e('NEJ','ev-intranet');?></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="company-vat"><?php _e('Är företaget momsregistrerat?','ev-intranet');?></label>
                                            <div class="select-box">
                                                <select name="company_vat" id="company-vat" class="selectpicker">
                                                    <option value=""><?php _e('Välj JA / NEJ','ev-intranet');?></option>
                                                    <option value="yes" <?php isset($user_meta['company_vat'][0]) ? selected('yes',$user_meta['company_vat'][0]) : '' ;?>><?php _e('JA','ev-intranet');?></option>
                                                    <option value="no" <?php isset($user_meta['company_vat'][0]) ? selected('no',$user_meta['company_vat'][0]) : '' ;?>><?php _e('NEJ','ev-intranet');?></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="company-email"><?php _e('Företagsemail', 'skradderiforbundet'); ?></label>
                                            <input type="email" id="company-email" name="company_email" value="<?php echo isset($user_meta['company_email'][0]) ? $user_meta['company_email'][0] : '' ;?>"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="company-website"><?php _e('Webbsida', 'skradderiforbundet'); ?></label>
                                            <input type="text" id="company-website" name="company_website" value="<?php echo isset($user_meta['company_website'][0]) ? $user_meta['company_website'][0] : '' ;?>"/>
                                        </div>
                                    </div>
                                </div>
                                <!---->


                                <!--Employee Section-->
                                <div class="employment-section clearfix">
                                    <h2><?php _e('Är du på företag?','ev-intranet');?></h2>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="employee-company-name"><?php _e('Företagsnamn', 'skradderiforbundet'); ?></label>
                                            <input type="text" id="employee-company-name" name="employee_company_name" value="<?php echo isset($user_meta['employee_company_name'][0]) ? $user_meta['employee_company_name'][0] : '' ;?>"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="employee-company-phone"><?php _e('Företagstelefon', 'skradderiforbundet'); ?></label>
                                            <input type="text" id="employee-company-phone" name="employee_company_phone" value="<?php echo isset($user_meta['employee_company_phone'][0]) ? $user_meta['employee_company_phone'][0] : '' ;?>"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="employee-company-address"><?php _e('Adress', 'skradderiforbundet'); ?></label>
                                            <input type="text" id="employee-company-address" name="employee_company_address" value="<?php echo isset($user_meta['employee_company_address'][0]) ? $user_meta['employee_company_address'][0] : '' ;?>"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="employee-company-post-number"><?php _e('Postnummer', 'skradderiforbundet'); ?></label>
                                            <input type="text" id="employee-company-post-number" name="employee_company_post_number" value="<?php echo isset($user_meta['employee_company_post_number'][0]) ? $user_meta['employee_company_post_number'][0] : '' ;?>"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="employee-company-city"><?php _e('Stad', 'skradderiforbundet'); ?></label>
                                            <input type="text" id="employee-company-city" name="employee_company_city" value="<?php echo isset($user_meta['employee_company_city'][0]) ? $user_meta['employee_company_city'][0] : '' ;?>"/>
                                        </div>
                                    </div>
                                </div>
                                <!---->

                                <!--Teacher Section-->
                                <div class="teacher-section clearfix">
                                    <h2><?php _e('Är du lärare med yrkeskompetens?','ev-intranet');?></h2>
                                  
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="school-name"><?php _e('Skola','ev-intranet');?></label>
                                            <input type="text" id="school-name" name="school_name" value="<?php echo isset($user_meta['school_name'][0]) ? $user_meta['school_name'][0] : '';?>" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="school-phone"><?php _e('Telefon arbetsplats','ev-intranet');?></label>
                                            <input type="text" id="school-phone" name="school_phone" value="<?php echo isset($user_meta['school_phone'][0]) ? $user_meta['school_phone'][0] : '';?>" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="school-email"><?php _e('Email', 'skradderiforbundet'); ?></label>
                                            <input type="email" id="school-email" name="school_email" value="<?php echo isset($user_meta['school_email'][0]) ? $user_meta['school_email'][0] : '';?>"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="school-address"><?php _e('Adress','ev-intranet');?></label>
                                            <input type="text" id="school-address" name="school_address" value="<?php echo isset($user_meta['school_address'][0]) ? $user_meta['school_address'][0] : '';?>" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="school-post-number"><?php _e('Postnummer','ev-intranet');?></label>
                                            <input type="text" id="school-post-number" name="school_post_number" value="<?php echo isset($user_meta['school_post_number'][0]) ? $user_meta['school_post_number'][0] : '';?>" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="school-ort"><?php _e('Stad', 'skradderiforbundet'); ?></label>
                                            <input type="text" id="school-ort" name="school_place" value="<?php echo isset($user_meta['school_place'][0]) ? $user_meta['school_place'][0] : '';?>"/>
                                        </div>
                                    </div>
                                </div>
                                <!---->

                                <!--Certificate Section-->
                                <div class="certificate-section sk-checkbox-style clearfix">
                                    <h2><?php _e('Gesällbrev','ev-intranet');?></h2>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="checkbox">
                                            <label for="herr-certificate" class="control-checkbox">
                                                <input type="checkbox" id="herr-certificate" name="certificate_in_man" <?php if(in_array('him',$certificate_in)){ echo 'checked="checked"'; } ?>/>
                                                <?php _e('Herr', 'skradderiforbundet'); ?>
                                                <div class="check-indicator"></div>
                                            </label>
                                            
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="checkbox">
                                            <label for="woman-certificate" class="control-checkbox">
                                                <input type="checkbox" id="woman-certificate" name="certificate_in_woman" <?php if(in_array('her',$certificate_in)){ echo 'checked="checked"'; } ?>/>
                                                <?php _e('Dam', 'skradderiforbundet'); ?>
                                                <div class="check-indicator"></div>
                                            </label>
                                            
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="checkbox">
                                            <label for="dress-certificate" class="control-checkbox">
                                                <input type="checkbox" id="dress-certificate" name="certificate_in_dress" <?php if(in_array('dress',$certificate_in)){ echo 'checked="checked"'; } ?>/>
                                                <?php _e('Klänning', 'skradderiforbundet'); ?>
                                                <div class="check-indicator"></div>
                                            </label>
                                            
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="date-of-certificate"><?php _e('År för utfärdande', 'skradderiforbundet'); ?>
                                            </label>
                                            <input type="text" id="date-of-certificate" name="date_of_certificate" value="<?php echo isset($user_meta['date_of_certificate'][0]) ? $user_meta['date_of_certificate'][0] : '';?>"/>
                                        </div>
                                    </div>
                                </div>
                                <!---->

                                <!--Master Section-->
                                <div class="master-section sk-checkbox-style clearfix">
                                    <h2><?php _e('Mästarbrev','ev-intranet');?></h2>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="checkbox">
                                            <label for="mastarbrev-herr" class="control-checkbox">
                                                <input type="checkbox" id="mastarbrev-herr" name="master_in_man" class="form-control" <?php if(in_array('him',$master_in)){ echo 'checked="checked"'; } ?>/>
                                                <?php _e('Herr', 'skradderiforbundet'); ?>
                                                <div class="check-indicator"></div>
                                            </label>
                                            
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="checkbox">
                                            <label for="master-in-woman" class="control-checkbox">
                                                    <input type="checkbox" id="master-in-woman" name="master_in_woman" class="form-control" <?php if(in_array('her',$master_in)){ echo 'checked="checked"'; } ?>/>
                                                    <?php _e('Dam', 'skradderiforbundet'); ?>
                                                    <div class="check-indicator"></div>
                                            </label>
                                        </div>
                                            
                                            
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="checkbox">
                                            <label for="membership-dress" class="control-checkbox">
                                                <input type="checkbox" id="membership-dress" name="master_in_dress" class="form-control" <?php if(in_array('dress',$master_in)){ echo 'checked="checked"'; } ?>/>
                                                <?php _e('Klänning', 'skradderiforbundet'); ?>
                                                <div class="check-indicator"></div>
                                            </label>
                                            
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="master-year"><?php _e('År för utfärdande', 'skradderiforbundet'); ?></label>
                                            <input type="text" id="master-year" name="master_year" value="<?php echo isset($user_meta['master_year'][0]) ? $user_meta['master_year'][0] : '';?>"/>
                                        </div>
                                    </div>
                                </div>
                                <!---->

                                <!--Login Info Section-->
                                <div class="login-section clearfix">
                                    <h2><?php _e('Inloggningsuppgifter','ev-intranet');?></h2>
                                    
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="username"><?php _e('Användarnamn','ev-intranet');?></label>
                                            <input type="text" id="username" name="username" value="<?php echo $current_user->data->user_login;?>" disabled/>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="email"><?php _e('Email','ev-intranet');?></label>
                                            <input type="email" id="email" name="email" value="<?php echo $current_user->data->user_email;?>" disabled/>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="password"><?php _e('Lösenord','ev-intranet');?></label>
                                            <input type="text" id="password" name="password"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label for="retype-password"><?php _e('Upprepa lösenord','ev-intranet');?></label>
                                            <input type="text" id="retype-password" name="retype_password"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <button type="submit" name="update_member" class="btn btn-primary c-btn btn-upload">SPARA</button>
                                        </div>
                                        
                                    </div>
                                </div>
                                <!---->
                                
                            </form>
                            <span class="update-member-loader" style="display: none">
                                <i class="fa fa-spinner fa-spin fa-2x"></i>
                            </span>
                        </div>
                    </div>
                </div>
        </main>
    </div>
<?php
endwhile; wp_reset_postdata();
get_footer();
//#ui-datepicker-div {display: none;}
?>	