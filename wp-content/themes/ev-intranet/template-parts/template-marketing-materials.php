<?php
/*Template Name: Marketing Materials Template*/
get_header();
global $ev_intranet;
while(have_posts()):the_post();
    ?>
    <div id="primary" class="content-area main-content">
        <main id="main" class="site-main" role="main">
            <?php get_template_part('template-parts/content','inner');?>
            <div class="inner-wrap">
                <div class = "container">
                    <div class="row">
                        <div class="marketing-materials">
                            <?php
                            $args = array(
                                'post_type' => 'ev_mm',
                                'post_status' => 'publish',
                                'posts_per_page' => -1,
                            );
                            $mm = new WP_Query($args);
                            if($mm->have_posts()):
                            ?>
                                <div class="col-sm-12">
                                   <div class="sk-cm-table sk-foo-table table-responsive">
                                        <table class="ev-table table table-striped" data-show-toggle="false">
                                            <thead>
                                            <tr>
                                                <th><?php _e('Logo','ev-intranet');?></th>
                                                <th data-breakpoints="xs"><?php _e('Verksamhet','ev-intranet');?></th>
                                                <th data-breakpoints="xs"><?php _e('Beskrivning','ev-intranet');?></th>
                                                <th><?php _e('Ladda ner','ev-intranet');?></th>
                                                <th data-breakpoints="all"></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php while($mm->have_posts()):$mm->the_post();?>
                                                <tr>
                                                    <td>
                                                        <?php
                                                        if(has_post_thumbnail()){
                                                            ?>
                                                            <a href="<?php the_post_thumbnail_url();?>" rel="prettyPhoto">
                                                                <?php  echo get_the_post_thumbnail( get_the_ID(), array( 35, 35) ); ?>
                                                            </a>
                                                            <?php
                                                        }
                                                        ?>
                                                    </td>
                                                    <td><?php the_title();?></td>
                                                    <td><?php the_content();?></td>
                                                    <td>
                                                        <i class="fa fa-download" aria-hidden="true"></i>
                                                        <a class="light-box-link" href="<?php the_post_thumbnail_url();?>" rel="prettyPhoto">
                                                            <i class="fa fa-search" aria-hidden="true"></i>
                                                        </a>
                                                    </td>
                                                    <td class="pdf-download-block">
                                                        <?php
                                                        $mm_files = get_field('file_upload');
                                                        if(!empty($mm_files)){
                                                            foreach((array)$mm_files as $mm_file){
                                                                $file_url = $mm_file['files']['url'];
                                                                $ext = pathinfo($file_url, PATHINFO_EXTENSION);
                                                                ?>
                                                                <div class="pdf-link">
                                                                    <a href="<?php echo $file_url;?>" download>
                                                                        <?php
                                                                        $title = $mm_file['files']['title'];
                                                                        echo $title.'.'.$ext;
                                                                        ?>
                                                                    </a>
                                                                </div>
                                                                <?php
                                                            }
                                                        }
                                                        $mm_images = get_field('image_upload');
                                                        if(!empty($mm_images)){
                                                            foreach((array)$mm_images as $mm_image){
                                                                $img_url = $mm_image['images']['url'];
                                                                $ext = pathinfo($img_url, PATHINFO_EXTENSION);
                                                                ?>
                                                                <div class="pdf-link">
                                                                    <a href="<?php echo $img_url?>" download>
                                                                        <?php
                                                                        $title = $mm_image['images']['title'];
                                                                        echo $title.'.'.$ext;
                                                                        ?>
                                                                    </a>
                                                                </div>
                                                            <?php
                                                            }
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
                                            <?php endwhile;wp_reset_postdata();?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            <?php endif; wp_reset_postdata();?>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
<?php
endwhile; wp_reset_postdata();
get_footer();
?>	