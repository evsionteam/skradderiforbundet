<?php
/*Template Name: Wholesaler Template*/
get_header();
while(have_posts()):the_post();
?>
<div id="primary" class="content-area main-content">
    <main id="main" class="site-main" role="main">
        <?php get_template_part('template-parts/content','inner');?>
            <div class = "container">
                <div class="row">
                    <div class="cm-padding sk-cm-table sk-member-list">
                        <?php
                        $selected_city = get_query_var('city');
                        $selected_product = get_query_var('product');

                        $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
                        $args = array(
                            'post_type' => 'ev_wholesaler',
                            'post_status' => 'publish',
                            'posts_per_page' => -1,
                            'paged' => $paged,
                        );

                        if(!empty($selected_city)){
                            $args['tax_query'][] = array(
                                'taxonomy' => 'wholesaler_city',
                                'field'    => 'slug',
                                'terms'    => $selected_city,
                            );
                        }

                        if(!empty($selected_product)){
                            $args['tax_query'][] = array(
                                'taxonomy' => 'wholesaler_product',
                                'field'    => 'slug',
                                'terms'    => $selected_product,
                            );
                            if(!empty($selected_city)){
                                $args['tax_query']['relation'] = 'AND';
                            }
                        }

                        $wholesalers = new WP_Query($args);
                            ?>
                            <div class="col-sm-12">
                                <div class="ev-select-filters filter clearfix">
                                    <form name="wholesaler-filter" id="wholesaler-filter" class="skrad-form" action="<?php echo site_url('/leverantorer');?>">
                                        <?php
                                        $cities = get_terms( 'wholesaler_city', array(
                                            'hide_empty' => true,
                                        ) );
                                        if(!is_wp_error($cities)){
                                            if(!empty($cities)){
                                                ?>
                                                    <div class="select-box">
                                                        <select name="city" id="city" class="selectpicker" data-size="10">
                                                            <option value=""><?php _e('Välj ort','ev-intranet');?></option>
                                                            <?php
                                                            foreach($cities as $city){
                                                                ?>
                                                                <option value="<?php echo $city->slug?>" <?php selected($city->slug,$selected_city);?>><?php echo $city->name?></option>
                                                                <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                <?php
                                            }
                                        }

                                        $products = get_terms( 'wholesaler_product', array(
                                            'hide_empty' => true,
                                            'parent' => 0,
                                        ) );
                                        if(!is_wp_error($products)){
                                            if(!empty($products)){
                                                ?>
                                                    <div class="select-box">
                                                        <select name="product" id="product" class="selectpicker" data-size="10">
                                                            <option value=""><?php _e('Välj produkt','ev-intranet');?></option>
                                                            <?php
                                                            foreach($products as $product){
                                                                ?>
                                                                <option value="<?php echo $product->slug?>" <?php selected($product->slug,$selected_product);?>><?php echo $product->name?></option>
                                                                <?php
                                                                //fetch child categories if any
                                                                $sub_cats = get_term_children( $product->term_id, 'wholesaler_product' );
                                                                if(!is_wp_error($sub_cats)){
                                                                    if(!empty($sub_cats)){
                                                                        foreach ( $sub_cats as $child ) {
                                                                            $term = get_term_by( 'id', $child, 'wholesaler_product' );
                                                                            ?>
                                                                            <option class="sub-cat" value="<?php echo $term->slug?>" <?php selected($term->slug,$selected_product);?>><?php echo $term->name?></option>
                                                                            <?php
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                            <?php
                                            }
                                        }
                                        ?>
                                    </form>
                                </div>
                                
                            </div>

                            <?php if($wholesalers->have_posts()): ?>
                                <div class="col-sm-12">
                                    <div class="sk-cm-table sk-foo-table table-responsive">
                                        <table class="ev-table table table-striped table-wholesaler" data-sorting="true" data-show-toggle="false">
                                            <thead>
                                                <tr>
                                                    <th><?php _e('Företag','ev-intranet');?></th>
                                                    <th data-breakpoints="xs"><?php _e('Stad','ev-intranet');?></th>
                                                    <th data-breakpoints="xs"><?php _e('Produkter','ev-intranet');?></th>
                                                    <th data-breakpoints="all"></th>
                                                    <th data-breakpoints="all"><?php _e('Övrigt','ev-intranet');?></th>
                                                    <th data-breakpoints="all"><?php _e('Hemsida','ev-intranet');?></th>
                                                    <th data-breakpoints="all"><?php _e('Telefon','ev-intranet');?></th>
                                                    <th data-breakpoints="all"><?php _e('Adress','ev-intranet');?></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php while($wholesalers->have_posts()):$wholesalers->the_post();?>
                                                <tr>
                                                    <td><span class="footable-toggle fooicon fooicon-plus hidden-xs"></span><?php the_title();?></td>
                                                    <td>
                                                        <?php
                                                        $cities_arr = array();
                                                        $cities = get_the_terms(get_the_ID(),'wholesaler_city');
                                                        if(!empty($cities)){
                                                            foreach($cities as $city){
                                                                $cities_arr[] = $city->name;
                                                            }
                                                            echo implode(',',$cities_arr);
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        $product_arr = array();
                                                        $products = get_the_terms(get_the_ID(),'wholesaler_product');
                                                        if(!empty($products)){
                                                            foreach($products as $product){
                                                                $product_arr[] = $product->name;
                                                            }
                                                            echo implode(',',$product_arr);
                                                        }
                                                        ?>
                                                    </td>
                                                    <td><?php if(has_post_thumbnail()):the_post_thumbnail('full');endif;?></td>
                                                    <td><?php the_content();?></td>
                                                    <td>
                                                        <?php $site_link = get_post_meta(get_the_ID(),'_wholesaler_site',true);?>
                                                        <a href="<?php echo esc_url($site_link)?>" target="_blank">
                                                            <?php echo $site_link;?>
                                                        </a>
                                                    </td>
                                                    <td><?php echo get_post_meta(get_the_ID(),'_wholesaler_phone',true)?></td>
                                                    <td><?php echo get_post_meta(get_the_ID(),'_wholesaler_address',true)?>
                                                    </td>
                                                </tr>
                                            <?php endwhile; ?>
                                            </tbody>
                                        </table>
                                        
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <?php
                                    else:
                                       echo '<h2 class="no-wholesalers">';
                                           _e('No wholesaler found.','ev-intranet');
                                       echo '</h2>';
                                    endif;
                                    ?>
                            </div>
                    </div>
                </div>
            </div><!-- .container -->
    </main><!-- #main -->
</div><!-- #primary -->
<?php
endwhile; wp_reset_postdata();
get_footer();
?>