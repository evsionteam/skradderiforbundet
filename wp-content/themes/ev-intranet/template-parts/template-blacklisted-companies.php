<?php
/*Template Name: Blacklisted Company Template*/
get_header();
while(have_posts()):the_post();
    ?>
    <div id="primary" class="content-area main-content">
        <main id="main" class="site-main" role="main">
            <?php get_template_part('template-parts/content','inner');?>
                <div class = "container">
                    <div class="row">
                        <div class="cm-padding sk-cm-table blacklist-company">
                            <?php
                            $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
                            $args = array(
                                'post_type' => 'ev_bl_companies',
                                'post_status' => 'publish',
                                'posts_per_page' => -1,
                                'paged' => $paged,
                            );
                            $bl_companies = new WP_Query($args);
                            if($bl_companies->have_posts()):
                            ?>
                            <div class="col-sm-12">
                                <div class="sk-cm-table sk-foo-table table-responsive">
                                    <table class="ev-table table table-striped table-wholesaler" data-sorting="true" data-show-toggle="false">
                                        <thead>
                                        <tr>
                                            <th><?php _e('Företag','ev-intranet');?></th>
                                            <th><?php _e('Stad','ev-intranet');?></th>
                                            <th data-breakpoints="xs"><?php _e('Produkter','ev-intranet');?></th>
                                            <th data-breakpoints="all"></th>
                                            <th data-breakpoints="all"><?php _e('Motivation','ev-intranet');?></th>
                                            <th data-breakpoints="all"><?php _e('Hemsida','ev-intranet');?></th>
                                            <th data-breakpoints="all"><?php _e('Links','ev-intranet');?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php while($bl_companies->have_posts()):$bl_companies->the_post();?>
                                            <tr>
                                                <td><span class="footable-toggle fooicon fooicon-plus"></span><?php the_title();?></td>
                                                <td>
                                                    <?php
                                                    $cities_arr = array();
                                                    $cities = get_the_terms(get_the_ID(),'bl_company_city');
                                                    if(!empty($cities)){
                                                        foreach($cities as $city){
                                                            $cities_arr[] = $city->name;
                                                        }
                                                        echo implode(',',$cities_arr);
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    $product_arr = array();
                                                    $products = get_the_terms(get_the_ID(),'bl_company_product');
                                                    if(!empty($products)){
                                                        foreach($products as $product){
                                                            $product_arr[] = $product->name;
                                                        }
                                                        echo implode(',',$product_arr);
                                                    }
                                                    ?>
                                                </td>
                                                <td><?php if(has_post_thumbnail()):the_post_thumbnail('full');endif;?></td>
                                                <td><?php the_content();?></td>
                                                <td>
                                                    <?php
                                                        $site_link = get_field('site_link');
                                                        if(!empty($site_link)){
                                                            echo "<a href=".esc_url($site_link)." target='_blank'>$site_link</a>";
                                                        }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                        $reference_links = get_field('reference_links');
                                                        if(!empty($reference_links) && is_array($reference_links)){
                                                            foreach($reference_links as $reference_link){
                                                                if(!empty($reference_link['link'])){
                                                                    ?>
                                                                    <div>
                                                                        <a href="<?php echo esc_url($reference_link['link']);?>" target="_blank">
                                                                            <?php echo $reference_link['link'];?>
                                                                        </a>
                                                                    </div>
                                                                    <?php
                                                                }
                                                            }
                                                        }
                                                    ?>
                                                </td>
                                            </tr>
                                        <?php endwhile; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <?php
                            else:
                                echo '<h2 class="no-blacklisted-companies">';
                                _e('No Blacklisted Company found.','ev-intranet');
                                echo '</h2>';
                            endif;
                            ?>
                        </div>
                    </div>
                </div><!-- .container -->
        </main><!-- #main -->
    </div><!-- #primary -->
<?php
endwhile; wp_reset_postdata();
get_footer();
?>