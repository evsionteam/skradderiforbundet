<?php
/*Template Name: Upload Image Template*/
get_header();
while(have_posts()):the_post();
?>
<div id="primary" class="content-area main-content">
    <main id="main" class="site-main" role="main">
        <?php get_template_part('template-parts/content','inner');?>
        <div class="sk-image-gallry-sec">
            <div class = "container">
                <div class="c-row">
                    <div class="col-sm-12 c-padding">
                        <div class="image-upload-form-wrapper">
                            <div class="upload-image-title clearfix">
                                <h3><?php _e('Ladda upp bild','ev-intranet'); ?></h3>
                                <button
                                    type="button"
                                    class="btn btn-default"
                                    data-container="body"
                                    data-toggle="popover"
                                    data-trigger="focus"
                                    data-placement="right"
                                    data-content='<?php
                                    $url = 'mailto:webredaktor@skradderiforbundet.se';
                                    printf( wp_kses( __( 'Maximal storlek är 500kb. Har du större storlek och inte vet hur man förminskar, kan du maila oss bilder på <a href="%s">webredaktor@skradderiforbundet.se</a> ', 'ev-intranet' ), array(  'a' => array( 'href' => array() ) ) ),  $url );?>'
                                    data-html="true">
                                  <i class="fa fa-question-circle-o" aria-hidden="true"></i>
                                </button>
                            </div>
                            <form name="upload_form" id="upload_form" class="skrad-form form-inline" method="post" enctype="multipart/form-data">
                                <?php wp_nonce_field( 'submit_content', 'image_submit_nonce' ); ?>
                                <div class="form-group">
                                    <div class="choose-file">
                                        <input type="file" name="image" id="file-image" accept="image/*" class="form-control files-data form-control" onchange="uploadFile(this)" required/>
                                        <label for="file-image">
                                            <span id="file-name" class="file-box"><?php _e('Ladda upp bild (.jpg)','ev-intranet');?></span>
                                            <span class="file-btn"><i class="fa fa-arrow-circle-o-up"></i></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="text" id="post_title" class="form-control" name="post_title" placeholder="<?php _e('Namn på bild','ev-intranet')?>" required/>
                                </div>
                                <div class="form-group">
                                    <input type="text" id="site-link" class="form-control" name="link" placeholder="<?php _e('Länk till hemsida','ev-intranet')?>" />
                                </div>
                                <div class="form-group">
                                    <div class="select-box">
                                        <select id="submit-as" name="submit_as" class="selectpicker" required>
                                            <option value=""><?php _e('Ladda upp som','ev-intranet');?></option>
                                            <option value="person"><?php _e('Ditt namn','ev-intranet');?></option>
                                            <option value="company"><?php _e('Företagsnamn','ev-intranet');?></option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group ev-submit">
                                    <input type="submit" name="submit_image" value="<?php _e('Publicera','ev-intranet');?>" class="btn btn-primary btn-upload c-btn" />
                                    <span class="add-image-loader" style="display: none">
                                        <i class="fa fa-spinner fa-spin fa-2x"></i>
                                    </span>     
                                </div>
                            </form>
                            <span id="status"></span>
                            <div id="images_wrap"></div>
                        </div> 
                    </div>
                    <div class="col-sm-4 c-padding"></div>
                </div>

                
                <div class="c-row">
                    <div class="sk-member-image-wrapper">
                            <?php
                            $images_args = array(
                                'post_type' => 'ev_image_gallery',
                                'post_status' => 'publish',
                                'posts_per_page' => 12,
                                'author' => get_current_user_id(),
                            );
                            $member_images = new WP_Query($images_args);
                            if($member_images->have_posts()):
                                echo '<h3>'.__('Dina uppladdade bilder','ev-intranet').'</h3>';
                                while($member_images->have_posts()):$member_images->the_post();
                                ?>
                                <div class="col-sm-4 c-padding member-image">
                                    <div class="sk-image-thumnail">
                                        <?php if(has_post_thumbnail()):?>
                                            <span class="remove-image-loader" style="display: none">
                                                <i class="fa fa-spinner fa-spin fa-2x"></i>
                                            </span>
                                            <div class="sk-member-image">
                                                <?php the_post_thumbnail('news-col-3');?>
                                            </div>
                                            <div class="sk-image-info clearfix">
                                                <div class="sk-title-date">
                                                    <h4 class="title">
                                                        <?php the_title();?>
                                                    </h4>
                                                    <span class="posted-on">
                                                        <?php echo get_the_date();?>
                                                    </span>
                                                </div>
                                                <div class="remove-image" data-id="<?php echo get_the_ID();?>" data-nonce="<?php echo wp_create_nonce('delete_image_nonce') ?>">
                                                    <a href="javascript:void(0)"><i class="fa fa-remove"></i></a>
                                                </div>
                                            <?php endif;?>
                                        </div>
                                    </div>
                                </div>    
                                <?php
                                endwhile;wp_reset_postdata();
                            endif;
                            ?>
                    </div>
                </div>
            </div><!-- .container -->  
        </div><!-- .sk-image-gallery-sec -->
    </main><!-- #main -->
</div><!-- #primary -->
<?php
endwhile; wp_reset_postdata();
get_footer();
?>